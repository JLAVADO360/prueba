package ndp.posapp.view.activity

import android.app.Activity
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.db.entity.TaxEntity
import ndp.posapp.data.db.entityView.SaleItem
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.databinding.ActivityItemSaleBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.ItemSaleDialog
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.adapter.ItemSaleAdapter
import ndp.posapp.view.adapter.ItemSaleDialogAdapter
import ndp.posapp.viewModel.ItemViewModel
import ndp.posapp.viewModel.KardexViewModel
import ndp.posapp.viewModel.ParameterViewModel
import ndp.posapp.viewModel.PriceListDetailViewModel
import ndp.posapp.viewModel.TaxViewModel
import ndp.posapp.viewModel.WhsKardexViewModel

class ItemSaleActivity : AppCompatActivity(), ItemSaleAdapter.Listener, ItemSaleDialogAdapter.Listener {

    /** VIEW MODEL **/
    private lateinit var itemViewModel: ItemViewModel
    private lateinit var priceListDetailViewModel: PriceListDetailViewModel
    private lateinit var whsKardexViewModel: WhsKardexViewModel
    private lateinit var kardexLocationViewModel: KardexViewModel
    private lateinit var taxViewModel: TaxViewModel
    private lateinit var parameterViewModel: ParameterViewModel

    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityItemSaleBinding
    private lateinit var searchView: SearchView
    private var itemDetail: MutableList<SaleOrderDetailModel> = ArrayList()
    private var excludedItemCodes: MutableList<String> = ArrayList()
    private var loggerUserModel = LoggerUserModel()
    private var taxList: MutableList<TaxEntity> = ArrayList()
    private lateinit var itemSaleDialog: ItemSaleDialog
    private var reservationInvoice = false
    private var filterPriceList: MutableList<Int> = ArrayList()

    /** ADAPTER **/
    private lateinit var itemSaleAdapter: ItemSaleAdapter

    companion object {
        private const val EXTRA_ITEM_DETAIL: String = "EXTRA_ITEM_DETAIL"
        private const val PARTNER_PRICE_LIST: String = "PARTNER_PRICE_LIST"
        private const val EXTRA_RESERVATION_INVOICE: String = "EXTRA_RESERVATION_INVOICE"

        fun newIntent(context: Context, itemDetail: MutableList<SaleOrderDetailModel>, partnerPriceList: Int, reservationInvoice: Boolean): Intent {
            return Intent(context, ItemSaleActivity::class.java).also {
                it.putExtra(EXTRA_ITEM_DETAIL, ArrayList(itemDetail))
                it.putExtra(PARTNER_PRICE_LIST, partnerPriceList)
                it.putExtra(EXTRA_RESERVATION_INVOICE, reservationInvoice)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityItemSaleBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initElements()
    }

    @Suppress("UNCHECKED_CAST")
    private fun initElements() {
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = getString(R.string.item)
        itemDetail = intent.getSerializableExtra(EXTRA_ITEM_DETAIL) as ArrayList<SaleOrderDetailModel>
        itemDetail.map { excludedItemCodes.add(it.itemCode!!) }
        filterPriceList.add(intent.getIntExtra(PARTNER_PRICE_LIST, 5))
        reservationInvoice = intent.getBooleanExtra(EXTRA_RESERVATION_INVOICE, false)
        binding.fbCart.count = itemDetail.size

        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        loggerUserModel.storePriceList?.let { filterPriceList.add(it) }
        initViewModel()
        initActions()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnSearchClickListener {
            binding.appBar.tvTitleToolbar.visibility = View.GONE
            binding.appBar.ivBackToolbar.visibility = View.GONE
        }
        searchView.setOnCloseListener {
            binding.appBar.tvTitleToolbar.visibility = View.VISIBLE
            binding.appBar.ivBackToolbar.visibility = View.VISIBLE
            false
        }
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                itemSaleAdapter.filter.filter(newText)
                return true
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initViewModel() {
        itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)
        priceListDetailViewModel = ViewModelProvider(this).get(PriceListDetailViewModel::class.java)
        whsKardexViewModel = ViewModelProvider(this).get(WhsKardexViewModel::class.java)
        kardexLocationViewModel = ViewModelProvider(this).get(KardexViewModel::class.java)
        taxViewModel = ViewModelProvider(this).get(TaxViewModel::class.java)
        parameterViewModel = ViewModelProvider(this).get(ParameterViewModel::class.java)
        initVariables()
    }

    private fun initVariables() {
        taxViewModel.list(loggerUserModel.entity).observe(this, {
            it.let { taxList.addAll(it) }
        })

        val loadBagLiveData = MutableLiveData(2).apply {
            observe(this@ItemSaleActivity){
                if (value == 0) initRecyclerView()
            }
        }

        parameterViewModel.get(loggerUserModel.entity, Const.Parameter.BAG_NAME).observe(this) { param ->
            param.value?.let { value ->
                itemViewModel.get(loggerUserModel.entity, loggerUserModel.warehouse!!, priceListDetailViewModel, whsKardexViewModel, kardexLocationViewModel, value).observe(this) {
                    it?.itemEntity?.code?.let { code -> excludedItemCodes.add(code) }
                    loadBagLiveData.value = loadBagLiveData.value!! - 1
                }
            }
        }

        parameterViewModel.get(loggerUserModel.entity, Const.Parameter.TAX_BAG_NAME).observe(this) { param ->
            param.value?.let { value ->
                itemViewModel.get(loggerUserModel.entity, loggerUserModel.warehouse!!, priceListDetailViewModel, whsKardexViewModel, kardexLocationViewModel, value).observe(this) {
                    it?.itemEntity?.code?.let { code -> excludedItemCodes.add(code) }
                    loadBagLiveData.value = loadBagLiveData.value!! - 1
                }
            }
        }
    }

    private fun initRecyclerView() {
        binding.rvItem.setHasFixedSize(true)
        itemSaleAdapter = ItemSaleAdapter(reservationInvoice, this)
        binding.rvItem.adapter = itemSaleAdapter
        binding.rvItem.layoutManager = LinearLayoutManager(this)
        setListRecyclerView()
    }

    private fun setListRecyclerView() {
        val liveDataItem = itemViewModel.list(loggerUserModel.entity, filterPriceList, loggerUserModel.warehouse!!, priceListDetailViewModel, whsKardexViewModel, kardexLocationViewModel, excludedItemCodes)
        liveDataItem.observe(this, {
            liveDataItem.removeObservers(this)
            if (it.isNotEmpty()) {
                itemSaleAdapter.setList(it)
            }
        })
    }

    override fun onItemSelected(saleItem: SaleItem) {
        val item = saleItem.itemEntity
        val saleOrderDetail = SaleOrderDetailModel()

        val priceListDetail = saleItem.priceListDetailEntity[0]
        var unitPrice = 0.0
        saleItem.priceListDetailEntity.forEach { unitPrice += it.unitPrice ?: 0.0 }
        unitPrice = UseApp.roundDecimal(unitPrice, 2)

        var stockDisponible = 0.0
        saleItem.kardex.forEach { kardex ->
            stockDisponible += kardex.stockDisponible
            kardex.kardexUbication.forEach {
                saleOrderDetail.kardexLocations.add(it)
            }
        }

        saleOrderDetail.kardex = saleItem.kardex
        saleOrderDetail.createDate = UseApp.getDate(Const.DATE_TIME_QUERY)
        saleOrderDetail.createUser = loggerUserModel.userIdentifier
        saleOrderDetail.currency = priceListDetail.currency
        saleOrderDetail.discount = 0.0
        saleOrderDetail.unitPrice = unitPrice
        saleOrderDetail.unitPriceReal = unitPrice
        saleOrderDetail.discountPercentage = 0.0
        saleOrderDetail.inventoryItem = if (item.inventoryItem != null) item.inventoryItem.toString() else Const.INVENTORY_ITEM_YES
        saleOrderDetail.itemCode = item.code
        saleOrderDetail.itemName = item.name
        saleOrderDetail.numberLine = itemDetail.size
        saleOrderDetail.onlyTax = Const.ONLY_TAX_NO //cuando es producto o servicio se pone 0, cuando es impuesto a la bolsa y cuando se bonifica el producto se pone 1
        saleOrderDetail.orderSaleCode = null
        saleOrderDetail.quantity = 1.0
        saleOrderDetail.quantityInvoiced = 0.0
        saleOrderDetail.quantityPicking = 0.0
        saleOrderDetail.sample = 0 // TODO xq 0?
        saleOrderDetail.subtotal = unitPrice * saleOrderDetail.quantity!!
        saleOrderDetail.subtotalWithDiscount = saleOrderDetail.subtotal
        saleOrderDetail.sunatAffectationType = Const.SUNAT_AFFECTATION_TYPE_GRAVADO_ONEROSO // 10 en duro, cuando se bonifica el producto se manda 15 y cuando es impuesto a la bolsa es 13
        saleOrderDetail.sunatOneroso = Const.SUNAT_ONEROSO // solo el impuesto a la bolsa es no oneroso
        saleOrderDetail.sunatOperation = Const.SUNAT_OPERATION
        saleOrderDetail.sunatOperationType = Const.SUNAT_OPERATION_TYPE
        saleOrderDetail.taxCode = item.taxCode ?: Const.TAX_CODE_DEFAULT
        saleOrderDetail.tax = taxList.first { it.code == saleOrderDetail.taxCode }.rate
        saleOrderDetail.taxAmount = UseApp.roundDecimal(unitPrice * (saleOrderDetail.tax!! / 100), 2)
        saleOrderDetail.total = UseApp.roundDecimal(saleOrderDetail.subtotal!! + saleOrderDetail.taxAmount!!, 2)
        saleOrderDetail.ubicationCode = if (saleOrderDetail.kardexLocations.isNotEmpty()) saleOrderDetail.kardexLocations[saleOrderDetail.locationIndex].ubication else null
        saleOrderDetail.ubicationEntry = if (saleOrderDetail.kardexLocations.isNotEmpty()) saleOrderDetail.kardexLocations[saleOrderDetail.locationIndex].ubicationEntry else null
        saleOrderDetail.unitGroupEntry = item.unitGroupEntry ?: Const.UNIT_GROUP_ENTRY_DEFAULT
        saleOrderDetail.unitMSRCode = priceListDetail.unitMSR
        saleOrderDetail.unitMSREntry = priceListDetail.unitMSREntry
        saleOrderDetail.unitMSRName = priceListDetail.unitMSRName
        saleOrderDetail.grossPrice = UseApp.roundDecimal(unitPrice * (1 + (saleOrderDetail.tax!! / 100)), 2)
        saleOrderDetail.warehouseCode =  if (saleItem.kardex.isNotEmpty()) saleItem.kardex.first().warehouse else null

        saleOrderDetail.stock = stockDisponible
        saleOrderDetail.type = item.type
        saleOrderDetail.u_VS_AFEDET = item.u_VS_AFEDET
        saleOrderDetail.u_VS_CODDET = item.u_VS_CODDET
        saleOrderDetail.u_VS_PORDET = item.u_VS_PORDET

        itemDetail.add(saleOrderDetail)

        binding.fbCart.count = itemDetail.size
    }

    private fun initActions() {
        binding.fbCart.setOnClickListener {
            if (itemDetail.size == 0) {
                PopUp.mdToast(this, getString(R.string.no_item_added), MDToast.TYPE_WARNING)
            } else {
                val itemSaleDialogAdapter = ItemSaleDialogAdapter(this, this)
                itemSaleDialog = ItemSaleDialog(this, itemDetail, itemSaleDialogAdapter)
                itemSaleDialog.open()
            }
        }

        binding.appBar.ivBackToolbar.setOnClickListener {
            setIntent()
            this.finish()
        }
    }

    override fun onDeleteSelected() {
        setListRecyclerView()
        binding.fbCart.count = itemDetail.size
        if (itemDetail.size == 0) itemSaleDialog.close()
    }

    private fun setIntent() {
        val intent = Intent()
        intent.putExtra(getString(R.string.sale_order), ArrayList(itemDetail))
        setResult(Activity.RESULT_OK, intent)
    }

    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.isIconified = true
            return
        }
        setIntent()
        super.onBackPressed()
    }
}