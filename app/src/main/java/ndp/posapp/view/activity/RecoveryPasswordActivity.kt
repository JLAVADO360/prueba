package ndp.posapp.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ndp.posapp.R
import ndp.posapp.databinding.ActivityRecoveryPasswordBinding

class RecoveryPasswordActivity : AppCompatActivity() {

    /** VIEW MODEL **/


    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityRecoveryPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRecoveryPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initElements()
    }

    private fun initElements() {
        initViewModel()
        initActions()
    }

    private fun initViewModel() {

    }

    private fun initActions() {
        binding.ivExit.setOnClickListener { this.finish() }
    }
}