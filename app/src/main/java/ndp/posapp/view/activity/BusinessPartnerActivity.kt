package ndp.posapp.view.activity

import android.app.Activity
import android.app.SearchManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import ndp.posapp.R
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.ActivityBusinessPartnerBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.adapter.BusinessPartnerAdapter
import ndp.posapp.viewModel.BusinessPartnerViewModel

class BusinessPartnerActivity : AppCompatActivity(), BusinessPartnerAdapter.Listener {


    /** VIEW MODEL **/
    private lateinit var businessPartnerViewModel: BusinessPartnerViewModel

    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityBusinessPartnerBinding
    private lateinit var searchView: SearchView
    lateinit var loggerUserModel: LoggerUserModel
    private var intentCode: Int? = null


    /** ADAPTER **/
    private lateinit var businessPartnerAdapter: BusinessPartnerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBusinessPartnerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        initElements()
    }

    private fun initElements() {
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = getString(R.string.business_partner)
        intentCode = intent.getIntExtra(Const.INTENT_BUSINESS_PARTNER_TAG, 0)
        initViewModel()
        initCall()
        initActions()
        initRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnSearchClickListener {
            binding.appBar.tvTitleToolbar.visibility = View.GONE
            binding.appBar.ivBackToolbar.visibility = View.GONE
        }
        searchView.setOnCloseListener {
            binding.appBar.tvTitleToolbar.visibility = View.VISIBLE
            binding.appBar.ivBackToolbar.visibility = View.VISIBLE
            false
        }
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                businessPartnerAdapter.filter.filter(newText)
                return true
            }

        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_search -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initViewModel() {
        businessPartnerViewModel = ViewModelProvider(this).get(BusinessPartnerViewModel::class.java)
    }

    private fun initCall() {

    }


    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener { this.finish() }

        binding.fabCreateClient.setOnClickListener {
            val intent = Intent(this, RegisterPartnerActivity::class.java)
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            startActivity(intent)
        }
    }

    private fun initRecyclerView() {
        binding.rvBusinessPartner.setHasFixedSize(true)
        businessPartnerAdapter = BusinessPartnerAdapter(this, this)
        binding.rvBusinessPartner.adapter = businessPartnerAdapter
        binding.rvBusinessPartner.layoutManager = LinearLayoutManager(this)
        businessPartnerViewModel.list(loggerUserModel.entity).observe(this, {
            if (it != null && it.isNotEmpty()) {
                binding.rvBusinessPartner.visibility = View.VISIBLE
                binding.layoutNoMatches.llNoMatches.visibility = View.GONE
                businessPartnerAdapter.setList(it)
            } else {
                binding.rvBusinessPartner.visibility = View.GONE
                binding.layoutNoMatches.ivNoMatches.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_client_not_found))
                binding.layoutNoMatches.llNoMatches.visibility = View.VISIBLE
            }
        })

    }

    override fun onSelectBusinessPartner(businessPartnerView: BusinessPartnerView) {
        if (intentCode != null && intentCode != 0) {
            val intent = Intent()
            intent.putExtra(Const.INTENT_BUSINESS_PARTNER_TAG, businessPartnerView)
            setResult(Activity.RESULT_OK, intent)
            this.finish()
        }
    }

    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.isIconified = true
            return
        }
        super.onBackPressed()
    }
}