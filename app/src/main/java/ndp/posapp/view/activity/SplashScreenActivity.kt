package ndp.posapp.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ndp.posapp.databinding.ActivitySplashScreenBinding

import android.os.Handler
import com.valdesekamdem.library.mdtoast.MDToast


import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin


class SplashScreenActivity : AppCompatActivity() {

    private lateinit var binding : ActivitySplashScreenBinding
    private var sharedPreferencesLogin = SharedPreferencesLogin()
    private var loggerUserModel = LoggerUserModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initElements()
    }

    private fun initElements() {
        loggerUserModel = sharedPreferencesLogin.getSharedPreferences(this)
        initDB()
        initSplash()
    }

    private fun initDB() {

    }

    private fun initSplash() {
        val handler = Handler()
        handler.postDelayed({
            if (loggerUserModel.isLogged) {
                PopUp.mdToast(this, "Bienvenido ${loggerUserModel.name}!", MDToast.TYPE_SUCCESS)
                intent(HomeActivity::class.java)
                this.finish()
            } else {
                intent(LoginActivity::class.java)
                this.finish()
            }
        }, 1000)
    }

    private fun intent(cls: Class<*>?) {
        val intent = Intent(this, cls)
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivity(intent)
    }
}