package ndp.posapp.view.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.*
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.db.entity.PaymentReceiptEntity
import ndp.posapp.data.model.*
import ndp.posapp.data.util.mapper.toEntity
import ndp.posapp.data.util.mapper.toModel
import ndp.posapp.databinding.ActivityPaymentReceivedBinding
import ndp.posapp.databinding.DialogSearchCreditNoteBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.utils.use.UseApp
import ndp.posapp.utils.use.UsePhone
import ndp.posapp.utils.use.UsePhone.hideKeyboard
import ndp.posapp.utils.use.UsePhone.textChangedEvent
import ndp.posapp.view.adapter.PaymentReceivedAdapter
import ndp.posapp.view.fragment.paymentreceipt.PaymentReceiptFragment
import ndp.posapp.viewModel.*
import java.lang.Exception

@SuppressLint("SetTextI18n")
class PaymentReceivedActivity : AppCompatActivity() {

    /** VIEW MODEL **/
    private lateinit var rateStoreViewModel: RateStoreViewModel
    private lateinit var paymentWayViewModel: PaymentWayViewModel
    private lateinit var paymentViewModel: PaymentViewModel
    private lateinit var creditNoteViewModel: CreditNoteViewModel
    private lateinit var cashRegisterViewModel: CashRegisterViewModel
    private lateinit var saleOrderViewModel: SaleOrderViewModel
    private lateinit var paymentReceiptViewModel: PaymentReceiptViewModel

    private lateinit var binding: ActivityPaymentReceivedBinding
    private lateinit var paymentReceipt: PaymentReceiptModel
    private var paymentReceiptEntity: PaymentReceiptEntity? = null
    private lateinit var paymentReceivedAdapter: PaymentReceivedAdapter
    private lateinit var loggerUserModel: LoggerUserModel
    private var listRateStoreModel: List<RateStoreModel> = ArrayList()
    private var listPaymentWayModel: List<PaymentWayModel> = ArrayList()
    private lateinit var selectedPaymentWay: PaymentWayModel
    private var selectedPaymentMethod: PaymentMethodModel? = null
    private var selectedCreditNote: CreditNoteModel? = null
    private var paymentModel = PaymentModel()
    private var totalRemainingAmount = 0.0
    private var totalReceived = 0.0
    private var totalChange = 0.0
    private var saveButtonMenu: MenuItem? = null
    private var voucherButtonMenu: MenuItem? = null
    private var paymentSaved = false

    companion object {
        private const val PAYMENT_RECEIPT_DATA = "PAYMENT_RECEIPT_DATA"
        private const val PAYMENT_RECEIPT_INTERNAL_ID = "PAYMENT_RECEIPT_INTERNAL_ID"

        fun newIntent(context: Context, paymentReceipt: PaymentReceiptModel? = null, paymentReceiptInternalId: Long? = null): Intent {
            return Intent(context, PaymentReceivedActivity::class.java).also {
                if (paymentReceipt != null) it.putExtra(PAYMENT_RECEIPT_DATA, paymentReceipt)
                if (paymentReceiptInternalId != null) it.putExtra(PAYMENT_RECEIPT_INTERNAL_ID, paymentReceiptInternalId)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPaymentReceivedBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = getString(R.string.receipt_collection)

        saleOrderViewModel = ViewModelProvider(this).get(SaleOrderViewModel::class.java)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)

        val auxPaymentReceipt = intent.getSerializableExtra(PAYMENT_RECEIPT_DATA) as? PaymentReceiptModel
        if (auxPaymentReceipt != null) {
            paymentReceipt = auxPaymentReceipt
            initialSetup()
        } else {
            val paymentReceiptId = intent.getLongExtra(PAYMENT_RECEIPT_INTERNAL_ID, -1)
            if (paymentReceiptId > 0) {
                paymentReceiptViewModel = ViewModelProvider(this).get(PaymentReceiptViewModel::class.java)
                val liveData = paymentReceiptViewModel.get(paymentReceiptId)
                liveData.observe(this) {
                    liveData.removeObservers(this)
                    if (it != null){
                        paymentReceiptEntity = it
                        if (paymentReceiptEntity?.code != null && UsePhone.isOnline()) {
                            searchPaymentReceiptFromServer(paymentReceiptEntity!!.code!!)
                        } else {
                            paymentReceipt = paymentReceiptEntity!!.toModel()
                            completePaymentReceiptWithLocalData()
                            initialSetup()
                        }
                    } else {
                        finish()
                    }
                }
            } else {
                finish()
                return
            }
        }
    }

    private fun initialSetup() {
        if (paymentReceipt.status == Const.PAYMENT_RECEIPT_EMITTED) {
            paymentModel.let {
                it.cashRegister = Const.CASH_REGISTER_DEFAULT //TODO
                it.changeRoundingDifference  = 0.0
                it.createUser = loggerUserModel.userIdentifier
                it.createUserEmail = loggerUserModel.email
                it.createUserName = "${loggerUserModel.name} ${loggerUserModel.surname}"
                it.identificationDocument = paymentReceipt.nif
                it.lostAmount = 0.0
                it.payerName = paymentReceipt.businessName
                it.paymentAmount = 0.0
                it.paymentReceipt = paymentReceipt.code
                it.roundingAmount = 0.0
                it.status = -1
                it.store = loggerUserModel.store
                it.totalChange = 0.0
                it.totalReceived = 0.0
            }
            saveButtonMenu?.isVisible = true
        } else if (paymentReceipt.payments.isNotEmpty()){
            paymentModel = paymentReceipt.payments.first()
            voucherButtonMenu?.isVisible = true
        }

        paymentReceivedAdapter = PaymentReceivedAdapter(paymentModel)
        totalRemainingAmount = paymentReceipt.credit!!
        initElements()
        initActions()
        initViewModels()
        if (paymentReceipt.status == Const.PAYMENT_RECEIPT_EMITTED) {
            initCall()
        }
    }

    private fun searchPaymentReceiptFromServer(code: String) {
        binding.content.visibility = View.INVISIBLE
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.cargando_comprobante))
        saleOrderViewModel.getPaymentReceipt(loggerUserModel, code) {
            PopUp.closeProgressDialog()
            try {
                if (it.successful) {
                    paymentReceipt = it.data!!.value!!.first()
                } else {
                    paymentReceipt = paymentReceiptEntity!!.toModel()
                    completePaymentReceiptWithLocalData()
                }
            } catch (e: Exception) {
                paymentReceipt = paymentReceiptEntity!!.toModel()
                completePaymentReceiptWithLocalData()
            }
            initialSetup()
            binding.content.visibility = View.VISIBLE
        }
    }

    private fun completePaymentReceiptWithLocalData() {
        paymentReceiptEntity?.saleOrderId?.let { saleOrderId ->
            saleOrderViewModel.get(saleOrderId).observe(this) { saleOrderModel ->
                saleOrderModel?.let {
                    paymentReceipt.bonus = it.bonus
                    paymentReceipt.createUserName = it.nameSalePerson
                    paymentReceipt.icbper = it.icbper
                    paymentReceipt.paymentConditionName = it.paymentConditionName
                    paymentReceipt.plateNumber = it.plateNumber
                    paymentReceipt.receiptType = it.paymentReceiptType
                    paymentReceipt.shipAddress = it.shipAddress
                    paymentReceipt.subtotal = it.subtotal
                    paymentReceipt.taxAmount = it.taxAmount

                    it.detail.forEach { orderDetai ->
                        paymentReceipt.detail.add(PaymentReceiptDetailModel().apply {
                            itemCode = orderDetai.itemCode
                            itemName = orderDetai.itemName
                            quantity = orderDetai.quantity
                            unitMSRCode = orderDetai.unitMSRCode
                            grossPrice = orderDetai.grossPrice
                            total = orderDetai.total
                        })
                    }
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_payment_received, menu)
        saveButtonMenu = menu?.findItem(R.id.action_payment_save)
        voucherButtonMenu = menu?.findItem(R.id.action_payment_voucher)
        if (::paymentReceipt.isInitialized) {
            saveButtonMenu?.isVisible = paymentReceipt.status == Const.PAYMENT_RECEIPT_EMITTED
            voucherButtonMenu?.isVisible = paymentReceipt.status != Const.PAYMENT_RECEIPT_EMITTED
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_payment_save -> {
                save()
                true
            }
            R.id.action_payment_voucher -> {
                PaymentReceiptFragment(paymentReceipt, loggerUserModel).show(supportFragmentManager, "dialog")
                true
            }
            else -> false
        }
    }

    private fun initElements() {
        binding.tvDate.text = UseApp.localDateFromServer(paymentReceipt.createDate)
        binding.tvInvoiceNumber.text = paymentReceipt.paymentReceiptNumber
        binding.tvPartner.text = "${paymentReceipt.nif} - ${paymentReceipt.businessName}"
        binding.tvTotal.text = moneyValue(paymentReceipt.total)
        binding.tvRemaining.text = moneyValue(totalRemainingAmount)
        binding.rvPayments.adapter = paymentReceivedAdapter
        if (paymentReceipt.status == Const.PAYMENT_RECEIPT_EMITTED) {
            enableSwipe()
        }
    }

    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener { exit() }

        binding.spPaymentWay.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedPaymentWay = listPaymentWayModel[position]
                binding.spPaymentMethod.adapter = ArrayAdapter(this@PaymentReceivedActivity, android.R.layout.simple_list_item_1, selectedPaymentWay.methods)
                if (selectedPaymentWay.methods.isEmpty()) selectedPaymentMethod = null
                cleanInputs()
                setupViewInputs()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

        binding.spPaymentMethod.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, position: Int, id: Long) {
                selectedPaymentMethod = selectedPaymentWay.methods[position]
                cleanInputs()
                setupViewInputs()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

        binding.etAmount.textChangedEvent {s: CharSequence, start: Int, _: Int, count: Int ->
            if (binding.etAmount.isFocused) {
                binding.etAmount.removeTextChangedListener(this)
                val amount = if (selectedPaymentWay.code == Const.PaymentWayCode.EFECTIVO) {
                    UseApp.stringToDouble(s.toString(), 1)
                } else {
                    val amount = UseApp.stringToDouble(s.toString(), 2)
                    if (amount > totalRemainingAmount) totalRemainingAmount else amount
                }

                val amountStr = when {
                    s.toString() == "" || s.toString() == "." -> s.toString()
                    s.toString().last() == '.' -> amount.toInt().toString() + "."
                    (!s.toString().contains('.') || selectedPaymentWay.code == Const.PaymentWayCode.EFECTIVO) && amount == amount.toInt().toDouble() -> amount.toInt().toString()
                    else -> amount.toString()
                }

                binding.etAmount.setText(amountStr)
                binding.etAmount.setSelection(if ((start + count) > amountStr.length) amountStr.length else (start + count))

                enableAddButton()
                binding.etAmount.addTextChangedListener(this)
            }
        }

        binding.etCode.textChangedEvent { _: CharSequence, _: Int, _: Int, _: Int ->
            enableAddButton()
        }

        binding.etVoucher.textChangedEvent { _: CharSequence, _: Int, _: Int, _: Int ->
            enableAddButton()
        }

        binding.etCardNumber.textChangedEvent { _: CharSequence, _: Int, _: Int, _: Int ->
            enableAddButton()
        }

        binding.etCode.setOnClickListener { showDialogSearchCreditNote() }

        binding.fabAddButton.setOnClickListener {
            val detail = buildPaymentDetail()
            totalRemainingAmount = UseApp.roundDecimal(totalRemainingAmount - detail.totalReceived!!, 2)
            totalReceived = UseApp.roundDecimal(totalReceived + detail.totalReceived!!, 2)
            totalChange = UseApp.roundDecimal(totalChange + detail.totalChange!!, 2)

            paymentModel.detail.add(detail)
            paymentReceivedAdapter.notifyItemInserted(paymentModel.detail.lastIndex)

            cleanInputs()
            recalculate()
        }
    }

    private fun initViewModels() {
        rateStoreViewModel = ViewModelProvider(this).get(RateStoreViewModel::class.java)
        paymentWayViewModel = ViewModelProvider(this).get(PaymentWayViewModel::class.java)
        paymentViewModel = ViewModelProvider(this).get(PaymentViewModel::class.java)
        creditNoteViewModel = ViewModelProvider(this).get(CreditNoteViewModel::class.java)
        cashRegisterViewModel = ViewModelProvider(this).get(CashRegisterViewModel::class.java)
    }

    private fun initCall() {
        var loadingCount = 2
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.loading))

        rateStoreViewModel.get(loggerUserModel.entity, 1, UseApp.getDate(Const.DATE_QUERY), loggerUserModel.store!!).observe(this) { list ->
            if (list.isNotEmpty()) {
                listRateStoreModel = list
                loadingCount--
                if (loadingCount <= 0) finishedLoading()
            } else {
                rateStoreViewModel.getRateStore(loggerUserModel, 1, UseApp.getDate(Const.DATE_QUERY))
                rateStoreViewModel.result.observe(this) { response ->
                    response.data?.value?.let {
                        listRateStoreModel = it
                    }
                    loadingCount--
                    if (loadingCount <= 0) finishedLoading()
                }
            }
        }

        paymentWayViewModel.get(loggerUserModel.entity, loggerUserModel.store!!).observe(this){
            if (it != null && it.isNotEmpty()) {
                listPaymentWayModel = it
                binding.inputsContainer.visibility = View.VISIBLE
                loadingCount--
                if (loadingCount <= 0) finishedLoading()
            } else {
                PopUp.closeProgressDialog()
                PopUp.ShowAlertDialog(this, getString(R.string.titulo_error), getString(R.string.unexpected_error)
                ) { dialog, _ ->
                    dialog.dismiss()
                    finish()
                }
            }
        }

        cashRegisterViewModel.get(loggerUserModel.entity, loggerUserModel.userIdentifier, Const.CASH_REGISTER_TYPE_DEFAULT).observe(this) {
            if (it != null) {
                paymentModel.cashRegister = it.code
            } else {
                PopUp.ShowAlertDialog(this, getString(R.string.store_without_cash_register), getString(R.string.store_without_cash_register_message)) { dialog, _ ->
                    dialog.dismiss()
                    finish()
                }
            }
        }
    }

    private fun finishedLoading() {
    //eliminar metodos de pago que no tienen tipo de cambio registrado (el tipo sol nunca se elimina)
        listPaymentWayModel.forEach { paymentWay ->
            val availableMethods: MutableList<PaymentMethodModel> = ArrayList()
            paymentWay.methods.forEach { paymentMethod ->
                if (paymentMethod.accountCurrencyCode != Const.CurrencyCode.SOL) {
                    if (listRateStoreModel.any { it.currencyTo == paymentMethod.accountCurrencyCode }) {
                        availableMethods.add(paymentMethod)
                    }
                } else {
                    availableMethods.add(paymentMethod)
                }
            }
            availableMethods.forEach {
                if (it.accountName.isNullOrBlank()){
                    when (paymentWay.code) {
                        Const.PaymentWayCode.EFECTIVO -> it.accountName = "${paymentWay.paymentWay} ${it.method}"
                        Const.PaymentWayCode.TARJETA -> it.accountName = it.cardType
                    }
                }
            }
            paymentWay.methods = availableMethods
        }

        binding.spPaymentWay.adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listPaymentWayModel)
        PopUp.closeProgressDialog()
    }

    private fun setupViewInputs() {
        when (selectedPaymentWay.code) {
            Const.PaymentWayCode.EFECTIVO -> {
                binding.spPaymentMethodTitle.visibility = View.VISIBLE
                binding.spPaymentMethod.visibility = View.VISIBLE
                binding.codeInputLayout.visibility = View.GONE
                binding.voucherInputLayout.visibility = View.GONE
                binding.cardNumberInputLayout.visibility = View.GONE
            }
            Const.PaymentWayCode.TARJETA -> {
                binding.spPaymentMethodTitle.visibility = View.VISIBLE
                binding.spPaymentMethod.visibility = View.VISIBLE
                binding.codeInputLayout.visibility = View.GONE
                binding.voucherInputLayout.visibility = View.VISIBLE
                binding.cardNumberInputLayout.visibility = View.VISIBLE
            }
            Const.PaymentWayCode.DEPOSITO -> {
                binding.spPaymentMethodTitle.visibility = View.VISIBLE
                binding.spPaymentMethod.visibility = View.VISIBLE
                binding.codeInputLayout.visibility = View.GONE
                binding.voucherInputLayout.visibility = if (selectedPaymentMethod?.accountName?.contains("YAPE") == true) View.GONE else View.VISIBLE
                binding.cardNumberInputLayout.visibility = View.GONE
            }
            Const.PaymentWayCode.NOTA_DE_CREDITO-> {
                binding.spPaymentMethodTitle.visibility = View.GONE
                binding.spPaymentMethod.visibility = View.GONE
                binding.codeInputLayout.visibility = View.VISIBLE
                binding.voucherInputLayout.visibility = View.GONE
                binding.cardNumberInputLayout.visibility = View.GONE
            }
        }
    }

    private fun cleanInputs() {
        when {
            binding.etCode.isFocused -> {
                binding.etCode.hideKeyboard()
                binding.etCode.clearFocus()
            }
            binding.etAmount.isFocused -> {
                binding.etAmount.hideKeyboard()
                binding.etAmount.clearFocus()
            }
            binding.etVoucher.isFocused -> {
                binding.etVoucher.hideKeyboard()
                binding.etVoucher.clearFocus()
            }
            binding.etCardNumber.isFocused -> {
                binding.etCardNumber.hideKeyboard()
                binding.etCardNumber.clearFocus()
            }
        }

        binding.etCode.setText("")
        binding.etAmount.setText("")
        binding.etVoucher.setText("")
        binding.etCardNumber.setText("")
    }

    private fun enableAddButton () {
        val amount = if (binding.etAmount.text.toString().isNotBlank() && binding.etAmount.text.toString() != ".") binding.etAmount.text.toString().toDouble() else 0.0
        binding.fabAddButton.isEnabled = when (selectedPaymentWay.code) {
            Const.PaymentWayCode.EFECTIVO -> amount > 0
            Const.PaymentWayCode.TARJETA -> (amount > 0 && binding.etVoucher.text.toString().isNotBlank() && binding.etCardNumber.text.toString().length == 4)
            Const.PaymentWayCode.DEPOSITO -> {
                (amount > 0 && (selectedPaymentMethod?.accountName?.contains("YAPE") == true || binding.etVoucher.text.toString().isNotBlank()))
            }
            Const.PaymentWayCode.NOTA_DE_CREDITO-> (amount > 0 && binding.etCode.text.toString().isNotBlank())
            else -> false
        }
    }

    private fun buildPaymentDetail(): PaymentDetailModel {
        val paymentDetail = PaymentDetailModel().also {
            it.accountNumber = if (selectedPaymentMethod?.accounts?.isNotEmpty() == true) selectedPaymentMethod?.accounts?.first()?.account else ""
            it.amount = binding.etAmount.text.toString().toDouble()
            it.code = ""
            it.currency = Const.CurrencyCode.SOL
            it.currencyReceived = selectedPaymentMethod?.accountCurrencyCode ?: if (selectedPaymentWay.code != Const.PaymentWayCode.TARJETA) selectedPaymentMethod?.code else Const.CurrencyCode.SOL
            it.cardNumber = ""
            it.numberLine = paymentModel.detail.size
            it.payWay = selectedPaymentWay.code
            it.payWayName = selectedPaymentWay.paymentWay ?: ""
            it.paymentCondition =  "-1"
            it.paymentMethod = selectedPaymentMethod?.code ?: ""
            it.paymentMethodName = selectedPaymentMethod?.method ?: ""
            it.rateValue = if (selectedPaymentMethod?.accountCurrencyCode == Const.CurrencyCode.SOL || selectedPaymentMethod == null) 1.0 else {
                listRateStoreModel.first { it2 -> it2.currencyTo == it.currencyReceived }.rate
            }
            it.receivedAmountForeignCurrency = if (it.currencyReceived == Const.CurrencyCode.SOL) 0.0 else it.amount
            it.receivedAmountNationalCurrency = if (it.currencyReceived == Const.CurrencyCode.SOL) it.amount else it.amount!! * it.rateValue!!
            it.remainingAmount = totalRemainingAmount
            it.saleAmount = paymentReceipt.total
            it.saleCurrency = paymentReceipt.currency ?: Const.CurrencyCode.SOL
            it.status = 1
            it.store = loggerUserModel.store
            it.totalChange = if (totalRemainingAmount >= it.receivedAmountNationalCurrency!!) 0.0 else {
                var value = UseApp.roundDecimal(totalRemainingAmount -  it.receivedAmountNationalCurrency!!, 2)
                if (selectedPaymentWay.code == Const.PaymentWayCode.EFECTIVO && totalRemainingAmount != UseApp.roundDecimal(totalRemainingAmount, 1)) {
                    value = UseApp.roundDecimal(totalRemainingAmount - 0.05 -  it.receivedAmountNationalCurrency!!, 2)
                }
                UseApp.roundDecimal(value * -1, 1)
            }
            it.totalReceived = UseApp.roundDecimal(it.receivedAmountNationalCurrency!! - it.totalChange!!, 2)
        }

        when (selectedPaymentWay.code) {
            Const.PaymentWayCode.EFECTIVO -> {
                paymentDetail.accountDescription = selectedPaymentMethod?.accountName ?: "${selectedPaymentWay.paymentWay} ${selectedPaymentMethod?.method}"
                paymentDetail.exchangeAccountNumber = paymentDetail.accountNumber //TODO preguntar como se debe llenar exchangeAccountNumber
            }
            Const.PaymentWayCode.TARJETA -> {
                paymentDetail.also {
                    it.accountDescription = selectedPaymentMethod?.accountName ?: selectedPaymentMethod?.cardType
                    it.cardType = selectedPaymentMethod?.cardType
                    it.cardNumber = binding.etCardNumber.text.toString()
                    it.paymentMethodName = selectedPaymentMethod?.code
                    it.voucherNumber = binding.etVoucher.text.toString()
                }
            }
            Const.PaymentWayCode.DEPOSITO -> {
                paymentDetail.accountDescription = selectedPaymentMethod?.accountName ?: ""
                paymentDetail.paymentMethodName = selectedPaymentMethod?.accountName ?: ""
                paymentDetail.voucherNumber = if (selectedPaymentMethod?.accountName?.contains("YAPE") == true) "YAPE" else binding.etVoucher.text.toString()

                if (paymentDetail.accountNumber != null) {
                    run loop@{
                        selectedPaymentWay.banks.forEach { bank ->
                            bank.accounts.forEach { acc ->
                                if (acc.accounts.any {it.account == paymentDetail.accountNumber}) {
                                    paymentDetail.bank = bank.bankCode
                                    return@loop
                                }
                            }
                        }

                    }
                }
            }
            Const.PaymentWayCode.NOTA_DE_CREDITO -> {
                paymentDetail.accountDescription = "NOTA DE CREDITO"
                paymentDetail.code = binding.etCode.text.toString()
            }
        }

        return paymentDetail
    }

    private fun showDialogSearchCreditNote() {
        val dialogBinding = DialogSearchCreditNoteBinding.inflate(layoutInflater)
        val dialog = Dialog(this).apply {
            setCancelable(false)
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(dialogBinding.root)
        }

        dialogBinding.btnCancel.setOnClickListener { dialog.dismiss() }
        dialogBinding.btnSave.setOnClickListener {
            if (selectedCreditNote != null) {
                binding.etCode.setText(selectedCreditNote?.code)
            } else {
                PopUp.mdToast(this, getString(R.string.no_selected_credit_note), MDToast.TYPE_WARNING)
            }
            dialog.dismiss()
        }
        dialogBinding.btnSearch.setOnClickListener {
            PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.searching))
            creditNoteViewModel.getCreditNote(loggerUserModel, dialogBinding.etCode.text.toString()) {
                PopUp.closeProgressDialog()
                if (it.successful) {
                    dialogBinding.tvMessage.visibility = View.GONE
                    dialogBinding.tvMessageTitle.visibility = View.GONE
                    try {
                        selectedCreditNote = it.data!!.value!!.first()
                    } catch (e: Exception) {}
                } else {
                    selectedCreditNote = null
                    if (it.code == Const.ERROR_CREDIT_NOTE_NOT_FOUND) {
                        dialogBinding.tvMessage.text = it.message
                        dialogBinding.tvMessage.visibility = View.VISIBLE
                        dialogBinding.tvMessageTitle.visibility = View.VISIBLE
                    } else {
                        dialogBinding.tvMessage.visibility = View.GONE
                        dialogBinding.tvMessageTitle.visibility = View.GONE
                        PopUp.showAlert(this, it.message)
                    }
                }
            }
        }

        dialog.show()
    }

    private fun recalculate() {
        paymentModel.lostAmount = 0.0
        paymentModel.roundingAmount = 0.0
        paymentModel.totalReceived = totalReceived
        paymentModel.totalChange = totalChange

        if (totalRemainingAmount < 0.1) {
            if (totalRemainingAmount > 0.0) {
                paymentModel.roundingAmount = totalReceived
                paymentModel.lostAmount = UseApp.roundDecimal(paymentReceipt.credit!! - totalReceived, 2)
            }
            binding.inputsContainer.visibility = View.GONE
            paymentModel.status = 1
        } else {
            binding.inputsContainer.visibility = View.VISIBLE
            paymentModel.status = -1
        }

        binding.tvRemaining.text = moneyValue(totalRemainingAmount)
        binding.tvReceived.text = moneyValue(totalReceived)
        binding.tvRounded.text = moneyValue(paymentModel.roundingAmount)
    }

    private fun save() {
        if (paymentModel.status == -1) {
            PopUp.mdToast(this, "Credito diferente a cero.", MDToast.TYPE_ERROR)
            return
        }

        PopUp.ShowDialog(
            this,
            getString(R.string.sure_save_business),
            getString(R.string.warning),
            PopUp.MSG_TYPE_WARNING,
            PopUp.BUTTON_TYPE_ACCEPT, {
                paymentReceipt.payments.add(paymentModel)
                if (UsePhone.isOnline() && paymentReceipt.code != null) {
                    PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.saving_sales_order))
                    paymentViewModel.postQuickPayment(loggerUserModel, paymentModel) {
                        PopUp.closeProgressDialog()
                        if (it.successful) {
                            if (paymentReceiptEntity != null) {
                                paymentReceiptEntity!!.internalStatus = Const.PaymentReceiptInternalStatus.SAVED_SERVER
                                saveLocal()
                            } else {
                                PopUp.mdToast(this, getString(R.string.saved_correctly), MDToast.TYPE_SUCCESS)
                                saveButtonMenu?.isVisible = false
                                voucherButtonMenu?.isVisible = true
                                paymentSaved = true
                            }
                        } else {
                            saveLocal()
                        }
                    }
                } else {
                    saveLocal()
                }
            },
            null
        )
    }

    private fun saveLocal() {
        if (paymentReceiptEntity != null) {
            paymentReceiptEntity?.payment = Gson().toJson(paymentModel, object : TypeToken<PaymentModel>() {}.type)
            paymentReceiptViewModel.update(paymentReceiptEntity!!)
        } else {
            if (!::paymentReceiptViewModel.isInitialized) {
                paymentReceiptViewModel = ViewModelProvider(this).get(PaymentReceiptViewModel::class.java)
            }
            paymentReceiptEntity = paymentReceipt.toEntity(loggerUserModel.entity, Const.PaymentReceiptInternalStatus.SAVED_LOCAL, paymentModel)
            paymentReceiptViewModel.insert(paymentReceiptEntity!!)
        }
        PopUp.mdToast(this, getString(R.string.saved_correctly), MDToast.TYPE_SUCCESS)
        saveButtonMenu?.isVisible = false
        voucherButtonMenu?.isVisible = true
        paymentSaved = true
    }

    private fun enableSwipe() {
        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if (direction == ItemTouchHelper.LEFT) {
                    val position = viewHolder.adapterPosition
                    val deleteRow = paymentModel.detail[position]
                    paymentModel.detail.remove(deleteRow)
                    paymentReceivedAdapter.notifyItemRemoved(position)

                    totalRemainingAmount = UseApp.roundDecimal(totalRemainingAmount + deleteRow.totalReceived!!, 2)
                    totalReceived = UseApp.roundDecimal(totalReceived - deleteRow.totalReceived!!, 2)
                    totalChange = UseApp.roundDecimal(totalChange - deleteRow.totalChange!!, 2)

                    //reorganizar numberLine
                    paymentModel.detail.forEachIndexed { index, paymentDetailModel ->  paymentDetailModel.numberLine = index}

                    recalculate()
                }
            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                val icon: Bitmap
                val p = Paint()
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    val itemView = viewHolder.itemView
                    val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                    val width = height / 3
                    if (dX < 0) {
                        p.color = ContextCompat.getColor(this@PaymentReceivedActivity, R.color.red)
                        val background = RectF(itemView.right.toFloat() + dX, itemView.top.toFloat(), itemView.right.toFloat(), itemView.bottom.toFloat())
                        c.drawRect(background, p)
                        icon = BitmapFactory.decodeResource(resources, R.drawable.ic_trash)
                        val iconDest = RectF(itemView.right.toFloat() - 2 * width, itemView.top.toFloat() + width, itemView.right.toFloat() - width, itemView.bottom.toFloat() - width)
                        c.drawBitmap(icon, null, iconDest, p)
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }).attachToRecyclerView(binding.rvPayments)
    }

    private fun exit() {
        if (paymentReceipt.status == Const.PAYMENT_RECEIPT_EMITTED) {
            PopUp.ShowDialog(
                this,
                getString(R.string.message_exit),
                getString(R.string.warning),
                PopUp.MSG_TYPE_WARNING,
                PopUp.BUTTON_TYPE_ACCEPT, {
                    finish()
                },
                null
            )
        } else {
            if (paymentSaved) setResult(Activity.RESULT_OK)
            finish()
        }
    }

    private fun moneyValue(amount: Double?): String {
        return "${getString(R.string.pen_symbol)} ${UseApp.doubleToString(amount, 2)}"
    }

    override fun onBackPressed() {
        exit()
    }
}