package ndp.posapp.view.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.db.entity.CorrelativeEntity
import ndp.posapp.data.db.entity.PaymentReceiptEntity
import ndp.posapp.data.db.entity.ReceiptTypeEntity
import ndp.posapp.data.db.entity.SaleOrderEntity
import ndp.posapp.data.model.DocumentInstallmentsModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.data.model.SaleOrderModel
import ndp.posapp.data.util.mapper.toEntity
import ndp.posapp.databinding.ActivityOrderBinding
import ndp.posapp.utils.Const
import ndp.posapp.view.fragment.order.OrderHeaderFragment
import ndp.posapp.view.fragment.order.OrderItemFragment
import ndp.posapp.view.fragment.order.OrderSummaryFragment
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.utils.use.UseApp
import ndp.posapp.utils.use.UsePhone
import ndp.posapp.viewModel.*

class OrderActivity : AppCompatActivity() {

    /** VIEW MODEL **/
    private lateinit var saleOrderViewModel: SaleOrderViewModel
    private lateinit var receiptTypeViewModel: ReceiptTypeViewModel
    private lateinit var paymentConditionsViewModel: PaymentConditionsViewModel
    private lateinit var cashRegisterViewModel: CashRegisterViewModel
    private lateinit var correlativeViewModel: CorrelativeViewModel
    private lateinit var objectViewModel: ObjectViewModel
    private lateinit var paymentReceiptViewModel: PaymentReceiptViewModel

    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityOrderBinding
    private var correlativeList: List<CorrelativeEntity> = ArrayList()
    private var orderHeaderFragment = OrderHeaderFragment(this)
    private var orderItemFragment = OrderItemFragment(this)
    private var orderSummaryFragment = OrderSummaryFragment(this)
    private var receiptTypeList: List<ReceiptTypeEntity> = ArrayList()
    private var paymentReceiptInternalId: Long? = null

    /** EXPORTED VARIABLES **/
    lateinit var loggerUserModel: LoggerUserModel
    var receiptType =  ReceiptTypeEntity()
    var saleOrderModel = SaleOrderModel()
    var partnerPriceList = 5

    companion object {
        const val PAYMENT_REQUEST_CODE = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrderBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)

        receiptType.receipt = getString(R.string.ticket)
        initSaleOrder()

        initElements()
    }

    private fun initSaleOrder() {
        saleOrderModel.additionalData = mutableMapOf<String, Any?>().apply {
            put("userInfoStore", loggerUserModel.store)
            put("userInfoUbication", loggerUserModel.ubication)
            put("userInfoUbicationEntry", loggerUserModel.ubicationEntry)
            put("userInfoWarehouse", loggerUserModel.warehouse)
            put("tentativeSchedule", "")
            put("reservationInvoice", "0")
            put("alternativeShipAddress", "")
        }
        saleOrderModel.bonus = 0.0
        saleOrderModel.comment = ""
        saleOrderModel.createUser = loggerUserModel.userIdentifier
        saleOrderModel.deliveryDate = UseApp.getDate(Const.DATE_TIME_QUERY)
        saleOrderModel.detraction = Const.DETRACTION_NO
        saleOrderModel.discount = 0.0
        saleOrderModel.discountPercentage = 0.0
        saleOrderModel.docCurrency = "SOL" //se manda en duro
        saleOrderModel.docDate = saleOrderModel.deliveryDate
        saleOrderModel.createDate = saleOrderModel.deliveryDate
        saleOrderModel.emailSalePerson = loggerUserModel.email
        saleOrderModel.icbper = 0.0 // Impuesto a la bolsa
        saleOrderModel.nameSalePerson = loggerUserModel.name
        saleOrderModel.paymentReceiptType = receiptType.code
        saleOrderModel.pickingStatus = ""
        saleOrderModel.reservationInvoice = Const.RESERVATION_INVOICE_NO
        saleOrderModel.sampleCode = ""
        saleOrderModel.saleType = Const.SALE_TYPE_QUICK_SALE
        saleOrderModel.status = Const.PAYMENT_RECEIPT_EMITTED
        saleOrderModel.store = loggerUserModel.store
        saleOrderModel.subtotal = 0.0
        saleOrderModel.subtotalDiscount = 0.0
        saleOrderModel.taxAmount = 0.0
        saleOrderModel.total = 0.0
        saleOrderModel.transactionalCode = UseApp.getDate(Const.TIME_STAMP)
        saleOrderModel.ubication = loggerUserModel.ubication
        saleOrderModel.ubicationEntry = loggerUserModel.ubicationEntry
        saleOrderModel.warehouse = loggerUserModel.warehouse
    }

    private fun initElements() {
        binding.appBar.tbToolbar.title = ""
        setSupportActionBar(binding.appBar.tbToolbar)
        binding.appBar.tvTitleToolbar.text = getString(R.string.quick_sale)

        val params = binding.appBar.tvTitleToolbar.layoutParams as RelativeLayout.LayoutParams
        params.removeRule(RelativeLayout.CENTER_IN_PARENT)
        params.addRule(RelativeLayout.CENTER_VERTICAL)
        params.addRule(RelativeLayout.END_OF, binding.appBar.ivBackToolbar.id)
        params.marginStart = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
        binding.appBar.tvTitleToolbar.layoutParams = params

        initViewModel()
        initActions()
        initCall()
    }

    private fun initViewModel() {
        saleOrderViewModel = ViewModelProvider(this).get(SaleOrderViewModel::class.java)
        receiptTypeViewModel = ViewModelProvider(this).get(ReceiptTypeViewModel::class.java)
        paymentConditionsViewModel = ViewModelProvider(this).get(PaymentConditionsViewModel::class.java)
        cashRegisterViewModel = ViewModelProvider(this).get(CashRegisterViewModel::class.java)
        correlativeViewModel = ViewModelProvider(this).get(CorrelativeViewModel::class.java)
        paymentReceiptViewModel = ViewModelProvider(this).get(PaymentReceiptViewModel::class.java)
    }

    private fun initCall() {
        receiptTypeViewModel.list(loggerUserModel.entity).observe(this) { list ->
            receiptTypeList = list
            setReceiptType(Const.BOLETA_CODE)
            saleOrderModel.paymentReceiptType = receiptType.code
        }

        paymentConditionsViewModel.get(loggerUserModel.entity, Const.PAYMENT_CONDITION_DEFAULT).observe(this) {
            val additionalData = saleOrderModel.additionalData
            additionalData?.set("paymentCondition", it.code)
            additionalData?.set("paymentConditionName", it.paymentTermsGroupName)
            additionalData?.set("paymentType", it.type)
        }

        cashRegisterViewModel.get(loggerUserModel.entity, loggerUserModel.userIdentifier, Const.CASH_REGISTER_TYPE_DEFAULT).observe(this) { cashRegister ->
            if (cashRegister != null) {
                saleOrderModel.additionalData?.set("cashRegisterCode", cashRegister.code)
                // Sincronizar correlativos con el servidor
                correlativeViewModel.listCorrelative(loggerUserModel, cashRegister.code)
                correlativeViewModel.result.observe(this){ apiResponse ->
                    if (!apiResponse.successful && apiResponse.code == Const.ERROR_SESSION_EXPIRED) {
                        objectViewModel = ViewModelProvider(this).get(ObjectViewModel::class.java)
                        PopUp.ShowAlertDialog(this, apiResponse.message, getString(R.string.re_login)) { dialog, _ ->
                            dialog.dismiss()
                            objectViewModel.deleteAll()
                            SharedPreferencesLogin().editSharedPreferencesLogin(this, LoggerUserModel())
                            startActivity(Intent(this, LoginActivity::class.java).apply {
                                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                            })
                            finish()
                        }
                    }
                }

                correlativeViewModel.list(loggerUserModel.entity, cashRegister.code).observe(this){
                    if (it != null) correlativeList = it
                }
            } else {
                PopUp.ShowAlertDialog(this, getString(R.string.store_without_cash_register), getString(R.string.store_without_cash_register_message)) { dialog, _ ->
                    dialog.dismiss()
                    finish()
                }
            }
        }
    }

    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener { exit() }

        binding.bnSaleOrder.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.nav_header -> {
                    binding.viewPager.currentItem = 0
                    true
                }
                R.id.nav_item -> {
                    binding.viewPager.currentItem = 1
                    true
                }
                R.id.nav_summary -> {
                    binding.viewPager.currentItem = 2
                    true
                }
                else -> false
            }
        }


        binding.viewPager.apply {
            adapter = ViewPagerAdapter(this@OrderActivity)
            binding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    binding.bnSaleOrder.selectedItemId = when (position) {
                        0 -> R.id.nav_header
                        1 -> R.id.nav_item
                        2 -> R.id.nav_summary
                        else -> R.id.nav_header
                    }
                }

            })
        }

    }

    inner class ViewPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
        override fun getItemCount(): Int {
            return 3
        }

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> orderHeaderFragment
                1 -> orderItemFragment
                2 -> orderSummaryFragment
                else -> orderHeaderFragment
            }
        }

    }


    override fun onBackPressed() {
        exit()
    }

    private fun exit() {
        PopUp.ShowDialog(
            this,
            getString(R.string.message_exit),
            getString(R.string.warning),
            PopUp.MSG_TYPE_WARNING,
            PopUp.BUTTON_TYPE_ACCEPT, {
                this.finish()
            },
            null
        )
    }

    fun checkDetraction() {
        saleOrderModel.detraction = Const.DETRACTION_NO
        saleOrderModel.totalDetraction = null
        saleOrderModel.documentInstallments = ArrayList()

        if (saleOrderModel.total!! >= Const.SALE_TOTAL_FOR_DETRACTION) {
            var saleOrderDetail: SaleOrderDetailModel? = null

            saleOrderModel.detail.forEach {
                if (it.type == Const.ITEM_TYPE_SERVICE && it.u_VS_AFEDET != null && it.u_VS_AFEDET == Const.DETRACTION_AFEDET && it.u_VS_PORDET != null) {
                    if (saleOrderDetail == null || it.u_VS_PORDET!! > saleOrderDetail!!.u_VS_PORDET!!) {
                        saleOrderDetail = it
                    }
                }
            }

            saleOrderDetail?.let {
                val documentInstallments1 = DocumentInstallmentsModel()
                documentInstallments1.createDate = saleOrderModel.deliveryDate
                documentInstallments1.createUser = loggerUserModel.userIdentifier
                documentInstallments1.dueDate = documentInstallments1.createDate?.split(' ')?.first()
                documentInstallments1.paymentOrdered = Const.PAYMENT_ORDERED
                documentInstallments1.percentage = it.u_VS_PORDET!!

                val documentInstallments2 = DocumentInstallmentsModel()
                documentInstallments2.createDate = saleOrderModel.deliveryDate
                documentInstallments2.createUser = loggerUserModel.userIdentifier
                documentInstallments2.dueDate = documentInstallments2.createDate?.split(' ')?.first()
                documentInstallments2.paymentOrdered = Const.PAYMENT_ORDERED
                documentInstallments2.percentage = 100 - it.u_VS_PORDET!!

                saleOrderModel.documentInstallments.add(documentInstallments1)
                saleOrderModel.documentInstallments.add(documentInstallments2)

                saleOrderModel.detraction = Const.DETRACTION_YES
                saleOrderModel.totalDetraction = UseApp.roundDecimal(saleOrderModel.total!! * (documentInstallments1.percentage!!/100), 2)
            }
        }
    }

    fun validate() {
        when {
            saleOrderModel.nif == null -> {
                PopUp.mdToast(this, getString(R.string.select_business_partner), MDToast.TYPE_WARNING)
            }
            saleOrderModel.detail.isEmpty() -> {
                PopUp.mdToast(this, getString(R.string.no_products_were_entered), MDToast.TYPE_WARNING)
            }
            !correlativeList.any { it.receiptTypeCode == saleOrderModel.paymentReceiptType } -> {
                PopUp.mdToast(this, getString(R.string.cannot_generate_voucher), MDToast.TYPE_WARNING)
            }
            else -> {
                //VALIDAR STOCK
                saleOrderModel.detail.forEach {
                    if (saleOrderModel.reservationInvoice == Const.RESERVATION_INVOICE_NO && it.inventoryItem == Const.INVENTORY_ITEM_YES) {
                        if (it.kardexLocations[it.locationIndex].stock < it.quantity!!) {
                            PopUp.showAlert(this, "¡Error de stock!", "${it.itemName}\nLa cantidad ingresada es mayor que el stock disponible en la ubicación")
                            return
                        }
                    }
                }

                PopUp.ShowDialog(
                    this,
                    getString(R.string.sure_save_business),
                    getString(R.string.warning),
                    PopUp.MSG_TYPE_WARNING,
                    PopUp.BUTTON_TYPE_ACCEPT, {
                        saveOrder()
                    },
                    null
                )
            }
        }
    }

    private fun saveOrder() {
        val receiptType = correlativeList.first { it.receiptTypeCode == saleOrderModel.paymentReceiptType }

        if (receiptType.endNumeration <= receiptType.actualNumeration) {
            PopUp.mdToast(this, getString(R.string.cannot_generate_more_correlative), MDToast.TYPE_WARNING)
            return
        }

        // eliminar data innecesaria
        saleOrderModel.detail.forEach {
            it.kardex = ArrayList()
            it.kardexLocations = ArrayList()
            it.stock = null
            it.type = null
            it.u_VS_AFEDET = null
            it.u_VS_CODDET = null
            it.u_VS_PORDET = null
        }

        val receiptNumber = "${receiptType.series}-${receiptType.actualNumeration.toString().padStart(receiptType.endNumeration.toString().length, '0')}"
        val saleOrderEntity = saleOrderModel.toEntity(loggerUserModel.entity, Const.SaleOrderInternalStatus.SAVED_LOCAL, receiptNumber)
        val liveData = saleOrderViewModel.insert(saleOrderEntity)
        liveData.observe(this) { id ->
            liveData.removeObservers(this)
            saleOrderEntity.internalId = id

            receiptType.actualNumeration = receiptType.actualNumeration + 1
            correlativeViewModel.insert(receiptType)

            if (UsePhone.isOnline()) {
                PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.saving_sales_order))
                saleOrderViewModel.postQuickSale(loggerUserModel, saleOrderModel) {
                    PopUp.closeProgressDialog()
                    if (it.successful) {
                        saleOrderEntity.code = it.data?.code
                        saleOrderEntity.internalReceiptNumber = it.data?.paymentReceiptNumber ?: saleOrderEntity.internalReceiptNumber
                        saleOrderEntity.paymentReceiptNumber = it.data?.paymentReceiptNumber
                        saleOrderEntity.internalStatus = Const.SaleOrderInternalStatus.SAVED_SERVER
                        saleOrderViewModel.update(saleOrderEntity)
                    }
                    buildPaymentReceipt(saleOrderEntity)
                }
            } else {
                buildPaymentReceipt(saleOrderEntity)
            }
        }
    }

    private fun buildPaymentReceipt(saleOrderEntity: SaleOrderEntity) {
        val paymentReceiptEntity = PaymentReceiptEntity().apply {
            saleOrderId = saleOrderEntity.internalId!!
            company = saleOrderEntity.company
            internalStatus = Const.PaymentReceiptInternalStatus.SAVED_LOCAL
            businessName = saleOrderEntity.businessPartnerName
            code = saleOrderEntity.code
            createDate = saleOrderEntity.createDate
            credit = saleOrderEntity.total
            currency = saleOrderEntity.docCurrency
            nif = saleOrderEntity.nif
            paymentReceiptNumber = saleOrderEntity.internalReceiptNumber
            status = Const.PAYMENT_RECEIPT_EMITTED
            total = saleOrderEntity.total
        }

        val liveData = paymentReceiptViewModel.insert(paymentReceiptEntity)
        liveData.observe(this){ id ->
            liveData.removeObservers(this)

            paymentReceiptInternalId = id
            orderItemFragment.isSavedSale = true
            orderSummaryFragment.isSavedSale = true
            PopUp.mdToast(this, getString(R.string.saved_correctly), MDToast.TYPE_SUCCESS)
        }
    }

    fun notifyClientDataSetChanged() = orderSummaryFragment.notifyClientDataSetChanged()

    fun notifyReservationInvoiceChanged() = orderSummaryFragment.notifyReservationInvoiceChanged()

    fun notifyItemDataSetChanged() = orderSummaryFragment.notifyItemDataSetChanged()

    fun setReceiptType (receiptCodeApplication: String) {
        receiptType = receiptTypeList.first { it.codeApplication == receiptCodeApplication }
    }

    fun toPay() {
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivityForResult(
            PaymentReceivedActivity.newIntent(context = this, paymentReceiptInternalId = paymentReceiptInternalId),
            PAYMENT_REQUEST_CODE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PAYMENT_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    finish()
                }
            }
        }
    }
}