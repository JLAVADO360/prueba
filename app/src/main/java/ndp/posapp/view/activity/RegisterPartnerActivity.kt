package ndp.posapp.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayout
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.model.BusinessPartnerModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.ActivityRegisterPartnerBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.fragment.partner.PartnerAddressFragment
import ndp.posapp.view.fragment.partner.PartnerContactFragment
import ndp.posapp.view.fragment.partner.PartnerDataFragment
import ndp.posapp.viewModel.BusinessPartnerAddressViewModel
import ndp.posapp.viewModel.BusinessPartnerViewModel
import ndp.posapp.viewModel.ParameterViewModel

class RegisterPartnerActivity : AppCompatActivity() {

    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityRegisterPartnerBinding
    private val partnerDataFragment = PartnerDataFragment()
    val partnerAddressFragment = PartnerAddressFragment()
    val partnerContactFragment = PartnerContactFragment()
    private var loggerUserModel = LoggerUserModel()

    private var emailDefault: String? = null
    private var currentDocumentType = ""
    private lateinit var businessPartnerModel: BusinessPartnerModel
    private lateinit var businessPartnerViewModel: BusinessPartnerViewModel
    private lateinit var businessPartnerAddressViewModel: BusinessPartnerAddressViewModel
    private lateinit var parameterViewModel: ParameterViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterPartnerBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initElements()
    }

    private fun initElements() {
        businessPartnerViewModel = ViewModelProvider(this).get(BusinessPartnerViewModel::class.java)
        businessPartnerAddressViewModel = ViewModelProvider(this).get(BusinessPartnerAddressViewModel::class.java)
        parameterViewModel = ViewModelProvider(this).get(ParameterViewModel::class.java)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        partnerDataFragment.setLoggerUser(loggerUserModel)
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = getString(R.string.register_business_partner)
        initActions()

        parameterViewModel.get(loggerUserModel.entity, Const.PARAMETER_EMAIL_DEFAULT_CUSTOMER).observe(this, {
            emailDefault = it.value
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_register_bussiness, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_business_save -> {
                validate()
                true
            }
            else -> false
        }
    }

    private fun validate() {
        currentDocumentType = partnerDataFragment.getCurrentDocumentType()
        businessPartnerModel = partnerDataFragment.getBusiness()
        businessPartnerModel.additionalData = mapOf("store" to loggerUserModel.store!!)

        if (!isValidPartnerData() || !isValidPartnerAddress() || !isValidPartnerContact()) return

        PopUp.ShowDialog(
            this,
            null,
            getString(R.string.sure_save_business),
            PopUp.MSG_TYPE_WARNING,
            PopUp.BUTTON_TYPE_ACCEPT,
            {
                PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.registering_partner))
                businessPartnerViewModel.postBusinessPartner(loggerUserModel, businessPartnerModel, businessPartnerAddressViewModel) {
                    PopUp.closeProgressDialog()
                    if (it.successful) {
                        PopUp.mdToast(this, getString(R.string.partner_successfully_registered), MDToast.TYPE_SUCCESS)
                        finish()
                    } else {
                        PopUp.showAlert(this, it.message)
                    }
                }
            },
            null
        )
    }

    private fun isValidPartnerData() : Boolean {
        val viewPagerItem = 0
        if (businessPartnerModel.personType == Const.JURIDICAL_PERSON && !UseApp.isRUC20(businessPartnerModel.nif!!)) {
            toastError(getString(R.string.invalid_ruc_20), viewPagerItem)
            return false
        }

        if (businessPartnerModel.personType == Const.NATURAL_PERSON) {
            if (currentDocumentType == Const.RUC && !UseApp.isRUC10(businessPartnerModel.nif!!)) {
                toastError(getString(R.string.invalid_ruc_10), viewPagerItem)
                return false
            }
            if (currentDocumentType == Const.DNI && !UseApp.isDNI(businessPartnerModel.nif!!)) {
                toastError(getString(R.string.invalid_dni), viewPagerItem)
                return false
            }
            if (currentDocumentType == Const.CARNET_EXTRANJERIA && !UseApp.isImmigrationCard(businessPartnerModel.nif!!)) {
                toastError(getString(R.string.invalid_immigration_card), viewPagerItem)
                return false
            }
            if (currentDocumentType == "" && (businessPartnerModel.nif.isNullOrBlank() || businessPartnerModel.nif!!.length < 6)) {
                toastError(getString(R.string.invalid_number_document), viewPagerItem)
                return false
            }
            if (currentDocumentType != Const.RUC) {
                if (businessPartnerModel.lastNameF.isNullOrBlank()) {
                    toastError(getString(R.string.required_last_name_f), viewPagerItem)
                    return false
                }
                if (businessPartnerModel.lastNameM.isNullOrBlank()) {
                    toastError(getString(R.string.required_last_name_m), viewPagerItem)
                    return false
                }
                if (businessPartnerModel.names.isNullOrBlank()) {
                    toastError(getString(R.string.required_name), viewPagerItem)
                    return false
                }
            }
        }

        if (businessPartnerModel.businessName.isNullOrBlank()) {
            toastError(getString(R.string.required_business_name), viewPagerItem)
            return false
        }

        businessPartnerModel.phoneNumber?.let {
            if (it.isNotEmpty() && !UseApp.isMobileNumber(it)) {
                toastError(getString(R.string.invalid_mobile_number), viewPagerItem)
                return false
            }
        }

        businessPartnerModel.email?.let {
            if (it.isBlank() || it == "-") {
                businessPartnerModel.email = emailDefault
            } else if (!UseApp.validateEmail(it)) {
                toastError(getString(R.string.invalid_email_format), viewPagerItem)
                return false
            }
        }

        return true
    }

    private fun isValidPartnerAddress() : Boolean {
        val viewPagerItem = 1
        if (businessPartnerModel.personType == Const.JURIDICAL_PERSON || (businessPartnerModel.personType == Const.NATURAL_PERSON && businessPartnerModel.nif!!.length == 11)) {
            if (businessPartnerModel.addresses[0].department.isNullOrEmpty() || businessPartnerModel.addresses[1].department.isNullOrEmpty()) {
                toastError(getString(R.string.required_department), viewPagerItem)
                return false
            }
            if (businessPartnerModel.addresses[0].province.isNullOrEmpty() || businessPartnerModel.addresses[1].province.isNullOrEmpty()) {
                toastError(getString(R.string.required_province), viewPagerItem)
                return false
            }
            if (businessPartnerModel.addresses[0].district.isNullOrEmpty() || businessPartnerModel.addresses[1].district.isNullOrEmpty()) {
                toastError(getString(R.string.required_district), viewPagerItem)
                return false
            }
            if (businessPartnerModel.addresses[0].address.isNullOrEmpty() || businessPartnerModel.addresses[1].address.isNullOrEmpty()) {
                toastError(getString(R.string.required_address), viewPagerItem)
                return false
            }
        }

        return true
    }

    private fun isValidPartnerContact() : Boolean {
        val viewPagerItem = 3
        businessPartnerModel.contactPersons.first().email?.let {
            if (it.isBlank() || it == "-") {
                businessPartnerModel.contactPersons.first().email = null
            } else if (!UseApp.validateEmail(it)) {
                toastError(getString(R.string.invalid_email_format), viewPagerItem)
                return false
            }
        }

        businessPartnerModel.contactPersons.first().cellPhoneNumber?.let {
            if (!UseApp.isMobileNumber(it)) {
                toastError(getString(R.string.invalid_mobile_number), viewPagerItem)
                return false
            }
        }

        businessPartnerModel.contactPersons.first().phone?.let {
            if (!UseApp.isMobileNumber(it)) {
                toastError(getString(R.string.invalid_phone_number), viewPagerItem)
                return false
            }
        }

        return true
    }

    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener { exit() }

        binding.tlRegisterClient.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                binding.vpRegisterClient.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}

        })

        binding.vpRegisterClient.apply {
            this.adapter = ViewPagerAdapter(this@RegisterPartnerActivity)
            this.currentItem = 1; this.currentItem = 0
            this.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    binding.tlRegisterClient.apply { selectTab(getTabAt(position)) }
                }

            })
        }
    }

    private fun toastError(message: String, viewPagerItem: Int? = null) {
        PopUp.mdToast(this, message, MDToast.TYPE_ERROR)
        viewPagerItem?.let { binding.vpRegisterClient.currentItem = it }
    }

    override fun onBackPressed() {
        exit()
    }

    private fun exit() {
        PopUp.ShowDialog(
            this,
            getString(R.string.message_exit),
            getString(R.string.warning),
            PopUp.MSG_TYPE_WARNING,
            PopUp.BUTTON_TYPE_ACCEPT, {
                finish()
            },
            null
        )
    }

    private inner class ViewPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
        override fun getItemCount(): Int {
            return 3
        }

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> partnerDataFragment
                1 -> partnerAddressFragment
                2 -> partnerContactFragment
                else -> partnerDataFragment
            }
        }

    }
}