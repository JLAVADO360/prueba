package ndp.posapp.view.activity

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.PaymentReceiptModel
import ndp.posapp.data.model.SaleOrderModel
import ndp.posapp.databinding.ActivityQueryBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.dialog.ReceiptPaymentDetailDialog
import ndp.posapp.utils.dialog.SaleOrderDetailDialog
import ndp.posapp.utils.event.EventObserver
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.adapter.QueryOrderAdapter
import ndp.posapp.viewModel.SaleOrderViewModel
import ndp.posapp.utils.use.Mappers
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.adapter.QueryReceiptAdapter
import java.util.*


class QueryActivity : AppCompatActivity(), QueryOrderAdapter.Listener, QueryReceiptAdapter.Listener {


    /** VIEW MODEL **/
    private lateinit var saleOrderViewModel: SaleOrderViewModel

    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityQueryBinding
    private var typeQuery = ""
    private var currentBusinessPartner: BusinessPartnerView? = null
    private lateinit var calendarFrom: Calendar
    private lateinit var calendarTo: Calendar
    private var from = ""
    private var to = ""
    private var loggerUserModel = LoggerUserModel()
    private var nextPage = 0
    private lateinit var receiptPaymentDetailDialog: ReceiptPaymentDetailDialog

    /** ADAPTER **/

    private lateinit var queryOrderAdapter: QueryOrderAdapter
    private lateinit var queryReceiptAdapter: QueryReceiptAdapter

    companion object {
        const val PAYMENT_REQUEST_CODE = 100
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityQueryBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initElements()
    }

    private fun initElements() {
        calendarFrom = Calendar.getInstance()
        calendarTo = Calendar.getInstance()
        to = UseApp.getDate(Const.DATE_QUERY)
        binding.llNoMatches.llNoMatches.visibility = View.VISIBLE
        binding.spTypeQuery.adapter = ArrayAdapter(this, R.layout.spinner_item, Mappers.getQueryType())
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        initViewModel()
        initActions()

    }

    private fun initViewModel() {
        saleOrderViewModel = ViewModelProvider(this).get(SaleOrderViewModel::class.java)
    }

    private fun initActions() {
        binding.tbView.setOnClickListener { this.finish() }

        binding.spTypeQuery.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, position: Int, id: Long) {
                when (position) {
                    0 -> typeQuery = "                                                                                                                                                              "
                    1 -> typeQuery = Const.SALE_ORDER
                    2 -> typeQuery = Const.RECEIPT
                    3 -> typeQuery = Const.VISIT
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }


        binding.tvBusinessPartner.setOnClickListener {
            val intent = Intent(this, BusinessPartnerActivity::class.java)
            intent.putExtra(Const.INTENT_BUSINESS_PARTNER_TAG, Const.INTENT_BUSINESS_PARTNER)
            overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            startActivityForResult(intent, Const.INTENT_BUSINESS_PARTNER)
        }


        binding.tvFrom.setOnClickListener {
            UseApp.datePick(this, binding.tvFrom, calendarFrom, true) { s ->
                from = s
            }
        }
        binding.tvTo.setOnClickListener {
            UseApp.datePick(this, binding.tvTo, calendarTo, true) { s ->
                to = s
            }
        }

        binding.fabFilter.setOnClickListener {
            filter()
        }
        binding.fabNextPage.setOnClickListener {
            when (typeQuery) {
                Const.SALE_ORDER -> saleOrderCall(nextPage)
                Const.RECEIPT -> receiptCall(nextPage)
                Const.VISIT -> {
                }
            }
        }
    }

    private fun saleOrderCall(page: Int) {
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_order))
        saleOrderViewModel.listSaleOrder(loggerUserModel, currentBusinessPartner?.businessPartnerEntity!!.code, from, to, page)
        saleOrderViewModel.resultSaleOrder.observe(this, EventObserver {
            PopUp.closeProgressDialog()
            if (it.successful) {
                if (it.data?.nextPage != null) {
                    binding.fabNextPage.visibility = View.VISIBLE
                    nextPage = it.data!!.nextPage!!.split("&")[0].replace("saleorder?page=", "").toInt()
                } else {
                    binding.fabNextPage.visibility = View.GONE
                }
                it.data?.value.let { list ->
                    binding.llNoMatches.llNoMatches.visibility = View.GONE
                    queryOrderAdapter.setList(list!!)
                }
            } else {
                PopUp.showAlert(this, it.message)
            }

        })
    }

    private fun initRecyclerOrder() {
        binding.rvQuery.setHasFixedSize(true)
        queryOrderAdapter = QueryOrderAdapter(this, this)
        binding.rvQuery.adapter = queryOrderAdapter
        binding.rvQuery.layoutManager = LinearLayoutManager(this)
    }


    private fun receiptCall(page: Int) {
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_receipt))
        saleOrderViewModel.listPaymentReceipt(loggerUserModel, currentBusinessPartner?.businessPartnerEntity!!.code, from, to, page)
        saleOrderViewModel.resultPaymentReceipt.observe(this, EventObserver {
            PopUp.closeProgressDialog()
            if (it.successful) {
                if (it.data?.nextPage != null) {
                    binding.fabNextPage.visibility = View.VISIBLE
                    nextPage = it.data!!.nextPage!!.split("&")[0].replace("paymentreceipt?page=", "").toInt()
                } else {
                    binding.fabNextPage.visibility = View.GONE
                }
                it.data?.value.let { list ->
                    binding.llNoMatches.llNoMatches.visibility = View.GONE
                    queryReceiptAdapter.setList(list!!)
                }
            } else {
                PopUp.showAlert(this, it.message)
            }

        })
    }

    private fun initRecyclerReceipt() {
        binding.rvQuery.setHasFixedSize(true)
        queryReceiptAdapter = QueryReceiptAdapter(this, this)
        binding.rvQuery.adapter = queryReceiptAdapter
        binding.rvQuery.layoutManager = LinearLayoutManager(this)
    }

    private fun filter() {
        nextPage = 0
        if (validateFilter()) {
            when (typeQuery) {
                Const.SALE_ORDER -> {
                    initRecyclerOrder()
                    saleOrderCall(0)
                }
                Const.RECEIPT -> {
                    initRecyclerReceipt()
                    receiptCall(0)
                }
                Const.VISIT -> {
                }
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.INTENT_BUSINESS_PARTNER -> {
                if (data != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        currentBusinessPartner = data.getSerializableExtra(Const.INTENT_BUSINESS_PARTNER_TAG) as BusinessPartnerView
                        binding.tvBusinessPartner.text = currentBusinessPartner?.businessPartnerEntity?.businessName

                    } else {
                        PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                    }
                } else {
                    PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                }
            }
            PAYMENT_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    if (::receiptPaymentDetailDialog.isInitialized) receiptPaymentDetailDialog.close()
                    filter()
                }
            }
        }
    }

    private fun validateFilter(): Boolean {
        if (typeQuery.isBlank()) {
            PopUp.mdToast(this, getString(R.string.select_type_query), MDToast.TYPE_ERROR)
            return false
        }

        if (currentBusinessPartner == null) {
            PopUp.mdToast(this, getString(R.string.select_business_partner), MDToast.TYPE_ERROR)
            return false
        }

        if (from.isBlank()) {
            PopUp.mdToast(this, getString(R.string.select_date_from), MDToast.TYPE_ERROR)
            return false
        }
        return true
    }

    override fun onSelectSaleOrder(saleOrderModel: SaleOrderModel) {
        val saleOrderDetailDialog = SaleOrderDetailDialog(this,saleOrderModel)
        saleOrderDetailDialog.open()
    }

    override fun onSelectReceipt(paymentReceiptModel: PaymentReceiptModel) {
        receiptPaymentDetailDialog = ReceiptPaymentDetailDialog(this,paymentReceiptModel)
        receiptPaymentDetailDialog.open()
    }
}