package ndp.posapp.view.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ndp.posapp.databinding.ActivityHomeBinding
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.lifecycle.ViewModelProvider
import ndp.posapp.utils.use.UsePhone
import android.content.Intent
import android.content.SharedPreferences
import android.view.MenuItem
import android.widget.Toast
import androidx.core.view.GravityCompat
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.db.entity.CashRegisterInfoEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.utils.sync.SyncData
import ndp.posapp.utils.sync.SyncSaleOrder
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.fragment.home.HomeFragment
import ndp.posapp.view.fragment.home.HomeSelectCompanyFragment
import ndp.posapp.viewModel.*
import xdroid.toaster.Toaster
import kotlin.system.exitProcess


class HomeActivity : AppCompatActivity() {

    /** VIEW MODEL **/
    private lateinit var objectViewModel: ObjectViewModel
    private lateinit var repositorySyncHistoryViewModel: RepositorySyncHistoryViewModel
    private lateinit var cashRegisterViewModel: CashRegisterViewModel
    private lateinit var correlativeViewModel: CorrelativeViewModel
    private lateinit var paymentWayViewModel: PaymentWayViewModel

    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityHomeBinding
    private var sharedPreferencesLogin = SharedPreferencesLogin()
    private var loggerUserModel = LoggerUserModel()
    private var timeBack: Long = 0
    private var showProgressSyncCorrelative = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initElements()
    }

    private fun initElements() {
        loggerUserModel = sharedPreferencesLogin.getSharedPreferences(this)
        initDrawer()
        initViewModel()
        initObservers()
        initCall()
        initActions()
    }


    private fun initDrawer() {
        setSupportActionBar(binding.includeAppBar.toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        val toggle = ActionBarDrawerToggle(
            this, binding.drawerLayout, binding.includeAppBar.toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun initViewModel() {
        objectViewModel = ViewModelProvider(this).get(ObjectViewModel::class.java)
        repositorySyncHistoryViewModel = ViewModelProvider(this).get(RepositorySyncHistoryViewModel::class.java)
        cashRegisterViewModel = ViewModelProvider(this).get(CashRegisterViewModel::class.java)
        correlativeViewModel = ViewModelProvider(this).get(CorrelativeViewModel::class.java)
        paymentWayViewModel = ViewModelProvider(this).get(PaymentWayViewModel::class.java)
    }

    private fun initObservers() {
        cashRegisterViewModel.result.observe(this) { apiResponse ->
            if (!apiResponse.successful && apiResponse.code == Const.ERROR_SESSION_EXPIRED) {
                if (showProgressSyncCorrelative) PopUp.closeProgressDialog()
                sessionExpiredDialog(apiResponse.message)
            } else {
                val cashRegister = apiResponse.data?.filter { it.cashRegisterCode == Const.CASH_REGISTER_TYPE_DEFAULT }
                if (cashRegister?.isNotEmpty() == true) {
                    println("linea 87")
                    correlativeViewModel.listCorrelative(loggerUserModel, cashRegister.first().cashRegisterCode)
                } else if (showProgressSyncCorrelative) {
                    PopUp.closeProgressDialog()
                    showSucces()
                }
            }
        }
        correlativeViewModel.result.observe(this){ apiResponse ->
            if (!apiResponse.successful && apiResponse.code == Const.ERROR_SESSION_EXPIRED) {
                if (showProgressSyncCorrelative) PopUp.closeProgressDialog()
                sessionExpiredDialog(apiResponse.message!!)
            } else if (showProgressSyncCorrelative) {
                PopUp.closeProgressDialog()
                showSucces()

                if (UsePhone.isOnline()) {
                    SyncSaleOrder(this, this, this, loggerUserModel, showProgressSyncCorrelative).sync()
                }
            }
        }
    }

    private fun initCall() {
        if (UsePhone.isOnline()) {
            verifyPermission()
        } else {
            PopUp.ShowAlertDialog(this, getString(R.string.titulo_error), getString(R.string.no_internet_should_continue), ({ dialog, which ->
                continueOffLine()
            }), ({ dialog, which -> logOut() }))
        }
    }

    private fun verifyPermission() {
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.loading_profile))
        val liveDataHomePermission =
            objectViewModel.permissionListByRole(
                loggerUserModel.token,
                loggerUserModel.fingerPrint,
                Const.HOME_NAV_OPTIONS_OBJECT_PERMISSION,
                binding
            )
        liveDataHomePermission.observe(this, { response ->
            liveDataHomePermission.removeObservers(this)
            if (response.successful) {
                val liveDataFragmentPermission = objectViewModel.permissionListByRole(
                    loggerUserModel.token,
                    loggerUserModel.fingerPrint,
                    Const.HOME_FRAGMENT_OPTIONS_OBJECT_PERMISSION,
                    binding
                )
                liveDataFragmentPermission.observe(this, { response1 ->
                    liveDataFragmentPermission.removeObservers(this)
                    PopUp.closeProgressDialog()

                    if (response1.successful) {
                        loadData()
                        openFragment()
                    } else {
                        popupError(response.message)
                        openFragment()
                    }
                })
            } else {
                PopUp.closeProgressDialog()
                popupError(response.message)
                openFragment()
            }
        })
    }

    private fun openFragment() {
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.add(R.id.content_home, HomeFragment()).addToBackStack(null).commit()
    }

    private fun loadData() {
        val liveDataRepositoryHistory = repositorySyncHistoryViewModel.list(loggerUserModel.entity)
        liveDataRepositoryHistory.observe(this, {
            liveDataRepositoryHistory.removeObservers(this)
            silentSync()
            if (it.isEmpty()) {
                showProgressSyncCorrelative = false
                syncCorrelative()
                syncData(1)
            } else {
                showProgressSyncCorrelative = true
                syncCorrelative()
            }
        })
    }

    fun loadDataAfterChangeCompany() {
        loggerUserModel = sharedPreferencesLogin.getSharedPreferences(this)
        loadData()
    }

    private fun syncCorrelative() {
        if (showProgressSyncCorrelative) PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.sync_correlatives))

        val cashRegisterLiveData = cashRegisterViewModel.get(loggerUserModel.entity, loggerUserModel.userIdentifier, Const.CASH_REGISTER_TYPE_DEFAULT)
        cashRegisterLiveData.observe(this) { cashRegister ->
            cashRegisterLiveData.removeObservers(this)
            if (cashRegister != null) {
                //if(cashRegister.assigned==1)
                println("linea 192")
                correlativeViewModel.listCorrelative(loggerUserModel, cashRegister.code)
                //GG
                SaveCashRegisterData(cashRegister)
                //GG
            } else {
                cashRegisterViewModel.listCashRegisterInfo(loggerUserModel)
                if (cashRegister != null) {
                    SaveCashRegisterData(cashRegister)
                }
            }
        }
    }

    //GG
    private fun SaveCashRegisterData(PCashRegister:CashRegisterInfoEntity) {
        val sharedPref: SharedPreferences = getSharedPreferences("CashRegisterData", Context.MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPref.edit()
        editor.putString("code", PCashRegister.code)
        editor.putString("name", PCashRegister.name)
        editor.putString("company", PCashRegister.company)
        editor.putString("openingCreationUser", PCashRegister.openingCreationUser)
        editor.putString("user", PCashRegister.user)
        editor.putString("type", PCashRegister.type)
        editor.putInt("assigned", PCashRegister.assigned!!)
        editor.putInt("status", PCashRegister.status!!)
        editor.commit()
    }
    //GG

    private fun syncData(syncType: Int) {
        val syncData = SyncData(this, this, this, loggerUserModel)
        syncData.sync(syncType) { ok, message ->
            if (ok) {
                showSucces()
            } else {
                popupError(message)
            }
        }
    }


    private fun silentSync () {
        val paymentWayLiveData = paymentWayViewModel.get(loggerUserModel.entity, loggerUserModel.store!!)
        paymentWayLiveData.observe(this){
            paymentWayLiveData.removeObservers(this)
            if (it == null || it.isEmpty()) {
                paymentWayViewModel.listPaymentWay(loggerUserModel)
            }
        }
    }


/*    val observable = Observable.just(liveDataListAffectation,liveDataListAffectationType)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread()) val disposable = observable.subscribe({it.observe(this, { response ->if (!response.successful) {
                PopUp.closeProgressDialogCount()popupError(response.message)}})},{},{PopUp.closeProgressDialogCount()
            Toaster.toastLong("Comencemos")})*/


    private fun initActions() {
        binding.includeAppBar.ivSyncHome.setOnClickListener {
            if (UsePhone.isOnline()) {
                PopUp.ShowDialogCustomButton(
                    this@HomeActivity,
                    getString(R.string.mensaje_home_sync),
                    getString(R.string.titulo_home_sync),
                    "SYNC   TOTAL", "SYNC PARCIAL",
                    PopUp.MSG_TYPE_SUCCESS,
                    PopUp.BUTTON_TYPE_SYNC,
                    {
                        syncData(1)
                    },
                    {
                        syncData(0)
                    }, null
                )
            } else {
                popupError(getString(R.string.no_internet_conected))
            }
        }

        binding.includeAppBar.ivSelectCompany.setOnClickListener {
            HomeSelectCompanyFragment().show(supportFragmentManager, "dialog")
        }

        /**NAV LATERAL VIEW **/
        binding.btnSetting.setOnClickListener { Toaster.toastLong("Click") }
        binding.btnLogOut.setOnClickListener { logOut() }

    }

    private fun sessionExpiredDialog(message: String) {
        PopUp.ShowAlertDialog(this, message, getString(R.string.re_login)) { dialog, _ ->
            dialog.dismiss()
            objectViewModel.deleteAll()
            sharedPreferencesLogin.editSharedPreferencesLogin(this, LoggerUserModel())
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun logOut() {

        PopUp.ShowDialog(
            this,
            getString(R.string.message_title),
            getString(R.string.sure_log_out),
            PopUp.MSG_TYPE_WARNING,
            PopUp.BUTTON_TYPE_ACCEPT,
            {
                val loggerUserModel = LoggerUserModel()
                objectViewModel.deleteAll()
                sharedPreferencesLogin.editSharedPreferencesLogin(this, loggerUserModel)
                intent(LoginActivity::class.java)
                this.finish()
            },
            null
        )

    }

    private fun continueOffLine() {
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.loading_profile))
        openFragment()
        objectViewModel.list(binding)
        PopUp.mdToast(this, getString(R.string.continue_offline), MDToast.TYPE_ERROR)
    }

    private fun popupError(message: String?) {
        PopUp.ShowDialog(
            this,
            getString(R.string.titulo_error_should_continue),
            message ?: "",
            PopUp.MSG_TYPE_WARNING,
            PopUp.BUTTON_TYPE_ACCEPT,
            { continueOffLine() },
            { logOut() }
        )
    }

    private fun showSucces() {
        PopUp.ShowAlertDialog(
            this, getString(R.string.ready_go), getString(R.string.update_end)
        ) { dialog, _ -> dialog.dismiss() }
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            timeBack = UseApp.timeBack(this, timeBack)
            if (timeBack.toInt() == -1) {
                finishAffinity()
                exitProcess(0)
            }
        }
    }


    private fun intent(cls: Class<*>?) {
        val intent = Intent(this, cls)
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivity(intent)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId)
        {
            R.id.btnHome -> PopUp.mdToast(this, "Bienasdasdasdasds", MDToast.TYPE_SUCCESS)
        }
        return super.onOptionsItemSelected(item)
    }

}