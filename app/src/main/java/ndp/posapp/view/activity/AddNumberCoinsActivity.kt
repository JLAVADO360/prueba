package ndp.posapp.view.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ndp.posapp.databinding.ActivityAddNumbersCoinsBinding

class AddNumberCoinsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddNumbersCoinsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddNumbersCoinsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initComponents()

        val valor = intent.getStringExtra("valor")
        binding.txtcoin.text = valor

        binding.appBar.ivBackToolbar.setOnClickListener(){
            this.finish()
        }
    }

    private fun initComponents(){
        binding.appBar.tvTitleToolbar.text = "Conteo de Monedas"

    }
}