package ndp.posapp.view.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import ndp.posapp.R
import ndp.posapp.databinding.ActivityProfileBinding
import ndp.posapp.utils.shared.SharedPreferencesLogin

class ProfileActivity : AppCompatActivity() {

    /** VIEW MODEL **/


    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityProfileBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initElements()
        initActions()
    }

    @SuppressLint("SetTextI18n")
    private fun initElements() {
        binding.appBar.tbToolbar.title = ""
        setSupportActionBar(binding.appBar.tbToolbar)
        binding.appBar.tvTitleToolbar.text = getString(R.string.sale_order)
        binding.appBar.ivBackToolbar.setOnClickListener { this.finish() }

        val loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        binding.tvCode.text = loggerUserModel.roleCode
        binding.tvName.text = "${loggerUserModel.name} ${loggerUserModel.surname}"
        binding.tvDocumentNumber.text = loggerUserModel.documentNumber
        binding.tvEmail.text = loggerUserModel.email
        binding.tvUserType.text = loggerUserModel.roleName
    }

    private fun initActions() {

    }
}