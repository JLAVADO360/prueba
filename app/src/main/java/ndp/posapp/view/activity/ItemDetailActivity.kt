package ndp.posapp.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import ndp.posapp.R
import ndp.posapp.data.db.entityView.ItemView
import ndp.posapp.databinding.ActivityItemDetailBinding
import ndp.posapp.view.fragment.item.ItemInfoFragment
import ndp.posapp.view.fragment.item.ItemPriceFragment
import ndp.posapp.view.fragment.item.ItemStockFragment

class ItemDetailActivity : AppCompatActivity() {



    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityItemDetailBinding
    private lateinit var itemView: ItemView
    private var itemInfoFragment  = ItemInfoFragment()
    private var itemStockFragment  = ItemStockFragment()
    private var itemPriceFragment = ItemPriceFragment()

    /** ADAPTER **/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityItemDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initElements()
    }

    private fun initElements() {
        binding.appBar.tbToolbar.title = ""
        setSupportActionBar(binding.appBar.tbToolbar)
        binding.appBar.tvTitleToolbar.text = getString(R.string.item)

        val itemIntent = intent.getSerializableExtra(getString(R.string.item)) as ItemView?
        if (itemIntent == null) {
            this.finish()
        } else {
            itemView = itemIntent
        }
        initActions()
    }

    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener { this.finish() }

        itemInfoFragment.setItem(itemView.itemEntity)
        itemStockFragment.setStock(itemView.whsKardexEntity)
        itemPriceFragment.setPriceListDetail(itemView.priceListDetailEntity)

        binding.bnItem.setOnNavigationItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.nav_info -> {
                    binding.viewPager.currentItem = 0
                    true
                }
                R.id.nav_stock -> {
                    binding.viewPager.currentItem = 1
                    true
                }
                R.id.nav_price -> {
                    binding.viewPager.currentItem = 2
                    true
                }
                else -> false
            }
        }

        binding.viewPager.apply {
            adapter = ViewPagerAdapter(this@ItemDetailActivity)
            binding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    binding.bnItem.selectedItemId = when (position) {
                        0 -> R.id.nav_info
                        1 -> R.id.nav_stock
                        2 -> R.id.nav_price
                        else -> R.id.nav_info
                    }
                }
            })
        }
    }

    inner class ViewPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {
        override fun getItemCount(): Int {
            return 3
        }

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> itemInfoFragment
                1 -> itemStockFragment
                2 -> itemPriceFragment
                else -> itemInfoFragment
            }
        }

    }
}