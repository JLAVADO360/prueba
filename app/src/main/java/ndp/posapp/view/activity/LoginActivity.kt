package ndp.posapp.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.ActivityLoginBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.secure.AESCrypt
import ndp.posapp.utils.use.UseApp
import ndp.posapp.utils.use.UsePhone
import ndp.posapp.viewModel.LoggerUserViewModel
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener
import xdroid.toaster.Toaster

import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.viewModel.UserInfoViewModel


class LoginActivity : AppCompatActivity() {


    /** VIEW MODEL **/
    private lateinit var loggerUserViewModel: LoggerUserViewModel
    private lateinit var userInfoViewModel: UserInfoViewModel


    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityLoginBinding
    private var isKeyBoardOpen: Boolean = false
    private var email: String = ""
    private var password: String = ""
    private var aesCrypt = AESCrypt()
    private var sharedPreferencesLogin = SharedPreferencesLogin()
    private var loggerUserModel = LoggerUserModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        keyBoardListener()
        initElements()

    }

    private fun initElements() {
        initViewModels()
        initActions()

    }


    private fun initViewModels() {
        loggerUserViewModel = ViewModelProvider(this).get(LoggerUserViewModel::class.java)
        userInfoViewModel   = ViewModelProvider(this).get(UserInfoViewModel::class.java)
    }

    private fun initActions() {
        loggerUserModel = sharedPreferencesLogin.getSharedPreferences(this)
        binding.etEmail.setText(loggerUserModel.email)
        login()
        binding.tvForgotPassword.setOnClickListener { intent(RecoveryPasswordActivity::class.java) }
    }

    private fun login() {
        binding.btnLogin.setOnClickListener {
            if (validate()) {
                if (UsePhone.isOnline()) {
                    PopUp.openProgressDialog(this, getString(R.string.titulo_autenticacion), getString(R.string.mensaje_autenticacion_validando))
                    binding.btnLogin.isEnabled = false
                    UsePhone.hideKeyBoard(this)
                    val liveDataLogin = loggerUserViewModel.login(email, aesCrypt.generateHash(password, email), UsePhone.getAndroidId(this), this)
                    liveDataLogin.observe(this, { response ->
                        liveDataLogin.removeObservers(this)
                        PopUp.closeProgressDialog()
                        if (response.successful) {
                            //SaveSP(response.data?.entity, response.data?.)
                            showSelectCompanyDialog()
                            var ccc:String = response.data?.id.toString()
                            var ppp:String = ccc
                        } else {
                            PopUp.showAlert(this, response.message)
                        }
                        binding.btnLogin.isEnabled = true
                    })
                } else {
                    popupOffLine()
                    binding.btnLogin.isEnabled = true
                }
            } else {
                binding.btnLogin.isEnabled = true
            }
        }
    }

    private fun showSelectCompanyDialog() {
        val loggerUser = SharedPreferencesLogin().getSharedPreferences(this)
        if (loggerUser.entities.size > 1) {
            val companies: MutableList<String> = ArrayList()
            loggerUser.entities.map { companies.add(it.businessName) }
            var dialog: AlertDialog? = null
            dialog = AlertDialog.Builder(this, R.style.LoginSelectCompanyDialog)
                .setTitle(R.string.select_company)
                .setSingleChoiceItems(companies.toTypedArray(), -1) { _, position ->
                    loggerUser.entity = loggerUser.entities[position].id
                    userInfo(loggerUser)
                    dialog?.dismiss()
                }
                .create()
            dialog.show()
        } else {
            userInfo(loggerUser)
        }
    }

    private fun userInfo(loggerUser: LoggerUserModel) {
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.get_user_info))
        val liveDataUserInfo = userInfoViewModel.getUserInfo(loggerUser)
        liveDataUserInfo.observe(this, { responseUserInfo ->
            liveDataUserInfo.removeObservers(this)
            PopUp.closeProgressDialog()
            if (responseUserInfo.successful) {
                loggerUser.store = responseUserInfo.data?.store
                loggerUser.storeAddress = responseUserInfo.data?.storeAddress
                loggerUser.storePriceList = responseUserInfo.data?.storePriceList
                loggerUser.ubication = responseUserInfo.data?.ubication
                loggerUser.ubicationEntry = responseUserInfo.data?.ubicationEntry
                loggerUser.warehouse = responseUserInfo.data?.warehouse
                loggerUser.isLogged = true
                SharedPreferencesLogin().editSharedPreferencesLogin(this, loggerUser)
                PopUp.mdToast(this, "Bienvenido ${loggerUser.name}!", MDToast.TYPE_SUCCESS)
                intent(HomeActivity::class.java)
                finish()
            } else {
                if (responseUserInfo.code == Const.ERROR_STORE_USER_NOT_FOUND && loggerUser.entities.size > 1) {
                    showSelectCompanyDialog()
                    PopUp.showAlert(this, getString(R.string.select_another_company), responseUserInfo.message)
                } else {
                    PopUp.showAlert(this, responseUserInfo.message)
                }
            }
        })
    }


    private fun validate(): Boolean {
        email = binding.etEmail.text.toString()
        password = binding.etPassWord.text.toString()

        if (email.isBlank()) {
            Toaster.toast(R.string.empty_email)
            return false
        }
        if (!UseApp.validateEmail(email)) {
            Toaster.toast(R.string.empty_email)
            return false
        }

        if (password.isEmpty()) {
            Toaster.toast(R.string.empty_password)
            return false
        }

        if (password.length < 8) {
            Toaster.toast(R.string.empty_password)
            return false
        }

        return true
    }

    private fun popupOffLine() {
        PopUp.ShowDialog(
            this@LoginActivity,
            getString(R.string.must_log_in_online),
            getString(R.string.titulo_sin_conexion_internet),
            PopUp.MSG_TYPE_ERROR
        ) { run { } }
    }


    private fun intent(cls: Class<*>?) {
        val intent = Intent(this, cls)
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivity(intent)
    }

    private fun keyBoardListener() {
        KeyboardVisibilityEvent.setEventListener(this, object : KeyboardVisibilityEventListener {
            override fun onVisibilityChanged(isOpen: Boolean) {
                isKeyBoardOpen = isOpen
            }
        })
    }
}