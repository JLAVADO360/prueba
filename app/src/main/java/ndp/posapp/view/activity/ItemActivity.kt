package ndp.posapp.view.activity

import android.app.SearchManager
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import ndp.posapp.R
import ndp.posapp.data.db.entityView.ItemView
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.ActivityItemBinding
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.adapter.ItemAdapter
import ndp.posapp.viewModel.ItemViewModel

class ItemActivity : AppCompatActivity(), ItemAdapter.Listener {

    /** VIEW MODEL **/
    private lateinit var itemViewModel: ItemViewModel

    /** TEMPORARY VARIABLES **/
    private lateinit var binding: ActivityItemBinding
    private lateinit var searchView: SearchView
    private lateinit var loggerUserModel: LoggerUserModel


    /** ADAPTER **/
    private lateinit var itemAdapter: ItemAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityItemBinding.inflate(layoutInflater)
        setContentView(binding.root)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        initElements()
    }

    private fun initElements() {
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = getString(R.string.item)
        initViewModel()
        initActions()
        initRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnSearchClickListener {
            binding.appBar.tvTitleToolbar.visibility = View.GONE
            binding.appBar.ivBackToolbar.visibility = View.GONE
        }
        searchView.setOnCloseListener {
            binding.appBar.tvTitleToolbar.visibility = View.VISIBLE
            binding.appBar.ivBackToolbar.visibility = View.VISIBLE
            false
        }
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                itemAdapter.filter.filter(newText)
                return true
            }

        })
        return true
    }

    private fun initViewModel() {
        itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)
    }


    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener {
            this.finish()
        }
    }

    private fun initRecyclerView() {
        binding.rvItem.setHasFixedSize(true)
        itemAdapter = ItemAdapter(this, this)
        binding.rvItem.adapter = itemAdapter
        binding.rvItem.layoutManager = LinearLayoutManager(this)
        itemViewModel.list(loggerUserModel.entity).observe(this, {
            it.let { itemAdapter.setList(it) }
        })

    }

    override fun onSelectedItem(itemView: ItemView) {
        val intent = Intent(this, ItemDetailActivity::class.java)
        intent.putExtra(getString(R.string.item),itemView)
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivity(intent)
    }

    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.isIconified = true
            return
        }
        super.onBackPressed()
    }
}