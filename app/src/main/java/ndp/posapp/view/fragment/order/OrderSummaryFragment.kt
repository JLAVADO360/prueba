package ndp.posapp.view.fragment.order

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import ndp.posapp.R
import ndp.posapp.databinding.FragmentOrderSummaryBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.activity.OrderActivity
import java.util.Calendar

class OrderSummaryFragment(private val orderActivity: OrderActivity) : Fragment() {

    private lateinit var binding: FragmentOrderSummaryBinding
    private lateinit var calendar: Calendar
    private lateinit var calendarFrom: Calendar
    private lateinit var calendarTo: Calendar
    private var tentativeDate = ""
    private var tentativeFrom = ""
    private var tentativeTo = ""
    private var savedButtonMenu: MenuItem? = null
    private var toPayButtonMenu: MenuItem? = null

    var isSavedSale = false
        set(value) {
            field = value
            setupButtonsMenu()
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentOrderSummaryBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_order_save, menu)
        toPayButtonMenu = menu.findItem(R.id.action_order_to_pay)
        savedButtonMenu = menu.findItem(R.id.action_order_save)
        setupButtonsMenu()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_order_save -> {
                orderActivity.saleOrderModel.comment = binding.etComment.text.toString()
                orderActivity.saleOrderModel.plateNumber = binding.etVehiclePlate.text.toString()
                orderActivity.validate()
                true
            }
            R.id.action_order_to_pay -> {
                orderActivity.toPay()
                true
            }
            else -> false
        }
    }

    private fun setupButtonsMenu() {
        toPayButtonMenu?.isVisible = isSavedSale
        savedButtonMenu?.isVisible = !isSavedSale
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initElements()
    }

    private fun initElements() {
        calendar = Calendar.getInstance()
        calendarFrom = Calendar.getInstance()
        calendarTo = Calendar.getInstance()
        binding.tvDateTime.text = UseApp.formatDateString(orderActivity.saleOrderModel.deliveryDate, Const.DATE_TIME_QUERY, Const.DATE_12)
        setHasOptionsMenu(true)
        notifyClientDataSetChanged()
        notifyReservationInvoiceChanged()
        notifyItemDataSetChanged()
        initActions()
    }

    private fun initActions() {
        binding.tvBusinessPartnerHeader.setOnClickListener {
            binding.generalInformationContainer.toggle()
            if (binding.generalInformationContainer.isExpanded) {
                binding.imBusinessPartnerArrow.animate().rotation(180f).start()
            } else {
                binding.imBusinessPartnerArrow.animate().rotation(0f).start()
            }
        }
        binding.tentativeScheduleHeader.setOnClickListener {
            binding.rlTentativeSchedule.toggle()
            if (binding.rlTentativeSchedule.isExpanded) {
                binding.tentativeScheduleArrow.animate().rotation(180f).start()
            } else {
                binding.tentativeScheduleArrow.animate().rotation(0f).start()
            }
        }

        binding.tvTentativeDate.setOnClickListener {
            UseApp.datePick(activity!!, binding.tvTentativeDate, calendar, false) { date ->
                binding.tvTentativeDate.text = UseApp.formatDateString(date, Const.DATE_QUERY, Const.DATE_APP)
                tentativeDate = date
                changeTentativeSchedule()
            }
        }

        binding.tvTentativeTimeFrom.setOnClickListener {
            UseApp.timePick(requireContext(), calendarFrom) { time12h, time24h ->
                binding.tvTentativeTimeFrom.text = time12h
                tentativeFrom = time24h
                changeTentativeSchedule()
            }
        }

        binding.tvTentativeTimeTo.setOnClickListener {
            UseApp.timePick(requireContext(), calendarTo) { time12h, time24h ->
                binding.tvTentativeTimeTo.text = time12h
                tentativeTo = time24h
                changeTentativeSchedule()
            }
        }
    }

    private fun changeTentativeSchedule() {
        orderActivity.saleOrderModel.additionalData?.set("tentativeSchedule", "$tentativeDate $tentativeFrom - $tentativeTo")
    }

    fun notifyClientDataSetChanged() {
        if (::binding.isInitialized) {
            binding.tvDocument.text = orderActivity.receiptType.receipt
            binding.tvSocialReason.text = orderActivity.saleOrderModel.businessPartnerName
            binding.tvDocumentNumber.text = orderActivity.saleOrderModel.nif
            binding.tvShippingAddress.text = orderActivity.saleOrderModel.shipAddress
        }
    }

    fun notifyReservationInvoiceChanged() {
        if (::binding.isInitialized) {
            if (orderActivity.saleOrderModel.reservationInvoice == Const.RESERVATION_INVOICE_YES) {
                binding.tentativeScheduleContainer.visibility = View.VISIBLE
                binding.tvTentativeDate.text = ""
                binding.tvTentativeTimeFrom.text = ""
                binding.tvTentativeTimeTo.text = ""
            } else {
                binding.tentativeScheduleContainer.visibility = View.GONE
            }
        }
    }

    fun notifyItemDataSetChanged() {
        if (::binding.isInitialized) {
            orderActivity.saleOrderModel.subtotal?.let { binding.tvSubTotal.text = UseApp.doubleToString(it, 2) }
            orderActivity.saleOrderModel.taxAmount?.let { binding.tvTax.text = UseApp.doubleToString(it, 2) }
            orderActivity.saleOrderModel.total?.let { binding.tvTotal.text = UseApp.doubleToString(it, 2) }

            if (orderActivity.saleOrderModel.icbper != null && orderActivity.saleOrderModel.icbper!! > 0) {
                binding.rlICBPERContainer.visibility = View.VISIBLE
                binding.tvICBPER.text = UseApp.doubleToString(orderActivity.saleOrderModel.icbper, 2)
            } else {
                binding.rlICBPERContainer.visibility = View.GONE
                binding.tvICBPER.text = null
            }
        }
    }
}