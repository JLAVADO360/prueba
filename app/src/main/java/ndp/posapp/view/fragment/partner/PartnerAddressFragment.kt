package ndp.posapp.view.fragment.partner

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModelProvider
import ndp.posapp.R
import ndp.posapp.data.db.entity.RegionEntity
import ndp.posapp.data.model.BusinessPartnerAddressModel
import ndp.posapp.databinding.FragmentPartnerAddressBinding
import ndp.posapp.utils.dialog.RegionDialog
import ndp.posapp.view.adapter.RegionAdapter
import ndp.posapp.viewModel.RegionRelationViewModel
import ndp.posapp.viewModel.RegionViewModel

class PartnerAddressFragment : Fragment() {

    private lateinit var binding: FragmentPartnerAddressBinding

    private var currentTypeAddress = ""
    private lateinit var regionDialog: RegionDialog

    private lateinit var regionViewModel: RegionViewModel
    private lateinit var regionRelationViewModel: RegionRelationViewModel

    private lateinit var addresses: List<BusinessPartnerAddressModel>
    private var currentFiscalDepartment: RegionEntity? = null
    private var currentFiscalProvince: RegionEntity? = null
    private var currentWarehouseDepartment: RegionEntity? = null
    private var currentWarehouseProvince: RegionEntity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPartnerAddressBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val tempAddresses = ArrayList<BusinessPartnerAddressModel>()
        tempAddresses.add(BusinessPartnerAddressModel())
        tempAddresses.add(BusinessPartnerAddressModel())
        tempAddresses[0].addressName = "FISCAL"
        tempAddresses[0].country = "PE"
        tempAddresses[0].type = "bo_BillTo"
        tempAddresses[1].addressName = "ALMACEN"
        tempAddresses[1].country = "PE"
        tempAddresses[1].type = "bo_ShipTo"
        addresses = tempAddresses

        initElements()
    }

    private fun initElements() {
        binding.warehouseAddress.tvTypeAddress.text = getText(R.string._warehouse)
        binding.warehouseAddress.addressIcon.setImageResource(R.drawable.ic_warehouse)
        initViewModel()
        initActions()
    }


    private fun initViewModel() {
        regionViewModel = ViewModelProvider(this).get(RegionViewModel::class.java)
        regionRelationViewModel = ViewModelProvider(this).get(RegionRelationViewModel::class.java)
    }


    private fun initActions() {
        binding.fiscalAddress.etDepartment.setOnClickListener {
            currentTypeAddress = "fiscal"
            initRecyclerViewDialog("DTO", null, ::onDepartmentSelected, R.string.department)
        }
        binding.fiscalAddress.etProvince.setOnClickListener {
            currentTypeAddress = "fiscal"
            initRecyclerViewDialog("PRO", currentFiscalDepartment, ::onProvinceSelected, R.string.province)
        }
        binding.fiscalAddress.etDistrict.setOnClickListener {
            currentTypeAddress = "fiscal"
            initRecyclerViewDialog("DIS", currentFiscalProvince, ::onDistrictSelected, R.string.district)
        }

        binding.warehouseAddress.etDepartment.setOnClickListener {
            currentTypeAddress = "almacen"
            initRecyclerViewDialog("DTO", null, ::onDepartmentSelected, R.string.department)
        }
        binding.warehouseAddress.etProvince.setOnClickListener {
            currentTypeAddress = "almacen"
            initRecyclerViewDialog("PRO", currentWarehouseDepartment, ::onProvinceSelected, R.string.province)
        }
        binding.warehouseAddress.etDistrict.setOnClickListener {
            currentTypeAddress = "almacen"
            initRecyclerViewDialog("DIS", currentWarehouseProvince, ::onDistrictSelected, R.string.district)
        }
    }

    private fun initRecyclerViewDialog(regionType: String, currentRegion: RegionEntity?, onRegionSelected: (regionSelected: RegionEntity) -> Unit, @StringRes fieldTextHint: Int) {
        val regionAdapter = RegionAdapter(onRegionSelected)
        regionDialog = RegionDialog(activity!!, regionViewModel, viewLifecycleOwner, regionAdapter, regionType, currentRegion, regionRelationViewModel, fieldTextHint)
        regionDialog.open()
    }

    fun setAddresses(listAddress: List<BusinessPartnerAddressModel>) {
        addresses = listAddress
        for (valor in listAddress){
            Log.e("valox:",""+valor.address)
            Log.e("valox:",""+valor.addressName)
            Log.e("valox:",""+valor.country)
            Log.e("valox:",""+valor.createDate)
            Log.e("valox:",""+valor.department)
            Log.e("valox:",""+valor.province)
            Log.e("valox:",""+valor.district)
            Log.e("valox:",""+valor.ubiquitous)
            Log.e("valox:",""+valor.invoicesCountry)
            Log.e("valox:",""+valor.logicStatus)
            Log.e("valox:",""+valor.rowNum)
            Log.e("valox:",""+valor.status)
            Log.e("valox:",""+valor.type)


        }
//        if (addresses.isNotEmpty()) {
//            val fiscalAddress = addresses[0]
//            binding.fiscalAddress.etAddress.setText(fiscalAddress.address)
//            regionViewModel.listDepartment(fiscalAddress.department!!, "DTO").observe(viewLifecycleOwner, { departments ->
//                if (departments.isNotEmpty()){
//                    currentFiscalDepartment = departments[0]
//                    fiscalAddress.department = currentFiscalDepartment?.region
//                    fiscalAddress.ubiquitous = currentFiscalDepartment?.code
//                    binding.fiscalAddress.etDepartment.setText(currentFiscalDepartment?.region)
//
//                    regionViewModel.listRelation(fiscalAddress.province!!, "PRO", currentFiscalDepartment, regionRelationViewModel).observe(viewLifecycleOwner, { provinces ->
//                        if (provinces.isNotEmpty()) {
//                            currentFiscalProvince = provinces[0]
//                            fiscalAddress.province = currentFiscalProvince?.region
//                            fiscalAddress.ubiquitous += currentFiscalProvince?.code
//                            binding.fiscalAddress.etProvince.setText(currentFiscalProvince?.region)
//
//                            regionViewModel.listRelation(fiscalAddress.district!!, "DIS", currentFiscalProvince, regionRelationViewModel).observe(viewLifecycleOwner, { districts ->
//                                if (districts.isNotEmpty()) {
//                                    fiscalAddress.district = districts[0].region
//                                    fiscalAddress.ubiquitous += districts[0].code
//                                    binding.fiscalAddress.etDistrict.setText(districts[0].region)
//                                } else {
//                                    fiscalAddress.district = null
//                                }
//                            })
//                        } else {
//                            fiscalAddress.province = null
//                            fiscalAddress.district = null
//                        }
//                    })
//                } else {
//                    fiscalAddress.department = null
//                    fiscalAddress.province = null
//                    fiscalAddress.district = null
//                    fiscalAddress.ubiquitous = null
//                }
//            })
//
//            if (addresses.size >= 2) {
//                val warehouseAddress = addresses[1]
//                binding.warehouseAddress.etAddress.setText(warehouseAddress.address)
//                regionViewModel.listDepartment(warehouseAddress.department!!, "DTO").observe(viewLifecycleOwner, { departments ->
//                    if (departments.isNotEmpty()){
//                        currentWarehouseDepartment = departments[0]
//                        warehouseAddress.department = currentWarehouseDepartment?.region
//                        warehouseAddress.ubiquitous = currentWarehouseDepartment?.code
//                        binding.warehouseAddress.etDepartment.setText(currentWarehouseDepartment?.region)
//
//                        regionViewModel.listRelation(warehouseAddress.province!!, "PRO", currentWarehouseDepartment, regionRelationViewModel).observe(viewLifecycleOwner, { provinces ->
//                            if (provinces.isNotEmpty()) {
//                                currentWarehouseProvince = provinces[0]
//                                warehouseAddress.province = currentWarehouseProvince?.region
//                                warehouseAddress.ubiquitous += currentWarehouseProvince?.code
//                                binding.warehouseAddress.etProvince.setText(currentWarehouseProvince?.region)
//
//                                regionViewModel.listRelation(warehouseAddress.district!!, "DIS", currentWarehouseProvince, regionRelationViewModel).observe(viewLifecycleOwner, { districts ->
//                                    if (districts.isNotEmpty()) {
//                                        warehouseAddress.district = districts[0].region
//                                        warehouseAddress.ubiquitous += districts[0].code
//                                        binding.warehouseAddress.etDistrict.setText(districts[0].region)
//                                    } else {
//                                        warehouseAddress.district = null
//                                    }
//                                })
//                            } else {
//                                warehouseAddress.province = null
//                                warehouseAddress.district = null
//                            }
//                        })
//                    } else {
//                        warehouseAddress.department = null
//                        warehouseAddress.province = null
//                        warehouseAddress.district = null
//                        warehouseAddress.ubiquitous = null
//                    }
//                })
//            }
//        }
    }

    fun getAddresses(): List<BusinessPartnerAddressModel>{
        addresses[0].address = binding.fiscalAddress.etAddress.text.toString()
        addresses[1].address = binding.warehouseAddress.etAddress.text.toString()
        return addresses
    }

    fun clean() {
        if (::binding.isInitialized) {
            binding.fiscalAddress.etDepartment.setText("")
            binding.fiscalAddress.etProvince.setText("")
            binding.fiscalAddress.etDistrict.setText("")
            binding.fiscalAddress.etAddress.setText("")
            binding.warehouseAddress.etDepartment.setText("")
            binding.warehouseAddress.etProvince.setText("")
            binding.warehouseAddress.etDistrict.setText("")
            binding.warehouseAddress.etAddress.setText("")
        }
    }

    private fun onDepartmentSelected(department: RegionEntity) {
        regionDialog.close()
        var addressIndex = 1
        if (currentTypeAddress == "fiscal") {
            addressIndex = 0
            currentFiscalDepartment = department
            binding.fiscalAddress.etDepartment.setText(department.region)
            binding.fiscalAddress.etProvince.setText("")
            binding.fiscalAddress.etDistrict.setText("")
        } else {
            currentWarehouseDepartment = department
            binding.warehouseAddress.etDepartment.setText(department.region)
            binding.warehouseAddress.etProvince.setText("")
            binding.warehouseAddress.etDistrict.setText("")
        }
        addresses[addressIndex].department = department.region
        addresses[addressIndex].province = null
        addresses[addressIndex].district = null
        addresses[addressIndex].ubiquitous = department.code
    }

    private fun onProvinceSelected(province: RegionEntity) {
        regionDialog.close()
        var addressIndex = 1
        if (currentTypeAddress == "fiscal") {
            addressIndex = 0
            currentFiscalProvince = province
            binding.fiscalAddress.etProvince.setText(province.region)
            binding.fiscalAddress.etDistrict.setText("")
        } else {
            currentWarehouseProvince = province
            binding.warehouseAddress.etProvince.setText(province.region)
            binding.warehouseAddress.etDistrict.setText("")
        }
        addresses[addressIndex].province = province.region
        addresses[addressIndex].district = null
        addresses[addressIndex].ubiquitous = addresses[addressIndex].ubiquitous?.substring(0, 2) + province.code
    }

    private fun onDistrictSelected(district: RegionEntity) {
        regionDialog.close()
        var addressIndex = 1
        if (currentTypeAddress == "fiscal") {
            addressIndex = 0
            binding.fiscalAddress.etDistrict.setText(district.region)
        } else {
            binding.warehouseAddress.etDistrict.setText(district.region)
        }
        addresses[addressIndex].district = district.region
        addresses[addressIndex].ubiquitous = addresses[addressIndex].ubiquitous?.substring(0, 4) + district.code
    }
}