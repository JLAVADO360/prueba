package ndp.posapp.view.fragment.home

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.model.EntityDTOModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.FragmentHomeSelectCompanyBinding
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.activity.HomeActivity
import ndp.posapp.view.adapter.CompanyAdapter
import ndp.posapp.viewModel.RepositorySyncHistoryViewModel
import ndp.posapp.viewModel.UserInfoViewModel
import java.util.*

class HomeSelectCompanyFragment: DialogFragment() {

    private lateinit var binding: FragmentHomeSelectCompanyBinding
    private lateinit var loggerUser: LoggerUserModel
    private var sharedPreferencesLogin = SharedPreferencesLogin()
    private lateinit var homeActivity: HomeActivity

    /** VIEW MODEL **/
    private lateinit var userInfoViewModel: UserInfoViewModel
    private lateinit var repositorySyncHistoryViewModel: RepositorySyncHistoryViewModel

    /** ADAPTER **/
    private lateinit var companyAdapter: CompanyAdapter


    override fun onAttach(context: Context) {
        super.onAttach(context)
        homeActivity = context as HomeActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.SelectCompanyDialog)
        loggerUser = sharedPreferencesLogin.getSharedPreferences(requireContext())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentHomeSelectCompanyBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        companyAdapter = CompanyAdapter(loggerUser.entities, loggerUser.entity, ::onCompanySelected)
        context
        initElements()
    }

    private fun initElements() {
        userInfoViewModel = ViewModelProvider(this).get(UserInfoViewModel::class.java)
        repositorySyncHistoryViewModel = ViewModelProvider(this).get(RepositorySyncHistoryViewModel::class.java)
        binding.rvCompanies.adapter = companyAdapter
        initActions()
    }

    private fun initActions() {
        binding.root.setOnClickListener { dismiss() }
    }

    private fun onCompanySelected(company: EntityDTOModel) {
        if (company.id != loggerUser.entity) {
            loggerUser.entity = company.id

            PopUp.openProgressDialog(requireContext(), getString(R.string.operation_running), getString(R.string.get_user_info))
            userInfoViewModel.getUserInfo(loggerUser).observe(this, { responseUserInfo ->
                PopUp.closeProgressDialog()
                if (responseUserInfo.successful) {
                    loggerUser.store = responseUserInfo.data?.store
                    loggerUser.storeAddress = responseUserInfo.data?.storeAddress
                    loggerUser.storePriceList = responseUserInfo.data?.storePriceList
                    loggerUser.ubication = responseUserInfo.data?.ubication
                    loggerUser.ubicationEntry = responseUserInfo.data?.ubicationEntry
                    loggerUser.warehouse = responseUserInfo.data?.warehouse
                    loggerUser.isLogged = true
                    SharedPreferencesLogin().editSharedPreferencesLogin(requireContext(), loggerUser)
                    PopUp.mdToast(requireContext(), "SE CAMBIÓ A ${company.businessName.toUpperCase(Locale.getDefault())}", MDToast.TYPE_SUCCESS)
                    homeActivity.loadDataAfterChangeCompany()
                } else {
                    PopUp.showAlert(requireContext(), responseUserInfo.message)
                    PopUp.mdToast(requireContext(), "NO SE CAMBIÓ DE EMPRESA", MDToast.TYPE_ERROR)
                }
                dismiss()
            })
        } else {
            dismiss()
        }
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val windowParams = window?.attributes
        windowParams?.dimAmount = 0.0f
        window?.attributes = windowParams
    }
}