package ndp.posapp.view.fragment.order

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.data.db.entity.BusinessPartnerEntity
import ndp.posapp.data.db.entity.PaymentConditionsEntity
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.FragmentOrderHeaderBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PaymentConditionDialog
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.activity.BusinessPartnerActivity
import ndp.posapp.view.activity.OrderActivity
import ndp.posapp.viewModel.PaymentConditionsViewModel

class OrderHeaderFragment(private val orderActivity: OrderActivity) : Fragment(), PaymentConditionDialog.Listener {

    private var fragmentBinding: FragmentOrderHeaderBinding? = null
    private val binding get() = fragmentBinding!!

    /** VIEW MODEL **/
    private lateinit var paymentConditionsViewModel: PaymentConditionsViewModel

    /** TEMPORARY VARIABLES **/
    private var currentPaymentConditionList: List<PaymentConditionsEntity> = ArrayList()
    private lateinit var paymentConditionDialog: PaymentConditionDialog
    lateinit var loggerUserModel: LoggerUserModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentBinding = FragmentOrderHeaderBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(requireContext())
        initElements()
    }

    private fun initElements() {
        binding.tvDocument.text = orderActivity.receiptType.receipt
        initViewModel()
        initActions()
    }

    private fun initViewModel() {
        paymentConditionsViewModel = ViewModelProvider(this).get(PaymentConditionsViewModel::class.java)
    }

    private fun initActions() {
        binding.rlBusinessPartner.setOnClickListener {
            binding.orderDetailCustomer.toggle()
            if (binding.orderDetailCustomer.isExpanded) {
                binding.orderHeaderCustomerArrow.animate().rotation(180f).start()
            } else {
                binding.orderHeaderCustomerArrow.animate().rotation(0f).start()
            }
        }

        binding.rlAdministrativeInformation.setOnClickListener {
            binding.orderDetailAdmin.toggle()
            if (binding.orderDetailAdmin.isExpanded) {
                binding.orderHeaderAdminArrow.animate().rotation(180f).start();
            } else {
                binding.orderHeaderAdminArrow.animate().rotation(0f).start();
            }
        }

        binding.tvBusinessPartnerCode.setOnClickListener {
            val intent = Intent(activity, BusinessPartnerActivity::class.java)
            intent.putExtra(Const.INTENT_BUSINESS_PARTNER_TAG, Const.INTENT_BUSINESS_PARTNER)
            activity!!.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            startActivityForResult(intent, Const.INTENT_BUSINESS_PARTNER)
        }

        binding.tvPaymentCondition.setOnClickListener {
            if (currentPaymentConditionList.isNotEmpty()) {
                paymentConditionDialog = PaymentConditionDialog(activity!!, currentPaymentConditionList, this, binding.tvPaymentCondition.text.toString())
                paymentConditionDialog.open()
            }
        }

        binding.swReservationInvoice.setOnCheckedChangeListener { _, isChecked ->
            orderActivity.saleOrderModel.reservationInvoice = if (isChecked) Const.RESERVATION_INVOICE_YES else Const.RESERVATION_INVOICE_NO
            val additionalData = orderActivity.saleOrderModel.additionalData
            additionalData?.set("reservationInvoice", orderActivity.saleOrderModel.reservationInvoice)
            additionalData?.set("tentativeSchedule", "")
            orderActivity.saleOrderModel.additionalData = additionalData
            orderActivity.notifyReservationInvoiceChanged()
        }
    }

    override fun onSelectedPaymentCondition(paymentConditionsEntity: PaymentConditionsEntity) {
        binding.tvPaymentCondition.text = paymentConditionsEntity.paymentTermsGroupName
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            if (resultCode == Activity.RESULT_OK) {
                val businessPartner = data.getSerializableExtra(Const.INTENT_BUSINESS_PARTNER_TAG) as BusinessPartnerView
                completeBusinessPartner(businessPartner.businessPartnerEntity)
                setSaleOrder(businessPartner)
            } else {
                PopUp.mdToast(context!!, activity!!.getString(R.string.no_get_client), MDToast.TYPE_WARNING)
            }
        } else {
            PopUp.mdToast(context!!, activity!!.getString(R.string.no_get_client), MDToast.TYPE_WARNING)
        }
    }

    private fun completeBusinessPartner(businessPartner: BusinessPartnerEntity) {
        binding.tvBusinessPartnerCode.text = businessPartner.code
        binding.tvSocialReason.text = businessPartner.businessName
        binding.tvDocumentNumber.text = businessPartner.nif
        binding.tvCellphone.text = businessPartner.phoneNumber
        binding.tvEmail.text = businessPartner.email
        binding.tvCreditLine.text = businessPartner.creditLine.toString()
        binding.tvDebitBalance.text = businessPartner.balanceSys.toString()

        val paymentCondition = paymentConditionsViewModel.list(loggerUserModel.entity, businessPartner.paymentCondition!!)
        paymentCondition.observe(this, {
            paymentCondition.removeObservers(this)
            currentPaymentConditionList = it
            it.forEach { condition ->
                if (condition.groupNumber == businessPartner.paymentCondition) {
                    binding.tvPaymentCondition.text = condition.paymentTermsGroupName
                }
            }
        })
    }

    private fun setSaleOrder(businessPartnerView: BusinessPartnerView) {
        val businessPartner = businessPartnerView.businessPartnerEntity
        val isRuc = (UseApp.isRUC20(businessPartner.nif!!) || UseApp.isRUC10(businessPartner.nif!!))
        orderActivity.setReceiptType( if (isRuc) Const.FACTURA_CODE else Const.BOLETA_CODE)

        orderActivity.partnerPriceList = businessPartner.priceList?.toIntOrNull() ?: 5
        orderActivity.saleOrderModel.businessPartnerCode = businessPartner.code
        orderActivity.saleOrderModel.businessPartnerName = businessPartner.businessName
        orderActivity.saleOrderModel.nif = businessPartner.nif
        orderActivity.saleOrderModel.payAddress = businessPartnerView.businessPartnerAddressEntity[0].address
        orderActivity.saleOrderModel.payAddressName = businessPartnerView.businessPartnerAddressEntity[0].addressName
        orderActivity.saleOrderModel.paymentReceiptType = orderActivity.receiptType.code
        orderActivity.saleOrderModel.shipAddress = businessPartnerView.businessPartnerAddressEntity[1].address
        orderActivity.saleOrderModel.shipAddressName = businessPartnerView.businessPartnerAddressEntity[1].addressName

        if (isRuc) {
            binding.swReservationInvoice.isChecked = true
            binding.swReservationInvoice.visibility = View.VISIBLE
        } else {
            binding.swReservationInvoice.isChecked = false
            binding.swReservationInvoice.visibility = View.INVISIBLE
        }
        binding.tvDocument.text = orderActivity.receiptType.receipt
        orderActivity.notifyClientDataSetChanged()
    }
}