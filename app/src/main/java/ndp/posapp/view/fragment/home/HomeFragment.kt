package ndp.posapp.view.fragment.home

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ndp.posapp.*
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.FragmentHomeBinding
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.activity.*
import ndp.posapp.viewModel.ObjectViewModel


class HomeFragment : Fragment() {

    private var fragmentBinding: FragmentHomeBinding? = null
    private val binding get() = fragmentBinding!!

    /** VIEW MODEL **/
    private lateinit var objectViewModel: ObjectViewModel

    /** TEMPORARY VARIABLES **/
    private var sharedPreferencesLogin = SharedPreferencesLogin()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentBinding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModel()
        initCall()
        initActions()
    }

    private fun initViewModel() {
        objectViewModel = ViewModelProvider(this).get(ObjectViewModel::class.java)
    }

    private fun initCall() {
        objectViewModel.list(binding)
    }


    private fun initActions() {
        binding.cvOrder.setOnClickListener { intent(OrderActivity::class.java) }
        binding.cvBusinessPartner.setOnClickListener { intent(BusinessPartnerActivity::class.java) }
        binding.cvItem.setOnClickListener{ intent(ItemActivity::class.java) }
        binding.cvQuery.setOnClickListener { intent(QueryActivity::class.java) }
        binding.cvProfile.setOnClickListener { intent(ProfileActivity::class.java) }
        binding.cvSingOut.setOnClickListener { singOut() }
        //GG
        binding.cvCashier.setOnClickListener{
            ShowCashierModal()
            //Toast.makeText(requireContext(), "casdasd", Toast.LENGTH_LONG).show()
        }
        //GG
    }

    private fun ShowCashierModal() {
        val ModalDialog = layoutInflater.inflate(R.layout.cashier_modal_layout, null)
        val CustomDialog = AlertDialog.Builder(requireContext())
            .setView(ModalDialog)
            .setCancelable(false)
            .show()
        CustomDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var CloseCashierModalIV: ImageView = ModalDialog.findViewById(R.id.IVCloseCashierModal)
        var OpeningListBtn: Button = ModalDialog.findViewById(R.id.BtnOpeningList)
        var ClosingListBtn: Button = ModalDialog.findViewById(R.id.BtnClosingList)
        //var SquareListBtn: Button = ModalDialog.findViewById(R.id.BtnSquareList)
        var BoxMovementBtn: Button = ModalDialog.findViewById(R.id.BtnBoxMovement)
        //var cajatexto:EditText = ModalDialog.findViewById(R.id.cajaET)
        OpeningListBtn.setOnClickListener()
        {

            /*val intent = Intent(this, InicioActivity::class.java)
            //intent.putExtra("nombre", cajatexto.text.toString())
            startActivity(intent)*/
            GoToOpeningList()
            CustomDialog.dismiss()
        }
        ClosingListBtn.setOnClickListener()
        {
            /*val intent = Intent(this, InicioActivity::class.java)
            //intent.putExtra("nombre", cajatexto.text.toString())
            startActivity(intent)*/
            intent(ClosingListCashierActivity::class.java)
            CustomDialog.dismiss()
        }
        /*SquareListBtn.setOnClickListener()
        {
            /*val intent = Intent(this, InicioActivity::class.java)
            //intent.putExtra("nombre", cajatexto.text.toString())
            startActivity(intent)*/
            intent(BalanceListCashierActivity::class.java)
            CustomDialog.dismiss()
        }*/
        BoxMovementBtn.setOnClickListener()
        {
            /*val intent = Intent(this, InicioActivity::class.java)
            //intent.putExtra("nombre", cajatexto.text.toString())
            startActivity(intent)*/
            intent(MovementListCashierActivity::class.java)
            CustomDialog.dismiss()
        }
        CloseCashierModalIV.setOnClickListener()
        {
            CustomDialog.dismiss()
        }
    }

    private fun GoToOpeningList() {
        /*val fr = OpeningListFragment()
        //fr.setArguments(bn)
        activity!!.supportFragmentManager.beginTransaction()
            .replace(R.id.content_home, fr)
            //.addToBackStack(null)
            .commit()*/
        //val intent = Intent()
        intent(OpeningListCashierActivity::class.java)
    }

    private fun singOut() {
        PopUp.ShowDialog(
            context!!,
            getString(R.string.message_title),
            getString(R.string.sure_log_out),
            PopUp.MSG_TYPE_WARNING,
            PopUp.BUTTON_TYPE_ACCEPT,
            {
                val loggerUserModel = LoggerUserModel()
                objectViewModel.deleteAll()
                sharedPreferencesLogin.editSharedPreferencesLogin(context!!, loggerUserModel)
                intent(LoginActivity::class.java)
                activity!!.finish()
            },
            null
        )
    }

    private fun intent(cls: Class<*>?) {
        val intent = Intent(activity, cls)
        activity!!.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivity(intent)
    }


}