package ndp.posapp.view.fragment.item

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import ndp.posapp.R
import ndp.posapp.data.db.entity.WhsKardexEntity
import ndp.posapp.databinding.FragmentItemStockBinding
import ndp.posapp.view.adapter.StockListAdapter

class ItemStockFragment : Fragment() {

    private var fragmentBinding  : FragmentItemStockBinding ? = null
    private val binding get() = fragmentBinding!!
    private lateinit var stockListAdapter : StockListAdapter
    private var whsKardexEntityList : List<WhsKardexEntity> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentBinding = FragmentItemStockBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initElements()
    }

    private fun initElements() {
        binding.rvItemStock.setHasFixedSize(true)
        stockListAdapter = StockListAdapter(activity!!)
        binding.rvItemStock.adapter = stockListAdapter
        binding.rvItemStock.layoutManager = LinearLayoutManager(activity!!)
        stockListAdapter.setList(whsKardexEntityList)
        if (whsKardexEntityList.isEmpty()){
            binding.layoutNoMatches.llNoMatches.visibility = View.VISIBLE
            binding.layoutNoMatches.ivNoMatches.setImageDrawable(ContextCompat.getDrawable(activity!!, R.drawable.ic_empty_items))
        }

    }


    fun setStock(whsKardexEntity: List<WhsKardexEntity>){
        this.whsKardexEntityList = whsKardexEntity
    }

}