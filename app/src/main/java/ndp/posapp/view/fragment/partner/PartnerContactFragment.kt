package ndp.posapp.view.fragment.partner

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.textfield.TextInputLayout
import ndp.posapp.data.model.ContactPersonModel
import ndp.posapp.databinding.FragmentPartnerContactBinding

class PartnerContactFragment : Fragment() {

    private lateinit var binding: FragmentPartnerContactBinding
    private var contactsPersons: List<ContactPersonModel> = ArrayList()

    fun setContactsPersons(contactsPersons: List<ContactPersonModel>) {
        this.contactsPersons = contactsPersons
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPartnerContactBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initElements()
    }

    private fun initElements() {
        initActions()
    }

    private fun initActions() {

    }

    override fun onResume() {
        super.onResume()
        if (contactsPersons.isNotEmpty()) {
            val contact = contactsPersons[0]
            binding.tiCargo.editText?.setText(contact.relationship)
            binding.tiName.editText?.setText(contact.name)
            binding.tiAddress.editText?.setText(contact.address)
            binding.tiCellPhone.editText?.setText(contact.cellPhoneNumber)
            binding.tiPhone.editText?.setText(contact.phone)
            binding.tiEmail.editText?.setText(contact.email)
        }
    }

    fun getContacts(): List<ContactPersonModel> {
        val contactPerson = if (contactsPersons.isNotEmpty()) contactsPersons[0] else ContactPersonModel()

        if (::binding.isInitialized) {
            contactPerson.relationship = getText(binding.tiCargo)
            contactPerson.name = getText(binding.tiName)
            contactPerson.address = getText(binding.tiAddress)
            contactPerson.cellPhoneNumber = getText(binding.tiCellPhone)
            contactPerson.phone = getText(binding.tiPhone)
            contactPerson.email = getText(binding.tiEmail)
        }

        if (contactsPersons.isEmpty()) {
            val contacts: ArrayList<ContactPersonModel> = ArrayList()
            contacts.add(contactPerson)
            return contacts
        }
        return contactsPersons
    }

    fun clean() {
        if (::binding.isInitialized) {
            binding.tiCargo.editText?.setText("")
            binding.tiName.editText?.setText("")
            binding.tiAddress.editText?.setText("")
            binding.tiCellPhone.editText?.setText("")
            binding.tiPhone.editText?.setText("")
            binding.tiEmail.editText?.setText("")
        }
    }

    private fun getText(textInputLayout: TextInputLayout): String? {
        val text = textInputLayout.editText?.text.toString()
        return if (text.isNotBlank()) text else null
    }
}