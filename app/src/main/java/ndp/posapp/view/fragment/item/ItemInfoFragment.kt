package ndp.posapp.view.fragment.item

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ndp.posapp.data.db.entity.ItemEntity
import ndp.posapp.databinding.FragmentItemInfoBinding

class ItemInfoFragment : Fragment() {

    private var fragmentBinding : FragmentItemInfoBinding ? = null
    private val binding get() = fragmentBinding!!

    private  var itemEntity = ItemEntity()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentBinding = FragmentItemInfoBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initElements()
    }

    private fun initElements() {
        binding.tvItemName.text = itemEntity.name
        binding.tvItemCode.text = itemEntity.code
        binding.tvUnitMeasure.text = itemEntity.salesUnit
    }

    fun setItem (itemEntity: ItemEntity) {
        this.itemEntity = itemEntity
    }

}