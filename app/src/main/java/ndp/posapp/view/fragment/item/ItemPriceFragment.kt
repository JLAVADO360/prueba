package ndp.posapp.view.fragment.item

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import ndp.posapp.R
import ndp.posapp.data.db.entity.PriceListDetailEntity
import ndp.posapp.databinding.FragmentItemPriceBinding


class ItemPriceFragment : Fragment() {
    private var fragmentBinding : FragmentItemPriceBinding ? =null
    private val binding get() = fragmentBinding!!



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentBinding = FragmentItemPriceBinding.inflate(inflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


    fun setPriceListDetail(priceListDetailEntity: List<PriceListDetailEntity>){

    }

}