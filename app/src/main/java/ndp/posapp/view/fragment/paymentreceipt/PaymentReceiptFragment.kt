package ndp.posapp.view.fragment.paymentreceipt

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Point
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import androidmads.library.qrgenearator.QRGContents
import androidmads.library.qrgenearator.QRGEncoder
import androidx.fragment.app.DialogFragment
import com.google.zxing.WriterException
import ndp.posapp.R
import ndp.posapp.data.model.EntityDTOModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.PaymentReceiptModel
import ndp.posapp.databinding.FragmentPaymentReceiptBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.activity.PaymentReceivedActivity
import ndp.posapp.view.adapter.PaymentWayAdapter
import ndp.posapp.view.adapter.ReceiptProductAdapter
import java.text.DecimalFormat

@SuppressLint("SetTextI18n")
class PaymentReceiptFragment(
    private val paymentReceipt: PaymentReceiptModel,
    private val loggerUser: LoggerUserModel
) : DialogFragment() {

    private lateinit var binding: FragmentPaymentReceiptBinding
    private lateinit var paymentReceivedActivity: PaymentReceivedActivity
    private lateinit var selectedEntity: EntityDTOModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PaymentReceivedActivity) {
            paymentReceivedActivity = context
        } else {
            dismiss()
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.paymentReceiptDialog)
        selectedEntity = loggerUser.entities.first { it.id == loggerUser.entity }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentPaymentReceiptBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initElements()
        initActions()
    }

    private fun initElements() {
        binding.tvWeb.text = "www.ladrillosmaxx.com" //loggerUserModel.web
        binding.tvCompanyName.text = selectedEntity.businessName
        binding.tvCompanyNumber.text = "RUC: ${selectedEntity.ruc}"
        binding.tvAddress.text = loggerUser.storeAddress

        binding.tvDocumentName.text = "${mapPaymentReceiptType(paymentReceipt.receiptType)} DE VENTA ELECTRONICA"
        binding.tvDocumentNumber.text = paymentReceipt.paymentReceiptNumber

        binding.tvDate.text = UseApp.formatDateString(paymentReceipt.createDate, Const.DATE_TIME_QUERY, Const.DATE_12)
        binding.tvCashier.text = paymentReceipt.payments.first().createUserName ?: "-"
        binding.tvSeller.text = paymentReceipt.createUserName ?: "-"
        binding.tvPartner.text = paymentReceipt.businessName
        binding.tvPartnerDocument.text = paymentReceipt.nif
        if (!paymentReceipt.shipAddress.isNullOrBlank()) {
            binding.tvShippingAddress.text = paymentReceipt.shipAddress
        } else {
            binding.tvShippingAddressTitle.visibility = View.GONE
        }
        if (!paymentReceipt.plateNumber.isNullOrBlank()) {
            binding.tvPlateNumber.text = paymentReceipt.plateNumber
        } else {
            binding.tvPlateNumberTitle.visibility = View.GONE
        }

        binding.rvItem.adapter = ReceiptProductAdapter(paymentReceipt.detail)

        binding.tvOpFree.text     = UseApp.doubleToString(paymentReceipt.bonus, 2)
        binding.tvSubTotal.text   = UseApp.doubleToString(paymentReceipt.subtotal, 2)
        binding.tvTax.text        = UseApp.doubleToString(paymentReceipt.taxAmount, 2)
        binding.tvICBPER.text     = UseApp.doubleToString(paymentReceipt.icbper, 2)
        binding.tvTotal.text      = UseApp.doubleToString(paymentReceipt.total, 2)
        binding.tvExonerated.text = UseApp.doubleToString(paymentReceipt.total, 2)

        binding.tvPaymentWay.text = paymentReceipt.paymentConditionName ?: "CONTADO"
        binding.tvTotalLetters.text = "${UseApp.numberToWords(paymentReceipt.total)} CON ${cents(paymentReceipt.total)} SOLES"

        if (paymentReceipt.payments.isNotEmpty() && paymentReceipt.payments.first().detail.isNotEmpty()) {
            binding.rvPayments.adapter = PaymentWayAdapter(paymentReceipt.payments.first().detail)
        }

        val typeDocument = if (paymentReceipt.nif?.length == 11) "06" else "01"
        val qr = "${selectedEntity.ruc}|${paymentReceipt.receiptType}|${paymentReceipt.paymentReceiptNumber}|${paymentReceipt.paymentReceiptNumber}|${paymentReceipt.taxAmount}|${paymentReceipt.total}|${paymentReceipt.createDate}|$typeDocument|${paymentReceipt.nif}|VALOR RESUMEN"
        generateQRCode(qr)

        binding.tvInfo.text = String.format(getString(R.string.payment_receipt_info), mapPaymentReceiptType(paymentReceipt.receiptType))

         if(paymentReceipt.comment!=""){binding.tvComment.text = paymentReceipt.comment}else{binding.tvComment.text="***"}
    }

    private fun initActions() {
        binding.ivBackToolbar.setOnClickListener { dismiss() }
        binding.ivShare.setOnClickListener { shareVoucher() }
    }

    private fun mapPaymentReceiptType(type: Any?): String {
        return when (type) {
            "01", "1", 1 -> "Factura"
            "03", "3", 3 -> "Boleta"
            "07", "7", 7 -> "Nota De Crédito"
            else -> "Sin Tipo"
        }
    }

    private fun cents(value: Double?): String? {
        if (value == null) return null
        return try {
            DecimalFormat("##0.00").format(value - value.toInt()).split('.')[1].toInt().toString() + "/100"
        } catch (e: Exception) {
            null
        }
    }

    private fun generateQRCode(text: String) {
        val manager = requireActivity().getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val display = manager.defaultDisplay
        val point = Point()
        display.getSize(point)

        val width: Int = point.x
        val height: Int = point.y

        var dimen = if (width < height) width else height
        dimen = dimen * 3 / 4

        val qrgEncoder = QRGEncoder(text, null, QRGContents.Type.TEXT, dimen)
        try {
            val bitmap = qrgEncoder.encodeAsBitmap()
            binding.qrCode.setImageBitmap(bitmap)
        } catch (e: WriterException) {}
    }

    private fun shareVoucher() {
        val view = binding.contentScreenShot
        val bitmap = Bitmap.createBitmap(view.width, view.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        val bgDrawable = view.background
        if (bgDrawable != null) {
            bgDrawable.draw(canvas)
        } else {
            canvas.drawColor(Color.WHITE)
        }
        view.draw(canvas)
        val bitmapPath = MediaStore.Images.Media.insertImage(requireContext().contentResolver, bitmap,"Comprobante", null);

        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(bitmapPath))
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.type = "image/png"
        startActivity(intent)
    }
}