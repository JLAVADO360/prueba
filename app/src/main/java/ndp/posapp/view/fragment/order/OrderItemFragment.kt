package ndp.posapp.view.fragment.order

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.*
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.R
import ndp.posapp.databinding.FragmentOrderItemBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.view.activity.ItemSaleActivity
import ndp.posapp.view.adapter.OrderItemAdapter
import ndp.posapp.viewModel.*
import com.google.android.material.snackbar.Snackbar
import ndp.posapp.data.db.entity.TaxEntity
import ndp.posapp.data.db.entityView.SaleItem
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.activity.OrderActivity

class OrderItemFragment(private val orderActivity: OrderActivity) : Fragment() {

    private var fragmentBinding: FragmentOrderItemBinding? = null
    private val binding get() = fragmentBinding!!

    /** VIEW MODEL **/
    private lateinit var itemViewModel: ItemViewModel
    private lateinit var priceListDetailViewModel: PriceListDetailViewModel
    private lateinit var whsKardexViewModel: WhsKardexViewModel
    private lateinit var kardexLocationViewModel: KardexViewModel
    private lateinit var parameterViewModel: ParameterViewModel
    private lateinit var taxViewModel: TaxViewModel

    /** TEMPORARY VARIABLES **/
    private lateinit var loggerUserModel: LoggerUserModel
    private lateinit var orderItemAdapter: OrderItemAdapter
    private var deleteAllButton: MenuItem? = null
    private var bagButtonMenu: MenuItem? = null
    private var addButtonMenu: MenuItem? = null
    private var bagItem: SaleItem? = null
    private var taxBagItem: SaleItem? = null
    private var taxList: MutableList<TaxEntity> = ArrayList()
    private var useTaxBag = false

    var isSavedSale = false
        set(value) {
            field = value
            setupButtonsMenu()
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        fragmentBinding = FragmentOrderItemBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initElements()
    }

    private fun initElements() {
        binding.rvItem.setHasFixedSize(true)
        orderItemAdapter = OrderItemAdapter(orderActivity)
        binding.rvItem.adapter = orderItemAdapter
        binding.rvItem.layoutManager = LinearLayoutManager(activity!!)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(requireContext())

        setHasOptionsMenu(true)
        initViewModel()
        initActions()
        initCall()
    }

    private fun initViewModel() {
        itemViewModel = ViewModelProvider(this).get(ItemViewModel::class.java)
        priceListDetailViewModel = ViewModelProvider(this).get(PriceListDetailViewModel::class.java)
        whsKardexViewModel = ViewModelProvider(this).get(WhsKardexViewModel::class.java)
        kardexLocationViewModel = ViewModelProvider(this).get(KardexViewModel::class.java)
        parameterViewModel = ViewModelProvider(this).get(ParameterViewModel::class.java)
        taxViewModel = ViewModelProvider(this).get(TaxViewModel::class.java)
    }

    private fun initCall() {
        parameterViewModel.get(loggerUserModel.entity,  Const.Parameter.USE_TAX_BAG).observe(viewLifecycleOwner) { param1 ->
            if (param1?.value?.toUpperCase() == "S") {
                val useTaxBagLiveData = MutableLiveData<Unit>().apply {
                    observe(viewLifecycleOwner){
                        if (bagItem != null && taxBagItem != null && taxList.isNotEmpty()) {
                            useTaxBag = true
                            setupButtonsMenu()
                        }
                    }
                }

                parameterViewModel.get(loggerUserModel.entity, Const.Parameter.BAG_NAME).observe(viewLifecycleOwner) { param ->
                    param.value?.let { value ->
                        itemViewModel.get(loggerUserModel.entity, orderActivity.loggerUserModel.warehouse!!, priceListDetailViewModel, whsKardexViewModel, kardexLocationViewModel, value).observe(viewLifecycleOwner) {
                            bagItem = it
                            orderItemAdapter.bagItem = bagItem
                            useTaxBagLiveData.value = Unit
                        }
                    }
                }

                parameterViewModel.get(loggerUserModel.entity, Const.Parameter.TAX_BAG_NAME).observe(viewLifecycleOwner) { param ->
                    param.value?.let { value ->
                        itemViewModel.get(loggerUserModel.entity, orderActivity.loggerUserModel.warehouse!!, priceListDetailViewModel, whsKardexViewModel, kardexLocationViewModel, value).observe(viewLifecycleOwner) {
                            taxBagItem = it
                            orderItemAdapter.taxBagItem = taxBagItem
                            useTaxBagLiveData.value = Unit
                        }
                    }
                }

                taxViewModel.list(loggerUserModel.entity).observe(viewLifecycleOwner) {
                    taxList.addAll(it)
                    useTaxBagLiveData.value = Unit
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_order_add, menu)
        addButtonMenu = menu.findItem(R.id.action_order_add)
        bagButtonMenu = menu.findItem(R.id.action_order_bag)
        deleteAllButton = menu.findItem(R.id.action_order_delete)
        setupButtonsMenu()
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_order_add -> {
                if (orderActivity.saleOrderModel.nif == null) {
                    PopUp.mdToast(context!!, activity!!.getString(R.string.select_business_partner), MDToast.TYPE_WARNING)
                } else {
                    itemSaleActivity()
                }
                true
            }
            R.id.action_order_bag -> {
                if (orderActivity.saleOrderModel.nif == null) {
                    PopUp.mdToast(context!!, activity!!.getString(R.string.select_business_partner), MDToast.TYPE_WARNING)
                } else if (bagItem == null || taxBagItem == null || taxList.isEmpty()) {
                    PopUp.mdToast(context!!, activity!!.getString(R.string.loading_try_again), MDToast.TYPE_WARNING)
                } else {
                    addBagItem()
                    orderActivity.checkDetraction()
                }
                true
            }
            R.id.action_order_delete -> {
                PopUp.ShowDialog(
                    activity!!,
                    getString(R.string.sure_delete_all_item),
                    getString(R.string.delete_items),
                    PopUp.MSG_TYPE_WARNING,
                    PopUp.BUTTON_TYPE_ACCEPT,
                    {
                        orderActivity.saleOrderModel.detail.removeAll { true }
                        orderItemAdapter.notifyDataSetChanged()

                        orderActivity.saleOrderModel.total = 0.0
                        orderActivity.saleOrderModel.subtotal = 0.0
                        orderActivity.saleOrderModel.taxAmount = 0.0
                        orderActivity.saleOrderModel.icbper = 0.0
                        orderActivity.notifyItemDataSetChanged()
                        orderActivity.checkDetraction()
                        toggleLayoutNoMatches()
                    },
                    { }
                )
                return true
            }
            else -> false
        }
    }

    private fun setupButtonsMenu() {
        addButtonMenu?.isVisible = !isSavedSale
        bagButtonMenu?.isVisible = (!isSavedSale && useTaxBag)
        deleteAllButton?.isVisible = (!isSavedSale && orderActivity.saleOrderModel.detail.isNotEmpty())
    }

    private fun itemSaleActivity() {
        activity!!.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivityForResult(
            ItemSaleActivity.newIntent(requireContext(), orderActivity.saleOrderModel.detail, orderActivity.partnerPriceList, orderActivity.saleOrderModel.reservationInvoice == Const.RESERVATION_INVOICE_YES),
            Const.INTENT_SALE_ORDER
        )
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            orderActivity.saleOrderModel.detail = data.getSerializableExtra(getString(R.string.sale_order)) as ArrayList<SaleOrderDetailModel>
            orderItemAdapter.notifyDataSetChanged()
            enableSwipe()

            orderActivity.saleOrderModel.total = 0.0
            orderActivity.saleOrderModel.subtotal = 0.0
            orderActivity.saleOrderModel.taxAmount = 0.0
            orderActivity.saleOrderModel.icbper = 0.0

            orderActivity.saleOrderModel.detail.forEach {
                orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! + it.total!!
                if (isTaxBagItem(it.itemCode)) {
                    orderActivity.saleOrderModel.icbper = it.taxAmount
                } else {
                    orderActivity.saleOrderModel.subtotal = orderActivity.saleOrderModel.subtotal!! + it.subtotal!!
                    orderActivity.saleOrderModel.taxAmount = orderActivity.saleOrderModel.taxAmount!! + it.taxAmount!!
                    orderActivity.saleOrderModel.subtotalDiscount = orderActivity.saleOrderModel.subtotal
                }
            }
            orderActivity.checkDetraction()
            toggleLayoutNoMatches()
            orderActivity.notifyItemDataSetChanged()
        }
    }

    private fun enableSwipe() {
        ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if (direction == ItemTouchHelper.LEFT) {
                    val position = viewHolder.adapterPosition
                    val deleteRow = orderActivity.saleOrderModel.detail[position]
                    var positionBag = -1
                    var deleteRowBag = SaleOrderDetailModel()
                    orderActivity.saleOrderModel.detail.remove(deleteRow)
                    orderItemAdapter.notifyItemRemoved(position)

                    orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! - deleteRow.total!!
                    if (isTaxBagItem(deleteRow.itemCode)) {
                        orderActivity.saleOrderModel.icbper = 0.0
                    } else {
                        orderActivity.saleOrderModel.subtotal = orderActivity.saleOrderModel.subtotal!! - deleteRow.subtotal!!
                        orderActivity.saleOrderModel.taxAmount = orderActivity.saleOrderModel.taxAmount!! - deleteRow.taxAmount!!
                        orderActivity.saleOrderModel.subtotalDiscount = orderActivity.saleOrderModel.subtotal
                    }

                    if (isBagItem(deleteRow.itemCode) || isTaxBagItem(deleteRow.itemCode)) {
                        run loop@{
                            orderActivity.saleOrderModel.detail.forEachIndexed { index, saleOrderDetailModel ->
                                if (isBagItem(saleOrderDetailModel.itemCode) || isTaxBagItem(saleOrderDetailModel.itemCode)) {
                                    positionBag = index
                                    deleteRowBag = saleOrderDetailModel
                                    orderActivity.saleOrderModel.detail.remove(deleteRowBag)
                                    orderItemAdapter.notifyItemRemoved(positionBag)

                                    orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! - deleteRowBag.total!!
                                    if (isTaxBagItem(deleteRowBag.itemCode)) {
                                        orderActivity.saleOrderModel.icbper = 0.0
                                    } else {
                                        orderActivity.saleOrderModel.subtotal = orderActivity.saleOrderModel.subtotal!! - deleteRowBag.subtotal!!
                                        orderActivity.saleOrderModel.taxAmount = orderActivity.saleOrderModel.taxAmount!! - deleteRowBag.taxAmount!!
                                        orderActivity.saleOrderModel.subtotalDiscount = orderActivity.saleOrderModel.subtotal
                                    }

                                    return@loop
                                }
                            }
                        }
                    }

                    orderActivity.checkDetraction()
                    toggleLayoutNoMatches()
                    orderActivity.notifyItemDataSetChanged()

                    Snackbar.make(binding.rlOrderFragmentContainer, getString(R.string.item_deleted), Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.undo)) {
                            if (isBagItem(deleteRow.itemCode) || isTaxBagItem(deleteRow.itemCode)) {
                                orderActivity.saleOrderModel.detail.add(positionBag, deleteRowBag)
                                orderItemAdapter.notifyItemInserted(positionBag)

                                orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! + deleteRowBag.total!!
                                if (isTaxBagItem(deleteRowBag.itemCode)) {
                                    orderActivity.saleOrderModel.icbper = deleteRowBag.taxAmount!!
                                } else {
                                    orderActivity.saleOrderModel.subtotal = orderActivity.saleOrderModel.subtotal!! + deleteRowBag.subtotal!!
                                    orderActivity.saleOrderModel.taxAmount = orderActivity.saleOrderModel.taxAmount!! + deleteRowBag.taxAmount!!
                                    orderActivity.saleOrderModel.subtotalDiscount = orderActivity.saleOrderModel.subtotal
                                }
                            }

                            orderActivity.saleOrderModel.detail.add(position, deleteRow)
                            orderItemAdapter.notifyItemInserted(position)

                            orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! + deleteRow.total!!
                            if (isTaxBagItem(deleteRow.itemCode)) {
                                orderActivity.saleOrderModel.icbper = deleteRow.taxAmount!!
                            } else {
                                orderActivity.saleOrderModel.subtotal = orderActivity.saleOrderModel.subtotal!! + deleteRow.subtotal!!
                                orderActivity.saleOrderModel.taxAmount = orderActivity.saleOrderModel.taxAmount!! + deleteRow.taxAmount!!
                                orderActivity.saleOrderModel.subtotalDiscount = orderActivity.saleOrderModel.subtotal
                            }

                            orderActivity.checkDetraction()
                            toggleLayoutNoMatches()
                            orderActivity.notifyItemDataSetChanged()
                        }.setActionTextColor(ContextCompat.getColor(activity!!, R.color.pantone152C))
                        .show()
                }
            }

            override fun onChildDraw(c: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
                val icon: Bitmap
                val p = Paint()
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
                    val itemView = viewHolder.itemView
                    val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                    val width = height / 3
                    if (dX < 0) {
                        p.color = ContextCompat.getColor(context!!, R.color.red)
                        val background = RectF(itemView.right.toFloat() + dX, itemView.top.toFloat(), itemView.right.toFloat(), itemView.bottom.toFloat())
                        c.drawRect(background, p)
                        icon = BitmapFactory.decodeResource(resources, R.drawable.ic_trash)
                        val iconDest = RectF(itemView.right.toFloat() - 2 * width, itemView.top.toFloat() + width, itemView.right.toFloat() - width, itemView.bottom.toFloat() - width)
                        c.drawBitmap(icon, null, iconDest, p)
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
            }
        }).attachToRecyclerView(binding.rvItem)
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initActions() {
        binding.layoutNoMatches.llNoMatches.visibility = View.VISIBLE
        binding.layoutNoMatches.tvMessage.text = activity!!.getString(R.string.no_item_added)
        binding.layoutNoMatches.ivNoMatches.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_empty_items))

        binding.ivDeleteAll.setOnClickListener {
            PopUp.ShowDialog(
                activity!!,
                getString(R.string.sure_delete_all_item),
                getString(R.string.delete_items),
                PopUp.MSG_TYPE_WARNING,
                PopUp.BUTTON_TYPE_ACCEPT,
                {
                    orderActivity.saleOrderModel.detail.removeAll { true }
                    orderItemAdapter.notifyDataSetChanged()

                    orderActivity.saleOrderModel.total = 0.0
                    orderActivity.saleOrderModel.subtotal = 0.0
                    orderActivity.saleOrderModel.taxAmount = 0.0
                    orderActivity.saleOrderModel.icbper = 0.0
                    orderActivity.notifyItemDataSetChanged()
                    orderActivity.checkDetraction()

                    toggleLayoutNoMatches()
                },
                { }
            )
        }
    }

    private fun toggleLayoutNoMatches() {
        if (orderActivity.saleOrderModel.detail.isEmpty()) {
            deleteAllButton?.isVisible = false
            binding.layoutNoMatches.llNoMatches.visibility=View.VISIBLE
            //binding.ivDeleteAll.visibility = View.GONE
        } else {

            deleteAllButton?.isVisible = true
            binding.layoutNoMatches.llNoMatches.visibility=View.GONE
            //binding.ivDeleteAll.visibility = View.VISIBLE
        }
    }

    private fun addBagItem() {
        var countBag = 0
        run loop@{
            orderActivity.saleOrderModel.detail.forEachIndexed { index, saleOrderDetail ->
                if (isBagItem(saleOrderDetail.itemCode)) {
                    countBag++
                    saleOrderDetail.quantity = saleOrderDetail.quantity!! + 1

                    orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! - saleOrderDetail.total!!
                    orderActivity.saleOrderModel.subtotal = orderActivity.saleOrderModel.subtotal!! - saleOrderDetail.subtotal!!
                    orderActivity.saleOrderModel.taxAmount = orderActivity.saleOrderModel.taxAmount!! - saleOrderDetail.taxAmount!!

                    saleOrderDetail.total = UseApp.roundDecimal(saleOrderDetail.grossPrice!! * saleOrderDetail.quantity!!, 2)
                    saleOrderDetail.subtotal = UseApp.roundDecimal(saleOrderDetail.unitPrice!! * saleOrderDetail.quantity!!, 2)
                    saleOrderDetail.taxAmount = UseApp.roundDecimal(saleOrderDetail.total!! - saleOrderDetail.subtotal!!, 2)
                    saleOrderDetail.subtotalWithDiscount = saleOrderDetail.subtotal

                    orderActivity.saleOrderModel.total = UseApp.roundDecimal(orderActivity.saleOrderModel.total!! + saleOrderDetail.total!!, 2)
                    orderActivity.saleOrderModel.subtotal = UseApp.roundDecimal(orderActivity.saleOrderModel.subtotal!! + saleOrderDetail.subtotal!!, 2)
                    orderActivity.saleOrderModel.taxAmount = UseApp.roundDecimal(orderActivity.saleOrderModel.taxAmount!! + saleOrderDetail.taxAmount!!, 2)
                    orderActivity.saleOrderModel.subtotalDiscount = orderActivity.saleOrderModel.subtotal

                    orderItemAdapter.notifyItemChanged(index)
                    if (countBag == 2) return@loop
                }

                if (isTaxBagItem(saleOrderDetail.itemCode)) {
                    countBag++
                    saleOrderDetail.quantity = saleOrderDetail.quantity!! + 1
                    orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! - saleOrderDetail.total!!

                    saleOrderDetail.taxAmount = UseApp.roundDecimal(saleOrderDetail.unitPrice!! * saleOrderDetail.quantity!! * (saleOrderDetail.tax!! / 100), 2)
                    saleOrderDetail.total = saleOrderDetail.taxAmount

                    orderActivity.saleOrderModel.total = UseApp.roundDecimal(orderActivity.saleOrderModel.total!! + saleOrderDetail.total!!, 2)
                    orderActivity.saleOrderModel.icbper = saleOrderDetail.taxAmount!!

                    orderItemAdapter.notifyItemChanged(index)
                    if (countBag == 2) return@loop
                }
            }
        }

        // Si aún no se agregó bolsa
        if (countBag == 0 && bagItem != null && taxBagItem != null) {
            var saleOrderDetail = getSaleOrderDetailForBag(bagItem!!)
            orderActivity.saleOrderModel.detail.add(saleOrderDetail)
            orderActivity.saleOrderModel.total = UseApp.roundDecimal(orderActivity.saleOrderModel.total!! + saleOrderDetail.total!!, 2)
            orderActivity.saleOrderModel.subtotal = UseApp.roundDecimal(orderActivity.saleOrderModel.subtotal!! + saleOrderDetail.subtotal!!, 2)
            orderActivity.saleOrderModel.taxAmount = UseApp.roundDecimal(orderActivity.saleOrderModel.taxAmount!! + saleOrderDetail.taxAmount!!, 2)
            orderActivity.saleOrderModel.subtotalDiscount = orderActivity.saleOrderModel.subtotal

            saleOrderDetail = getSaleOrderDetailForBag(taxBagItem!!)
            orderActivity.saleOrderModel.detail.add(saleOrderDetail)
            orderActivity.saleOrderModel.total = UseApp.roundDecimal(orderActivity.saleOrderModel.total!! + saleOrderDetail.total!!, 2)
            orderActivity.saleOrderModel.icbper = saleOrderDetail.taxAmount!!

            orderItemAdapter.notifyItemRangeInserted(orderActivity.saleOrderModel.detail.size -2, 2)
            enableSwipe()
        }
        orderActivity.notifyItemDataSetChanged()
        toggleLayoutNoMatches()
    }

    private fun getSaleOrderDetailForBag(saleItem: SaleItem): SaleOrderDetailModel {
        val item = saleItem.itemEntity
        val saleOrderDetail = SaleOrderDetailModel()

        val priceListDetail = saleItem.priceListDetailEntity.filter { it.priceList == orderActivity.partnerPriceList || it.priceList == loggerUserModel.storePriceList }
        val firstPriceListDetail = priceListDetail[0]
        var unitPrice = 0.0
        priceListDetail.forEach { unitPrice += it.unitPrice ?: 0.0 }
        unitPrice = UseApp.roundDecimal(unitPrice, 2)

        var stockDisponible = 0.0
        saleItem.kardex.forEach { kardex ->
            stockDisponible += kardex.stockDisponible
            kardex.kardexUbication.forEach {
                saleOrderDetail.kardexLocations.add(it)
            }
        }

        saleOrderDetail.kardex = saleItem.kardex
        saleOrderDetail.createDate = UseApp.getDate(Const.DATE_24)
        saleOrderDetail.createUser = loggerUserModel.userIdentifier
        saleOrderDetail.currency = firstPriceListDetail.currency
        saleOrderDetail.discount = 0.0
        saleOrderDetail.unitPrice = unitPrice
        saleOrderDetail.unitPriceReal = unitPrice
        saleOrderDetail.discountPercentage = 0.0
        saleOrderDetail.inventoryItem = Const.INVENTORY_ITEM_NO
        saleOrderDetail.itemCode = item.code
        saleOrderDetail.itemName = item.name
        saleOrderDetail.numberLine = orderActivity.saleOrderModel.detail.size
        saleOrderDetail.onlyTax = if (isTaxBagItem(saleItem.itemEntity.code)) Const.ONLY_TAX_YES else Const.ONLY_TAX_NO //cuando es producto o servicio se pone 0, cuando es impuesto a la bolsa y cuando se bonifica el producto se pone 1
        saleOrderDetail.orderSaleCode = null
        saleOrderDetail.quantity = 1.0
        saleOrderDetail.quantityInvoiced = 0.0
        saleOrderDetail.quantityPicking = 0.0
        saleOrderDetail.sample = 0 // TODO xq 0?
        saleOrderDetail.subtotal = if (isTaxBagItem(saleItem.itemEntity.code)) 0.0 else unitPrice * saleOrderDetail.quantity!!
        saleOrderDetail.subtotalWithDiscount = saleOrderDetail.subtotal
        saleOrderDetail.sunatAffectationType = if (isTaxBagItem(saleItem.itemEntity.code)) Const.SUNAT_AFFECTATION_TYPE_GRAVADO_IMPUESTO else Const.SUNAT_AFFECTATION_TYPE_GRAVADO_ONEROSO // 10 en duro, cuando se bonifica el producto se manda 15 y cuando es impuesto a la bolsa es 13
        saleOrderDetail.sunatOneroso = if (isTaxBagItem(saleItem.itemEntity.code)) Const.SUNAT_NO_ONEROSO else Const.SUNAT_ONEROSO // solo el impuesto a la bolsa es no oneroso
        saleOrderDetail.sunatOperation = Const.SUNAT_OPERATION
        saleOrderDetail.sunatOperationType = Const.SUNAT_OPERATION_TYPE
        saleOrderDetail.taxCode = item.taxCode ?: Const.TAX_CODE_DEFAULT
        saleOrderDetail.tax = taxList.first { it.code == saleOrderDetail.taxCode }.rate
        saleOrderDetail.taxAmount = UseApp.roundDecimal(unitPrice * (saleOrderDetail.tax!! / 100), 2)
        saleOrderDetail.total = UseApp.roundDecimal(saleOrderDetail.subtotal!! + saleOrderDetail.taxAmount!!, 2)
        saleOrderDetail.ubicationCode = if (saleOrderDetail.kardexLocations.isNotEmpty()) saleOrderDetail.kardexLocations[saleOrderDetail.locationIndex].ubication else loggerUserModel.ubication
        saleOrderDetail.ubicationEntry = if (saleOrderDetail.kardexLocations.isNotEmpty()) saleOrderDetail.kardexLocations[saleOrderDetail.locationIndex].ubicationEntry else loggerUserModel.ubicationEntry
        saleOrderDetail.unitGroupEntry = item.unitGroupEntry ?: Const.UNIT_GROUP_ENTRY_DEFAULT
        saleOrderDetail.unitMSRCode = firstPriceListDetail.unitMSR
        saleOrderDetail.unitMSREntry = firstPriceListDetail.unitMSREntry
        saleOrderDetail.unitMSRName = firstPriceListDetail.unitMSRName
        saleOrderDetail.grossPrice = if (isTaxBagItem(saleItem.itemEntity.code)) unitPrice else UseApp.roundDecimal(unitPrice * (1 + (saleOrderDetail.tax!! / 100)), 2)
        saleOrderDetail.warehouseCode =  if (saleItem.kardex.isNotEmpty()) saleItem.kardex.first().warehouse else null

        saleOrderDetail.stock = stockDisponible
        saleOrderDetail.type = item.type
        return saleOrderDetail
    }

    private fun isBagItem(itemCode: String?): Boolean {
        return itemCode == bagItem?.itemEntity?.code
    }

    private fun isTaxBagItem(itemCode: String?): Boolean {
        return itemCode == taxBagItem?.itemEntity?.code
    }
}