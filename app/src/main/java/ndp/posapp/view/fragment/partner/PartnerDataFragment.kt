package ndp.posapp.view.fragment.partner

import android.content.Context
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProvider
import com.valdesekamdem.library.mdtoast.MDToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ndp.posapp.R
import ndp.posapp.data.model.*
import ndp.posapp.databinding.FragmentPartnerDataBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.use.Mappers
import ndp.posapp.view.activity.RegisterPartnerActivity
import ndp.posapp.viewModel.BusinessPartnerViewModel

class PartnerDataFragment : Fragment() {

    private lateinit var binding: FragmentPartnerDataBinding

    private lateinit var loggerUserModel: LoggerUserModel
    private var currentDocumentType = ""
    private var currentBusinessPartner = BusinessPartnerModel()

    private var personType = Const.JURIDICAL_PERSON
    private lateinit var businessPartnerViewModel: BusinessPartnerViewModel

    private var registerPartnerActivity: RegisterPartnerActivity? = null

    fun getCurrentDocumentType() = currentDocumentType

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            if (context is RegisterPartnerActivity) {
                registerPartnerActivity = context
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPartnerDataBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initElements()
    }

    private fun initElements() {
        businessPartnerViewModel = ViewModelProvider(this).get(BusinessPartnerViewModel::class.java)
        binding.spPersonType.apply {
            adapter = ArrayAdapter(requireContext(), R.layout.spinner_item_dark, Mappers.getPersonType())
            currentDocumentType = Const.DNI
            setSelection(1)
        }
        binding.spDocumentType.adapter = ArrayAdapter(requireContext(), R.layout.spinner_item_dark, Mappers.getDocumentType())
        initActions()
    }

    private fun initActions() {

        binding.spPersonType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, position: Int, id: Long) {
                clean()
                when (position) {
                    0 -> {
                        binding.ivSearch.visibility = View.VISIBLE
                        elementsSetupNaturalPerson()
                        binding.spDocumentType.setSelection(0)
                        personType = Const.NATURAL_PERSON
                    }
                    1 -> {
                        binding.ivSearch.visibility = View.VISIBLE
                        elementsSetupJuridicalPerson()
                        currentDocumentType = Const.RUC
                        personType = Const.JURIDICAL_PERSON
                        configDocumentNumberInput(11)
                    }
                    2 -> {
                        binding.ivSearch.visibility = View.GONE
                        elementsSetupPersonWithoutDenomination()
                        personType = Const.WITHOUT_DENOMINATION
                    }
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }
        binding.spDocumentType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>?, view: View, position: Int, id: Long) {
                clean()
                when (position) {
                    0 -> {
                        binding.ivSearch.visibility = View.VISIBLE
                        currentDocumentType = Const.RUC
                        elementsSetupNaturalPerson()
                        configDocumentNumberInput(11)
                    }
                    1 -> {
                        binding.ivSearch.visibility = View.VISIBLE
                        currentDocumentType = Const.DNI
                        elementsSetupNaturalPersonWithoutRUC()
                        configDocumentNumberInput(8)
                    }
                    2 -> {
                        binding.ivSearch.visibility = View.GONE
                        currentDocumentType = Const.CARNET_EXTRANJERIA
                        elementsSetupNaturalPersonWithoutRUC()
                        configDocumentNumberInput(12)
                    }
                    else -> {
                        binding.ivSearch.visibility = View.GONE
                        currentDocumentType = ""
                        elementsSetupNaturalPersonWithoutRUC()
                        configDocumentNumberInput(12, 0, false)
                    }
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {}
        }

        binding.ivSearch.setOnClickListener {
            val documentNumber = binding.etDocumentNumber.text.toString()
            clean()
            binding.etDocumentNumber.setText(documentNumber)
            binding.etDocumentNumber.setSelection(documentNumber.length)
            initDocumentSearch(documentNumber)
        }
    }

    private fun initDocumentSearch(nif: String) {
        when (currentDocumentType) {
            Const.DNI -> {
                if (nif.length == 8) {
                    validateBusiness(nif)
                } else {
                    PopUp.mdToast(context!!, context!!.getString(R.string.enter_valid_document_number), MDToast.TYPE_ERROR)
                }
            }
            Const.RUC -> {
                if (nif.length == 11) {
                    validateBusiness(nif)
                } else {
                    PopUp.mdToast(context!!, context!!.getString(R.string.enter_valid_document_number), MDToast.TYPE_ERROR)
                }
            }
        }
    }

    private fun configDocumentNumberInput(inputMaxLength: Int, counterMaxLength: Int? = null, isInputTypeNumber: Boolean = true) {
        binding.tiDocumentNumber.counterMaxLength = counterMaxLength ?: inputMaxLength
        binding.etDocumentNumber.filters = arrayOf(InputFilter.LengthFilter(inputMaxLength))
        binding.etDocumentNumber.inputType = if (isInputTypeNumber) InputType.TYPE_CLASS_NUMBER else InputType.TYPE_TEXT_VARIATION_PERSON_NAME
    }

    private fun validateBusiness(nif: String) {
        PopUp.openProgressDialog(context!!, context!!.getString(R.string.operation_running), context!!.getString(R.string.checking_business))
        businessPartnerViewModel.getBusinessPartner(loggerUserModel, nif) {
            PopUp.closeProgressDialog()
            if (it.successful) {
                currentBusinessPartner = it.data!!.value!![0]
                completeBusinessPartner(currentBusinessPartner)
            } else {
                if (it.code == "ERROR_BUSINESSPARTNER_NOT_FOUND") {
                    consultDocument(nif)
                } else {
                    PopUp.showAlert(context!!, it.message)
                    currentBusinessPartner.approved = "0"
                }
            }
        }

    }

    private fun consultDocument(nif: String) {
        PopUp.openProgressDialog(context!!, context!!.getString(R.string.operation_running), context!!.getString(R.string.consulting_document))
        when (currentDocumentType) {
            Const.DNI -> {
                businessPartnerViewModel.consultDNI(loggerUserModel, nif) {
                    PopUp.closeProgressDialog()
                    if (it.data == null) {
                        currentBusinessPartner.approved = "0"
                        PopUp.showAlert(context!!, it.message)
                    } else {
                        currentBusinessPartner.nif = it.data!!["code"].asString
                        currentBusinessPartner.businessName = it.data!!["fullName"].asString
                        currentBusinessPartner.lastNameF = it.data!!["lastName"].asString
                        currentBusinessPartner.lastNameM = it.data!!["secondLastName"].asString
                        currentBusinessPartner.names = it.data!!["name"].asString
                        currentBusinessPartner.approved = "1"

                        val fiscalAddress = BusinessPartnerAddressModel()
                        fiscalAddress.addressName = "FISCAL"
                        fiscalAddress.address = "-"
                        fiscalAddress.country = "PE"
                        fiscalAddress.department = "LIMA"
                        fiscalAddress.province = "LIMA"
                        fiscalAddress.district = "LIMA"
                        fiscalAddress.type = "bo_BillTo"

                        val warehouseAddress = BusinessPartnerAddressModel()
                        warehouseAddress.addressName = "ALMACEN"
                        warehouseAddress.address = fiscalAddress.address
                        warehouseAddress.country = fiscalAddress.country
                        warehouseAddress.department = fiscalAddress.department
                        warehouseAddress.province = fiscalAddress.province
                        warehouseAddress.district = fiscalAddress.district
                        warehouseAddress.type = "bo_ShipTo"

                        val addresses = ArrayList<BusinessPartnerAddressModel>()
                        addresses.add(fiscalAddress)
                        addresses.add(warehouseAddress)
                        currentBusinessPartner.addresses = addresses
                        completeBusinessPartner(currentBusinessPartner)
                    }
                }
            }
            Const.RUC -> {
                businessPartnerViewModel.consultRUC(loggerUserModel, nif) {
                    PopUp.closeProgressDialog()
                    if (it.data == null) {
                        currentBusinessPartner.approved = "0"
                        PopUp.showAlert(context!!, it.message)
                    } else {
                        currentBusinessPartner.nif = it.data!!["code"].asString
                        currentBusinessPartner.businessName = it.data!!["name"].asString
                        currentBusinessPartner.approved = "1"

                        val fiscalAddress = BusinessPartnerAddressModel()
                        fiscalAddress.addressName = "FISCAL"
                        fiscalAddress.address = it.data!!["streetDescription"].asString

                        if (it.data!!["countryCode"].isJsonNull)    fiscalAddress.country    = null else fiscalAddress.country    = it.data!!["countryCode"].asString
                        if (it.data!!["departmentName"].isJsonNull) fiscalAddress.department = null else fiscalAddress.department = it.data!!["departmentName"].asString
                        if (it.data!!["provinceName"].isJsonNull)   fiscalAddress.province   = null else fiscalAddress.province   = it.data!!["provinceName"].asString
                        if (it.data!!["ubigeoName"].isJsonNull)     fiscalAddress.district   = null else fiscalAddress.district   = it.data!!["ubigeoName"].asString
                        if (it.data!!["ubigeoCode"].isJsonNull)     fiscalAddress.ubiquitous = null else fiscalAddress.ubiquitous = it.data!!["ubigeoCode"].asString
//                        fiscalAddress.country = it.data!!["countryCode"].asString
//                        fiscalAddress.department = it.data!!["departmentName"].asString
//                        fiscalAddress.province = it.data!!["provinceName"].asString
//                        fiscalAddress.district = it.data!!["ubigeoName"].asString
//                        fiscalAddress.ubiquitous = it.data!!["ubigeoCode"].asString
                        fiscalAddress.type = "bo_BillTo"

                        val warehouseAddress = BusinessPartnerAddressModel()
                        warehouseAddress.addressName = "ALMACEN"
                        warehouseAddress.address = fiscalAddress.address
                        warehouseAddress.country = fiscalAddress.country
                        warehouseAddress.department = fiscalAddress.department
                        warehouseAddress.province = fiscalAddress.province
                        warehouseAddress.district = fiscalAddress.district
                        warehouseAddress.ubiquitous = fiscalAddress.ubiquitous
                        warehouseAddress.type = "bo_ShipTo"

                        val addresses = ArrayList<BusinessPartnerAddressModel>()
                        addresses.add(fiscalAddress)
                        addresses.add(warehouseAddress)
                        currentBusinessPartner.addresses = addresses
                        completeBusinessPartner(currentBusinessPartner)
                    }
                }

            }
        }
    }

    private fun completeBusinessPartner(businessPartnerModel: BusinessPartnerModel) {
        GlobalScope.launch(Dispatchers.Main) {
            binding.etDocumentNumber.setText(businessPartnerModel.nif)
            binding.etSocialReason.setText(businessPartnerModel.businessName)
            binding.etLastNameP.setText(businessPartnerModel.lastNameF)
            binding.etLastNameM.setText(businessPartnerModel.lastNameM)
            binding.etNames.setText(businessPartnerModel.names)
            binding.etCellPhone.setText(businessPartnerModel.phoneNumber)
            binding.etEmail.setText(businessPartnerModel.email)

            registerPartnerActivity?.partnerAddressFragment?.setAddresses(businessPartnerModel.addresses)
            registerPartnerActivity?.partnerContactFragment?.setContactsPersons(businessPartnerModel.contactPersons)
        }
    }

    private fun elementsSetupJuridicalPerson() {
        binding.tiDocumentNumber.visibility = View.VISIBLE
        binding.spDocumentType.visibility = View.GONE
        binding.tiSocialReason.visibility = View.VISIBLE
        binding.personNameContainer.visibility = View.GONE
    }

    private fun elementsSetupNaturalPerson() {
        binding.spDocumentType.visibility = View.VISIBLE
        binding.tiDocumentNumber.visibility = View.VISIBLE
        binding.tiSocialReason.visibility = View.VISIBLE
        binding.personNameContainer.visibility = View.GONE
    }

    private fun elementsSetupNaturalPersonWithoutRUC() {
        binding.tiDocumentNumber.visibility = View.VISIBLE
        binding.tiSocialReason.visibility = View.GONE
        binding.personNameContainer.visibility = View.VISIBLE
    }

    private fun elementsSetupPersonWithoutDenomination() {
        binding.spDocumentType.visibility = View.GONE
        binding.tiDocumentNumber.visibility = View.GONE
        binding.tiSocialReason.visibility = View.VISIBLE
        binding.personNameContainer.visibility = View.GONE
    }

    fun setLoggerUser(loggerUserModel: LoggerUserModel) {
        this.loggerUserModel = loggerUserModel
    }


    private fun clean() {
        currentBusinessPartner = BusinessPartnerModel()
        binding.etDocumentNumber.setText("")
        binding.etSocialReason.setText("")
        binding.etLastNameP.setText("")
        binding.etLastNameM.setText("")
        binding.etNames.setText("")
        binding.etCellPhone.setText("")
        binding.etEmail.setText("")
        registerPartnerActivity?.partnerAddressFragment?.clean()
        registerPartnerActivity?.partnerContactFragment?.clean()
    }

    fun getBusiness(): BusinessPartnerModel {
        currentBusinessPartner.personType = currentBusinessPartner.personType ?: personType
        currentBusinessPartner.nif = if (currentBusinessPartner.personType == Const.WITHOUT_DENOMINATION) null else binding.etDocumentNumber.text.toString()
        currentBusinessPartner.code = "CL${currentBusinessPartner.nif}"
        currentBusinessPartner.businessName = binding.etSocialReason.text.toString()
        currentBusinessPartner.email = binding.etEmail.text.toString()
        currentBusinessPartner.phoneNumber = binding.etCellPhone.text.toString()
        currentBusinessPartner.migration = currentBusinessPartner.migration ?: "Y"
        currentBusinessPartner.partnerType = currentBusinessPartner.partnerType ?: "cCustomer"
        currentBusinessPartner.paymentCondition = currentBusinessPartner.paymentCondition ?: -1
        currentBusinessPartner.customerType = when (currentBusinessPartner.personType) {
            Const.NATURAL_PERSON->"TPN";Const.JURIDICAL_PERSON->"TPJ"; else->"SND"
        }
        currentBusinessPartner.priceList = currentBusinessPartner.priceList ?: Const.DEFAULT_PRICE_LIST_NEW_PARTNER
        currentBusinessPartner.currency = currentBusinessPartner.currency ?: "##"
        currentBusinessPartner.commercialName = currentBusinessPartner.commercialName ?: "-"
        currentBusinessPartner.approved = if (currentBusinessPartner.customerType == "SND") "0" else currentBusinessPartner.approved ?: "0"

        if (currentBusinessPartner.personType == Const.NATURAL_PERSON) {
            if (currentDocumentType == Const.RUC) {
                val personName = currentBusinessPartner.businessName!!.split(' ')
                currentBusinessPartner.lastNameF = if (personName.isNotEmpty()) personName[0] else null
                currentBusinessPartner.lastNameM = if (personName.size >= 2) personName[1] else null
                currentBusinessPartner.names = if (personName.size >= 3) personName[2] else null
            } else {
                currentBusinessPartner.lastNameF = binding.etLastNameP.text.toString()
                currentBusinessPartner.lastNameM = binding.etLastNameM.text.toString()
                currentBusinessPartner.names = binding.etNames.text.toString()
                currentBusinessPartner.businessName = "${currentBusinessPartner.lastNameF} ${currentBusinessPartner.lastNameM} ${currentBusinessPartner.names}"
            }
        }

        currentBusinessPartner.addresses = registerPartnerActivity!!.partnerAddressFragment.getAddresses()
        currentBusinessPartner.contactPersons = registerPartnerActivity!!.partnerContactFragment.getContacts()

        return currentBusinessPartner
    }

}