package ndp.posapp.view.adapter
import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.db.entity.KardexEntity
import ndp.posapp.databinding.ItemsSingleSelectBinding

class KardexLocationAdapter(
    private var itemList: List<KardexEntity>,
    private val onLocationSelected: (position: Int) -> Unit
) : RecyclerView.Adapter<KardexLocationAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.items_single_select, parent, false)
        )
    }

    override fun getItemCount(): Int = itemList.size

    @SuppressLint("NotifyDataSetChanged")
    fun setList(itemList: List<KardexEntity>) {
        this.itemList = itemList
        this.notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.textItem.text = itemList[position].ubication
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemsSingleSelectBinding.bind(view)

        init {
            itemView.setOnClickListener { onLocationSelected(adapterPosition) }
        }
    }
}
