package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.db.entityView.ItemView
import ndp.posapp.databinding.ItemListItemBinding
import kotlin.collections.ArrayList

class ItemAdapter internal constructor(activity: Activity, private val listener: Listener) : RecyclerView.Adapter<ItemAdapter.ViewHolder>(), Filterable {

    private val inflater: LayoutInflater = LayoutInflater.from(activity)

    private var itemViewList: List<ItemView> = ArrayList()
    private var itemViewFilteredList: List<ItemView> = ArrayList()

    interface Listener {
        fun onSelectedItem(itemView: ItemView)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_list_item, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemView = itemViewList[position]
        val item = itemView.itemEntity
        holder.binding.tvItemName.text = item.name
        holder.binding.tvItemCode.text = "COD SAP: ${item.code}"
        holder.binding.tvUnitMeasure.text = "UNIDAD: ${item.salesUnit}"
        holder.binding.llItem.setOnClickListener {
            listener.onSelectedItem(itemView)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(itemViewList: List<ItemView>) {
        this.itemViewList = itemViewList
        this.itemViewFilteredList = itemViewList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return itemViewList.size
    }


    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val charString: String = constraint.toString().lowercase()
                itemViewList = if (charString.trim().isEmpty()) {
                    itemViewFilteredList
                } else {
                    val filteredList: ArrayList<ItemView> = ArrayList()

                    for (rowFilter in itemViewFilteredList) {
                        if (rowFilter.itemEntity.name!!.lowercase().contains(charString) ||
                            rowFilter.itemEntity.code.lowercase().contains(charString)
                        ) {
                            filteredList.add(rowFilter)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = itemViewList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                itemViewList = results.values as List<ItemView>
                notifyDataSetChanged()
            }
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemListItemBinding.bind(itemView)
    }


}