package ndp.posapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.ClosingListCashierModel

class ClosingListCashierAdapter (val ols:List<ClosingListCashierModel> , val caja:String):RecyclerView.Adapter<ClosingListCashierAdapter.CLCHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CLCHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return CLCHolder(layoutInflater.inflate(R.layout.opening_list_cashier, parent, false))
    }

    override fun onBindViewHolder(holder: CLCHolder, position: Int) {
        holder.pintar(ols[position])
        holder.CashierCodeTV.text = caja
    }

    override fun getItemCount(): Int = ols.size

    class CLCHolder(var view: View): RecyclerView.ViewHolder(view)
    {
        var CashierCodeTV: TextView = view.findViewById(R.id.tvCashierCode)

        fun pintar(ol: ClosingListCashierModel)
        {
            //var CashierCodeTV: TextView = view.findViewById(R.id.tvCashierCode)
            var CashierNameTV: TextView = view.findViewById(R.id.tvCashierName)
            var DateHourTV: TextView = view.findViewById(R.id.tvDateHour)
            var TurnTV: TextView = view.findViewById(R.id.tvTurn)

            CashierNameTV.setText(ol.createUserName)
            DateHourTV.setText(ol.closureDate)
            TurnTV.setText(ol.turn.toString())
        }
    }
}