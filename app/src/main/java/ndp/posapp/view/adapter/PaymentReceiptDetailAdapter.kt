package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.PaymentReceiptDetailModel
import ndp.posapp.databinding.ItemDetailDocumentBinding
import ndp.posapp.utils.use.Mappers
import ndp.posapp.utils.use.UseApp

class PaymentReceiptDetailAdapter internal constructor(activity: Activity) : RecyclerView.Adapter<PaymentReceiptDetailAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var listSaleOrderReciptModel: List<PaymentReceiptDetailModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_detail_document, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val paymentReceiptDetailModel = listSaleOrderReciptModel[position]
        holder.binding.tvItemName.text = paymentReceiptDetailModel.itemName
        holder.binding.tvItemCode.text = paymentReceiptDetailModel.itemCode
        holder.binding.tvSubTotal.text = "${Mappers.mapCurrencyName(paymentReceiptDetailModel.currency)} ${UseApp.doubleToString(paymentReceiptDetailModel.subtotal, 2)}"
        holder.binding.tvQuantity.text = UseApp.doubleToString(paymentReceiptDetailModel.quantity, 2)
        holder.binding.tvUnit.text = paymentReceiptDetailModel.unitMSRName
        holder.binding.tvTax.text = UseApp.doubleToString(paymentReceiptDetailModel.taxAmount, 2)
        holder.binding.tvTotal.text = "${Mappers.mapCurrencyName(paymentReceiptDetailModel.currency)} ${UseApp.doubleToString(paymentReceiptDetailModel.total, 2)}"
    }

    override fun getItemCount(): Int {
        return listSaleOrderReciptModel.size
    }

    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(listSaleOrderDetailModel: List<PaymentReceiptDetailModel>) {
        this.listSaleOrderReciptModel = listSaleOrderDetailModel
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemDetailDocumentBinding.bind(itemView)
    }
}