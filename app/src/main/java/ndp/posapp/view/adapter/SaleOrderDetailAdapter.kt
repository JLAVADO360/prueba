package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.databinding.ItemDetailDocumentBinding
import ndp.posapp.utils.use.Mappers
import ndp.posapp.utils.use.UseApp

class SaleOrderDetailAdapter internal   constructor(activity: Activity) : RecyclerView.Adapter<SaleOrderDetailAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var listSaleOrderDetailModel: List<SaleOrderDetailModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_detail_document, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val saleOrderDetailModel = listSaleOrderDetailModel[position]
        holder.binding.tvItemName.text = saleOrderDetailModel.itemName
        holder.binding.tvItemCode.text = saleOrderDetailModel.itemCode
        holder.binding.tvSubTotal.text = "${Mappers.mapCurrencyName(saleOrderDetailModel.currency)} ${UseApp.doubleToString(saleOrderDetailModel.subtotal, 2)}"
        holder.binding.tvQuantity.text = UseApp.doubleToString(saleOrderDetailModel.quantity, 2)
        holder.binding.tvUnit.text = saleOrderDetailModel.unitMSRName
        holder.binding.tvTax.text = UseApp.doubleToString(saleOrderDetailModel.taxAmount, 2)
        holder.binding.tvTotal.text = "${Mappers.mapCurrencyName(saleOrderDetailModel.currency)} ${UseApp.doubleToString(saleOrderDetailModel.total, 2)}"
    }

    override fun getItemCount(): Int {
        return listSaleOrderDetailModel.size
    }
    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(listSaleOrderDetailModel: List<SaleOrderDetailModel>){
        this.listSaleOrderDetailModel = listSaleOrderDetailModel
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemDetailDocumentBinding.bind(itemView)
    }


}