package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.db.entityView.SaleItem
import ndp.posapp.databinding.ItemSaleItemBinding
import java.util.*
import kotlin.collections.ArrayList

class ItemSaleAdapter internal constructor(private val reservationInvoice: Boolean, private val listener: Listener ) :
    RecyclerView.Adapter<ItemSaleAdapter.ViewHolder>(), Filterable {

    private var saleItemList: MutableList<SaleItem> = ArrayList()
    private var saleItemFilteredList: MutableList<SaleItem> = ArrayList()

    interface Listener {
        fun onItemSelected(saleItem: SaleItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sale_item, parent, false))
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemView = saleItemList[position]
        val item = itemView.itemEntity
        var stockDisponible = 0.0
        saleItemList[position].kardex.forEach { stockDisponible += it.stockDisponible }
        holder.binding.tvItemName.text = item.name
        holder.binding.tvItemCode.text = item.code
        if (stockDisponible <= 0) {
            holder.binding.tvStockAvailable.setTextColor(Color.RED)
            if (reservationInvoice) {
                holder.binding.ivAdd.isEnabled = true
                holder.binding.ivAdd.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.tahiti_Gold))
            } else {
                holder.binding.ivAdd.isEnabled = false
                holder.binding.ivAdd.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.light_gray))
            }
        } else {
            holder.binding.tvStockAvailable.setTextColor(Color.BLACK)
            holder.binding.ivAdd.isEnabled = true
            holder.binding.ivAdd.setColorFilter(ContextCompat.getColor(holder.itemView.context, R.color.tahiti_Gold))
        }
        holder.binding.tvStockAvailable.text = stockDisponible.toString()
        holder.binding.ivAdd.setOnClickListener {
            saleItemList.removeAt(position)
            notifyDataSetChanged()
            listener.onItemSelected(itemView)
        }
    }


    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(saleItemList: MutableList<SaleItem>) {
        this.saleItemList = saleItemList
        this.saleItemFilteredList = saleItemList
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return saleItemList.size
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val charString: String = constraint.toString().lowercase(Locale.getDefault())
                saleItemList = if (charString.trim().isEmpty()) {
                    saleItemFilteredList
                } else {
                    val filteredList: ArrayList<SaleItem> = ArrayList()

                    for (rowFilter in saleItemFilteredList) {
                        if (rowFilter.itemEntity.name!!.lowercase().contains(charString) ||
                            rowFilter.itemEntity.code.lowercase().contains(charString)
                        ) {
                            filteredList.add(rowFilter)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = saleItemList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                saleItemList = results.values as MutableList<SaleItem>
                notifyDataSetChanged()
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemSaleItemBinding.bind(itemView)
    }
}