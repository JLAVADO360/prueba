package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.databinding.ItemBusinessPartnerBinding
import java.util.*
import kotlin.collections.ArrayList

class BusinessPartnerAdapter internal constructor(activity: Activity, private val listener: Listener) : RecyclerView.Adapter<BusinessPartnerAdapter.ViewHolder>(), Filterable {


    private val inflater: LayoutInflater = LayoutInflater.from(activity)

    private var businessPartnerViewList: List<BusinessPartnerView> = ArrayList()
    private var businessPartnerViewFilteredList: List<BusinessPartnerView> = ArrayList()

    interface Listener {
        fun onSelectBusinessPartner(businessPartnerView: BusinessPartnerView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_business_partner, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val businessPartnerView = businessPartnerViewList[position]
        val businessPartner = businessPartnerView.businessPartnerEntity
        holder.binding.tvSocialReason.text = businessPartner.businessName
        holder.binding.tvCode.text = businessPartner.code
        holder.binding.tvDocumentNumber.text = businessPartner.nif
        holder.binding.llBusinessPartner.setOnClickListener {
            listener.onSelectBusinessPartner(businessPartnerView)
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(businessPartnerViewList: List<BusinessPartnerView>) {
        this.businessPartnerViewList = businessPartnerViewList
        this.businessPartnerViewFilteredList = businessPartnerViewList
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return businessPartnerViewList.size
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence): FilterResults {
                val charString: String = constraint.toString().lowercase(Locale.getDefault())
                businessPartnerViewList = if (charString.trim().isEmpty()) {
                    businessPartnerViewFilteredList
                } else {
                    val filteredList: ArrayList<BusinessPartnerView> = ArrayList()

                    for (rowFilter in businessPartnerViewFilteredList) {
                        if (rowFilter.businessPartnerEntity.businessName!!.lowercase(Locale.getDefault()).contains(charString) ||
                            rowFilter.businessPartnerEntity.code.contains(charString)
                        ) {
                            filteredList.add(rowFilter)
                        }
                    }
                    filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = businessPartnerViewList
                return filterResults
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun publishResults(constraint: CharSequence, results: FilterResults) {
                businessPartnerViewList = results.values as List<BusinessPartnerView>
                notifyDataSetChanged()
            }
        }
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemBusinessPartnerBinding.bind(itemView)
    }


}
