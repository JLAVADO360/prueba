package ndp.posapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.OpeningListCashierModel

class OpeningListCashierAdapter (val ols:List<OpeningListCashierModel>):RecyclerView.Adapter<OpeningListCashierAdapter.OLCHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OLCHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return OLCHolder(layoutInflater.inflate(R.layout.opening_list_cashier, parent, false))
    }

    override fun onBindViewHolder(holder: OLCHolder, position: Int) {
        holder.pintar(ols[position])
    }

    override fun getItemCount(): Int = ols.size

    class OLCHolder(var view:View):RecyclerView.ViewHolder(view)
    {
        fun pintar(ol:OpeningListCashierModel)
        {
            var CashierCodeTV:TextView = view.findViewById(R.id.tvCashierCode)
            var CashierNameTV:TextView = view.findViewById(R.id.tvCashierName)
            var DateHourTV:TextView = view.findViewById(R.id.tvDateHour)
            var TurnTV:TextView = view.findViewById(R.id.tvTurn)
            CashierCodeTV.setText(ol.cashRegister)
            CashierNameTV.setText(ol.createUserName)
            DateHourTV.setText(ol.openingCashRegisterDate)
            TurnTV.setText(ol.turn.toString())
        }
    }
}