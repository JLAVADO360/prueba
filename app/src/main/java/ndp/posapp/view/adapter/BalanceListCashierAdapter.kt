package ndp.posapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.BalanceListCashierModel
import ndp.posapp.data.model.ClosingListCashierModel

class BalanceListCashierAdapter (val ols:List<BalanceListCashierModel>): RecyclerView.Adapter<BalanceListCashierAdapter.BLCHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BLCHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return BLCHolder(layoutInflater.inflate(R.layout.opening_list_cashier, parent, false))
    }

    override fun onBindViewHolder(holder: BLCHolder, position: Int) {
        holder.pintar(ols[position])
    }

    override fun getItemCount(): Int = ols.size

    class BLCHolder(var view: View): RecyclerView.ViewHolder(view)
    {
        fun pintar(ol: BalanceListCashierModel)
        {
            var CashierCodeTV: TextView = view.findViewById(R.id.tvCashierCode)
            var CashierNameTV: TextView = view.findViewById(R.id.tvCashierName)
            var DateHourTV: TextView = view.findViewById(R.id.tvDateHour)
            var TurnTV: TextView = view.findViewById(R.id.tvTurn)
            CashierCodeTV.setText(ol.cashRegister)
            CashierNameTV.setText(ol.createUserName)
            DateHourTV.setText(ol.balanceCashRegisterDate)
            TurnTV.setText(ol.turn.toString())
        }
    }
}