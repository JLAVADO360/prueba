package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.SaleOrderModel
import ndp.posapp.databinding.ItemQueryOrderBinding
import ndp.posapp.utils.use.Mappers
import ndp.posapp.utils.use.UseApp

class QueryOrderAdapter internal constructor(activity: Activity, private val listener: Listener) : RecyclerView.Adapter<QueryOrderAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var listSaleOrderModel: List<SaleOrderModel> = ArrayList()

    interface Listener {
        fun onSelectSaleOrder(saleOrderModel: SaleOrderModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_query_order, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val saleOrderModel = listSaleOrderModel[position]
        holder.binding.tvBusinessPartner.text = saleOrderModel.businessPartnerName
        holder.binding.tvBusinessPartnerCode.text = saleOrderModel.businessPartnerCode
        holder.binding.tvDocumentNumber.text = "DocNum NDP: ${saleOrderModel.docNum} | DocNum SAP: ${saleOrderModel.erpSerialNumber ?: "-"}"
        holder.binding.tvCreateDate.text = "Fec Creación: ${UseApp.localDateFromServer(saleOrderModel.createDate)}"
        holder.binding.tvNameSalePerson.text = "Vendedor: ${saleOrderModel.nameSalePerson}"
        holder.binding.tvStatus.text = "Estado: ${Mappers.mapOrderStatus(saleOrderModel.status)}"
        holder.binding.tvSaleType.text = "Tipo Venta: ${Mappers.mapSaleType(saleOrderModel.saleType)}"
        holder.binding.tvTotal.text = "Total Documento: ${Mappers.mapCurrencyName(saleOrderModel.docCurrency)} ${UseApp.doubleToString(saleOrderModel.total, 2)}"
        holder.binding.container.setOnClickListener {
            listener.onSelectSaleOrder(saleOrderModel)
        }
    }

    override fun getItemCount(): Int {
        return listSaleOrderModel.size
    }

    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(saleOrderModelList: List<SaleOrderModel>) {
        this.listSaleOrderModel = saleOrderModelList
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemQueryOrderBinding.bind(itemView)
    }


}