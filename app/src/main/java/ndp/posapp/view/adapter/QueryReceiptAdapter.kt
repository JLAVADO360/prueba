package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.PaymentReceiptDetailModel
import ndp.posapp.data.model.PaymentReceiptModel
import ndp.posapp.databinding.ItemQueryReceiptBinding
import ndp.posapp.utils.use.Mappers
import ndp.posapp.utils.use.UseApp

class QueryReceiptAdapter internal constructor(activity: Activity, private val listener: Listener) : RecyclerView.Adapter<QueryReceiptAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var paymentReceiptModelList: List<PaymentReceiptModel> = ArrayList()

    internal interface Listener {
        fun onSelectReceipt(paymentReceiptModel: PaymentReceiptModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_query_receipt, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val paymentReceiptModel = paymentReceiptModelList[position]
        holder.binding.tvBusinessPartner.text = paymentReceiptModel.businessName
        holder.binding.tvBusinessPartnerCode.text = paymentReceiptModel.businessPartner
        holder.binding.tvLegalSeries.text = "${Mappers.mapReceiptTypeName(paymentReceiptModel.receiptType)} ${paymentReceiptModel.paymentReceiptNumber}"
        holder.binding.tvDocumentNumber.text = "DocNum NDP: ${paymentReceiptModel.docNum} | DocNum SAP: ${paymentReceiptModel.erpSerialNumber ?: "-"}"
        holder.binding.tvCreateDate.text = "Fec Creación: ${UseApp.localDateFromServer(paymentReceiptModel.createDate)}"
        holder.binding.tvNameSalePerson.text = "Vendedor: ${paymentReceiptModel.createUserName}"
        holder.binding.tvStatus.text = "Estado: ${Mappers.mapOrderStatus(paymentReceiptModel.status)}"
        holder.binding.tvSaleType.text = "Tipo Venta: ${Mappers.mapSaleType(paymentReceiptModel.saleType)}"
        holder.binding.tvTotal.text = "Total Documento: ${Mappers.mapCurrencyName(paymentReceiptModel.currency)} ${UseApp.doubleToString(paymentReceiptModel.total, 2)}"
        holder.binding.container.setOnClickListener {
            listener.onSelectReceipt(paymentReceiptModel)
        }
    }

    override fun getItemCount(): Int {
        return paymentReceiptModelList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(receiptModelList: List<PaymentReceiptModel>) {
        this.paymentReceiptModelList = receiptModelList
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemQueryReceiptBinding.bind(itemView)
    }


}