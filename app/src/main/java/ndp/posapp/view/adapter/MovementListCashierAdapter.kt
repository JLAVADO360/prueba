package ndp.posapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.BalanceListCashierModel
import ndp.posapp.data.model.MovementListCashierModel

class MovementListCashierAdapter (val ols:List<MovementListCashierModel>): RecyclerView.Adapter<MovementListCashierAdapter.MLCHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MLCHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return MLCHolder(layoutInflater.inflate(R.layout.movement_list_cashier_item, parent, false))
    }

    override fun onBindViewHolder(holder: MLCHolder, position: Int) {
        holder.pintar(ols[position])
    }

    override fun getItemCount(): Int = ols.size

    class MLCHolder(var view: View): RecyclerView.ViewHolder(view)
    {
        fun pintar(ol: MovementListCashierModel)
        {
            var DateHourMovTV: TextView = view.findViewById(R.id.tvDateHourMov)
            var DescriptionMovTV: TextView = view.findViewById(R.id.tvDescriptionMov)
            var CashierCodeMovTV: TextView = view.findViewById(R.id.tvCashierCodeMov)
            var CorrelativeMovTV: TextView = view.findViewById(R.id.tvCorrelativeMov)
            var PaymentMethodMovTV: TextView = view.findViewById(R.id.tvPaymentMethodMov)
            var UserNameMovTV: TextView = view.findViewById(R.id.tvUserNameMov)
            var DirectionMovTV: TextView = view.findViewById(R.id.tvDirectionMov)
            var AmountMovTV: TextView = view.findViewById(R.id.tvAmountMov)
            var CurrencyMovTV: TextView = view.findViewById(R.id.tvCurrencyMov)
            DateHourMovTV.setText(ol.movementDate)
            DescriptionMovTV.setText(ol.description)
            CashierCodeMovTV.setText(ol.cashRegister)
            CorrelativeMovTV.setText(ol.docReference)
            PaymentMethodMovTV.setText(ol.payWay + "-" + ol.payMethod)
            UserNameMovTV.setText(ol.createUserName)
            DirectionMovTV.setText(ol.movementType)
            AmountMovTV.setText(ol.amount.toString())
            CurrencyMovTV.setText(ol.currency)

        }
    }
}