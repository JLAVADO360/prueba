package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.PaymentReceiptDetailModel
import ndp.posapp.databinding.ItemReceiptProductBinding
import ndp.posapp.utils.use.UseApp

@SuppressLint("SetTextI18n")
class ReceiptProductAdapter(private var itemList: List<PaymentReceiptDetailModel>) : RecyclerView.Adapter<ReceiptProductAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_receipt_product, parent, false)
        )
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val saleOrderDetail = itemList[position]

        holder.binding.tvItemName.text = "${saleOrderDetail.itemCode} - ${saleOrderDetail.itemName}"
        holder.binding.tvQuantity.text = "${saleOrderDetail.quantity}(${saleOrderDetail.unitMSRCode})"
        holder.binding.tvPb.text = UseApp.doubleToString(saleOrderDetail.grossPrice, 2)
        holder.binding.tvTotal.text = UseApp.doubleToString(saleOrderDetail.total, 2)
    }

    @SuppressLint("NotifyDataSetChanged")
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemReceiptProductBinding.bind(view)
    }
}
