package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.db.entity.WhsKardexEntity
import ndp.posapp.databinding.ItemListStockBinding

class StockListAdapter internal constructor(activity: Activity) : RecyclerView.Adapter<StockListAdapter.ViewHolder>(){

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var whsKardexEntityList : List<WhsKardexEntity> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_list_stock,parent,false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val whsKardexEntity = whsKardexEntityList[position]
        holder.binding.tvWarehouseName.text = whsKardexEntity.warehouse
        holder.binding.tvStock.text = "STOCK: ${whsKardexEntity.stock}"
        holder.binding.tvStockCommitted.text = "COMPROMETIDO: ${whsKardexEntity.stockCommitted}"
        holder.binding.tvStockAvailable.text = "DISPONIBLE: ${whsKardexEntity.stockDisponible}"
    }

    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(whsKardexEntityList : List<WhsKardexEntity>){
        this.whsKardexEntityList = whsKardexEntityList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return whsKardexEntityList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemListStockBinding.bind(itemView)
    }
}