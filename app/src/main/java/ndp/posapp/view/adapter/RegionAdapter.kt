package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.db.entity.RegionEntity
import ndp.posapp.databinding.ItemsSingleSelectBinding


class RegionAdapter(
    private val onRegionSelected: (regionSelected: RegionEntity) -> Unit
) : RecyclerView.Adapter<RegionAdapter.ViewHolder>() {

    private var regionList: List<RegionEntity> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.items_single_select, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(regionList[position])
    }

    override fun getItemCount(): Int {
        return regionList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(regionList: List<RegionEntity>) {
        this.regionList = regionList
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemsSingleSelectBinding.bind(itemView)

        fun bind(region: RegionEntity) {
            binding.textItem.text = region.region!!.uppercase()
            itemView.setOnClickListener {
                onRegionSelected(region)
            }
        }
    }

}