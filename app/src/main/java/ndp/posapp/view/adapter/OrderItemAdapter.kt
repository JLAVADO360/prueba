package ndp.posapp.view.adapter

import android.graphics.Color
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.db.entityView.SaleItem
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.databinding.DialogItemSelectBinding
import ndp.posapp.databinding.ItemSaleOrderItemBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.use.UseApp
import ndp.posapp.view.activity.OrderActivity

class OrderItemAdapter(private val orderActivity: OrderActivity) : RecyclerView.Adapter<OrderItemAdapter.ViewHolder>() {

    var bagItem: SaleItem? = null
    var taxBagItem: SaleItem? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_sale_order_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = orderActivity.saleOrderModel.detail.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val saleOrderDetailModel = orderActivity.saleOrderModel.detail[position]
        holder.removeTextChangedListener()
        holder.binding.tvItemName.text = saleOrderDetailModel.itemName
        holder.binding.tvItemCode.text = saleOrderDetailModel.itemCode
        holder.binding.tvStockAvailable.text = saleOrderDetailModel.stock.toString()
        holder.binding.tvStockAvailable.setTextColor(if (saleOrderDetailModel.stock!! <= 0) Color.RED else Color.BLACK)
        holder.binding.tvBasePrice.setText(UseApp.doubleToString(saleOrderDetailModel.grossPrice, 2))
        holder.binding.tvSubTotal.text = UseApp.doubleToString(saleOrderDetailModel.subtotal, 2)
        holder.binding.tvQuantity.setText(UseApp.doubleToString(saleOrderDetailModel.quantity, 0))
        holder.binding.tvTax.text = UseApp.doubleToString(saleOrderDetailModel.taxAmount, 2)
        holder.binding.tvTotal.text = UseApp.doubleToString(saleOrderDetailModel.total, 2)
        holder.binding.spUnit.adapter = ArrayAdapter(holder.itemView.context, R.layout.spinner_item_dark, listOf(saleOrderDetailModel.unitMSRCode))
        if (saleOrderDetailModel.kardexLocations.isNotEmpty()) {
            holder.binding.tvLocation.text = saleOrderDetailModel.kardexLocations[saleOrderDetailModel.locationIndex].ubication
            holder.binding.tvStockLocation.text = saleOrderDetailModel.kardexLocations[saleOrderDetailModel.locationIndex].stock.toString()
            holder.binding.tvLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_drop_down, 0)
        } else {
            holder.binding.tvLocation.text = holder.itemView.context.getString(R.string.not_inventored)
            holder.binding.tvStockLocation.text = "0"
            holder.binding.tvLocation.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        }
        if (holder.isTaxBagItem(saleOrderDetailModel.itemCode)) {
            holder.binding.tvBasePrice.isEnabled = false
            holder.binding.tvQuantity.isEnabled = false
        } else {
            holder.binding.tvBasePrice.isEnabled = true
            holder.binding.tvQuantity.isEnabled = saleOrderDetailModel.type != Const.ITEM_TYPE_SERVICE
        }
        holder.addTextChangedListener()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemSaleOrderItemBinding.bind(itemView)

        private val priceTextWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                removeTextChangedListener()
                val saleOrderDetailModel = orderActivity.saleOrderModel.detail[adapterPosition]
                saleOrderDetailModel.grossPrice = UseApp.stringToDouble(s.toString(), 2)
                saleOrderDetailModel.unitPrice = saleOrderDetailModel.grossPrice!! / (1 + saleOrderDetailModel.tax!! / 100)
                calculateNewValues(saleOrderDetailModel)

                val newPrice = when {
                    s.contains('.') -> {
                        when (s.split('.')[1].length) {
                            0 -> UseApp.doubleToString(saleOrderDetailModel.grossPrice, 0) + "."
                            1 -> UseApp.doubleToString(saleOrderDetailModel.grossPrice, 1)
                            else -> UseApp.doubleToString(saleOrderDetailModel.grossPrice, 2)
                        }
                    }
                    s.isNotEmpty() -> UseApp.doubleToString(saleOrderDetailModel.grossPrice, 0)
                    else -> ""
                }

                binding.tvBasePrice.setText(newPrice)
                binding.tvBasePrice.setSelection(if ((start + count) > newPrice.length) newPrice.length else (start + count))
                orderActivity.checkDetraction()
                addTextChangedListener()
            }

            override fun afterTextChanged(s: Editable) {}
        }

        private val quantityWatcher = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                removeTextChangedListener()
                var quantity = UseApp.stringToDouble(s.toString(), 0)
                val saleOrderDetailModel = orderActivity.saleOrderModel.detail[adapterPosition]

                // VALIDAR STOCK
                if (orderActivity.saleOrderModel.reservationInvoice == Const.RESERVATION_INVOICE_NO && saleOrderDetailModel.inventoryItem == Const.INVENTORY_ITEM_YES) {
                    if (saleOrderDetailModel.kardexLocations[saleOrderDetailModel.locationIndex].stock < quantity) {
                        quantity = saleOrderDetailModel.kardexLocations[saleOrderDetailModel.locationIndex].stock
                    }
                }

                saleOrderDetailModel.quantity = quantity
                calculateNewValues(saleOrderDetailModel)

                if (isBagItem(saleOrderDetailModel.itemCode)) {
                    run loop@{
                        orderActivity.saleOrderModel.detail.forEachIndexed { index, taxBagItemDetail ->
                            if (isTaxBagItem(taxBagItemDetail.itemCode)) {
                                taxBagItemDetail.quantity = saleOrderDetailModel.quantity
                                orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! - taxBagItemDetail.total!!

                                taxBagItemDetail.taxAmount = UseApp.roundDecimal(taxBagItemDetail.unitPrice!! * taxBagItemDetail.quantity!! * (taxBagItemDetail.tax!! / 100), 2)
                                taxBagItemDetail.total = taxBagItemDetail.taxAmount

                                orderActivity.saleOrderModel.total = UseApp.roundDecimal(orderActivity.saleOrderModel.total!! + taxBagItemDetail.total!!, 2)
                                orderActivity.saleOrderModel.icbper = taxBagItemDetail.taxAmount!!

                                notifyItemChanged(index)
                                orderActivity.notifyItemDataSetChanged()
                                return@loop
                            }
                        }
                    }
                }


                val newQuantity = UseApp.doubleToString(quantity, 0)
                binding.tvQuantity.setText(newQuantity)
                binding.tvQuantity.setSelection(if ((start + count) > newQuantity.length) newQuantity.length else (start + count))
                orderActivity.checkDetraction()
                addTextChangedListener()
            }

            override fun afterTextChanged(s: Editable) {}
        }

        init {
            binding.tvLocation.setOnClickListener {
                if (orderActivity.saleOrderModel.detail[adapterPosition].kardexLocations.isNotEmpty()) {
                    lateinit var dialog: AlertDialog
                    val bindingDialog = DialogItemSelectBinding.inflate(LayoutInflater.from(itemView.context))
                    val alertDialog = AlertDialog.Builder(itemView.context)
                    alertDialog.setView(bindingDialog.root)

                    val adapter = KardexLocationAdapter(orderActivity.saleOrderModel.detail[adapterPosition].kardexLocations) { index ->
                        val saleOrderDetail = orderActivity.saleOrderModel.detail[adapterPosition]
                        saleOrderDetail.locationIndex = index
                        saleOrderDetail.ubicationCode = saleOrderDetail.kardexLocations[index].ubication
                        saleOrderDetail.ubicationEntry = saleOrderDetail.kardexLocations[index].ubicationEntry
                        orderActivity.saleOrderModel.detail[adapterPosition] = saleOrderDetail

                        binding.tvLocation.text = saleOrderDetail.kardexLocations[index].ubication
                        binding.tvStockLocation.text = saleOrderDetail.kardexLocations[index].stock.toString()
                        dialog.dismiss()
                    }

                    bindingDialog.rvItems.adapter = adapter
                    bindingDialog.tvTitleDialog.setText(R.string.select_location)

                    dialog = alertDialog.create()
                    dialog.show()
                }
            }
        }

        private fun calculateNewValues(saleOrderDetailModel: SaleOrderDetailModel) {
            orderActivity.saleOrderModel.total = orderActivity.saleOrderModel.total!! - saleOrderDetailModel.total!!
            orderActivity.saleOrderModel.subtotal = orderActivity.saleOrderModel.subtotal!! - saleOrderDetailModel.subtotal!!
            orderActivity.saleOrderModel.taxAmount = orderActivity.saleOrderModel.taxAmount!! - saleOrderDetailModel.taxAmount!!

            saleOrderDetailModel.total = UseApp.roundDecimal(saleOrderDetailModel.grossPrice!! * saleOrderDetailModel.quantity!!, 2)
            saleOrderDetailModel.subtotal = UseApp.roundDecimal(saleOrderDetailModel.unitPrice!! * saleOrderDetailModel.quantity!!, 2)
            saleOrderDetailModel.taxAmount = UseApp.roundDecimal(saleOrderDetailModel.total!! - saleOrderDetailModel.subtotal!!, 2)
            saleOrderDetailModel.subtotalWithDiscount = saleOrderDetailModel.subtotal

            orderActivity.saleOrderModel.total = UseApp.roundDecimal(orderActivity.saleOrderModel.total!! + saleOrderDetailModel.total!!, 2)
            orderActivity.saleOrderModel.subtotal = UseApp.roundDecimal(orderActivity.saleOrderModel.subtotal!! + saleOrderDetailModel.subtotal!!, 2)
            orderActivity.saleOrderModel.taxAmount = UseApp.roundDecimal(orderActivity.saleOrderModel.taxAmount!! + saleOrderDetailModel.taxAmount!!, 2)
            orderActivity.saleOrderModel.subtotalDiscount = orderActivity.saleOrderModel.subtotal
            orderActivity.saleOrderModel.detail[adapterPosition] = saleOrderDetailModel
            orderActivity.notifyItemDataSetChanged()

            binding.tvSubTotal.text = UseApp.doubleToString(saleOrderDetailModel.subtotal, 2)
            binding.tvTax.text = UseApp.doubleToString(saleOrderDetailModel.taxAmount, 2)
            binding.tvTotal.text = UseApp.doubleToString(saleOrderDetailModel.total, 2)
        }

        fun addTextChangedListener() {
            binding.tvBasePrice.addTextChangedListener(priceTextWatcher)
            binding.tvQuantity.addTextChangedListener(quantityWatcher)
        }

        fun removeTextChangedListener() {
            binding.tvBasePrice.removeTextChangedListener(priceTextWatcher)
            binding.tvQuantity.removeTextChangedListener(quantityWatcher)
        }

        fun isBagItem(itemCode: String?): Boolean {
            return itemCode == bagItem?.itemEntity?.code
        }

        fun isTaxBagItem(itemCode: String?): Boolean {
            return itemCode == taxBagItem?.itemEntity?.code
        }
    }

}