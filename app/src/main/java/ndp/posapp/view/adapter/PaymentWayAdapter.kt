package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.PaymentDetailModel
import ndp.posapp.databinding.ItemPaymentWayBinding
import ndp.posapp.utils.use.UseApp

class PaymentWayAdapter(private var itemList: List<PaymentDetailModel>) : RecyclerView.Adapter<PaymentWayAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_payment_way, parent, false)
        )
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val paymentDetail = itemList[position]
        val received = moneyValue(paymentDetail.receivedAmountNationalCurrency, paymentDetail.currency)
        val charged = moneyValue(paymentDetail.totalReceived, paymentDetail.currency)
        val change = moneyValue(paymentDetail.totalChange, paymentDetail.currency)

        holder.binding.tvPaymentWayName.text = paymentDetail.payWayName
        holder.binding.tvPaymentDetail.text = String.format(holder.binding.root.context.getString(R.string.payment_way_detail), received, charged, change)
    }

    @SuppressLint("NotifyDataSetChanged")
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemPaymentWayBinding.bind(view)
    }

    private fun moneyValue(amount: Double?, currency: String?) = "${UseApp.doubleToString(amount, 2)} $currency"
}
