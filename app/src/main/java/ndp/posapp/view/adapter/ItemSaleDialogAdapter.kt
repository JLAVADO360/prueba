package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.databinding.ItemSaleItemDialogBinding
import kotlin.collections.ArrayList

class ItemSaleDialogAdapter internal constructor(activity: Activity, private val listener: Listener) :
    RecyclerView.Adapter<ItemSaleDialogAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(activity)
    private var saleOrderDetailModelList: MutableList<SaleOrderDetailModel> = ArrayList()

    interface Listener {
        fun onDeleteSelected()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.item_sale_item_dialog, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val saleOrderDetail = saleOrderDetailModelList[position]
        holder.binding.tvItemName.text = saleOrderDetail.itemName
        holder.binding.tvItemCode.text = saleOrderDetail.itemCode
        holder.binding.ivDelete.setOnClickListener {
            saleOrderDetailModelList.removeAt(position)
            listener.onDeleteSelected()
            notifyDataSetChanged()
        }

    }


    @SuppressLint("NotifyDataSetChanged")
    internal fun setList(saleOrderDetailModelList: MutableList<SaleOrderDetailModel>) {
        this.saleOrderDetailModelList = saleOrderDetailModelList
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int {
        return saleOrderDetailModelList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemSaleItemDialogBinding.bind(itemView)
    }
}