package ndp.posapp.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.EntityDTOModel
import ndp.posapp.databinding.ItemsSingleSelectBinding

class CompanyAdapter(
    private var itemList: List<EntityDTOModel>,
    private val companySelected: String,
    private val onCompanySelected: (company: EntityDTOModel) -> Unit
) : RecyclerView.Adapter<CompanyAdapter.ViewHolder>() {

    var positionSelected = -1;

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        itemList.forEachIndexed { i, company -> if (company.id == companySelected) positionSelected = i}
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.items_single_select, parent, false)
        )
    }

    override fun getItemCount(): Int = itemList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.textItem.text = itemList[position].businessName
        holder.binding.radiobutton.isChecked = (positionSelected == position)
    }

    @SuppressLint("NotifyDataSetChanged")
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemsSingleSelectBinding.bind(view)

        init {
            itemView.setOnClickListener {
                positionSelected = adapterPosition
                onCompanySelected(itemList[adapterPosition])
                notifyDataSetChanged()
            }
        }
    }
}
