package ndp.posapp.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ndp.posapp.R
import ndp.posapp.data.model.PaymentDetailModel
import ndp.posapp.data.model.PaymentModel
import ndp.posapp.databinding.ItemListPaymentBinding
import ndp.posapp.utils.use.UseApp

class PaymentReceivedAdapter(private val paymentModel: PaymentModel) : RecyclerView.Adapter<PaymentReceivedAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_payment, parent, false)
        )
    }

    override fun getItemCount(): Int = paymentModel.detail.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(paymentModel.detail[position])
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val binding = ItemListPaymentBinding.bind(view)

        fun bind(paymentDetail: PaymentDetailModel) {
            binding.tvPaymentWay.text = paymentDetail.accountDescription
            binding.tvCashier.text = paymentModel.createUserName
            if (paymentModel.createDate != null) {
                binding.tvDate.text = UseApp.localDateFromServer(paymentModel.createDate)
                binding.tvDate.visibility = View.VISIBLE
                binding.tvDateTitle.visibility = View.VISIBLE
            } else {
                binding.tvDate.text = null
                binding.tvDate.visibility = View.GONE
                binding.tvDateTitle.visibility = View.GONE
            }
            binding.tvExchangeRate.text = paymentDetail.rateValue.toString()
            binding.tvReceived.text = moneyValue(paymentDetail.receivedAmountNationalCurrency, paymentDetail.currency)
            binding.tvCharged.text = moneyValue(paymentDetail.totalReceived, paymentDetail.currency)
            binding.tvChange.text = moneyValue(paymentDetail.totalChange, paymentDetail.currency)
            binding.tvMN.text = moneyValue(paymentDetail.receivedAmountNationalCurrency, paymentDetail.currency)
            binding.tvME.text = moneyValue(paymentDetail.receivedAmountForeignCurrency, paymentDetail.currency)
            binding.tvMV.text = moneyValue(paymentDetail.saleAmount, paymentDetail.currency)
            binding.tvMR.text = moneyValue(paymentDetail.remainingAmount, paymentDetail.currency)
        }

        private fun moneyValue(amount: Double?, currency: String?) = "${UseApp.doubleToString(amount, 2)} $currency"
    }
}
