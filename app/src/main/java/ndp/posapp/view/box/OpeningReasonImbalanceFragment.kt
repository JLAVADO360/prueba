package ndp.posapp.view.box

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import ndp.posapp.ClosingCashRegisterActivity
import ndp.posapp.R
import ndp.posapp.databinding.FragmentReasonImbalanceBinding
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.viewModel.ReasonImbalanceViewModel

import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import ndp.posapp.OpeningCashRegisterActivity
import ndp.posapp.data.model.*
import ndp.posapp.utils.interaction.*


class OpeningReasonImbalanceFragment: DialogFragment() {
    private lateinit var binding: FragmentReasonImbalanceBinding
    private lateinit var loggerUser: LoggerUserModel
    private var sharedPreferencesLogin = SharedPreferencesLogin()
    private lateinit var openingCashRegisterActivity: OpeningCashRegisterActivity

    private lateinit var reasonImbalanceViewModel: ReasonImbalanceViewModel

    private var reasonImbalanceObjectModel = ReasonImbalanceObjectModel()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        openingCashRegisterActivity = context as OpeningCashRegisterActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.SelectCompanyDialog)
        loggerUser = sharedPreferencesLogin.getSharedPreferences(requireContext())
        reasonImbalanceObjectModel = arguments?.getParcelable<ReasonImbalanceObjectModel>("objectImbalance")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentReasonImbalanceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context
        initElements()

        binding.btRIAceptar.setOnClickListener(){
            saveData()
            dismiss()
        }

        binding.btRICancelar.setOnClickListener(){
            dismiss()
        }
    }

    private fun initElements(){
        reasonImbalanceViewModel = ViewModelProvider(this).get(ReasonImbalanceViewModel::class.java)
        binding.etRIAmount.setText(reasonImbalanceObjectModel.amountImbalance.toString())
        initSpinner()
        initPreferences()
    }

    private fun initSpinner(){
        var listSpinner = ArrayList<String>()
        for (valor in reasonImbalanceObjectModel.listReasonImbalance!!){
            listSpinner.add(valor.reason!!)
        }
        loadSpinner(listSpinner,binding.spRI,context)
    }

    private fun initPreferences(){
        val dataPref = context?.getSharedPreferences("reason_imbalance_open", AppCompatActivity.MODE_PRIVATE);
        val g = Gson()
        val json = dataPref?.getString(reasonImbalanceObjectModel.method ,"no hay objeto")
        val type = object : TypeToken<ReasonImbalanceSaveModel>() {}.type
        var objectImbalance = ReasonImbalanceSaveModel()
        try {
            objectImbalance = g.fromJson<ReasonImbalanceSaveModel>(json,type)
            binding.spRI.setSelection(objectImbalance.position!!)
            binding.etRIComment.setText(""+objectImbalance.imbalanceComment)
        }catch (e : JsonParseException){
            Log.e("errorJson:",""+e)
        }
    }

    private fun saveData(){
        val dataPref = context?.getSharedPreferences("reason_imbalance_open", AppCompatActivity.MODE_PRIVATE);
        val gson = Gson()
        var reasonImbalanceModel = getDataSpinner(binding.spRI.selectedItem.toString())
        val data = gson.toJson(ReasonImbalanceSaveModel(reasonImbalanceModel.reason,binding.etRIComment.text.toString(),reasonImbalanceModel.accountNumber,binding.spRI.selectedItemPosition))
        with(dataPref!!.edit()){
            putString(reasonImbalanceObjectModel.method,data)
            apply()
        }

    }

    private fun getDataSpinner(reason:String):ReasonImbalanceModel{
        var reasonImbalanceModel= ReasonImbalanceModel()
        for (valor in reasonImbalanceObjectModel.listReasonImbalance!!){
            if(reason == valor.reason){
               reasonImbalanceModel = ReasonImbalanceModel(valor.code,valor.reason,valor.accountNumber,valor.status)
            }
        }

        return  reasonImbalanceModel
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        val windowParams = window?.attributes
//        windowParams?.dimAmount = 0.0f
//        window?.attributes = windowParams
    }




}