package ndp.posapp.view.box

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import ndp.posapp.R
import ndp.posapp.databinding.FragmentMovementExternalBinding
import ndp.posapp.utils.shared.SharedPreferencesLogin

import ndp.posapp.MovementListCashierActivity
import ndp.posapp.data.model.*
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.use.UsePhone
import ndp.posapp.viewModel.CurrencyDataViewModel
import ndp.posapp.viewModel.MovementCashRegisterViewModel
import xdroid.toaster.Toaster
import ndp.posapp.utils.Constants


class MovementExternalFragment: DialogFragment() {
    private lateinit var binding: FragmentMovementExternalBinding
    private lateinit var loggerUser: LoggerUserModel
    var codeBox = ""
    var nameBox = ""
    private var sharedPreferencesLogin = SharedPreferencesLogin()
   // private lateinit var openingCashRegisterActivity: OpeningCashRegisterActivity

    private lateinit var movementCashRegisterViewModel: MovementCashRegisterViewModel
    private lateinit var currencyDataViewModel: CurrencyDataViewModel
    var currencyModel:MutableList<CurrencyModel>?= null


    override fun onAttach(context: Context) {
        super.onAttach(context)
       // openingCashRegisterActivity = context as OpeningCashRegisterActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.SelectCompanyDialog)
        loggerUser = sharedPreferencesLogin.getSharedPreferences(requireContext())
        codeBox = arguments?.getString("codeBox","Codigo")!!
        nameBox = arguments?.getString("nameBox","Caja")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentMovementExternalBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context
        initElements()
        binding.ETMCRCash.setText("$codeBox - $nameBox")
        interactionCheckBox(binding.CBMCRMethod1,binding.ETMCRMethod1)
        interactionCheckBox(binding.CBMCRMethod2,binding.ETMCRMethod2)
        interactionCheckBox(binding.CBMCRMethod3,binding.ETMCRMethod3)

        binding.btMCRAceptar.setOnClickListener(){
            saveData()
        }

        binding.btMCRCancelar.setOnClickListener(){
            dismiss()
        }
        loadComponetsService()
    }

    private fun initElements(){
        movementCashRegisterViewModel = ViewModelProvider(this).get(MovementCashRegisterViewModel::class.java)
        currencyDataViewModel         = ViewModelProvider(this).get(CurrencyDataViewModel::class.java)
//        binding.etRIAmount.setText(reasonImbalanceObjectModel.amountImbalance.toString())
//        initSpinner()
//        initPreferences()
    }

    private fun saveData(){
        if (UsePhone.isOnline()) {
            if(validate()){
              PopUp.openProgressDialog(context as MovementListCashierActivity, getString(R.string.operation_running), "Enviando Cierre de Caja...")
               getData()
            }
        }else {
            Toaster.toast("Error en la conexion a internet")
        }
    }

    private fun getData(){
            var movementTransferIOModel = MovementTransferIOModel()
            movementTransferIOModel.company         = ""
            movementTransferIOModel.createUser      = loggerUser.userIdentifier
            movementTransferIOModel.createUserEmail = loggerUser.email
            movementTransferIOModel.createUserName  = loggerUser.name
            movementTransferIOModel.status          = "PROCESSED"
            movementTransferIOModel.store           = loggerUser.store
            movementTransferIOModel.type            = getEstateTranfer()
            movementTransferIOModel.vault           = codeBox
            movementTransferIOModel.operationNumber = binding.ETMCROperation.text.toString()
            movementTransferIOModel.docNumERP       = binding.ETMCRDocument.text.toString()
            movementTransferIOModel.detail          = getDataDetail()
            movementCashRegisterViewModel.movementCashRegister(loggerUser, movementTransferIOModel) {
                PopUp.closeProgressDialog()
                if (it.successful) {
                    Toaster.toast("Se envio correctamente!!!")
                    dismiss()
                }else{
                    Toaster.toast("No se pudo enviar,intente en un momento")
                    dismiss()
                }
            }
    }

    private fun prueba(){
        var movementTransferIOModel = MovementTransferIOModel()
        movementTransferIOModel.company         = ""
        movementTransferIOModel.createUser      = loggerUser.userIdentifier
        movementTransferIOModel.createUserEmail = loggerUser.email
        movementTransferIOModel.createUserName  = loggerUser.name
        movementTransferIOModel.status          = "PROCESSED"
        movementTransferIOModel.store           = loggerUser.store
        movementTransferIOModel.type            = getEstateTranfer()
        movementTransferIOModel.vault           = codeBox
        movementTransferIOModel.operationNumber = binding.ETMCROperation.text.toString()
        movementTransferIOModel.docNumERP       = binding.ETMCRDocument.text.toString()
        movementTransferIOModel.detail          = getDataDetail()

        Log.e("dato.company",""+movementTransferIOModel.company)
        Log.e("dato.createUser",""+movementTransferIOModel.createUser)
        Log.e("dato.createUserEmail",""+movementTransferIOModel.createUserEmail)
        Log.e("dato.createUserName",""+movementTransferIOModel.createUserName)
        Log.e("dato.status",""+movementTransferIOModel.status)
        Log.e("dato.store",""+movementTransferIOModel.store)
        Log.e("dato.type",""+movementTransferIOModel.type)
        Log.e("dato.vault",""+movementTransferIOModel.vault)
        Log.e("dato.operationNumber",""+movementTransferIOModel.operationNumber)
        Log.e("dato.docNumERP",""+movementTransferIOModel.docNumERP)
        for (valor in movementTransferIOModel.detail!!){
            Log.e("dato.amount",""+valor.amount)
            Log.e("dato.currency",""+valor.currency)
            Log.e("dato.name",""+valor.name)
            Log.e("dato.numberLine",""+valor.numberLine)
        }

    }



    private fun getDataDetail():MutableList<MovementTransferIODetailModel>{
        var listDetail: MutableList<MovementTransferIODetailModel> = mutableListOf()
        for (method in currencyModel!!){
            var detail:MovementTransferIODetailModel = MovementTransferIODetailModel()
            when(method.code){
                Constants.PAY_METHOD_1->{
                    if(binding.CBMCRMethod1.isChecked){
                        detail= getObjectDetail(binding.ETMCRMethod1,method.code,method.name,0)
                        listDetail.add(detail)
                    }
                }
                Constants.PAY_METHOD_2->{
                    if(binding.CBMCRMethod2.isChecked){
                        detail= getObjectDetail(binding.ETMCRMethod2,method.code,method.name,1)
                        listDetail.add(detail)
                    }
                }
                Constants.PAY_METHOD_3->{
                    if(binding.CBMCRMethod3.isChecked) {
                        detail= getObjectDetail(binding.ETMCRMethod3,method.code,method.name,2)
                        listDetail.add(detail)
                    }
                }
                else->{
                   // detail= getObjectDetail(binding.ETMCRMethod1,"SOL","Soles",0)
                   // listDetail.add(detail)
                }
            }
            //listDetail.add(detail)
        }
        return listDetail
    }

    private fun getObjectDetail(etAmount:EditText, currency:String, name:String, numberLine:Int):MovementTransferIODetailModel{
        var detail = MovementTransferIODetailModel()
        detail.amount     = etAmount.text.toString().toDouble()
        detail.currency   = currency
        detail.name       = name
        detail.numberLine = numberLine
        return detail
    }

    private fun loadComponetsService(){
        currencyDataViewModel.getCurrencyData(loggerUser)
        currencyDataViewModel.result.observe(viewLifecycleOwner,{
            if (it.successful){
                it.data?.let {
                        objeto -> currencyModel = objeto.value
                        for (method in objeto.value!!){
                             when(method.code){
                                 Constants.PAY_METHOD_1->{
                                     initComponentsMethods(binding.LYMCRAPayMethod1)
                                 }
                                 Constants.PAY_METHOD_2->{
                                     initComponentsMethods(binding.LYMCRAPayMethod2)
                                 }
                                 Constants.PAY_METHOD_3->{
                                     initComponentsMethods(binding.LYMCRAPayMethod3)
                                 }
                                 else->{
                                     initComponentsMethods(binding.LYMCRAPayMethod1)
                                 }
                             }
                        }
                }
            }else{
                Toaster.toast("Ocurrio un problema!!")
            }
        })
    }

    private fun initComponentsMethods(ly: LinearLayout){
        ly.visibility = View.VISIBLE
    }

    private fun validate():Boolean{
       var rgTransfer = binding.RGMCR.indexOfChild(binding.RGMCR.findViewById(binding.RGMCR.checkedRadioButtonId))
        if(rgTransfer==-1){
            Toaster.toast("Debe Seleccionar una opcion (Entrada/Salida)")
            return false
        }
        if (binding.ETMCROperation.text.toString()==""){
            Toaster.toast("Debe ingresar #Operacíón")
            return false
        }

        if (binding.CBMCRMethod1.isChecked && binding.ETMCRMethod1.text.toString()==""){
            Toaster.toast("Debe ingresar un monto en SOLES")
            return false
        }
        if (binding.CBMCRMethod2.isChecked && binding.ETMCRMethod2.text.toString()==""){
            Toaster.toast("Debe ingresar un monto en EUROS")
            return false
        }
        if (binding.CBMCRMethod3.isChecked && binding.ETMCRMethod3.text.toString()==""){
            Toaster.toast("Debe ingresar un monto en DOLARES")
            return false
        }

        if(!binding.CBMCRMethod1.isChecked && !binding.CBMCRMethod2.isChecked && !binding.CBMCRMethod3.isChecked ){
            Toaster.toast("Debe seleccionar al menos un monto!!")
            return false
        }
        return true
    }

    private fun getEstateTranfer():String {
        var estate = ""
        var rgTransfer = binding.RGMCR.indexOfChild(binding.RGMCR.findViewById(binding.RGMCR.checkedRadioButtonId))
        when(rgTransfer){
            1-> estate="I"
            2-> estate="O"
        }
        return estate
    }

    private fun interactionCheckBox(cb:CheckBox,et:EditText){
        cb.setOnClickListener(){
            if (cb.isChecked){
                et.isEnabled = true
                et.hint = "Ingrese Monto"
            }else{
                et.isEnabled = false
                et.setText("")
                et.hint = "----------------------"
            }
        }
    }

    override fun onStart() {
        super.onStart()
    }
}