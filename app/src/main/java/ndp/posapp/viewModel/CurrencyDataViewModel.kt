package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.CashRegisterInfoEntity
import ndp.posapp.data.model.*
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.ClosingListCashierRepository
import ndp.posapp.repository.CurrencyDataRepository
import ndp.posapp.repository.GenerateRepository

class CurrencyDataViewModel(application: Application) : AndroidViewModel(application) {
    private val repository : CurrencyDataRepository
    var result = MutableLiveData<ApiResponse<CurrencyDataModel>>()

    init {
        val currencyDataDao = PosAppDb.getDatabase(application, viewModelScope).currencyDataDao()
        repository = CurrencyDataRepository(currencyDataDao)
    }

    fun getCurrencyData(loggerUserModel: LoggerUserModel) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val userIdentifier = loggerUserModel.userIdentifier

            val apiResponse = repository.getObjectCurrency(entity, fingerprint, signature, token)
            result.postValue(apiResponse)
        }
    }

}