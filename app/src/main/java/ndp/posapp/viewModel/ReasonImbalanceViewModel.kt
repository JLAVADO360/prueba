package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.ReasonImbalanceModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.ReasonImbalanceRepository
import ndp.posapp.utils.event.Event

class ReasonImbalanceViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ReasonImbalanceRepository

    var resultReasonImbalance = MutableLiveData<Event<ApiResponse<List<ReasonImbalanceModel>>>>()

    init {
        val listReasonImbalance =PosAppDb.getDatabase(application,viewModelScope).reasonListImbalanceDao()
        repository = ReasonImbalanceRepository(listReasonImbalance)
    }

    fun listReasonImbalance(
        loggerUserModel: LoggerUserModel
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val entity      = loggerUserModel.entity
            val fingerprint = loggerUserModel.fingerPrint
            val signature   = loggerUserModel.signature
            val token       = loggerUserModel.token

            val apiResponse = repository.listReasonImbalance(entity, fingerprint, signature, token)
            resultReasonImbalance.postValue(Event(apiResponse))
        }
    }


}