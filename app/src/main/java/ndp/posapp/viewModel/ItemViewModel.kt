package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.ItemEntity
import ndp.posapp.data.db.entity.KardexEntity
import ndp.posapp.data.db.entity.PriceListDetailEntity
import ndp.posapp.data.db.entity.WhsKardexEntity
import ndp.posapp.data.db.entityView.ItemView
import ndp.posapp.data.db.entityView.Kardex
import ndp.posapp.data.db.entityView.SaleItem
import ndp.posapp.data.model.ItemModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.ItemRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class ItemViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ItemRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<ItemModel>>>>()

    init {
        val itemDao = PosAppDb.getDatabase(application, viewModelScope).itemDao()
        repository = ItemRepository(itemDao)
    }

    fun listItem(
        loggerUserModel: LoggerUserModel,
        page: Int,
        syncType: Int,
        whsKardexViewModel: WhsKardexViewModel,
        kardexViewModel: KardexViewModel,
        priceListDetailViewModel: PriceListDetailViewModel
    ) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listItem(signature, token, fingerprint, entity, repositoryApp, page, syncType)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listItem = apiPaginationResponse.data!!.value
                    if (listItem != null && listItem.isNotEmpty()) {
                        listItem.forEach { itemModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val item = ItemEntity()
                            item.code = itemModel.code
                            item.company = itemModel.company ?: loggerUserModel.entity
                            item.barCode = itemModel.barCode
                            item.color = itemModel.color
                            item.createDate = itemModel.createDate
                            item.createUser = itemModel.createUser
                            item.inventoryItem = itemModel.inventoryItem
                            item.inventoryUnit = itemModel.inventoryUnit
                            item.isbn = itemModel.isbn
                            item.logicStatus = itemModel.logicStatus
                            item.name = itemModel.name
                            item.pricingUnit = itemModel.pricingUnit
                            item.projectName = itemModel.projectName
                            item.salesItem = itemModel.salesItem
                            item.salesItemsPerUnit = itemModel.salesItemsPerUnit
                            item.salesUnit = itemModel.salesUnit
                            item.size = itemModel.size
                            item.status = itemModel.status
                            item.taxCode = itemModel.taxCode
                            item.u_VS_AFEDET = itemModel.u_VS_AFEDET
                            item.u_VS_CODDET = itemModel.u_VS_CODDET
                            item.u_VS_PORDET = itemModel.u_VS_PORDET
                            item.unitGroupEntry = itemModel.unitGroupEntry
                            item.updateDate = itemModel.updateDate
                            item.updateUser = itemModel.updateUser
                            item.weight = itemModel.weight
                            insert(item)
                            if (itemModel.kardex.isNotEmpty()) {
                                itemModel.kardex.forEach { whsKardexModel ->
                                    val whsKardex = WhsKardexEntity()
                                    whsKardex.warehouse = whsKardexModel.warehouse
                                    whsKardex.itemEntity = whsKardexModel.itemEntity
                                    whsKardex.company = item.company
                                    whsKardex.logicStatus = whsKardexModel.logicStatus
                                    whsKardex.stock = whsKardexModel.stock
                                    whsKardex.stockCommitted = whsKardexModel.stockCommitted
                                    whsKardex.stockDisponible = whsKardexModel.stockDisponible
                                    whsKardex.stockNoForSale = whsKardexModel.stockNoForSale
                                    whsKardex.stockRequested = whsKardexModel.stockRequested
                                    whsKardex.store = whsKardexModel.store
                                    whsKardexViewModel.insert(whsKardex)
                                    if (whsKardexModel.kardexUbication.isNotEmpty()) {
                                        whsKardexModel.kardexUbication.forEach { kardexModel ->
                                            val kardex = KardexEntity()
                                            kardex.ubication = kardexModel.ubication
                                            kardex.createDate = kardexModel.createDate
                                            kardex.createUser = kardexModel.createUser
                                            kardex.item = kardexModel.item
                                            kardex.company = item.company
                                            kardex.logicStatus = kardexModel.logicStatus
                                            kardex.status = kardexModel.status
                                            kardex.stock = kardexModel.stock
                                            kardex.stockCommitted = kardexModel.stockCommitted
                                            kardex.stockRequest = kardexModel.stockRequest
                                            kardex.store = kardexModel.store
                                            kardex.ubicationEntry = kardexModel.ubicationEntry
                                            kardex.updateDate = kardexModel.updateDate
                                            kardex.updateUser = kardexModel.updateUser
                                            kardex.warehouse = kardexModel.warehouse
                                            kardexViewModel.insert(kardex)
                                        }
                                    }
                                }
                            }
                            if (itemModel.priceList.isNotEmpty()) {
                                itemModel.priceList.forEach { priceListDetailModel ->
                                    val priceListDetail = PriceListDetailEntity()
                                    priceListDetail.priceList = priceListDetailModel.priceList
                                    priceListDetail.applyTax = priceListDetailModel.applyTax
                                    priceListDetail.createDate = priceListDetailModel.createDate
                                    priceListDetail.createUser = priceListDetailModel.createUser
                                    priceListDetail.currency = priceListDetailModel.currency
                                    priceListDetail.grossPrice = priceListDetailModel.grossPrice
                                    priceListDetail.item = priceListDetailModel.item
                                    priceListDetail.company = item.company
                                    priceListDetail.logicStatus = priceListDetailModel.logicStatus
                                    priceListDetail.priceListName = priceListDetailModel.priceListName
                                    priceListDetail.priceListType = priceListDetailModel.priceListType
                                    priceListDetail.status = priceListDetailModel.status
                                    priceListDetail.store = priceListDetailModel.store
                                    priceListDetail.tax = priceListDetailModel.tax
                                    priceListDetail.taxPercentage = priceListDetailModel.taxPercentage
                                    priceListDetail.unitMSR = priceListDetailModel.unitMSR
                                    priceListDetail.unitMSREntry = priceListDetailModel.unitMSREntry
                                    priceListDetail.unitMSRName = priceListDetailModel.unitMSRName
                                    priceListDetail.unitPrice = priceListDetailModel.unitPrice
                                    priceListDetail.updateDate = priceListDetailModel.updateDate
                                    priceListDetail.updateUser = priceListDetailModel.updateUser
                                    priceListDetailViewModel.insert(priceListDetail)
                                }
                            }
                        }
                        if (nextPage != null && index < 3) { //TODO QUITAR VALIDACION INDEX < 1
                            val nextPageValue = nextPage.split("&")[0].replace("page=", "").toInt()
                            listItem(loggerUserModel, nextPageValue, syncType, whsKardexViewModel, kardexViewModel, priceListDetailViewModel)
                        } else {
                            result.postValue(Event(apiPaginationResponse))
                            PopUp.onUpdateProgressDialogCount(0, 0)
                        }
                    } else {
                        result.postValue(Event(apiPaginationResponse))
                    }
                }
            } else {
                if (apiPaginationResponse.code == "ERROR_ITEM_NOT_FOUND") apiPaginationResponse.successful = true
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }

    fun listItemFilter(
        loggerUserModel: LoggerUserModel,
        whsKardexViewModel: WhsKardexViewModel,
        kardexViewModel: KardexViewModel,
        priceListDetailViewModel: PriceListDetailViewModel,
        itemCode: String
    ) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity

            val apiPaginationResponse = repository.listItemFilter(signature, token, fingerprint, entity, itemCode)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listItem = apiPaginationResponse.data!!.value
                    if (listItem != null && listItem.isNotEmpty()) {
                        listItem.forEach { itemModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val item = ItemEntity()
                            item.code = itemModel.code
                            item.company = itemModel.company ?: loggerUserModel.entity
                            item.barCode = itemModel.barCode
                            item.color = itemModel.color
                            item.createDate = itemModel.createDate
                            item.createUser = itemModel.createUser
                            item.inventoryItem = itemModel.inventoryItem
                            item.inventoryUnit = itemModel.inventoryUnit
                            item.isbn = itemModel.isbn
                            item.logicStatus = itemModel.logicStatus
                            item.name = itemModel.name
                            item.pricingUnit = itemModel.pricingUnit
                            item.projectName = itemModel.projectName
                            item.salesItem = itemModel.salesItem
                            item.salesItemsPerUnit = itemModel.salesItemsPerUnit
                            item.salesUnit = itemModel.salesUnit
                            item.size = itemModel.size
                            item.status = itemModel.status
                            item.taxCode = itemModel.taxCode
                            item.u_VS_AFEDET = itemModel.u_VS_AFEDET
                            item.u_VS_CODDET = itemModel.u_VS_CODDET
                            item.u_VS_PORDET = itemModel.u_VS_PORDET
                            item.unitGroupEntry = itemModel.unitGroupEntry
                            item.updateDate = itemModel.updateDate
                            item.updateUser = itemModel.updateUser
                            item.weight = itemModel.weight
                            insert(item)
                            if (itemModel.kardex.isNotEmpty()) {
                                itemModel.kardex.forEach { whsKardexModel ->
                                    val whsKardex = WhsKardexEntity()
                                    whsKardex.warehouse = whsKardexModel.warehouse
                                    whsKardex.itemEntity = whsKardexModel.itemEntity
                                    whsKardex.company = item.company
                                    whsKardex.logicStatus = whsKardexModel.logicStatus
                                    whsKardex.stock = whsKardexModel.stock
                                    whsKardex.stockCommitted = whsKardexModel.stockCommitted
                                    whsKardex.stockDisponible = whsKardexModel.stockDisponible
                                    whsKardex.stockNoForSale = whsKardexModel.stockNoForSale
                                    whsKardex.stockRequested = whsKardexModel.stockRequested
                                    whsKardex.store = whsKardexModel.store
                                    whsKardexViewModel.insert(whsKardex)
                                    if (whsKardexModel.kardexUbication.isNotEmpty()) {
                                        whsKardexModel.kardexUbication.forEach { kardexModel ->
                                            val kardex = KardexEntity()
                                            kardex.ubication = kardexModel.ubication
                                            kardex.createDate = kardexModel.createDate
                                            kardex.createUser = kardexModel.createUser
                                            kardex.item = kardexModel.item
                                            kardex.company = item.company
                                            kardex.logicStatus = kardexModel.logicStatus
                                            kardex.status = kardexModel.status
                                            kardex.stock = kardexModel.stock
                                            kardex.stockCommitted = kardexModel.stockCommitted
                                            kardex.stockRequest = kardexModel.stockRequest
                                            kardex.store = kardexModel.store
                                            kardex.ubicationEntry = kardexModel.ubicationEntry
                                            kardex.updateDate = kardexModel.updateDate
                                            kardex.updateUser = kardexModel.updateUser
                                            kardex.warehouse = kardexModel.warehouse
                                            kardexViewModel.insert(kardex)
                                        }
                                    }
                                }
                            }
                            if (itemModel.priceList.isNotEmpty()) {
                                itemModel.priceList.forEach { priceListDetailModel ->
                                    val priceListDetail = PriceListDetailEntity()
                                    priceListDetail.priceList = priceListDetailModel.priceList
                                    priceListDetail.applyTax = priceListDetailModel.applyTax
                                    priceListDetail.createDate = priceListDetailModel.createDate
                                    priceListDetail.createUser = priceListDetailModel.createUser
                                    priceListDetail.currency = priceListDetailModel.currency
                                    priceListDetail.grossPrice = priceListDetailModel.grossPrice
                                    priceListDetail.item = priceListDetailModel.item
                                    priceListDetail.company = item.company
                                    priceListDetail.logicStatus = priceListDetailModel.logicStatus
                                    priceListDetail.priceListName = priceListDetailModel.priceListName
                                    priceListDetail.priceListType = priceListDetailModel.priceListType
                                    priceListDetail.status = priceListDetailModel.status
                                    priceListDetail.store = priceListDetailModel.store
                                    priceListDetail.tax = priceListDetailModel.tax
                                    priceListDetail.taxPercentage = priceListDetailModel.taxPercentage
                                    priceListDetail.unitMSR = priceListDetailModel.unitMSR
                                    priceListDetail.unitMSREntry = priceListDetailModel.unitMSREntry
                                    priceListDetail.unitMSRName = priceListDetailModel.unitMSRName
                                    priceListDetail.unitPrice = priceListDetailModel.unitPrice
                                    priceListDetail.updateDate = priceListDetailModel.updateDate
                                    priceListDetail.updateUser = priceListDetailModel.updateUser
                                    priceListDetailViewModel.insert(priceListDetail)
                                }
                            }
                        }
                        result.postValue(Event(apiPaginationResponse))
                    }
                }
            } else {
                if (apiPaginationResponse.code == "ERROR_ITEM_NOT_FOUND") apiPaginationResponse.successful = true
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }

    fun insert(itemEntity: ItemEntity) {
        repository.insert(itemEntity)
    }

    fun list(
        company: String,
        priceList: List<Int>,
        warehouse: String,
        priceListDetailViewModel: PriceListDetailViewModel,
        whsKardexViewModel: WhsKardexViewModel,
        kardexUbicationViewModel: KardexViewModel,
        excludedItemCodes: List<String>
    ): LiveData<MutableList<SaleItem>> {
        val result = MutableLiveData<MutableList<SaleItem>>()
        viewModelScope.launch(Dispatchers.IO) {
            val saleItemList: MutableList<SaleItem> = ArrayList()
            repository.list(company, excludedItemCodes).forEach { itemEntity ->
                val saleItemView = SaleItem(itemEntity, ArrayList(), ArrayList())
                priceListDetailViewModel.list(company, priceList, itemEntity.code).forEach {
                    saleItemView.priceListDetailEntity.add(it)
                }
                whsKardexViewModel.list(company, warehouse, itemEntity.code).forEach { kardexEntity ->
                    val kardex = Kardex(
                        warehouse = kardexEntity.warehouse,
                        itemEntity = kardexEntity.itemEntity,
                        logicStatus = kardexEntity.logicStatus,
                        stock = kardexEntity.stock,
                        stockCommitted = kardexEntity.stockCommitted,
                        stockDisponible = kardexEntity.stockDisponible,
                        stockNoForSale = kardexEntity.stockNoForSale,
                        stockRequested = kardexEntity.stockRequested,
                        store = kardexEntity.store,
                    )
                    kardexUbicationViewModel.list(company, warehouse, itemEntity.code).forEach {
                        kardex.kardexUbication.add(it)
                    }
                    saleItemView.kardex.add(kardex)
                }
                saleItemList.add(saleItemView)
            }
            result.postValue(saleItemList)
        }
        return result
    }

    fun get(
        company: String,
        warehouse: String,
        priceListDetailViewModel: PriceListDetailViewModel,
        whsKardexViewModel: WhsKardexViewModel,
        kardexUbicationViewModel: KardexViewModel,
        itemCode: String
    ): LiveData<SaleItem> {
        val result = MutableLiveData<SaleItem>()
        viewModelScope.launch(Dispatchers.IO) {
            repository.get(company, itemCode).let { itemEntity ->
                if (itemEntity != null) {
                    val saleItemView = SaleItem(itemEntity, ArrayList(), ArrayList())
                    priceListDetailViewModel.list(company, itemEntity.code).forEach {
                        saleItemView.priceListDetailEntity.add(it)
                    }
                    whsKardexViewModel.list(company, warehouse, itemEntity.code).forEach { kardexEntity ->
                        val kardex = Kardex(
                            warehouse = kardexEntity.warehouse,
                            itemEntity = kardexEntity.itemEntity,
                            logicStatus = kardexEntity.logicStatus,
                            stock = kardexEntity.stock,
                            stockCommitted = kardexEntity.stockCommitted,
                            stockDisponible = kardexEntity.stockDisponible,
                            stockNoForSale = kardexEntity.stockNoForSale,
                            stockRequested = kardexEntity.stockRequested,
                            store = kardexEntity.store,
                        )
                        kardexUbicationViewModel.list(company, warehouse, itemEntity.code).forEach {
                            kardex.kardexUbication.add(it)
                        }
                        saleItemView.kardex.add(kardex)
                    }
                    result.postValue(saleItemView)
                } else {
                    result.postValue(null)
                }
            }
        }
        return result
    }

    fun list(company: String): LiveData<List<ItemView>>{
        return repository.list(company)
    }
}