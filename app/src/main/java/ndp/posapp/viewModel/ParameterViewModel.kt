package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.ParameterEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.ParameterModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.ParameterRepository
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event
import xdroid.toaster.Toaster

class ParameterViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ParameterRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<ParameterModel>>>>()

    init {
        val parameterDao = PosAppDb.getDatabase(application, viewModelScope).parameterDao()
        repository = ParameterRepository(parameterDao)
    }


    fun listParameter(loggerUserModel: LoggerUserModel, page: Int, syncType: Int) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            try {

                val apiPaginationResponse = repository.listParameter(signature, token, fingerprint, entity, repositoryApp, page, syncType)
                if (apiPaginationResponse.successful) {
                    if (apiPaginationResponse.data != null) {
                        val nextPage = apiPaginationResponse.data!!.nextPage
                        val count = apiPaginationResponse.data!!.count
                        val listParameter = apiPaginationResponse.data!!.value
                        if (listParameter != null && listParameter.isNotEmpty()) {
                            listParameter.forEach {
                                index++
                                PopUp.onUpdateProgressDialogCount(count, index)
                                val parameter = ParameterEntity()
                                parameter.code = it.code
                                parameter.company = it.company ?: loggerUserModel.entity
                                parameter.application = it.application
                                parameter.createDate = it.createDate
                                parameter.description = it.description
                                parameter.logicStatus = it.logicStatus
                                parameter.name = it.name
                                parameter.objectUI = it.objectUI
                                parameter.parameterType = it.parameterType
                                parameter.status = it.status
                                parameter.updateDate = it.updateDate
                                parameter.value = it.value
                                insert(parameter)
                            }
                            if (nextPage != null) {
                                val nextPageValue = nextPage.split("&")[0].replace("page=", "").toInt()
                                listParameter(loggerUserModel, nextPageValue, syncType)
                            } else {
                                result.postValue(Event(apiPaginationResponse))
                                PopUp.onUpdateProgressDialogCount(0, 0)
                            }
                        }
                    }
                } else {
                    if (apiPaginationResponse.code == Const.PARAMETER_UPDATE) apiPaginationResponse.successful = true
                    result.postValue(Event(apiPaginationResponse))
                }
            } catch (e : Exception){
                Toaster.toastLong(e.message)
            }

        }
    }


    fun insert(parameterEntity: ParameterEntity) {
        repository.insert(parameterEntity)
    }

    fun get(company: String, name: String): LiveData<ParameterModel> {
        return repository.get(company, name)
    }

}