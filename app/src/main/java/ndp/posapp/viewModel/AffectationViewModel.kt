package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.AffectationEntity
import ndp.posapp.data.model.AffectationModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.AffectationRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event
import xdroid.toaster.Toaster

class AffectationViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: AffectationRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<AffectationModel>>>>()


    init {
        val affectationDao = PosAppDb.getDatabase(application, viewModelScope).affectationDao()
        repository = AffectationRepository(affectationDao)
    }


    fun listAffectation(loggerUserModel: LoggerUserModel) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listAffectation(signature, token, fingerprint, entity, repositoryApp)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listAffectation = apiPaginationResponse.data!!.value
                    if (listAffectation != null && listAffectation.isNotEmpty()) {
                        listAffectation.forEach {
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val affectationEntity = AffectationEntity()
                            affectationEntity.code = it.code
                            affectationEntity.company = it.company ?: loggerUserModel.entity
                            affectationEntity.createDate = it.createDate
                            affectationEntity.createUser = it.createUser
                            affectationEntity.description = it.description
                            affectationEntity.updateDate = it.updateDate
                            affectationEntity.updateUser = it.updateUser
                            insert(affectationEntity)
                        }
                    }
                    if (nextPage != null) {
                        listAffectation(loggerUserModel)
                    } else {
                        result.postValue(Event(apiPaginationResponse))
                        PopUp.onUpdateProgressDialogCount(0, 0)
                    }
                }
            } else {
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }


    private fun insert(affectationEntity: AffectationEntity) {
        repository.insert(affectationEntity)
    }


}