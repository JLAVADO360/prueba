package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.*
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.ClosingCashRegisterRepository
import ndp.posapp.repository.GenerateRepository
import ndp.posapp.repository.MovementCashRegisterRepository

class MovementCashRegisterViewModel(application: Application) : AndroidViewModel(application) {
    private val repository : MovementCashRegisterRepository
    var result = MutableLiveData<ApiResponse<MovementTransferIOModel>>()

    init {
        val movementCashRegisterDAO = PosAppDb.getDatabase(application, viewModelScope).movementCashRegisterDAO()
        repository = MovementCashRegisterRepository(movementCashRegisterDAO)
    }

    fun movementCashRegister(
        loggerUserModel: LoggerUserModel,
        movementTransferIOModel: MovementTransferIOModel,
        callback: (ApiResponse<MovementTransferIOModel>) -> Unit
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            var apiResponse:ApiResponse<MovementTransferIOModel>
            withContext(Dispatchers.IO) {
                val signature   = loggerUserModel.signature
                val token       = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity      = loggerUserModel.entity
                apiResponse = repository.movementCashRegister(entity, fingerprint, signature, token, movementTransferIOModel)
            }
            callback(apiResponse)
        }
    }

}