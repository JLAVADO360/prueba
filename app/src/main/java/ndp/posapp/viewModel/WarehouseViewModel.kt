package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.UbicationEntity
import ndp.posapp.data.db.entity.WarehouseEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.WarehouseModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.WarehouseRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class WarehouseViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: WarehouseRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<WarehouseModel>>>>()

    var warehouseZoneList = MutableLiveData<List<WarehouseEntity>>()

    init {
        val warehouseDao = PosAppDb.getDatabase(application, viewModelScope).warehouseDao()
        repository = WarehouseRepository(warehouseDao)
    }

    fun listWarehouse(loggerUserModel: LoggerUserModel, page: Int, syncType: Int, ubicationViewModel: UbicationViewModel) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listWarehouse(signature, token, fingerprint, entity, repositoryApp, page, syncType)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listWarehouse = apiPaginationResponse.data!!.value
                    if (listWarehouse != null && listWarehouse.isNotEmpty()) {
                        listWarehouse.forEach { warehouseModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val warehouse = WarehouseEntity()
                            warehouse.code = warehouseModel.code
                            warehouse.company = warehouseModel.company ?: loggerUserModel.entity
                            warehouse.address = warehouseModel.address
                            warehouse.description = warehouseModel.description
                            warehouse.distributionList = warehouseModel.distributionList
                            warehouse.logicStatus = warehouseModel.logicStatus
                            warehouse.name = warehouseModel.name
                            warehouse.status = warehouseModel.status
                            warehouse.store = warehouseModel.store
                            insert(warehouse)
                            if (warehouseModel.ubications != null && warehouseModel.ubications!!.isNotEmpty()) {
                                warehouseModel.ubications!!.forEach { ubicationModel ->
                                    val ubication = UbicationEntity()
                                    ubication.code = ubicationModel.code
                                    ubication.company = warehouse.company
                                    ubication.createDate = ubicationModel.createDate
                                    ubication.createUser = ubicationModel.createUser
                                    ubication.entry = ubicationModel.entry
                                    ubication._isSale = ubicationModel.isSale
                                    ubication.logicStatus = ubicationModel.logicStatus
                                    ubication.name = ubicationModel.name
                                    ubication.status = ubicationModel.status
                                    ubication.type = ubicationModel.type
                                    ubication.updateDate = ubicationModel.updateDate
                                    ubication.updateUser = ubicationModel.updateUser
                                    ubication.warehouse = ubicationModel.warehouse
                                    ubicationViewModel.insert(ubication)
                                }
                            }
                        }
                        if (nextPage != null) {
                            val nextPageValue = nextPage.split("&")[0].replace("page=", "").toInt()
                            listWarehouse(loggerUserModel, nextPageValue, syncType, ubicationViewModel)
                        } else {
                            result.postValue(Event(apiPaginationResponse))
                            PopUp.onUpdateProgressDialogCount(0, 0)
                        }
                    }
                }
            } else {
                if (apiPaginationResponse.code == "ERROR_WAREHOUSE_NOT_FOUND") apiPaginationResponse.successful = true
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }


    fun insert(warehouseEntity: WarehouseEntity) {
        repository.insert(warehouseEntity)
    }

    fun list(company: String, zoneWarehouseViewModel: ZoneWarehouseViewModel) {
        viewModelScope.launch(Dispatchers.IO) {
            val warehouses = ArrayList<String>()
            zoneWarehouseViewModel.list().forEach {
                warehouses.add(it.warehouse)
            }
            warehouseZoneList.postValue(repository.list(company, warehouses))
        }
    }


}