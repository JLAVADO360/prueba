package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.CashRegisterInfoEntity
import ndp.posapp.data.model.CashRegisterBalanceDTOModel
import ndp.posapp.data.model.CashRegisterInfoModel
import ndp.posapp.data.model.ClosingListCashierGenerateModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.ClosingListCashierRepository
import ndp.posapp.repository.GenerateRepository

class GenerateViewModel(application: Application) : AndroidViewModel(application) {
    private val repository : GenerateRepository
    var result = MutableLiveData<ApiResponse<ClosingListCashierGenerateModel>>()

    init {
        val closingCashierGenerateDAO = PosAppDb.getDatabase(application, viewModelScope).closingCashierGenerateDAO()
        repository = GenerateRepository(closingCashierGenerateDAO)
    }

    fun listClosingCashGenerate(loggerUserModel: LoggerUserModel,cashRegisterBalanceDTOModel: CashRegisterBalanceDTOModel) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val userIdentifier = loggerUserModel.userIdentifier

            val apiResponse = repository.listClosingListCashierGenerate(entity, fingerprint, signature, token,cashRegisterBalanceDTOModel)
            result.postValue(apiResponse)
        }
    }

}