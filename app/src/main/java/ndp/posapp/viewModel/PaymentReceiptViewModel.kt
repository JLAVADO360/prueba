package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.PaymentReceiptEntity
import ndp.posapp.repository.PaymentReceiptRepository

class PaymentReceiptViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: PaymentReceiptRepository

    init {
        val paymentReceiptDao = PosAppDb.getDatabase(application, viewModelScope).paymentReceiptDao()
        repository = PaymentReceiptRepository(paymentReceiptDao)
    }

    fun insert(paymentReceiptEntity: PaymentReceiptEntity): LiveData<Long?> {
        val liveData = MutableLiveData<Long?>()
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.insert(paymentReceiptEntity)
            }.also {
                liveData.value = it
            }
        }
        return liveData
    }

    fun update(paymentReceiptEntity: PaymentReceiptEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.update(paymentReceiptEntity)
        }
    }

    fun updateCode(saleOrderId: Long, code: String) {
        repository.updateCode(saleOrderId, code)
    }

    fun get(internalId: Long) : LiveData<PaymentReceiptEntity?> {
        return repository.get(internalId)
    }

    fun listPendingSend(): LiveData<List<PaymentReceiptEntity>> {
        val liveData = MutableLiveData<List<PaymentReceiptEntity>>()

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.listPendingSend()
            }.also {
                liveData.value = it
            }
        }
        return liveData
    }

    fun list(saleOrderId: Long): LiveData<List<PaymentReceiptEntity>> {
        val liveData = MutableLiveData<List<PaymentReceiptEntity>>()

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.list(saleOrderId)
            }.also {
                liveData.value = it
            }
        }
        return liveData
    }
}