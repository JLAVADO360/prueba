package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.CorrelativeEntity
import ndp.posapp.data.model.CorrelativeModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.CorrelativeRepository

class CorrelativeViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CorrelativeRepository

    var result = MutableLiveData<ApiPaginationResponse<List<CorrelativeModel>>>()

    init {
        val correlativeDao = PosAppDb.getDatabase(application, viewModelScope).correlativeDao()
        repository = CorrelativeRepository(correlativeDao)
    }

    fun listCorrelative(loggerUserModel: LoggerUserModel, cashRegisterCode: String, observe: Boolean = true) {
        viewModelScope.launch(Dispatchers.IO) {
            val entity = loggerUserModel.entity
            val fingerprint = loggerUserModel.fingerPrint
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token

            val apiPaginationResponse = repository.listCorrelative(entity, fingerprint, signature, token, cashRegisterCode)

            if (apiPaginationResponse.successful && apiPaginationResponse.data?.value?.isNotEmpty() == true) {
                apiPaginationResponse.data?.value?.forEach {
                    val correlativeEntity = CorrelativeEntity().apply {
                        id = it.id
                        company = it.companyIdentifier
                        this.cashRegisterCode = cashRegisterCode
                        actualNumeration = it.actualNumeration ?: 0
                        endNumeration = it.endNumeration ?: 999999999
                        initialNumeration = it.initialNumeration
                        modified = it.modified
                        numerationLength = it.numerationLength
                        parentReceiptType = it.parentReceiptType
                        receiptTypeCode = it.receiptTypeCode ?: ""
                        series = it.series
                        status = it.status
                        storeCode = it.storeCode
                        version = it.version
                    }
                    insert(correlativeEntity)
                }
            }

            if (observe) result.postValue(apiPaginationResponse)
        }
    }


    fun insert(correlativeEntity: CorrelativeEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.insert(correlativeEntity)
        }
    }

    fun list(company: String, cashRegisterCode: String): LiveData<List<CorrelativeEntity>> {
        return repository.list(company, cashRegisterCode)
    }

}