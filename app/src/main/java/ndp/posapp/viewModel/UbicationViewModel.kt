package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.UbicationEntity
import ndp.posapp.repository.UbicationRepository

class UbicationViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: UbicationRepository

    init {
        val ubicationDao = PosAppDb.getDatabase(application, viewModelScope).ubicationDao()
        repository = UbicationRepository(ubicationDao)
    }

    fun insert(ubicationEntity: UbicationEntity) {
        repository.insert(ubicationEntity)
    }


}