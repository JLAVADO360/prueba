package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.RepositorySyncHistoryEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.RepositorySyncHistoryModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.RepositorySyncHistoryRepository
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event
import xdroid.toaster.Toaster


class RepositorySyncHistoryViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: RepositorySyncHistoryRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiResponse<RepositorySyncHistoryModel>>>()

    init {
        val repositorySyncHistoryDao = PosAppDb.getDatabase(application, viewModelScope).repositorySyncHistoryDao()
        repository = RepositorySyncHistoryRepository(repositorySyncHistoryDao)
    }


    fun syncHistory(loggerUserModel: LoggerUserModel, warehouse: String?) {

            viewModelScope.launch(Dispatchers.IO) {
                val repositorySyncHistoryModel = RepositorySyncHistoryModel()
                repositorySyncHistoryModel.user = loggerUserModel.email
                repositorySyncHistoryModel.company = loggerUserModel.entity
                repositorySyncHistoryModel.channel = Const.APPLICATION_CHANNEL
                repositorySyncHistoryModel.repository = loggerUserModel.fingerPrint
                repositorySyncHistoryModel.warehouse = warehouse

                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity

                val apiPagination = repository.syncHistory(signature, token, fingerprint, entity, repositorySyncHistoryModel)

                if (apiPagination.successful) {
                    if (apiPagination.data != null) {
                        val model = apiPagination.data
                        index++
                        val repositorySyncHistory = RepositorySyncHistoryEntity()
                        repositorySyncHistory.user = model!!.user
                        repositorySyncHistory.company = model.company ?: loggerUserModel.entity
                        repositorySyncHistory.channel = model.channel
                        repositorySyncHistory.lastSync = model.lastSync
                        repositorySyncHistory.repository = model.repository
                        repositorySyncHistory.warehouse = model.warehouse
                        insert(repositorySyncHistory)

                        PopUp.onUpdateProgressDialogCount(1, index)
                        result.postValue(Event(apiPagination))
                    }
                } else {
                    result.postValue(Event(apiPagination))
                }
            }


    }


    fun insert(repositorySyncHistoryEntity: RepositorySyncHistoryEntity) {
        repository.insert(repositorySyncHistoryEntity)

    }

    fun list(company: String): LiveData<List<RepositorySyncHistoryEntity>> {
        val result = MutableLiveData<List<RepositorySyncHistoryEntity>>()
        viewModelScope.launch(Dispatchers.IO) {
            result.postValue(repository.list(company))
        }
        return result
    }

}