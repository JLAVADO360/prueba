package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.WhsKardexEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.WhsKardexModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.WhsKardexRepository
import ndp.posapp.utils.event.Event

class WhsKardexViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: WhsKardexRepository

    var result = MutableLiveData<Event<ApiResponse<List<WhsKardexModel>>>>()

    init {
        val whsKardexDao = PosAppDb.getDatabase(application, viewModelScope).whsKardexDao()
        repository = WhsKardexRepository(whsKardexDao)
    }

    fun syncWhskardex(
        loggerUserModel: LoggerUserModel,
        warehouse: String,
        syncType: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val apiResponse = repository.syncWhskardex(signature, token, fingerprint, entity, warehouse, syncType)
            if (apiResponse.successful) {
                if (apiResponse.data != null) {
                    val whsKardexModelList = apiResponse.data
                    if (!whsKardexModelList.isNullOrEmpty()) {
                        whsKardexModelList.forEach { whsKardexModel ->
                            val whsKardex = WhsKardexEntity()
                            whsKardex.warehouse = whsKardexModel.warehouse
                            whsKardex.itemEntity = whsKardexModel.itemEntity
                            whsKardex.logicStatus = whsKardexModel.logicStatus
                            whsKardex.stock = whsKardexModel.stock
                            whsKardex.stockCommitted = whsKardexModel.stockCommitted
                            whsKardex.stockDisponible = whsKardexModel.stockDisponible
                            whsKardex.stockNoForSale = whsKardexModel.stockNoForSale
                            whsKardex.stockRequested = whsKardexModel.stockRequested
                            whsKardex.store = whsKardexModel.store
                            insert(whsKardex)
                        }

                        result.postValue(Event(apiResponse))
                    }
                }

            } else {
                result.postValue(Event(apiResponse))
            }
        }
    }


    fun insert(whsKardexEntity: WhsKardexEntity) {
        repository.insert(whsKardexEntity)
    }

    fun list(company: String, warehouse: String, item: String): List<WhsKardexEntity> {
        return repository.list(company, warehouse, item)
    }
}