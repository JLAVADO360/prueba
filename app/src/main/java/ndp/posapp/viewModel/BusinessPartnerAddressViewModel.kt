package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.BusinessPartnerAddressEntity
import ndp.posapp.repository.BusinessPartnerAddressRepository

class BusinessPartnerAddressViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: BusinessPartnerAddressRepository

    init {
        val businessPartnerAddressDao = PosAppDb.getDatabase(application, viewModelScope).businessPartnerAddressDao()
        repository = BusinessPartnerAddressRepository(businessPartnerAddressDao)
    }

    fun insert(businessPartnerAddressEntity: BusinessPartnerAddressEntity) {
        repository.insert(businessPartnerAddressEntity)
    }

}