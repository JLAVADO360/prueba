package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.PriceListDetailEntity
import ndp.posapp.repository.PriceListDetailRepository

class PriceListDetailViewModel(application: Application) : AndroidViewModel(application) {

    private val repository : PriceListDetailRepository
    init {
        val priceListDetailDao = PosAppDb.getDatabase(application,viewModelScope).priceListDetailDao()
        repository = PriceListDetailRepository(priceListDetailDao)
    }

    fun insert(priceListDetailEntity: PriceListDetailEntity){
        repository.insert(priceListDetailEntity)
    }

    fun list(company: String, priceList: List<Int>, item: String) : List<PriceListDetailEntity>{
        return repository.list(company, priceList,item)
    }

    fun list(company: String, item: String) : List<PriceListDetailEntity>{
        return repository.list(company, item)
    }
}