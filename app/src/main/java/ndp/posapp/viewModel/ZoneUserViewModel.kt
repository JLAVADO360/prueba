package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.ZoneUserEntity
import ndp.posapp.repository.ZoneUserRepository


class ZoneUserViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ZoneUserRepository

    init {
        val zoneUserDao = PosAppDb.getDatabase(application, viewModelScope).zoneUserDao()
        repository = ZoneUserRepository(zoneUserDao)
    }

    fun insert(zoneUserEntity: ZoneUserEntity) {
        repository.insert(zoneUserEntity)
    }
}