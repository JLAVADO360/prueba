package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.BalanceListCashierModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.MovementListCashierModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.BalanceListCashierRepository
import ndp.posapp.repository.MovementListCashierRepository
import ndp.posapp.utils.event.Event

class MovementListCashierViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: MovementListCashierRepository

    var resultMovementListCashier = MutableLiveData<Event<ApiPaginationResponse<List<MovementListCashierModel>>>>()

    init {
        val movementListCashierDao = PosAppDb.getDatabase(application, viewModelScope).movementListCashierDao()
        repository = MovementListCashierRepository(movementListCashierDao)
    }

    fun listMovementListCashier(
        loggerUserModel: LoggerUserModel,
        page: Int,
        cashRegisterCode: String,
        store:String,
        turnDate: String,
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity

            val apiPaginationResponse = repository.MovementListCashier(entity, fingerprint, signature, token, page, cashRegisterCode, store, turnDate)
            resultMovementListCashier.postValue(Event(apiPaginationResponse))
        }
    }
}