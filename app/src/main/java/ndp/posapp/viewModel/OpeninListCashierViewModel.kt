package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.OpeningListCashierModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.OpeningListCashierRepository
import ndp.posapp.utils.event.Event

class OpeninListCashierViewModel(application: Application) : AndroidViewModel(application)  {

    private val repository: OpeningListCashierRepository

    var resultOpeningListCashier = MutableLiveData<Event<ApiPaginationResponse<List<OpeningListCashierModel>>>>()

    init {
        val openingListCashierDao = PosAppDb.getDatabase(application, viewModelScope).openingListCashierDao()
        repository = OpeningListCashierRepository(openingListCashierDao)
    }

    fun listOpeningListCashier(
        loggerUserModel: LoggerUserModel,
        codclient: String,
        startDate: String,
        endDate: String,
        page: Int,
        itemPerPage: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity

            val apiPaginationResponse = repository.OpeningListCashier(entity, fingerprint, signature, token, codclient, startDate, endDate, page,itemPerPage)
            resultOpeningListCashier.postValue(Event(apiPaginationResponse))
        }
    }
}