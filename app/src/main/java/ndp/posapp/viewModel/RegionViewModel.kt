package ndp.posapp.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.RegionEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.RegionModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.RegionRepository
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class RegionViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: RegionRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<RegionModel>>>>()

    init {
        val regionDao = PosAppDb.getDatabase(application, viewModelScope).regionDao()
        repository = RegionRepository(regionDao)
    }


    fun listRegion(
        loggerUserModel: LoggerUserModel,
        page: Int,
        syncType: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {

            //if (countRegion() < Const.NUMBER_OF_REGIONS) {
 //           if (countRegion()==Const.NUMBER_OF_REGIONS) {
                Log.e("valer1:","")
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                val repositoryApp = loggerUserModel.fingerPrint

                val apiPaginationResponse = repository.listRegion(signature, token, fingerprint, entity, repositoryApp, page, syncType)
                if (apiPaginationResponse.successful) {
                    if (apiPaginationResponse.data != null) {
                        Log.e("valer2:",""+apiPaginationResponse.data!!.value!!.size)
                        val nextPage = apiPaginationResponse.data!!.nextPage
                        val count = apiPaginationResponse.data!!.count
                        val listRegion = apiPaginationResponse.data!!.value
                        if (listRegion != null && listRegion.isNotEmpty()) {
                            listRegion.forEach { regionModel ->
                                index++
                                PopUp.onUpdateProgressDialogCount(count, index)
                                val region = RegionEntity()
                                region.code = regionModel.code
                                region.concatCode = regionModel.concatCode
                                region.country = regionModel.country
                                region.identifier = regionModel.identifier
                                region.region = regionModel.region
                                region.regionType = regionModel.regionType
                                insert(region)

                            }

                            if (nextPage != null) {
                                val nextPageValue = nextPage.split("&")[0].replace("page=", "").toInt()
                                listRegion(loggerUserModel, nextPageValue, syncType)
                            } else {
                                result.postValue(Event(apiPaginationResponse))
                                PopUp.onUpdateProgressDialogCount(0, 0)
                            }

                        }
                    }
                } else {
                    Log.e("valer3:","")
                    apiPaginationResponse.successful = true
                    result.postValue(Event(apiPaginationResponse))
                }
//            } else {
//                Log.e("valer4:","")
//                result.postValue(Event(ApiPaginationResponse<List<RegionModel>>().apply { successful = true }))
//            }
        }
    }


    fun insert(regionEntity: RegionEntity) {
        repository.insert(regionEntity)
    }

    fun listDepartment(region: String, regionType : String): LiveData<List<RegionEntity>> {
        return repository.listDepartment(region,regionType)
    }

    fun listRelation(region : String, regionType : String, regionEntity: RegionEntity?, regionRelationViewModel: RegionRelationViewModel) : LiveData<List<RegionEntity>>{
        val result = MutableLiveData<List<RegionEntity>>()
        viewModelScope.launch (Dispatchers.IO){
            val  relation  = regionRelationViewModel.list(regionEntity?.identifier)
            result.postValue(repository.listRelation(region,regionType,relation))
        }
        return result
    }

    private fun countRegion(): Int {
        return repository.countRegion()
    }

}