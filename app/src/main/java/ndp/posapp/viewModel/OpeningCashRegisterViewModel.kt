package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.ClosingCashRegisterSaveModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.OpeningCashRegisterModel
import ndp.posapp.data.model.OpeningCashRegisterSaveModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.OpeningCashRegisterRepository
import ndp.posapp.utils.event.Event

class OpeningCashRegisterViewModel(application: Application) : AndroidViewModel(application)  {
    private val repository: OpeningCashRegisterRepository
    var result = MutableLiveData<Event<ApiResponse<OpeningCashRegisterSaveModel>>>()

    init {
        val openingCashRegisterDao = PosAppDb.getDatabase(application, viewModelScope).openingCashRegisterDao()
        repository = OpeningCashRegisterRepository(openingCashRegisterDao)
    }

    fun openingCashRegister(
        loggerUserModel: LoggerUserModel,
        OpeningCashRegisterModelDTO: OpeningCashRegisterSaveModel,
        callback: (ApiResponse<OpeningCashRegisterSaveModel>) -> Unit
    ) {
       // var apiPaginationResponse:ApiResponse<OpeningCashRegisterSaveModel> = ApiResponse()
        viewModelScope.launch(Dispatchers.IO) {
            var apiResponse:ApiResponse<OpeningCashRegisterSaveModel>
            withContext(Dispatchers.IO) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                apiResponse = repository.OpeningCashRegister(entity, fingerprint, signature, token, OpeningCashRegisterModelDTO)
            }
            callback(apiResponse)
        }
    }
}