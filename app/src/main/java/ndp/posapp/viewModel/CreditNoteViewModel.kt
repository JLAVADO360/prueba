package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.model.CreditNoteModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.CreditNoteRepository

class CreditNoteViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = CreditNoteRepository()

    fun getCreditNote(
        loggerUserModel: LoggerUserModel,
        creditNoteNumber : String,
        callback: (ApiPaginationResponse<List<CreditNoteModel>>) -> Unit
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                repository.getCreditNote(entity, fingerprint, signature, token, creditNoteNumber)
            }.also {
                callback(it)
            }
        }
    }
}