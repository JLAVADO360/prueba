package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.PaymentWayEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.PaymentWayModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.PaymentWayRepository

class PaymentWayViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: PaymentWayRepository

    var result = MutableLiveData<ApiPaginationResponse<List<PaymentWayModel>>>()

    init {
        val paymentWayDao = PosAppDb.getDatabase(application, viewModelScope).paymentWayDao()
        repository = PaymentWayRepository(paymentWayDao)
    }

    fun listPaymentWay(loggerUserModel: LoggerUserModel) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val store = loggerUserModel.store!!

            val apiPaginationResponse = repository.listPaymentWay(signature, token, fingerprint, entity, store)

            if (apiPaginationResponse.successful && apiPaginationResponse.data?.value?.isNotEmpty() == true) {
                val paymentWayEntity = PaymentWayEntity().apply {
                    this.company = entity
                    this.store = store
                    this.paymentWayBody = Gson().toJson(
                        apiPaginationResponse.data?.value,
                        object : TypeToken<List<PaymentWayModel>>() {}.type
                    )
                }
                insert(paymentWayEntity)
            }

            result.postValue(apiPaginationResponse)
        }
    }

    fun insert(paymentWayEntity: PaymentWayEntity) {
        repository.insert(paymentWayEntity)
    }

    fun get(company: String, store: String): LiveData<List<PaymentWayModel>> {
        val liveData = MutableLiveData<List<PaymentWayModel>>()

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                Gson().fromJson(
                    repository.get(company, store)?.paymentWayBody,
                    object : TypeToken<List<PaymentWayModel>>() {}.type
                ) as? List<PaymentWayModel>
            }.also {
                liveData.value = it
            }
        }

        return liveData
    }
}