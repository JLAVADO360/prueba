package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.PaymentConditionsEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.PaymentConditionsModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.PaymentConditionsRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class PaymentConditionsViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: PaymentConditionsRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<PaymentConditionsModel>>>>()

    init {
        val paymentConditionsDao = PosAppDb.getDatabase(application, viewModelScope).paymentConditionsDao()
        repository = PaymentConditionsRepository(paymentConditionsDao)
    }


    fun listPaymentConditions(loggerUserModel: LoggerUserModel, page: Int, syncType: Int) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listPaymentConditions(signature, token, fingerprint, entity, repositoryApp, page, syncType)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listPaymentCondition = apiPaginationResponse.data!!.value
                    if (listPaymentCondition != null && listPaymentCondition.isNotEmpty()) {
                        listPaymentCondition.forEach { paymentConditionsModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val paymentConditions = PaymentConditionsEntity()
                            paymentConditions.code = paymentConditionsModel.code
                            paymentConditions.company = paymentConditionsModel.company ?: loggerUserModel.entity
                            paymentConditions.createDate = paymentConditionsModel.createDate
                            paymentConditions.createUser = paymentConditionsModel.createUser
                            paymentConditions.creditLimit = paymentConditionsModel.creditLimit
                            paymentConditions.generalDiscount = paymentConditionsModel.generalDiscount
                            paymentConditions.groupNumber = paymentConditionsModel.groupNumber
                            paymentConditions.interestOnArrears = paymentConditionsModel.interestOnArrears
                            paymentConditions.loadLimit = paymentConditionsModel.loadLimit
                            paymentConditions.numberOfAdditionalDays = paymentConditionsModel.numberOfAdditionalDays
                            paymentConditions.numberOfAdditionalMonths = paymentConditionsModel.numberOfAdditionalMonths
                            paymentConditions.numberOfToleranceDays = paymentConditionsModel.numberOfToleranceDays
                            paymentConditions.paymentTermsGroupName = paymentConditionsModel.paymentTermsGroupName
                            paymentConditions.priceListNo = paymentConditionsModel.priceListNo
                            paymentConditions.type = paymentConditionsModel.type
                            paymentConditions.updateDate = paymentConditionsModel.updateDate
                            paymentConditions.updateUser = paymentConditionsModel.updateUser
                            insert(paymentConditions)
                        }
                    }
                    if (nextPage != null) {
                        val nextPageValue = nextPage.split("&")[0].replace("page=", "").toInt()
                        listPaymentConditions(loggerUserModel, nextPageValue, syncType)
                    } else {
                        result.postValue(Event(apiPaginationResponse))
                        PopUp.onUpdateProgressDialogCount(0, 0)
                    }
                }
            } else {
                if (apiPaginationResponse.code == "ERROR_PAYMENTCONDITIONS_NOT_FOUND") apiPaginationResponse.successful = true
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }

    fun insert(paymentConditionsEntity: PaymentConditionsEntity) {
        repository.insert(paymentConditionsEntity)
    }

    fun list(company: String, groupNumber: Int): LiveData<List<PaymentConditionsEntity>> {
        return repository.list(company, groupNumber)
    }

    fun get(company: String, name: String): LiveData<PaymentConditionsEntity> {
        return repository.get(company, name)
    }
}