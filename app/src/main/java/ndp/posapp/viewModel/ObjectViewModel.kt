package ndp.posapp.viewModel

import android.app.Application
import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.R
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.ObjectEntity
import ndp.posapp.data.model.ObjectModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.databinding.ActivityHomeBinding
import ndp.posapp.databinding.FragmentHomeBinding
import ndp.posapp.repository.ObjectRepository
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import xdroid.toaster.Toaster

class ObjectViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ObjectRepository

    init {
        val objectDao = PosAppDb.getDatabase(application, viewModelScope).objectDao()
        repository = ObjectRepository(objectDao)
    }

    fun permissionListByRole(
        token: String,
        fingerprint: String,
        objectId: String,
        binding: ActivityHomeBinding
    ): LiveData<ApiResponse<List<ObjectModel>>> {
        val result = MutableLiveData<ApiResponse<List<ObjectModel>>>()
        viewModelScope.launch(Dispatchers.IO) {
            val response = repository.permissionListByRole(token, fingerprint, objectId)
            if (response.successful) {


                val listObjectModel: List<ObjectModel> = response.data!!

                listObjectModel.forEach { objectModel ->

                    withContext(Dispatchers.Main) {
                        when (objectModel.code) {
                            Const.NAV_VISIT -> binding.navView.menu.findItem(R.id.btnVisit).isVisible = true
                            Const.NAV_PENDING -> binding.navView.menu.findItem(R.id.btnPending).isVisible = true
                            Const.NAV_SALEORDER -> binding.navView.menu.findItem(R.id.btnSaleOrder).isVisible = true
                            Const.NAV_HOME -> binding.navView.menu.findItem(R.id.btnHome).isVisible = true
                            Const.NAV_QUERY -> binding.navView.menu.findItem(R.id.btnQuery).isVisible = true
                            Const.NAV_BUSINESSPARTNER -> binding.navView.menu.findItem(R.id.btnClient).isVisible = true
                            Const.NAV_ITEM -> {binding.navView.menu.findItem(R.id.btnItem).isVisible = true
                                binding.navView.menu.findItem(R.id.btnCashier).isVisible = true}
                            Const.NAV_PROFILE -> binding.btnSetting.visibility = View.VISIBLE
                            Const.NAV_EXIT -> binding.btnLogOut.visibility = View.VISIBLE
                            Const.NAV_APPROVAL -> binding.navView.menu.findItem(R.id.btnApproval).isVisible = true
                            //GG
                            //Const.NAV_CASHIER -> binding.navView.menu.findItem(R.id.btnCashier).isVisible = true
                            //GG
                        }
                        print(objectModel.code)
                    }
                    val objectEntity = ObjectEntity()
                    objectEntity.code = objectModel.code
                    objectEntity.description = if (objectModel.description == null) "" else objectModel.description
                    objectEntity.id = objectModel.id
                    objectEntity.name = objectModel.name
                    insert(objectEntity)
                }


            }
            result.postValue(response)
        }
        return result
    }

    private fun insert(objectEntity: ObjectEntity) {
        repository.insert(objectEntity)
    }


    fun list(binding: ActivityHomeBinding) {
        viewModelScope.launch(Dispatchers.IO) {
            val listObject = repository.list()
            listObject.forEach { objectEntity ->
                withContext(Dispatchers.Main) {
                    when (objectEntity.code) {
                        Const.NAV_VISIT -> binding.navView.menu.findItem(R.id.btnVisit).isVisible = true
                        Const.NAV_PENDING -> binding.navView.menu.findItem(R.id.btnPending).isVisible = true
                        Const.NAV_SALEORDER -> binding.navView.menu.findItem(R.id.btnSaleOrder).isVisible = true
                        Const.NAV_HOME -> binding.navView.menu.findItem(R.id.btnHome).isVisible = true
                        Const.NAV_QUERY -> binding.navView.menu.findItem(R.id.btnQuery).isVisible = true
                        Const.NAV_BUSINESSPARTNER -> binding.navView.menu.findItem(R.id.btnClient).isVisible = true
                        Const.NAV_ITEM -> {binding.navView.menu.findItem(R.id.btnItem).isVisible = true
                            binding.navView.menu.findItem(R.id.btnCashier).isVisible = true}
                        Const.NAV_PROFILE -> binding.btnSetting.visibility = View.VISIBLE
                        Const.NAV_EXIT -> binding.btnLogOut.visibility = View.VISIBLE
                        Const.NAV_APPROVAL -> binding.navView.menu.findItem(R.id.btnApproval).isVisible = true
                        //GG
                        //Const.NAV_CASHIER -> binding.navView.menu.findItem(R.id.btnCashier).isVisible = true
                        //GG
                    }
                }
            }
            PopUp.closeProgressDialog()
        }
    }

    fun list(binding: FragmentHomeBinding) {
        viewModelScope.launch(Dispatchers.IO) {
            val listObject = repository.list()
            listObject.forEach { objectEntity ->
                withContext(Dispatchers.Main) {
                    when (objectEntity.code) {
                        Const.CV_ORDER -> binding.cvOrder.visibility = View.VISIBLE
                        Const.CV_VISIT -> binding.cvVisit.visibility = View.VISIBLE
                        Const.CV_BUSINESSPARTNER -> binding.cvBusinessPartner.visibility = View.VISIBLE
                        Const.CV_ITEM -> {binding.cvItem.visibility = View.VISIBLE
                        binding.cvCashier.visibility = View.VISIBLE}
                        Const.CV_QUERY -> binding.cvQuery.visibility = View.VISIBLE
                        Const.CV_PENDING -> binding.cvPending.visibility = View.VISIBLE
                        Const.CV_PROFILE -> binding.cvProfile.visibility = View.VISIBLE
                        Const.CV_EXIT -> binding.cvSingOut.visibility = View.VISIBLE
                        Const.CV_APPROVAL -> binding.cvApproval.visibility = View.VISIBLE
                        //GG
                        //Const.CV_CASHIER -> binding.cvCashier.visibility = View.VISIBLE
                        //GG
                    }
                }
            }

            withContext(Dispatchers.Main) {
                val elements = ArrayList<View>()
                for (i in 0 until binding.glHome.childCount) {
                    if (binding.glHome.getChildAt(i).visibility == View.GONE) {
                        elements.add(binding.glHome.getChildAt(i))
                    }
                }
                elements.forEach { element ->
                    binding.glHome.removeView(element)
                }
            }
        }
    }

    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAll()
        }
    }

}