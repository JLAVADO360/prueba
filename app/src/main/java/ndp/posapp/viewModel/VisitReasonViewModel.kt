package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.VisitReasonEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.VisitReasonModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.VisitReasonRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class VisitReasonViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: VisitReasonRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<VisitReasonModel>>>>()

    init {
        val visitReasonDao = PosAppDb.getDatabase(application, viewModelScope).visitReasonDao()
        repository = VisitReasonRepository(visitReasonDao)
    }

    fun listVisitReason(loggerUserModel: LoggerUserModel, page: Int, syncType: Int) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listVisitReason(signature, token, fingerprint, entity, repositoryApp, page, syncType)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listVisitReason = apiPaginationResponse.data!!.value
                    if (listVisitReason != null && listVisitReason.isNotEmpty()) {
                        listVisitReason.forEach {
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val visitReason = VisitReasonEntity()
                            visitReason.code = it.code
                            visitReason.reason = it.reason
                            visitReason.status = it.status
                            insert(visitReason)
                        }
                        if (nextPage != null) {
                            listVisitReason(loggerUserModel, page, syncType)
                        } else {
                            result.postValue(Event(apiPaginationResponse))
                            PopUp.onUpdateProgressDialogCount(0, 0)
                        }
                    }
                }
            } else {
                apiPaginationResponse.successful = true //TODO VERIFICAR CAIDA SER SERVICIO SYNCRONIZACION PARCIAL
                result.postValue(Event(apiPaginationResponse))

            }
        }
    }


    fun insert(visitReasonEntity: VisitReasonEntity) {
        repository.insert(visitReasonEntity)
    }


}