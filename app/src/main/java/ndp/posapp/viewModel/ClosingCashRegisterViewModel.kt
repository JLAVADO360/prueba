package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.*
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.ClosingCashRegisterRepository
import ndp.posapp.repository.GenerateRepository

class ClosingCashRegisterViewModel(application: Application) : AndroidViewModel(application) {
    private val repository : ClosingCashRegisterRepository
    var result = MutableLiveData<ApiResponse<ClosingCashRegisterSaveModel>>()

    init {
        val closingCashRegisterDAO = PosAppDb.getDatabase(application, viewModelScope).closingCashRegisterDAO()
        repository = ClosingCashRegisterRepository(closingCashRegisterDAO)
    }

//    fun closingCashRegister(
//        loggerUserModel: LoggerUserModel,
//        closingCashRegisterSaveModel: ClosingCashRegisterSaveModel
//    ):ApiResponse<ClosingCashRegisterSaveModel> {
//        var apiResponse:ApiResponse<ClosingCashRegisterSaveModel> = ApiResponse()
//        viewModelScope.launch(Dispatchers.IO) {
//            val signature = loggerUserModel.signature
//            val token = loggerUserModel.token
//            val fingerprint = loggerUserModel.fingerPrint
//            val entity = loggerUserModel.entity
//            apiResponse = repository.closingCashRegister(entity, fingerprint, signature, token,closingCashRegisterSaveModel)
//            result.postValue(apiResponse)
//        }
//        return apiResponse
//    }

    fun closingCashRegister(
        loggerUserModel: LoggerUserModel,
        closingCashRegisterSaveModel: ClosingCashRegisterSaveModel,
        callback: (ApiResponse<ClosingCashRegisterSaveModel>) -> Unit
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            var apiResponse:ApiResponse<ClosingCashRegisterSaveModel>
            withContext(Dispatchers.IO) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                apiResponse = repository.closingCashRegister(entity, fingerprint, signature, token, closingCashRegisterSaveModel)
            }
            callback(apiResponse)
        }
    }

}