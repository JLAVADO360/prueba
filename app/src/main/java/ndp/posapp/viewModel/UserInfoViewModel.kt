package ndp.posapp.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.UserInfoModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.UserInfoRepository

class UserInfoViewModel : ViewModel() {

    private val userInfoRepository = UserInfoRepository()


    fun getUserInfo(
        loggerUserModel: LoggerUserModel
    ): LiveData<ApiResponse<UserInfoModel>> {
        val result = MutableLiveData<ApiResponse<UserInfoModel>>()
        val signature = loggerUserModel.signature
        val token = loggerUserModel.token
        val fingerprint = loggerUserModel.fingerPrint
        val entity = loggerUserModel.entity
        val userIdentifier = loggerUserModel.userIdentifier
        viewModelScope.launch (Dispatchers.IO){
            result.postValue(userInfoRepository.getUserInfo(signature, token, fingerprint, entity, userIdentifier))
        }
        return result
    }
}