package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.BalanceListCashierModel
import ndp.posapp.data.model.ClosingListCashierModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.BalanceListCashierRepository
import ndp.posapp.repository.ClosingListCashierRepository
import ndp.posapp.utils.event.Event

class BalanceListCashierViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: BalanceListCashierRepository

    var resultBalanceListCashier = MutableLiveData<Event<ApiPaginationResponse<List<BalanceListCashierModel>>>>()

    init {
        val balanceListCashierDao = PosAppDb.getDatabase(application, viewModelScope).balanceListCashierDao()
        repository = BalanceListCashierRepository(balanceListCashierDao)
    }

    fun listBalanceListCashier(
        loggerUserModel: LoggerUserModel,
        codclient: String,
        startDate: String,
        endDate: String,
        page: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity

            val apiPaginationResponse = repository.BalanceListCashier(entity, fingerprint, signature, token, codclient, startDate, endDate, page)
            resultBalanceListCashier.postValue(Event(apiPaginationResponse))
        }
    }
}