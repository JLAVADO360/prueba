package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.BusinessPartnerAddressEntity
import ndp.posapp.data.db.entity.BusinessPartnerEntity
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.BusinessPartnerModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.BusinessPartnerRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class BusinessPartnerViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: BusinessPartnerRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<BusinessPartnerModel>>>>()


    init {
        val businessPartnerDao = PosAppDb.getDatabase(application, viewModelScope).businessPartnerDao()
        repository = BusinessPartnerRepository(businessPartnerDao)
    }

    fun listBusinessPartner(loggerUserModel: LoggerUserModel, page: Int, syncType: Int, businessPartnerAddressViewModel: BusinessPartnerAddressViewModel) {

        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listBusinessPartner(signature, token, fingerprint, entity, repositoryApp, page, syncType)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listBusinessPartner = apiPaginationResponse.data!!.value
                    if (listBusinessPartner != null && listBusinessPartner.isNotEmpty()) {
                        listBusinessPartner.forEach { businessPartnerModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val businessPartner = BusinessPartnerEntity()
                            businessPartner.code = businessPartnerModel.code
                            businessPartner.company = businessPartnerModel.company ?: loggerUserModel.entity
                            businessPartner.alternativeEmail = businessPartnerModel.alternativeEmail
                            businessPartner.balanceSys = businessPartnerModel.balanceSys
                            businessPartner.businessName = businessPartnerModel.businessName
                            businessPartner.comment = businessPartnerModel.comment
                            businessPartner.commercialName = businessPartnerModel.commercialName
                            businessPartner.createDate = businessPartnerModel.createDate
                            businessPartner.createUser = businessPartnerModel.createUser
                            businessPartner.creditBalance = businessPartnerModel.creditBalance
                            businessPartner.creditLine = businessPartnerModel.creditLine
                            businessPartner.currency = businessPartnerModel.currency
                            businessPartner.email = businessPartnerModel.email
                            businessPartner.hasBusiness = businessPartnerModel.hasBusiness
                            businessPartner.lastNameF = businessPartnerModel.lastNameF
                            businessPartner.lastNameM = businessPartnerModel.lastNameM
                            businessPartner.logicStatus = businessPartnerModel.logicStatus
                            businessPartner.migration = businessPartnerModel.migration
                            businessPartner.migrationDate = businessPartnerModel.migrationDate
                            businessPartner.migrationMessage = businessPartnerModel.migrationMessage
                            businessPartner.migrationStatus = businessPartnerModel.migrationStatus
                            businessPartner.names = businessPartnerModel.names
                            businessPartner.nif = businessPartnerModel.nif
                            businessPartner.partnerType = businessPartnerModel.partnerType
                            businessPartner.paymentCondition = businessPartnerModel.paymentCondition
                            businessPartner.personType = businessPartnerModel.personType
                            businessPartner.phoneNumber = businessPartnerModel.phoneNumber
                            businessPartner.priceList = businessPartnerModel.priceList
                            businessPartner.recordType = businessPartnerModel.recordType
                            businessPartner.sunatAffectationType = businessPartnerModel.sunatAffectationType
                            businessPartner.sunatOneroso = businessPartnerModel.sunatOneroso
                            businessPartner.sunatOperation = businessPartnerModel.sunatOperation
                            businessPartner.sunatOperationType = businessPartnerModel.sunatOperationType
                            businessPartner.updateDate = businessPartnerModel.updateDate
                            businessPartner.updateUser = businessPartnerModel.updateUser
                            insert(businessPartner)
                            if (businessPartnerModel.addresses.isNotEmpty()) {
                                businessPartnerModel.addresses.forEach { businessPartnerAddressModel ->
                                    val businessPartnerAddress = BusinessPartnerAddressEntity()
                                    businessPartnerAddress.codeBusinessPartner = businessPartnerModel.code
                                    businessPartnerAddress.company = businessPartnerModel.company ?: loggerUserModel.entity
                                    businessPartnerAddress.address = businessPartnerAddressModel.address
                                    businessPartnerAddress.addressName = businessPartnerAddressModel.addressName
                                    businessPartnerAddress.country = businessPartnerAddressModel.country
                                    businessPartnerAddress.createDate = businessPartnerAddressModel.createDate
                                    businessPartnerAddress.department = businessPartnerAddressModel.department
                                    businessPartnerAddress.district = businessPartnerAddressModel.district
                                    businessPartnerAddress.invoicesCountry = businessPartnerAddressModel.invoicesCountry
                                    businessPartnerAddress.logicStatus = businessPartnerAddressModel.logicStatus
                                    businessPartnerAddress.province = businessPartnerAddressModel.province
                                    businessPartnerAddress.rowNum = businessPartnerAddressModel.rowNum
                                    businessPartnerAddress.status = businessPartnerAddressModel.status
                                    businessPartnerAddress.type = businessPartnerAddressModel.type
                                    businessPartnerAddress.ubiquitous = businessPartnerAddressModel.ubiquitous
                                    businessPartnerAddressViewModel.insert(businessPartnerAddress)

                                }
                            }

                        }

                        if (nextPage != null) {
                            val nextPageValue = nextPage.split("&")[0].replace("page=", "").toInt()
                            listBusinessPartner(loggerUserModel, nextPageValue, syncType, businessPartnerAddressViewModel)
                        } else {
                            result.postValue(Event(apiPaginationResponse))
                            PopUp.onUpdateProgressDialogCount(0, 0)
                        }

                    }
                }
            } else {
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }

    fun getBusinessPartner(
        loggerUserModel: LoggerUserModel,
        nif: String,
        callback: (ApiPaginationResponse<List<BusinessPartnerModel>>) -> Unit
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            callback(repository.getBusinessPartner(signature, token, fingerprint, entity, nif))
        }
    }

    fun postBusinessPartner(
        loggerUserModel: LoggerUserModel,
        businessPartnerDTO: BusinessPartnerModel,
        businessPartnerAddressViewModel: BusinessPartnerAddressViewModel,
        callback: (ApiResponse<BusinessPartnerModel>) -> Unit
    ) {
        viewModelScope.launch {
            var apiResponse: ApiResponse<BusinessPartnerModel>
            withContext(Dispatchers.IO) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                apiResponse = repository.postBusinessPartner(signature, token, fingerprint, entity, businessPartnerDTO)

                if (apiResponse.successful && apiResponse.data != null) {
                    val businessPartnerModel = apiResponse.data!!
                    val businessPartner = BusinessPartnerEntity()
                    businessPartner.code = businessPartnerModel.code
                    businessPartner.alternativeEmail = businessPartnerModel.alternativeEmail
                    businessPartner.balanceSys = businessPartnerModel.balanceSys
                    businessPartner.businessName = businessPartnerModel.businessName
                    businessPartner.comment = businessPartnerModel.comment
                    businessPartner.commercialName = businessPartnerModel.commercialName
                    businessPartner.createDate = businessPartnerModel.createDate
                    businessPartner.createUser = businessPartnerModel.createUser
                    businessPartner.creditBalance = businessPartnerModel.creditBalance
                    businessPartner.creditLine = businessPartnerModel.creditLine
                    businessPartner.currency = businessPartnerModel.currency
                    businessPartner.email = businessPartnerModel.email
                    businessPartner.hasBusiness = businessPartnerModel.hasBusiness
                    businessPartner.lastNameF = businessPartnerModel.lastNameF
                    businessPartner.lastNameM = businessPartnerModel.lastNameM
                    businessPartner.logicStatus = businessPartnerModel.logicStatus
                    businessPartner.migration = businessPartnerModel.migration
                    businessPartner.migrationDate = businessPartnerModel.migrationDate
                    businessPartner.migrationMessage = businessPartnerModel.migrationMessage
                    businessPartner.migrationStatus = businessPartnerModel.migrationStatus
                    businessPartner.names = businessPartnerModel.names
                    businessPartner.nif = businessPartnerModel.nif
                    businessPartner.partnerType = businessPartnerModel.partnerType
                    businessPartner.paymentCondition = businessPartnerModel.paymentCondition
                    businessPartner.personType = businessPartnerModel.personType
                    businessPartner.phoneNumber = businessPartnerModel.phoneNumber
                    businessPartner.priceList = businessPartnerModel.priceList
                    businessPartner.recordType = businessPartnerModel.recordType
                    businessPartner.sunatAffectationType = businessPartnerModel.sunatAffectationType
                    businessPartner.sunatOneroso = businessPartnerModel.sunatOneroso
                    businessPartner.sunatOperation = businessPartnerModel.sunatOperation
                    businessPartner.sunatOperationType = businessPartnerModel.sunatOperationType
                    businessPartner.updateDate = businessPartnerModel.updateDate
                    businessPartner.updateUser = businessPartnerModel.updateUser
                    insert(businessPartner)
                    if (businessPartnerModel.addresses.isNotEmpty()) {
                        businessPartnerModel.addresses.forEach { businessPartnerAddressModel ->
                            val businessPartnerAddress = BusinessPartnerAddressEntity()
                            businessPartnerAddress.codeBusinessPartner = businessPartnerModel.code
                            businessPartnerAddress.address = businessPartnerAddressModel.address
                            businessPartnerAddress.addressName = businessPartnerAddressModel.addressName
                            businessPartnerAddress.country = businessPartnerAddressModel.country
                            businessPartnerAddress.createDate = businessPartnerAddressModel.createDate
                            businessPartnerAddress.department = businessPartnerAddressModel.department
                            businessPartnerAddress.district = businessPartnerAddressModel.district
                            businessPartnerAddress.invoicesCountry = businessPartnerAddressModel.invoicesCountry
                            businessPartnerAddress.logicStatus = businessPartnerAddressModel.logicStatus
                            businessPartnerAddress.province = businessPartnerAddressModel.province
                            businessPartnerAddress.rowNum = businessPartnerAddressModel.rowNum
                            businessPartnerAddress.status = businessPartnerAddressModel.status
                            businessPartnerAddress.type = businessPartnerAddressModel.type
                            businessPartnerAddress.ubiquitous = businessPartnerAddressModel.ubiquitous
                            businessPartnerAddressViewModel.insert(businessPartnerAddress)
                        }
                    }
                }
            }
            callback(apiResponse)
        }
    }

    fun consultDNI(loggerUserModel: LoggerUserModel, dni: String, callback: (ApiResponse<JsonObject>) -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            callback(repository.consultDNI(signature, token, fingerprint, entity, dni))
        }
    }

    fun consultRUC(loggerUserModel: LoggerUserModel, ruc: String, callback: (ApiResponse<JsonObject>) -> Unit) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            callback(repository.consultRUC(signature, token, fingerprint, entity, ruc))
        }
    }



    fun insert(businessPartnerEntity: BusinessPartnerEntity) {
        repository.insert(businessPartnerEntity)
    }

    fun list(company: String): LiveData<List<BusinessPartnerView>> {
        return repository.list(company)
    }

}