package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.RegionRelationEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.RegionRelationModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.RegionRelationRepository
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class RegionRelationViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: RegionRelationRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<RegionRelationModel>>>>()

    init {
        val regionRelationDao = PosAppDb.getDatabase(application, viewModelScope).regionRelationDao()
        repository = RegionRelationRepository(regionRelationDao)
    }


    fun listRegionRelation(
        loggerUserModel: LoggerUserModel,
        page: Int,
        syncType: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {

 //           if (countRegionRelation()==Const.NUMBER_OF_REGION_RELATIONS) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                val repositoryApp = loggerUserModel.fingerPrint

                val apiPaginationResponse = repository.listRegionRelation(signature, token, fingerprint, entity, repositoryApp, page, syncType)
                if (apiPaginationResponse.successful) {
                    if (apiPaginationResponse.data != null) {
                        val nextPage = apiPaginationResponse.data!!.nextPage
                        val count = apiPaginationResponse.data!!.count
                        val listRegionRelation = apiPaginationResponse.data!!.value
                        if (listRegionRelation != null && listRegionRelation.isNotEmpty()) {
                            listRegionRelation.forEach { regionRelationModel ->
                                index++
                                PopUp.onUpdateProgressDialogCount(count, index)
                                val regionRelation = RegionRelationEntity()
                                regionRelation.childrenRegion = regionRelationModel.childrenRegion
                                regionRelation.country = regionRelationModel.country
                                regionRelation.parentRegion = regionRelationModel.parentRegion
                                regionRelation.active = regionRelationModel.active
                                insert(regionRelation)

                            }

                            if (nextPage != null) {
                                val nextPageValue = nextPage.split("&")[0].replace("page=", "").toInt()
                                listRegionRelation(loggerUserModel, nextPageValue, syncType)
                            } else {
                                result.postValue(Event(apiPaginationResponse))
                                PopUp.onUpdateProgressDialogCount(0, 0)
                            }

                        }
                    }
                } else {
                    apiPaginationResponse.successful = true
                    result.postValue(Event(apiPaginationResponse))
                }
//            } else {
//                result.postValue(Event(ApiPaginationResponse<List<RegionRelationModel>>().apply { successful = true }))
//            }
        }
    }

    /**DATABASE **/
    fun insert(regionRelationEntity: RegionRelationEntity) {
        repository.insert(regionRelationEntity)
    }

    fun list(parentRegion : String?) : List<String>{
        return repository.list(parentRegion)
    }

    private fun countRegionRelation(): Int {
        return repository.countRegionRelation()
    }
}