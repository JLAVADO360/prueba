package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.BalanceListCashierModel
import ndp.posapp.data.model.CashRegisterBalanceDTOModel
import ndp.posapp.data.model.CashRegisterBalanceModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.BalanceListCashierRepository
import ndp.posapp.repository.CashRegisterBalanceRepository
import ndp.posapp.utils.event.Event

class CashRegisterBalanceViewModel(application: Application) : AndroidViewModel(application)
{
    private val repository: CashRegisterBalanceRepository

    var resultCashRegisterBalanceModel = MutableLiveData<Event<ApiPaginationResponse<List<CashRegisterBalanceModel>>>>()

    init {
        val cashRegisterBalanceDao = PosAppDb.getDatabase(application, viewModelScope).cashRegisterBalanceDao()
        repository = CashRegisterBalanceRepository(cashRegisterBalanceDao)
    }

    fun cashRegisterBalance(
        loggerUserModel: LoggerUserModel,
        cashRegisterBalanceDTOModel: CashRegisterBalanceDTOModel
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity

            val apiPaginationResponse = repository.CashRegisterBalance(entity, fingerprint, signature, token, cashRegisterBalanceDTOModel)
            resultCashRegisterBalanceModel.postValue(Event(apiPaginationResponse))
        }
    }
}
