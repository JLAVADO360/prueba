package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.ClosingListCashierModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.ClosingListCashierRepository
import ndp.posapp.repository.OpeningListCashierRepository
import ndp.posapp.utils.event.Event

class ClosingListCashierViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ClosingListCashierRepository

    var resultClosingListCashier = MutableLiveData<Event<ApiPaginationResponse<List<ClosingListCashierModel>>>>()

    init {
        val closingListCashierDao = PosAppDb.getDatabase(application, viewModelScope).closingListCashierDao()
        repository = ClosingListCashierRepository(closingListCashierDao)
    }

    fun listClosingListCashier(
        loggerUserModel: LoggerUserModel,
        codclient: String,
        startDate: String,
        endDate: String,
        page: Int,
        itemPerPage: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val apiPaginationResponse = repository.ClosingListCashier(entity, fingerprint, signature, token, codclient, startDate, endDate, page,itemPerPage)
            resultClosingListCashier.postValue(Event(apiPaginationResponse))
        }
    }
}