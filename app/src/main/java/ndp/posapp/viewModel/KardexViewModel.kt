package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.KardexEntity
import ndp.posapp.repository.KardexRepository

class KardexViewModel(application: Application) : AndroidViewModel(application){

    private val repository : KardexRepository
    init {
        val kardexDao = PosAppDb.getDatabase(application,viewModelScope).kardexDao()
        repository = KardexRepository(kardexDao)
    }

    fun insert(kardexEntity: KardexEntity){
        repository.insert(kardexEntity)
    }

    fun list(company: String, warehouse: String, item: String): List<KardexEntity> {
        return repository.list(company, warehouse, item)
    }
}