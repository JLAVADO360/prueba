package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.PriceListEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.PriceListModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.PriceListRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class PriceListViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: PriceListRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<PriceListModel>>>>()

    init {
        val priceListDao = PosAppDb.getDatabase(application, viewModelScope).priceListDao()
        repository = PriceListRepository(priceListDao)
    }


    fun listPriceList(loggerUserModel: LoggerUserModel, page: Int, syncType: Int) {

        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listPriceList(signature, token, fingerprint, entity, repositoryApp, page, syncType)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listPriceList = apiPaginationResponse.data!!.value
                    if (listPriceList != null && listPriceList.isNotEmpty()) {
                        listPriceList.forEach { priceListModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val priceList = PriceListEntity()
                            priceList.code = priceListModel.code
                            priceList.company = priceListModel.company ?: loggerUserModel.entity
                            priceList.createDate = priceListModel.createDate
                            priceList.createUser = priceListModel.createUser
                            priceList.currency = priceListModel.currency
                            priceList.description = priceListModel.description
                            priceList.item = priceListModel.item
                            priceList.logicStatus = priceListModel.logicStatus
                            priceList.name = priceListModel.name
                            priceList.price = priceListModel.price
                            priceList.status = priceListModel.status
                            priceList.type = priceListModel.type
                            priceList.updateDate = priceListModel.updateDate
                            priceList.updateUser = priceListModel.updateUser
                            insert(priceList)
                        }

                        if (nextPage != null) {
                            val nextPageValue = nextPage.split("&")[0].replace("page=", "").toInt()
                            listPriceList(loggerUserModel, nextPageValue, syncType)
                        } else {
                            result.postValue(Event(apiPaginationResponse))
                            PopUp.onUpdateProgressDialogCount(0, 0)
                        }

                    }
                }
            } else {
                if (apiPaginationResponse.code == "ERROR_PRICELIST_NOT_FOUND") apiPaginationResponse.successful = true
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }


    fun insert(priceListEntity: PriceListEntity) {
        repository.insert(priceListEntity)
    }
    fun get(company: String, code: Int) : LiveData<PriceListEntity> {
        return repository.get(company, code)
    }
}