package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.ZoneEntity
import ndp.posapp.data.db.entity.ZoneUserEntity
import ndp.posapp.data.db.entity.ZoneWarehouseEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.ZoneModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.ZoneRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event


class ZoneViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ZoneRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<ZoneModel>>>>()


    init {
        val zoneDao = PosAppDb.getDatabase(application, viewModelScope).zoneDao()
        repository = ZoneRepository(zoneDao)
    }

    fun listZone(loggerUserModel: LoggerUserModel, page: Int, syncType: Int, zoneUserViewModel: ZoneUserViewModel, zoneWarehouseViewModel: ZoneWarehouseViewModel) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint
            val user = loggerUserModel.userIdentifier

            val apiPaginationResponse = repository.listZone(signature, token, fingerprint, entity, repositoryApp, page, syncType, user)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listZone = apiPaginationResponse.data!!.value
                    if (listZone != null && listZone.isNotEmpty()) {
                        listZone.forEach { zoneModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val zone = ZoneEntity()
                            zone.code = zoneModel.code
                            zone.createDate = zoneModel.createDate
                            zone.createUser = zoneModel.createUser
                            zone.description = zoneModel.description
                            zone.status = zoneModel.status
                            zone.updateDate = zoneModel.updateDate
                            zone.updateUser = zoneModel.updateUser
                            zone.zone = zoneModel.zone
                            insert(zone)
                            zoneModel.users.forEach { zoneUserModel ->
                                val zoneUser = ZoneUserEntity()
                                zoneUser.relationCode = zoneUserModel.relationCode
                                zoneUser.createUser = zoneUserModel.createUser
                                zoneUser.relationCreateDate = zoneUserModel.relationCreateDate
                                zoneUser.relationUpdateDate = zoneUserModel.relationUpdateDate
                                zoneUser.status = zoneUserModel.status
                                zoneUser.updateUser = zoneUserModel.updateUser
                                zoneUser.user = zoneUserModel.user
                                zoneUserViewModel.insert(zoneUser)
                            }
                            zoneModel.warehouses.forEach { zoneWarehouseModel ->
                                val zoneWarehouse = ZoneWarehouseEntity()
                                zoneWarehouse.relationCode = zoneWarehouseModel.relationCode
                                zoneWarehouse.createUser = zoneWarehouseModel.createUser
                                zoneWarehouse.relationCreateDate = zoneWarehouseModel.relationCreateDate
                                zoneWarehouse.relationUpdateDate = zoneWarehouseModel.relationUpdateDate
                                zoneWarehouse.status = zoneWarehouseModel.status
                                zoneWarehouse.updateUser = zoneWarehouseModel.updateUser
                                zoneWarehouse.warehouse = zoneWarehouseModel.warehouse
                                zoneWarehouseViewModel.insert(zoneWarehouse)
                            }
                        }
                    }
                    if (nextPage != null) {
                        listZone(loggerUserModel, page, syncType, zoneUserViewModel, zoneWarehouseViewModel)
                    } else {
                        result.postValue(Event(apiPaginationResponse))
                        PopUp.onUpdateProgressDialogCount(0, 0)
                    }
                }
            } else {
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }


    fun insert(zoneEntity: ZoneEntity) {
        repository.insert(zoneEntity)
    }

}