package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.SaleOrderEntity
import ndp.posapp.data.model.*
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.data.util.mapper.toModel
import ndp.posapp.repository.SaleOrderRepository
import ndp.posapp.utils.event.Event

class SaleOrderViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: SaleOrderRepository

    var resultSaleOrder = MutableLiveData<Event<ApiPaginationResponse<List<SaleOrderModel>>>>()
    var resultPaymentReceipt = MutableLiveData<Event<ApiPaginationResponse<List<PaymentReceiptModel>>>>()

    init {
        val saleOrderDao = PosAppDb.getDatabase(application, viewModelScope).saleOrderDao()
        repository = SaleOrderRepository(saleOrderDao)
    }

    fun listSaleOrder(
        loggerUserModel: LoggerUserModel,
        codclient: String,
        startDate: String,
        endDate: String,
        page: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity

            val apiPaginationResponse = repository.listSaleOrder(entity, fingerprint, signature, token, codclient, startDate, endDate, page)
            resultSaleOrder.postValue(Event(apiPaginationResponse))
        }
    }

    fun listPaymentReceipt(
        loggerUserModel: LoggerUserModel,
        businessPartner: String,
        startDate: String,
        endDate: String,
        page: Int
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val store = loggerUserModel.store!!

            val apiPaginationResponse = repository.listPaymentReceipt(entity, fingerprint, signature, token, businessPartner, startDate, endDate, store, page)
            resultPaymentReceipt.postValue(Event(apiPaginationResponse))
        }
    }

    fun getPaymentReceipt(
        loggerUserModel: LoggerUserModel,
        code: String,
        callback: (ApiPaginationResponse<List<PaymentReceiptModel>>) -> Unit
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                repository.getPaymentReceipt(entity, fingerprint, signature, token, code)
            }.also {
                callback(it)
            }
        }
    }

    fun postSaleOrder(
        loggerUserModel: LoggerUserModel,
        saleOrderDTO: SaleOrderModel,
        callback: (ApiResponse<SaleOrderModel>) -> Unit
    ) {
        viewModelScope.launch {
            var apiResponse: ApiResponse<SaleOrderModel>
            withContext(Dispatchers.IO) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                apiResponse = repository.postSaleOrder(entity, fingerprint, signature, token, saleOrderDTO)
            }
            callback(apiResponse)
        }
    }

    fun postQuickSale(
        loggerUserModel: LoggerUserModel,
        saleOrderDTO: SaleOrderModel,
        callback: (ApiResponse<SaleOrderModel>) -> Unit
    ) {
        viewModelScope.launch {
            var apiResponse: ApiResponse<SaleOrderModel>
            withContext(Dispatchers.IO) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                apiResponse = repository.postQuickSale(entity, fingerprint, signature, token, saleOrderDTO)
            }
            callback(apiResponse)
        }
    }

    fun insert(saleOrderEntity: SaleOrderEntity): LiveData<Long> {
        val liveData = MutableLiveData<Long>()
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.insert(saleOrderEntity)
            }.also {
                liveData.value = it
            }
        }
        return liveData
    }

    fun update(saleOrderEntity: SaleOrderEntity) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.update(saleOrderEntity)
        }
    }

    fun get(internalId: Long): LiveData<SaleOrderModel> {
        val liveData = MutableLiveData<SaleOrderModel>()
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.get(internalId)
            }.also {
                liveData.value = it?.toModel()
            }
        }
        return liveData
    }

    fun list(internalStatus: Int): LiveData<List<SaleOrderEntity>> {
        val liveData = MutableLiveData<List<SaleOrderEntity>>()

        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.list(internalStatus)
            }.also {
                liveData.value = it
            }
        }
        return liveData
    }
}