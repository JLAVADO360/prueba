package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.TaxEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.TaxModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.TaxRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class TaxViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: TaxRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<TaxModel>>>>()

    init {
        val taxDao = PosAppDb.getDatabase(application, viewModelScope).taxDao()
        repository = TaxRepository(taxDao)
    }

    fun listTax(loggerUserModel: LoggerUserModel, page: Int, syncType: Int) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listTax(signature, token, fingerprint, entity, repositoryApp, page, syncType)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listTax = apiPaginationResponse.data!!.value
                    if (listTax != null && listTax.isNotEmpty()) {
                        listTax.forEach { taxModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val tax = TaxEntity()
                            tax.code = taxModel.code
                            tax.company = taxModel.company ?: loggerUserModel.entity
                            tax.createDate = taxModel.createDate
                            tax.createUser = taxModel.createUser
                            tax.logicStatus = taxModel.logicStatus
                            tax.rate = taxModel.rate
                            tax.status = taxModel.status
                            tax.tax = taxModel.tax
                            tax.updateDate = taxModel.updateDate
                            tax.updateUser = taxModel.updateUser
                            insert(tax)
                        }
                        if (nextPage != null) {
                            listTax(loggerUserModel, page, syncType)
                        } else {
                            result.postValue(Event(apiPaginationResponse))
                            PopUp.onUpdateProgressDialogCount(0, 0)
                        }
                    }
                }
            } else {
                if (apiPaginationResponse.code == "ERROR_TAX_NOT_FOUND") apiPaginationResponse.successful = true
                result.postValue(Event(apiPaginationResponse))
            }
        }

    }


    fun insert(taxEntity: TaxEntity) {
        repository.insert(taxEntity)
    }

    fun list(company: String) : LiveData<List<TaxEntity>> {
        return repository.list(company)
    }
}