package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.ZoneWarehouseEntity
import ndp.posapp.repository.ZoneWarehouseRepository

class ZoneWarehouseViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: ZoneWarehouseRepository

    init {
        val zoneWarehouseDao = PosAppDb.getDatabase(application, viewModelScope).zoneWarehouseDao()
        repository = ZoneWarehouseRepository(zoneWarehouseDao)
    }

    fun insert(zoneWarehouseEntity: ZoneWarehouseEntity) {
        repository.insert(zoneWarehouseEntity)
    }

    fun list() : List<ZoneWarehouseEntity>{
        return repository.list()
    }
}