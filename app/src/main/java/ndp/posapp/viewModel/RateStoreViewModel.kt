package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.*
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.RateStoreModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.RateStoreRepository

class RateStoreViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: RateStoreRepository

    var result = MutableLiveData<ApiPaginationResponse<List<RateStoreModel>>>()

    init {
        val rateStoreDao = PosAppDb.getDatabase(application, viewModelScope).rateStoreDao()
        repository = RateStoreRepository(rateStoreDao)
    }

    fun getRateStore(
        loggerUserModel: LoggerUserModel,
        status: Int,
        rateDate: String,
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val store = loggerUserModel.store!!

            val apiPaginationResponse = repository.getRateStore(signature, token, fingerprint, entity, status, rateDate, store)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val rateStoreList = apiPaginationResponse.data!!.value
                    if (rateStoreList != null && rateStoreList.isNotEmpty()) {
                        val rateStore = rateStoreList.first()
                        val rateStoreEntity = RateStoreEntity()
                        rateStoreEntity.code = rateStore.code
                        rateStoreEntity.company = rateStore.company ?: loggerUserModel.entity
                        rateStoreEntity.createDate = rateStore.createDate
                        rateStoreEntity.currencyFrom = rateStore.currencyFrom
                        rateStoreEntity.createDate = rateStore.createDate
                        rateStoreEntity.currencyTo = rateStore.currencyTo
                        rateStoreEntity.rate = rateStore.rate ?: 1.0
                        rateStoreEntity.rateDate = rateStore.rateDate
                        rateStoreEntity.status = rateStore.status ?: -1
                        rateStoreEntity.store = rateStore.store
                        insert(rateStoreEntity)
                        result.postValue(apiPaginationResponse)
                    } else {
                        result.postValue(apiPaginationResponse)
                    }
                }
            } else {
                result.postValue(apiPaginationResponse)
            }
        }
    }

    fun insert(rateStoreEntity: RateStoreEntity) {
        repository.insert(rateStoreEntity)
    }

    fun get(company: String, status: Int, rateDate: String, store: String): LiveData<List<RateStoreModel>> {
        val result = MutableLiveData<List<RateStoreModel>>()
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.get(company, status, rateDate, store)
            }.also { entityList ->
                val list: MutableList<RateStoreModel> = ArrayList()
                entityList.forEach {
                    list.add(
                        RateStoreModel(
                            code = it.code,
                            createDate = it.createDate,
                            currencyFrom = it.currencyFrom,
                            currencyTo = it.currencyTo,
                            rate = it.rate,
                            rateDate = it.rateDate,
                            status = it.status,
                            store = it.store
                        )
                    )
                }
                result.value = list
            }
        }
        return result
    }
}