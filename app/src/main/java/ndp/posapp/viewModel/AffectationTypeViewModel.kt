package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.AffectationTypeEntity
import ndp.posapp.data.model.AffectationTypeModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.AffectationTypeRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class AffectationTypeViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: AffectationTypeRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<AffectationTypeModel>>>>()

    init {
        val affectationTypeDao = PosAppDb.getDatabase(application, viewModelScope).affectationTypeDao()
        repository = AffectationTypeRepository(affectationTypeDao)
    }

    fun listAffectationType(loggerUserModel: LoggerUserModel) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint


            val apiPaginationResponse = repository.listAffectationType(signature, token, fingerprint, entity, repositoryApp)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listAffectationType = apiPaginationResponse.data!!.value
                    if (listAffectationType != null && listAffectationType.isNotEmpty()) {
                        listAffectationType.forEach {
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val affectationTypeEntity = AffectationTypeEntity()
                            affectationTypeEntity.code = it.code
                            affectationTypeEntity.company = it.company ?: loggerUserModel.entity
                            affectationTypeEntity.createDate = it.createDate
                            affectationTypeEntity.createUser = it.createUser
                            affectationTypeEntity.description = it.description
                            affectationTypeEntity.updateDate = it.updateDate
                            affectationTypeEntity.updateUser = it.updateUser
                            insert(affectationTypeEntity)
                        }
                    }
                    if (nextPage != null) {
                        listAffectationType(loggerUserModel)
                    } else {
                        result.postValue(Event(apiPaginationResponse))
                        PopUp.onUpdateProgressDialogCount(0, 0)
                    }
                }
            } else {
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }

    fun insert(affectationTypeEntity: AffectationTypeEntity) {
        repository.insert(affectationTypeEntity)
    }


}