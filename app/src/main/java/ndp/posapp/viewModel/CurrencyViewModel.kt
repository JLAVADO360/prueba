package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.CashRegisterInfoEntity
import ndp.posapp.data.db.entity.CurrencyEntity
import ndp.posapp.data.model.CurrencyModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.CurrencyRepository
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.Event

class CurrencyViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CurrencyRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<CurrencyModel>>>>()

    init {
        val currencyDao = PosAppDb.getDatabase(application, viewModelScope).currencyDao()
        repository = CurrencyRepository(currencyDao)
    }

    fun getCurrency(loggerUserModel: LoggerUserModel): LiveData<MutableList<CurrencyModel>>
    {
            val liveData = MutableLiveData<MutableList<CurrencyModel>>()
            viewModelScope.launch {
                withContext(Dispatchers.IO) {
                    repository.getCurrency(loggerUserModel.signature, loggerUserModel.token, loggerUserModel.fingerPrint, loggerUserModel.entity)
                }.also {
                    liveData.value = it
                }
            }
            return liveData

    }

    fun listCurrency(loggerUserModel: LoggerUserModel, page: Int, syncType: Int) {
        viewModelScope.launch(Dispatchers.IO) {

            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listCurrency(signature, token, fingerprint, entity, repositoryApp, page, syncType)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listCurrency = apiPaginationResponse.data!!.value
                    if (listCurrency != null && listCurrency.isNotEmpty()) {
                        listCurrency.forEach { currencyModel ->
                            index++
                            PopUp.onUpdateProgressDialogCount(count, index)
                            val currency = CurrencyEntity()
                            currency.code = currencyModel.code
                            currency.company = currencyModel.company ?: loggerUserModel.entity
                            currency.createDate = currencyModel.createDate
                            currency.createUser = currencyModel.createUser
                            currency.description = currencyModel.description
                            currency._isDefault = currencyModel.isDefault
                            currency.name = currencyModel.name
                            currency.status = currencyModel.status
                            currency.symbol = currencyModel.symbol
                            currency.updateDate = currencyModel.updateDate
                            currency.updateUser = currencyModel.updateUser
                            insert(currency)
                        }
                        if (nextPage != null) {
                            listCurrency(loggerUserModel, page, syncType)
                        } else {
                            result.postValue(Event(apiPaginationResponse))
                            PopUp.onUpdateProgressDialogCount(0, 0)
                        }
                    }
                }
            } else {
                if (apiPaginationResponse.code == "ERROR_CURRENCY_NOT_FOUND") apiPaginationResponse.successful = true
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }


    fun insert(currencyEntity: CurrencyEntity) {
        repository.insert(currencyEntity)
    }

}