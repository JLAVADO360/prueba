package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.CashRegisterInfoEntity
import ndp.posapp.data.model.CashRegisterInfoModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.CashRegisterRepository

class CashRegisterViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: CashRegisterRepository

    var result = MutableLiveData<ApiResponse<List<CashRegisterInfoModel>>>()

    init {
        val cashRegisterInfoDao = PosAppDb.getDatabase(application, viewModelScope).cashRegisterInfoDao()
        repository = CashRegisterRepository(cashRegisterInfoDao)
    }

    fun listCashRegisterInfo(loggerUserModel: LoggerUserModel) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val userIdentifier = loggerUserModel.userIdentifier

            val apiResponse = repository.listCashRegisterInfo(entity, fingerprint, signature, token, userIdentifier)

            if (apiResponse.successful && apiResponse.data?.isNotEmpty() == true) {
                apiResponse.data?.forEach {
                    val cashRegisterInfoEntity = CashRegisterInfoEntity().apply {
                        code = it.cashRegisterCode
                        company = loggerUserModel.entity
                        user = loggerUserModel.userIdentifier
                        assigned = it.cashRegisterAssigned
                        name = it.cashRegisterName ?: ""
                        status = it.cashRegisterStatus
                        type = it.cashRegisterType
                        openingCreationUser = it.openingCreationUser
                    }
                    insert(cashRegisterInfoEntity)
                }
            }

            result.postValue(apiResponse)
        }
    }


    fun insert(cashRegisterInfoEntity: CashRegisterInfoEntity) {
        repository.insert(cashRegisterInfoEntity)
    }

    fun get(company: String, user: String, type: String) : LiveData<CashRegisterInfoEntity?> {
        val liveData = MutableLiveData<CashRegisterInfoEntity?>()
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                repository.get(company, user, type)
            }.also {
                liveData.value = it
            }
        }
        return liveData
    }



}