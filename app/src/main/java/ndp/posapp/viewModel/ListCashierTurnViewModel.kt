package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.TurnModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.TurnRepository
import ndp.posapp.utils.event.Event

class ListCashierTurnViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: TurnRepository

    //var resultTurn = MutableLiveData<Event<ApiPaginationResponse<List<TurnModel>>>>()
    var resultTurn = MutableLiveData<List<TurnModel>>()

    init {
        val turnDao = PosAppDb.getDatabase(application, viewModelScope).turnDao()
        repository = TurnRepository(turnDao)
    }

    fun listTurnCashier(
        loggerUserModel: LoggerUserModel,
        dateTurn: String,
        cashRegisterCode: String,
        store: String,
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity

            val apiPaginationResponse = repository.ListTurn(entity, fingerprint, signature, token, dateTurn, cashRegisterCode, store)
            resultTurn.postValue(apiPaginationResponse)
        }
    }
}