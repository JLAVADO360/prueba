package ndp.posapp.viewModel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.PosAppDb
import ndp.posapp.data.db.entity.ReceiptTypeEntity
import ndp.posapp.data.model.ReceiptTypeModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.repository.ReceiptTypeRepository
import ndp.posapp.utils.event.Event

class ReceiptTypeViewModel(application: Application) : AndroidViewModel(application) {

    private val repository: ReceiptTypeRepository

    private var index: Int = 0
    var result = MutableLiveData<Event<ApiPaginationResponse<List<ReceiptTypeModel>>>>()

    init {
        val receiptTypeDao = PosAppDb.getDatabase(application, viewModelScope).receiptTypeDao()
        repository = ReceiptTypeRepository(receiptTypeDao)
    }

    fun listReceiptType(loggerUserModel: LoggerUserModel) {
        Log.e("valer00:","")
        viewModelScope.launch(Dispatchers.IO) {
            val signature = loggerUserModel.signature
            val token = loggerUserModel.token
            val fingerprint = loggerUserModel.fingerPrint
            val entity = loggerUserModel.entity
            val repositoryApp = loggerUserModel.fingerPrint

            val apiPaginationResponse = repository.listReceiptType(signature, token, fingerprint, entity, repositoryApp)
            if (apiPaginationResponse.successful) {
                if (apiPaginationResponse.data != null) {
                    val nextPage = apiPaginationResponse.data!!.nextPage
                    val count = apiPaginationResponse.data!!.count
                    val listReceiptType = apiPaginationResponse.data!!.value
                    if (listReceiptType != null && listReceiptType.isNotEmpty()) {
                        listReceiptType.forEach {
                            index++
                            val receiptTypeEntity = ReceiptTypeEntity()
                            receiptTypeEntity.code = it.code
                            receiptTypeEntity.company = it.company ?: loggerUserModel.entity
                            receiptTypeEntity.codeApplication = it.codeApplication
                            receiptTypeEntity.logicStatus = it.logicStatus
                            receiptTypeEntity.receipt = it.receipt
                            receiptTypeEntity.status = it.status
                            insert(receiptTypeEntity)
                        }
                    }
                    if (nextPage != null) {
                        listReceiptType(loggerUserModel)
                    } else {
                        result.postValue(Event(apiPaginationResponse))
                    }
                }
            } else {
                result.postValue(Event(apiPaginationResponse))
            }
        }
    }

    fun insert(receiptTypeEntity: ReceiptTypeEntity) {
        repository.insert(receiptTypeEntity)
    }

    fun list(company: String): LiveData<List<ReceiptTypeEntity>> {
        return repository.list(company)
    }

}