package ndp.posapp.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.PaymentModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.PaymentRepository

class PaymentViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = PaymentRepository()

    fun postQuickPayment(
        loggerUserModel: LoggerUserModel,
        paymentModelDTO: PaymentModel,
        callback: (ApiResponse<PaymentModel>) -> Unit
    ) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val signature = loggerUserModel.signature
                val token = loggerUserModel.token
                val fingerprint = loggerUserModel.fingerPrint
                val entity = loggerUserModel.entity
                repository.postQuickPayment(entity, fingerprint, signature, token, paymentModelDTO)
            }.also {
                callback(it)
            }
        }
    }
}