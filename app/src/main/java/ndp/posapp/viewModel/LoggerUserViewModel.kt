package ndp.posapp.viewModel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.model.MemberDTOModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.repository.LoggerUserRepository

class LoggerUserViewModel : ViewModel() {

    private val repository = LoggerUserRepository()
    fun login(email: String, signature: String, fingerprint: String, context: Context): LiveData<ApiResponse<MemberDTOModel>> {
        val result = MutableLiveData<ApiResponse<MemberDTOModel>>()
        viewModelScope.launch(Dispatchers.IO) {
            result.postValue(repository.login(email, signature, fingerprint, context))
        }
        return result
    }

}