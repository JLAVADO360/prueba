package ndp.posapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.BalanceListCashierModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.ActivityBalanceListCashierBinding
import ndp.posapp.databinding.ActivityClosingListCashierBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.DatePickerFragment
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.EventObserver
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.activity.QueryActivity
import ndp.posapp.view.adapter.BalanceListCashierAdapter
import ndp.posapp.view.adapter.ClosingListCashierAdapter
import ndp.posapp.viewModel.BalanceListCashierViewModel
import ndp.posapp.viewModel.ClosingListCashierViewModel
import java.util.*

class BalanceListCashierActivity : AppCompatActivity() {
    private lateinit var searchView: SearchView
    private lateinit var binding: ActivityBalanceListCashierBinding

    private lateinit var loggerUserModel: LoggerUserModel

    //private lateinit var sharedPref: SharedPreferences = getSharedPreferences("CashRegisterData", Context.MODE_PRIVATE)
    private lateinit var Code:String
    private lateinit var Name:String
    private lateinit var Company:String
    private lateinit var OeningCreationUser:String
    private lateinit var User:String
    private lateinit var Type:String
    private var Assigned:Int=-1
    private var Status:Int=-1

    private var currentBusinessPartner: BusinessPartnerView? = null
    private var from = ""
    private var to = ""

    private lateinit var balanceListCashierAdapter: BalanceListCashierAdapter

    /** VIEW MODEL **/
    private lateinit var balanceListCashierViewModel: BalanceListCashierViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityBalanceListCashierBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        initElements()
        GetCashREgisterData()
        val cal = Calendar.getInstance()
        val d = cal.get(Calendar.DAY_OF_MONTH)
        val m = cal.get(Calendar.MONTH)+1
        val y = cal.get(Calendar.YEAR)
        binding.IVSearchOSqLC.setOnClickListener()
        {
            from = binding.ETStartDateSC.text.toString().trim()
            to = binding.ETEndDateSC.text.toString().trim()

            /*val f1 = binding.ETStartDate.text.toString()
            val f2 = binding.ETEndDate.text.toString()

            val da1 = LocalDate.parse(f1, DateTimeFormatter.ISO_DATE)
            val da2 = LocalDate.parse(f2, DateTimeFormatter.ISO_DATE)
            //val d1 = LocalDate.parse(f1, formatter)
            //val d2 = LocalDate.parse(f2, formatter)
            from = da1.toString()
            to = da2.toString()*/
            initViewModel()
        }
        binding.IVCleanSqLC.setOnClickListener(){}
        binding.TVSqLCashier.setText(Name)
        var dd:String = d.toString()
        var mm:String = m.toString()
        if(d<10)
            dd = "0"+d.toString()
        if(m<10)
            mm = "0"+m.toString()
        binding.ETStartDateSC.setText("   "+y.toString()+"-"+mm+"-"+dd)
        binding.ETEndDateSC.setText("   "+y.toString()+"-"+mm+"-"+dd)
        binding.ETStartDateSC.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_calendar, 0, 0, 0);
        binding.ETEndDateSC.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_calendar, 0, 0, 0);
        binding.ETStartDateSC.setOnClickListener()
        {
            ShowStartDatePickerDialog()
        }
        binding.ETEndDateSC.setOnClickListener()
        {
            ShowEndDatePickerDialog()
        }
    }
    private fun ShowStartDatePickerDialog() {
        val datePicker = DatePickerFragment {day, month, year -> onStartDateSelected(day, month+1, year)}
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun ShowEndDatePickerDialog() {
        val datePicker = DatePickerFragment {day, month, year -> onEndDateSelected(day, month+1, year)}
        datePicker.show(supportFragmentManager, "datePicker")
    }

    fun onStartDateSelected(day:Int, month:Int, year:Int)
    {
        var dd:String = day.toString()
        var mm:String = month.toString()
        if(day<10)
            dd = "0"+day.toString()
        if(month<10)
            mm = "0"+month.toString()
        //binding.ETStartDate.setText("   "+day.toString()+"/"+month.toString()+"/"+year.toString())
        //binding.ETStartDate.setText("   "+year.toString()+"-"+month.toString()+"-"+day.toString())
        binding.ETStartDateSC.setText("   "+year.toString()+"-"+mm+"-"+dd)
    }

    fun onEndDateSelected(day:Int, month:Int, year:Int)
    {
        var dd:String = day.toString()
        var mm:String = month.toString()
        if(day<10)
            dd = "0"+day.toString()
        if(month<10)
            mm = "0"+month.toString()
        //binding.ETEndDate.setText("   "+year.toString()+"-"+month.toString()+"-"+day.toString())
        binding.ETEndDateSC.setText("   "+year.toString()+"-"+mm+"-"+dd)
    }

    private fun GetCashREgisterData() {
        val sharedPref: SharedPreferences = getSharedPreferences("CashRegisterData", Context.MODE_PRIVATE)
        Code = sharedPref.getString("code", null).toString()
        Name = sharedPref.getString("name", null).toString()
        Company = sharedPref.getString("company", null).toString()
        OeningCreationUser = sharedPref.getString("openingCreationUser", null).toString()
        User = sharedPref.getString("user", null).toString()
        Type = sharedPref.getString("type", null).toString()
        Assigned = sharedPref.getInt("assigned",-1)
        Status = sharedPref.getInt("status",-1)
    }

    private fun initElements() {
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = getString(R.string.balance_list_cahsier)
        //initViewModel()
        initActions()
        //initRecyclerView()

    }

    private fun initViewModel() {
        balanceListCashierViewModel = ViewModelProvider(this).get(BalanceListCashierViewModel::class.java)
        balanceListCashierCall(0)
    }


    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener {
            this.finish()
        }
    }

    private fun initRecyclerView(PbalanceListCashierAdapter:BalanceListCashierAdapter) {
        binding.rvSquareList.setHasFixedSize(true)
        binding.rvSquareList.adapter = PbalanceListCashierAdapter
        binding.rvSquareList.layoutManager = LinearLayoutManager(this)
        /*itemViewModel.list(loggerUserModel.entity).observe(this, {
            it.let { itemAdapter.setList(it) }
        })*/

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.INTENT_BUSINESS_PARTNER -> {
                if (data != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        currentBusinessPartner = data.getSerializableExtra(Const.INTENT_BUSINESS_PARTNER_TAG) as BusinessPartnerView
                        //binding.tvBusinessPartner.text = currentBusinessPartner?.businessPartnerEntity?.businessName

                    } else {
                        PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                    }
                } else {
                    PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                }
            }
            QueryActivity.PAYMENT_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    /*if (::receiptPaymentDetailDialog.isInitialized) receiptPaymentDetailDialog.close()
                    filter()*/
                }
            }
        }
    }

    private fun balanceListCashierCall(page: Int) {
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_order))
        balanceListCashierViewModel.listBalanceListCashier(loggerUserModel, Code, from, to, page)
        balanceListCashierViewModel.resultBalanceListCashier.observe(this, EventObserver {
            PopUp.closeProgressDialog()
            if (it.successful) {
                if (it.data?.nextPage != null) {
                    //binding.fabNextPage.visibility = View.VISIBLE
                    //nextPage = it.data!!.nextPage!!.split("&")[0].replace("saleorder?page=", "").toInt()
                } else {
                    //binding.fabNextPage.visibility = View.GONE
                }
                it.data?.value.let { list ->
                    //binding.llNoMatches.llNoMatches.visibility = View.GONE
                    //queryOrderAdapter.setList(list!!)
                    balanceListCashierAdapter = BalanceListCashierAdapter(list!!)
                    initRecyclerView(balanceListCashierAdapter)
                }
            } else {
                PopUp.showAlert(this, it.message)
            }

        })
    }

    /*private fun initRecyclerView() {
        binding.rvOpeningList.setHasFixedSize(true)
        itemAdapter = ItemAdapter(this, this)
        binding.rvOpeningList.adapter = itemAdapter
        binding.rvOpeningList.layoutManager = LinearLayoutManager(this)
        itemViewModel.list(loggerUserModel.entity).observe(this, {
            it.let { itemAdapter.setList(it) }
        })

    }*/

    /*override fun onSelectedItem(itemView: ItemView) {
        val intent = Intent(this, ItemDetailActivity::class.java)
        intent.putExtra(getString(R.string.item),itemView)
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivity(intent)
    }*/

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //return super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        //searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnSearchClickListener {
            binding.appBar.tvTitleToolbar.visibility = View.GONE
            binding.appBar.ivBackToolbar.visibility = View.GONE
        }
        searchView.setOnCloseListener {
            binding.appBar.tvTitleToolbar.visibility = View.VISIBLE
            binding.appBar.ivBackToolbar.visibility = View.VISIBLE
            false
        }
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                itemAdapter.filter.filter(newText)
                return true
            }

        })
        return true
    }*/

    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.isIconified = true
            return
        }
        super.onBackPressed()
    }
}