package ndp.posapp.repository

import ndp.posapp.data.db.dao.WarehouseDao
import ndp.posapp.data.db.entity.WarehouseEntity
import ndp.posapp.data.model.WarehouseModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class WarehouseRepository(private val warehouseDao: WarehouseDao) {

    private val api = AdmOperationService()

    /** SERVICE **/
    fun listWarehouse(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<WarehouseModel>> {
        return api.listWarehouse(signature, token, fingerprint, entity, repository, page, syncType)
    }

    /**DATABASE **/
    fun insert(warehouseEntity: WarehouseEntity) {
        warehouseDao.insert(warehouseEntity)
    }

    fun list(company: String, warehouses: List<String>) : List<WarehouseEntity> {
        return warehouseDao.list(company, warehouses)
    }
}