package ndp.posapp.repository

import ndp.posapp.data.db.dao.RepositorySyncHistoryDao
import ndp.posapp.data.db.entity.RepositorySyncHistoryEntity
import ndp.posapp.data.model.RepositorySyncHistoryModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiResponse

class RepositorySyncHistoryRepository(private val repositorySyncHistoryDao: RepositorySyncHistoryDao) {

    private val api = AdmOperationService()


    fun syncHistory(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repositorySyncHistoryModel: RepositorySyncHistoryModel
    ): ApiResponse<RepositorySyncHistoryModel> {
        return api.syncHistory(signature, token, fingerprint, entity, repositorySyncHistoryModel)
    }

    fun insert(repositorySyncHistoryEntity: RepositorySyncHistoryEntity) {
        repositorySyncHistoryDao.insert(repositorySyncHistoryEntity)
    }

    fun list(company: String): List<RepositorySyncHistoryEntity> {
        return repositorySyncHistoryDao.list(company)
    }


}