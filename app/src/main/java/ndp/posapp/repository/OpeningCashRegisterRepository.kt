package ndp.posapp.repository

import ndp.posapp.data.db.dao.OpeningCashRegisterDao
import ndp.posapp.data.model.OpeningCashRegisterModel
import ndp.posapp.data.model.OpeningCashRegisterSaveModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiResponse

class OpeningCashRegisterRepository(private val openingCashRegisterDao: OpeningCashRegisterDao) {
    private val api = PosEngineService()

    fun OpeningCashRegister(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        OpeningCashRegisterModelDTO: OpeningCashRegisterSaveModel
    ): ApiResponse<OpeningCashRegisterSaveModel> {
        return api.OpeningCashRegister(entity, fingerprint, signature, token, OpeningCashRegisterModelDTO)
    }
}