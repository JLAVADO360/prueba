package ndp.posapp.repository

import ndp.posapp.data.db.dao.TurnDao
import ndp.posapp.data.model.TurnModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class TurnRepository(private val turnDao: TurnDao) {

    private val api = PosEngineService()

    fun ListTurn(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        startDate: String,
        cashRegisterCode: String,
        store: String,
    //): ApiPaginationResponse<List<TurnModel>> {
    ): List<TurnModel> {

        return api.ListCashRegisterTurn(entity, fingerprint, signature, token, startDate, cashRegisterCode, store)
    }
}