package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.PaymentReceiptDao
import ndp.posapp.data.db.entity.PaymentReceiptEntity
import ndp.posapp.data.network.api.posEngine.PosEngineService

class PaymentReceiptRepository(private val paymentReceiptDao: PaymentReceiptDao) {

    private val api = PosEngineService()

    /** SERVICE **/


    /**DATABASE **/
    fun insert(paymentReceiptEntity: PaymentReceiptEntity): Long {
        return paymentReceiptDao.insert(paymentReceiptEntity)
    }

    fun update(paymentReceiptEntity: PaymentReceiptEntity) {
        paymentReceiptDao.update(paymentReceiptEntity)
    }

    fun updateCode(saleOrderId: Long, code: String) {
        paymentReceiptDao.updateCode(saleOrderId, code)
    }

    fun get(internalId: Long) : LiveData<PaymentReceiptEntity?> {
        return paymentReceiptDao.get(internalId)
    }

    fun listPendingSend() : List<PaymentReceiptEntity> {
        return paymentReceiptDao.listPendingSend()
    }

    fun list(saleOrderId: Long) : List<PaymentReceiptEntity> {
        return paymentReceiptDao.list(saleOrderId)
    }

}