package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.ReceiptTypeDao
import ndp.posapp.data.db.entity.ReceiptTypeEntity
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.ReceiptTypeModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class ReceiptTypeRepository(private val receiptTypeDao: ReceiptTypeDao) {

    private val api = AdmOperationService()

    /** SERVICE **/
    fun listReceiptType(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String
    ): ApiPaginationResponse<List<ReceiptTypeModel>> {
        return api.listReceiptType(signature, token, fingerprint, entity, repository)
    }


    /**DATABASE **/
    fun insert(receiptTypeEntity: ReceiptTypeEntity) {
        receiptTypeDao.insert(receiptTypeEntity)
    }

    fun list(company: String): LiveData<List<ReceiptTypeEntity>> {
        return receiptTypeDao.list(company)
    }


}