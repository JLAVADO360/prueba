package ndp.posapp.repository

import ndp.posapp.data.db.dao.VisitReasonDao
import ndp.posapp.data.db.entity.VisitReasonEntity
import ndp.posapp.data.model.VisitReasonModel
import ndp.posapp.data.network.api.takeOrderOperation.TakeOrderOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class VisitReasonRepository(private val visitReasonDao: VisitReasonDao) {

    private val api = TakeOrderOperationService()

    /** SERVICE **/
    fun listVisitReason(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<VisitReasonModel>> {
        return api.listVisitReason(signature, token, fingerprint, entity, repository, page, syncType)
    }


    /**DATABASE **/


    fun insert(visitReasonEntity: VisitReasonEntity) {
        visitReasonDao.insert(visitReasonEntity)
    }

}