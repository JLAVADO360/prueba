package ndp.posapp.repository

import ndp.posapp.data.db.dao.ClosingListCashierDao
import ndp.posapp.data.model.ClosingListCashierModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class ClosingListCashierRepository(private val closingListCashierDao: ClosingListCashierDao) {

    private val api = PosEngineService()

    fun ClosingListCashier(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterCode: String,
        startDate: String,
        endDate: String,
        page: Int,
        itemPerPage:Int
    ): ApiPaginationResponse<List<ClosingListCashierModel>> {
        return api.ClosingListCashier(entity, fingerprint, signature, token, cashRegisterCode, startDate, endDate, page,itemPerPage)
    }
}