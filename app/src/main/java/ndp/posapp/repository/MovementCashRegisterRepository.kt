package ndp.posapp.repository

import ndp.posapp.data.db.dao.MovementCashRegisterDAO
import ndp.posapp.data.model.CashRegisterBalanceDTOModel
import ndp.posapp.data.model.ClosingCashRegisterSaveModel
import ndp.posapp.data.model.ClosingListCashierGenerateModel
import ndp.posapp.data.model.MovementTransferIOModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiResponse

class MovementCashRegisterRepository(private val movementCashRegisterDAO: MovementCashRegisterDAO) {
    private val api = PosEngineService()

    fun movementCashRegister(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        movementTransferIOModel: MovementTransferIOModel
    ): ApiResponse<MovementTransferIOModel> {
        return api.movementCashierRegister(entity,fingerprint,signature,token,movementTransferIOModel)
    }
}