package ndp.posapp.repository

import ndp.posapp.data.db.dao.ClosingCashierGenerateDAO
import ndp.posapp.data.db.dao.CurrencyDataDAO
import ndp.posapp.data.model.CashRegisterBalanceDTOModel
import ndp.posapp.data.model.ClosingListCashierGenerateModel
import ndp.posapp.data.model.ClosingListCashierModel
import ndp.posapp.data.model.CurrencyDataModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiResponse

class CurrencyDataRepository(private val currencyDataDAO: CurrencyDataDAO) {
    private val api = AdmOperationService()

    fun getObjectCurrency(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String
    ):ApiResponse<CurrencyDataModel>{
        return api.getCurrencyData(entity,fingerprint,signature,token)
    }


}