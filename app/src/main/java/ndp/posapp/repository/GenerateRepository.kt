package ndp.posapp.repository

import ndp.posapp.data.db.dao.ClosingCashierGenerateDAO
import ndp.posapp.data.model.CashRegisterBalanceDTOModel
import ndp.posapp.data.model.ClosingListCashierGenerateModel
import ndp.posapp.data.model.ClosingListCashierModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiResponse

class GenerateRepository(private val closingCashierGenerateDAO: ClosingCashierGenerateDAO) {
    private val api = PosEngineService()

    fun listClosingListCashierGenerate(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterBalanceDTOModel: CashRegisterBalanceDTOModel
    ):ApiResponse<ClosingListCashierGenerateModel>{
        return api.listClosingListCashierGenerate(entity,fingerprint,signature,token,cashRegisterBalanceDTOModel)
    }


}