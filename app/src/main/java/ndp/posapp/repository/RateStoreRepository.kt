package ndp.posapp.repository

import ndp.posapp.data.db.dao.RateStoreDao
import ndp.posapp.data.db.entity.RateStoreEntity
import ndp.posapp.data.model.RateStoreModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class RateStoreRepository(private val rateStoreDao: RateStoreDao) {

    private val api = AdmOperationService()

    /** SERVICE **/
    fun getRateStore(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        status: Int,
        rateDate: String,
        store: String
    ): ApiPaginationResponse<List<RateStoreModel>> {
        return api.getRateStore(signature, token, fingerprint, entity, status, rateDate, store)
    }

    /**DATABASE **/

    fun insert(rateStoreEntity: RateStoreEntity){
        rateStoreDao.insert(rateStoreEntity)
    }

    fun get(company: String, status: Int, rateDate: String, store: String): List<RateStoreEntity> {
        return rateStoreDao.get(company, status, rateDate, store)
    }
}