package ndp.posapp.repository

import ndp.posapp.data.db.dao.BalanceListCashierDao
import ndp.posapp.data.model.BalanceListCashierModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class BalanceListCashierRepository(private val balanceListCashierDao: BalanceListCashierDao) {

    private val api = PosEngineService()

    fun BalanceListCashier(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterCode: String,
        startDate: String,
        endDate: String,
        page: Int
    ): ApiPaginationResponse<List<BalanceListCashierModel>> {
        return api.BalanceListCashier(entity, fingerprint, signature, token, cashRegisterCode, startDate, endDate, page)
    }
}