package ndp.posapp.repository

import ndp.posapp.data.db.dao.OpeningListCashierDao
import ndp.posapp.data.model.OpeningListCashierModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class OpeningListCashierRepository(private val openingListCashierDao: OpeningListCashierDao) {

    private val api = PosEngineService()

    fun OpeningListCashier(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterCode: String,
        startDate: String,
        endDate: String,
        page: Int,
        itemPerPage:Int
    ): ApiPaginationResponse<List<OpeningListCashierModel>> {
        return api.OpeningListCashier(entity, fingerprint, signature, token, cashRegisterCode, startDate, endDate, page,itemPerPage)
    }
}