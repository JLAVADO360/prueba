package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.PriceListDao
import ndp.posapp.data.db.entity.PriceListEntity
import ndp.posapp.data.model.PriceListModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class PriceListRepository(private val priceListDao: PriceListDao) {

    private val api = AdmOperationService()

    /** SERVICE **/

    fun listPriceList(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<PriceListModel>> {
        return api.listPriceList(signature, token, fingerprint, entity, repository, page, syncType)
    }


    /** DATABASE **/
    fun insert(priceListEntity: PriceListEntity) {
        priceListDao.insert(priceListEntity)
    }

    fun get(company: String, code: Int) : LiveData<PriceListEntity>{
        return priceListDao.get(company, code)
    }
}