package ndp.posapp.repository

import ndp.posapp.data.db.dao.WhsKardexDao
import ndp.posapp.data.db.entity.WhsKardexEntity
import ndp.posapp.data.model.WhsKardexModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiResponse

class WhsKardexRepository(private val whsKardexDao: WhsKardexDao) {

    private val api = AdmOperationService()

    /** SERVICE **/


    fun syncWhskardex(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        warehouse: String,
        syncType: Int
    ) : ApiResponse<List<WhsKardexModel>> {
        return api.syncWhskardex(signature,token, fingerprint, entity, warehouse, syncType)
    }


    /**DATABASE **/

    fun insert(whsKardexEntity: WhsKardexEntity) {
        whsKardexDao.insert(whsKardexEntity)
    }

    fun list(company: String, warehouse : String, item : String) : List<WhsKardexEntity> {
        return whsKardexDao.list(company, warehouse,item)
    }

}