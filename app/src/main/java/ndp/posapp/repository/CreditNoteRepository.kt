package ndp.posapp.repository

import ndp.posapp.data.model.CreditNoteModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class CreditNoteRepository {

    private val api = PosEngineService()

    /** SERVICE **/
    fun getCreditNote(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        creditNoteNumber : String
    ): ApiPaginationResponse<List<CreditNoteModel>> {
        return api.getCreditNote(entity, fingerprint, signature, token,creditNoteNumber)
    }

}