package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.CorrelativeDao
import ndp.posapp.data.db.entity.CorrelativeEntity
import ndp.posapp.data.model.CorrelativeModel
import ndp.posapp.data.network.api.posOperation.PosOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class CorrelativeRepository(private val correlativeDao: CorrelativeDao) {

    private val api = PosOperationService()

    /** SERVICE **/
    fun listCorrelative(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterCode: String
    ): ApiPaginationResponse<List<CorrelativeModel>> {
        return api.listCorrelative(entity, fingerprint, signature, token, cashRegisterCode)
    }


    /**DATABASE **/
    fun insert(correlativeEntity: CorrelativeEntity) {
        correlativeDao.insert(correlativeEntity)
    }

    fun list(company: String, cashRegisterCode: String): LiveData<List<CorrelativeEntity>> {
        return correlativeDao.list(company, cashRegisterCode)
    }

}