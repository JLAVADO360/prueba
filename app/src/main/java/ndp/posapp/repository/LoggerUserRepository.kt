package ndp.posapp.repository

import android.content.Context
import ndp.posapp.data.model.MemberDTOModel
import ndp.posapp.data.network.api.secEngine.SecEngineService
import ndp.posapp.data.network.response.ApiResponse

class LoggerUserRepository {

    private val api = SecEngineService()

    fun login(email: String, signature: String, fingerprint: String, context: Context): ApiResponse<MemberDTOModel> {
        return api.login(email, signature, fingerprint, context)
    }
}