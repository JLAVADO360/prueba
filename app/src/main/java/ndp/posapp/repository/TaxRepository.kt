package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.TaxDao
import ndp.posapp.data.db.entity.TaxEntity
import ndp.posapp.data.model.TaxModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class TaxRepository(private val taxDao: TaxDao) {

    private val api = AdmOperationService()


    /** SERVICE **/
    fun listTax(signature: String, token: String, fingerprint: String, entity: String, repository: String, page: Int, syncType: Int): ApiPaginationResponse<List<TaxModel>> {
        return api.listTax(signature, token, fingerprint, entity, repository, page, syncType)
    }


    /**DATABASE **/
    fun insert(taxEntity: TaxEntity) {
        taxDao.insert(taxEntity)
    }

    fun list(company: String) : LiveData<List<TaxEntity>> {
        return taxDao.list(company)
    }

}