package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.ItemDao
import ndp.posapp.data.db.entity.ItemEntity
import ndp.posapp.data.db.entityView.ItemView
import ndp.posapp.data.model.ItemModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class ItemRepository(private val itemDao: ItemDao) {

    private val api = AdmOperationService()

    /** SERVICE **/
    fun listItem(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<ItemModel>> {
        return api.listItem(signature, token, fingerprint, entity, repository, page, syncType)
    }

    fun listItemFilter(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        itemCode: String
    ): ApiPaginationResponse<List<ItemModel>> {
        return api.listItemFilter(signature, token, fingerprint, entity, itemCode)
    }

    /**DATABASE **/

    fun insert(itemEntity: ItemEntity) {
        itemDao.insert(itemEntity)
    }

    fun get(company: String, itemCode: String): ItemEntity? {
        return itemDao.get(company, itemCode)
    }

    fun list(company: String, itemCodes: List<String>?): List<ItemEntity> {
        return itemDao.list(company, itemCodes)
    }

    fun list(company: String): LiveData<List<ItemView>> {
        return itemDao.list(company)
    }
}