package ndp.posapp.repository

import ndp.posapp.data.db.dao.CashRegisterInfoDao
import ndp.posapp.data.db.entity.CashRegisterInfoEntity
import ndp.posapp.data.model.CashRegisterInfoModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiResponse

class CashRegisterRepository(private val cashRegisterInfoDao: CashRegisterInfoDao) {

    private val api = PosEngineService()

    /** SERVICE **/
    fun listCashRegisterInfo(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        userIdentifier: String
    ): ApiResponse<List<CashRegisterInfoModel>> {
        return api.listCashRegisterInfo(signature, token, fingerprint, entity, userIdentifier)
    }


    /**DATABASE **/
    fun insert(cashRegisterInfoEntity: CashRegisterInfoEntity) {
        cashRegisterInfoDao.insert(cashRegisterInfoEntity)
    }

    fun get(company: String, user: String, type: String) : CashRegisterInfoEntity? {
        return cashRegisterInfoDao.get(company, user, type)
    }

}