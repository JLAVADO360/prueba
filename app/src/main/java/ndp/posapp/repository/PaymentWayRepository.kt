package ndp.posapp.repository

import ndp.posapp.data.db.dao.PaymentWayDao
import ndp.posapp.data.db.entity.PaymentWayEntity
import ndp.posapp.data.model.PaymentWayModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class PaymentWayRepository(private val paymentWayDao: PaymentWayDao) {

    private val api = AdmOperationService()

    /** SERVICE **/
    fun listPaymentWay(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        store: String
    ): ApiPaginationResponse<List<PaymentWayModel>> {
        return api.listPaymentWay(signature, token, fingerprint, entity, store)
    }

    /**DATABASE **/
    fun insert(paymentWayEntity: PaymentWayEntity) {
        paymentWayDao.insert(paymentWayEntity)
    }

    fun get(company: String, store: String): PaymentWayEntity? {
        return paymentWayDao.get(company, store)
    }
}