package ndp.posapp.repository

import ndp.posapp.data.db.dao.BalanceListCashierDao
import ndp.posapp.data.db.dao.CashRegisterBalanceDao
import ndp.posapp.data.model.BalanceListCashierModel
import ndp.posapp.data.model.CashRegisterBalanceDTOModel
import ndp.posapp.data.model.CashRegisterBalanceModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class CashRegisterBalanceRepository(private val cashRegisterBalanceDao: CashRegisterBalanceDao)
{
    private val api = PosEngineService()

    fun CashRegisterBalance(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterBalanceDTOModel: CashRegisterBalanceDTOModel
    ): ApiPaginationResponse<List<CashRegisterBalanceModel>> {
        return api.cashRegisterBalance(entity, fingerprint, signature, token, cashRegisterBalanceDTOModel)
    }
}
