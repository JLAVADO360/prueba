package ndp.posapp.repository

import ndp.posapp.data.db.dao.AffectationDao
import ndp.posapp.data.db.entity.AffectationEntity
import ndp.posapp.data.model.AffectationModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class AffectationRepository(private val affectationDao: AffectationDao) {

    private val api = AdmOperationService()

    /** SERVICE **/
    fun listAffectation(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String
    ): ApiPaginationResponse<List<AffectationModel>> {
        return api.listAffectation(signature, token, fingerprint, entity, repository)
    }


    /**DATABASE **/
    fun insert(affectationEntity: AffectationEntity) {
        affectationDao.insert(affectationEntity)
    }
}