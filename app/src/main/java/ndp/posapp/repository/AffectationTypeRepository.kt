package ndp.posapp.repository

import ndp.posapp.data.db.dao.AffectationTypeDao
import ndp.posapp.data.db.entity.AffectationTypeEntity
import ndp.posapp.data.model.AffectationTypeModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class AffectationTypeRepository(private val affectationTypeDao: AffectationTypeDao) {


    private val api = AdmOperationService()

    /** SERVICE **/
    fun listAffectationType(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String
    ): ApiPaginationResponse<List<AffectationTypeModel>> {
        return api.listAffectationType(signature, token, fingerprint, entity, repository)
    }


    /**DATABASE **/

    fun insert(affectationTypeEntity: AffectationTypeEntity) {
        affectationTypeDao.insert(affectationTypeEntity)
    }


}