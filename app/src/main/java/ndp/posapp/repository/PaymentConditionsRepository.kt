package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.PaymentConditionsDao
import ndp.posapp.data.db.entity.PaymentConditionsEntity
import ndp.posapp.data.model.PaymentConditionsModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class PaymentConditionsRepository(private val paymentConditionsDao: PaymentConditionsDao) {

    private val api = AdmOperationService()

    /** SERVICE **/

    fun listPaymentConditions(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<PaymentConditionsModel>> {
        return api.listPaymentConditions(signature, token, fingerprint, entity, repository, page, syncType)
    }

    /** DATABASE **/

    fun insert(paymentConditionsEntity: PaymentConditionsEntity) {
        paymentConditionsDao.insert(paymentConditionsEntity)
    }

    fun list(company: String, groupNumber: Int): LiveData<List<PaymentConditionsEntity>>{
        return paymentConditionsDao.list(company, groupNumber)
    }

    fun get(company: String, name: String): LiveData<PaymentConditionsEntity>{
        return paymentConditionsDao.get(company, name)
    }
}