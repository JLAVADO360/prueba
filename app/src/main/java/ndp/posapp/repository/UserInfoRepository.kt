package ndp.posapp.repository

import ndp.posapp.data.model.UserInfoModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiResponse

class UserInfoRepository {

    private val apiAdmOperation = AdmOperationService()

    fun getUserInfo(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        userIdentifier: String
    ): ApiResponse<UserInfoModel> {
        return apiAdmOperation.getUserInfo(signature, token, fingerprint, entity, userIdentifier)
    }
}