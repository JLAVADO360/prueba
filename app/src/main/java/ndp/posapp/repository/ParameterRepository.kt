package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.ParameterDao
import ndp.posapp.data.db.entity.ParameterEntity
import ndp.posapp.data.model.ParameterModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse

class ParameterRepository(private val parameterDao: ParameterDao) {


    private val api = AdmOperationService()

    /** SERVICE **/

    fun listParameter(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<ParameterModel>> {
        return api.listParameter(signature, token, fingerprint, entity, repository, page, syncType)
    }


    /** DATABASE **/

    fun insert(parameterEntity: ParameterEntity) {
        parameterDao.insert(parameterEntity)
    }

    fun get(company: String, name: String): LiveData<ParameterModel> {
        return parameterDao.get(company, name)
    }

}