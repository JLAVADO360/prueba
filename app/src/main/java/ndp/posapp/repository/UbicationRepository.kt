package ndp.posapp.repository

import ndp.posapp.data.db.dao.UbicationDao
import ndp.posapp.data.db.entity.UbicationEntity

class UbicationRepository(private val ubicationDao: UbicationDao) {

    fun insert(ubicationEntity: UbicationEntity){
        ubicationDao.insert(ubicationEntity)
    }

}