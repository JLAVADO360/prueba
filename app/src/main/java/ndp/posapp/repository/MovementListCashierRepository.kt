package ndp.posapp.repository

import ndp.posapp.data.db.dao.MovementListCashierDao
import ndp.posapp.data.model.MovementListCashierModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class MovementListCashierRepository(private val movementListCashierDao: MovementListCashierDao) {

    private val api = PosEngineService()

    fun MovementListCashier(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        page: Int,
        cashRegisterCode: String,
        store:String,
        turnDate: String,
    ): ApiPaginationResponse<List<MovementListCashierModel>> {
        return api.MovementListCashier(entity, fingerprint, signature, token, page, cashRegisterCode, store, turnDate)
    }
}