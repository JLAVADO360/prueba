package ndp.posapp.repository

import ndp.posapp.data.db.dao.ListReasonImbalanceDao
import ndp.posapp.data.model.ReasonImbalanceModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiResponse

class ReasonImbalanceRepository(private val listReasonImbalanceDao: ListReasonImbalanceDao) {
    private val api = PosEngineService()

    fun listReasonImbalance(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String
    ): ApiResponse<List<ReasonImbalanceModel>>{
        return api.listReasonImbalance(entity,fingerprint,signature, token)
    }

}