package ndp.posapp.repository

import ndp.posapp.data.db.dao.KardexDao
import ndp.posapp.data.db.entity.KardexEntity

class KardexRepository(private val kardexDao: KardexDao) {

    fun insert(kardexEntity: KardexEntity){
        kardexDao.insert(kardexEntity)
    }

    fun list(company: String, warehouse : String, item : String) : List<KardexEntity> {
        return kardexDao.list(company, warehouse,item)
    }
}