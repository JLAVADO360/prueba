package ndp.posapp.repository

import ndp.posapp.data.model.PaymentModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiResponse

class PaymentRepository {

    private val api = PosEngineService()

    /** SERVICE **/
    fun postQuickPayment(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        paymentModelDTO: PaymentModel
    ): ApiResponse<PaymentModel> {
        return api.postQuickPayment(entity, fingerprint, signature, token, paymentModelDTO)
    }

}