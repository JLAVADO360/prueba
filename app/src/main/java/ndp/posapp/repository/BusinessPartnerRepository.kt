package ndp.posapp.repository

import androidx.lifecycle.LiveData
import com.google.gson.JsonObject
import ndp.posapp.data.db.dao.BusinessPartnerDao
import ndp.posapp.data.db.entity.BusinessPartnerEntity
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.BusinessPartnerModel
import ndp.posapp.data.network.api.admEngine.AdmEngineService
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.ApiResponse

class BusinessPartnerRepository(private val businessPartnerDao: BusinessPartnerDao) {

    private val apiAdmOperation = AdmOperationService()
    private val apiAdmEngine = AdmEngineService()
    /** SERVICE **/


    fun listBusinessPartner(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<BusinessPartnerModel>> {
        return apiAdmOperation.listBusinessPartner(signature, token, fingerprint, entity, repository, page, syncType)
    }


    fun getBusinessPartner(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        nif: String
    ): ApiPaginationResponse<List<BusinessPartnerModel>> {
        return apiAdmOperation.getBusinessPartner(signature, token, fingerprint, entity, nif)
    }

    fun postBusinessPartner(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        businessPartnerDTO: BusinessPartnerModel
    ):  ApiResponse<BusinessPartnerModel> {
        return apiAdmOperation.postBusinessPartner(signature, token, fingerprint, entity, businessPartnerDTO)
    }

    fun consultDNI(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        dni: String
    ): ApiResponse<JsonObject> {
        return apiAdmEngine.consultDNI(signature, token, fingerprint, entity, dni)
    }

    fun consultRUC(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        ruc: String
    ): ApiResponse<JsonObject> {
        return apiAdmEngine.consultRUC(signature, token, fingerprint, entity, ruc)
    }



    /**DATABASE **/
    fun insert(businessPartnerEntity: BusinessPartnerEntity) {
        businessPartnerDao.insert(businessPartnerEntity)
    }

    fun list(company: String): LiveData<List<BusinessPartnerView>> {
        return businessPartnerDao.list(company)
    }
}