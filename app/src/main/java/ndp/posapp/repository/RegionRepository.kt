package ndp.posapp.repository

import androidx.lifecycle.LiveData
import ndp.posapp.data.db.dao.RegionDao
import ndp.posapp.data.db.entity.RegionEntity
import ndp.posapp.data.model.RegionModel
import ndp.posapp.data.network.api.admEngine.AdmEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class RegionRepository(private val regionDao: RegionDao) {
    private val apiAdmEngine = AdmEngineService()

    /** SERVICE **/

    fun listRegion(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<RegionModel>> {
        return apiAdmEngine.listRegion(signature, token, fingerprint, entity, repository, page, syncType)
    }



    /**DATABASE **/

    fun insert(regionEntity: RegionEntity) {
        regionDao.insert(regionEntity)
    }

    fun listDepartment(region : String, regionType : String) : LiveData<List<RegionEntity>>{
        return regionDao.listDepartment(region,regionType)
    }

    fun listRelation(region : String, regionType : String, relation : List<String>) : List<RegionEntity>{
        return regionDao.listRelation(region,regionType,relation)
    }

    fun countRegion() : Int {
        return regionDao.countRegion()
    }
}