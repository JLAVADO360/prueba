package ndp.posapp.repository

import ndp.posapp.data.db.dao.ZoneUserDao
import ndp.posapp.data.db.entity.ZoneUserEntity

class ZoneUserRepository(private val zoneUserDao: ZoneUserDao) {

    fun insert(zoneUserEntity: ZoneUserEntity){
        zoneUserDao.insert(zoneUserEntity)
    }
}