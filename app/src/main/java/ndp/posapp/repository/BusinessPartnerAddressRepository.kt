package ndp.posapp.repository

import ndp.posapp.data.db.dao.BusinessPartnerAddressDao
import ndp.posapp.data.db.entity.BusinessPartnerAddressEntity

class BusinessPartnerAddressRepository(private val businessPartnerAddressDao: BusinessPartnerAddressDao) {

    fun insert(businessPartnerAddressEntity: BusinessPartnerAddressEntity) {
        businessPartnerAddressDao.insert(businessPartnerAddressEntity)
    }

}