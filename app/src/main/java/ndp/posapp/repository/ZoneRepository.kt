package ndp.posapp.repository

import ndp.posapp.data.db.dao.ZoneDao
import ndp.posapp.data.db.entity.ZoneEntity
import ndp.posapp.data.model.ZoneModel
import ndp.posapp.data.network.api.takeOrderEngine.TakeOrderEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class ZoneRepository(private val zoneDao: ZoneDao) {

    private val api = TakeOrderEngineService()

    /** SERVICE **/
    fun listZone(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int,
        user: String
    ): ApiPaginationResponse<List<ZoneModel>> {
        return api.listZone(signature, token, fingerprint, entity, repository, page, syncType, user)
    }


    /**DATABASE **/
    fun insert(zoneEntity: ZoneEntity) {
        zoneDao.insert(zoneEntity)
    }
}