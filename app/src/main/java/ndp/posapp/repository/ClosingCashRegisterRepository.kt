package ndp.posapp.repository

import ndp.posapp.data.db.dao.ClosingCashRegisterDAO
import ndp.posapp.data.model.CashRegisterBalanceDTOModel
import ndp.posapp.data.model.ClosingCashRegisterSaveModel
import ndp.posapp.data.model.ClosingListCashierGenerateModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiResponse

class ClosingCashRegisterRepository(private val closingCashRegisterDAO: ClosingCashRegisterDAO) {
    private val api = PosEngineService()

    fun closingCashRegister(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        closingCashRegisterSaveModel: ClosingCashRegisterSaveModel
    ): ApiResponse<ClosingCashRegisterSaveModel> {
        return api.closingCashierRegister(entity,fingerprint,signature,token,closingCashRegisterSaveModel)
    }
}