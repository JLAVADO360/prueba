package ndp.posapp.repository

import ndp.posapp.data.db.dao.RegionRelationDao
import ndp.posapp.data.db.entity.RegionRelationEntity
import ndp.posapp.data.model.RegionRelationModel
import ndp.posapp.data.network.api.admEngine.AdmEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse

class RegionRelationRepository(private val regionRelationDao: RegionRelationDao) {

    private val apiAdmEngine = AdmEngineService()

    /** SERVICE **/
    fun listRegionRelation(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<RegionRelationModel>> {
        return apiAdmEngine.listRegionRelation(signature, token, fingerprint, entity, repository, page, syncType)
    }

    /**DATABASE **/
    fun insert(regionRelationEntity: RegionRelationEntity){
        regionRelationDao.insert(regionRelationEntity)
    }

    fun list(parentRegion : String?) : List<String>{
        return regionRelationDao.list(parentRegion)
    }

    fun countRegionRelation() : Int {
        return regionRelationDao.countRegionRelation()
    }
}