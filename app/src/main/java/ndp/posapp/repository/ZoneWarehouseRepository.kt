package ndp.posapp.repository

import ndp.posapp.data.db.dao.ZoneWarehouseDao
import ndp.posapp.data.db.entity.ZoneWarehouseEntity

class ZoneWarehouseRepository(private val zoneWarehouseDao: ZoneWarehouseDao) {

    fun insert(zoneWarehouseEntity: ZoneWarehouseEntity) {
        zoneWarehouseDao.insert(zoneWarehouseEntity)
    }

    fun list() : List<ZoneWarehouseEntity>{
        return zoneWarehouseDao.list()
    }

}