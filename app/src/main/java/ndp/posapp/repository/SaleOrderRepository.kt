package ndp.posapp.repository

import ndp.posapp.data.db.dao.SaleOrderDao
import ndp.posapp.data.db.entity.SaleOrderEntity
import ndp.posapp.data.model.PaymentReceiptModel
import ndp.posapp.data.model.SaleOrderModel
import ndp.posapp.data.network.api.posEngine.PosEngineService
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.ApiResponse

class SaleOrderRepository(private val saleOrderDao: SaleOrderDao) {

    private val api = PosEngineService()

    /** SERVICE **/
    fun listSaleOrder(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        codclient: String,
        startDate: String,
        endDate: String,
        page: Int
    ): ApiPaginationResponse<List<SaleOrderModel>> {
        return api.listSaleOrder(entity, fingerprint, signature, token, codclient, startDate, endDate, page)
    }

    fun listPaymentReceipt(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        businessPartner: String,
        startDate: String,
        endDate: String,
        store: String,
        page: Int
    ): ApiPaginationResponse<List<PaymentReceiptModel>> {
        return api.listPaymentReceipt(entity, fingerprint, signature, token, businessPartner, startDate, endDate, store, page)
    }

    fun getPaymentReceipt(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        code: String
    ): ApiPaginationResponse<List<PaymentReceiptModel>> {
        return api.getPaymentReceipt(entity, fingerprint, signature, token, code)
    }

    fun postSaleOrder(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        saleOrderDTO: SaleOrderModel
    ): ApiResponse<SaleOrderModel> {
        return api.postSaleOrder(entity, fingerprint, signature, token, saleOrderDTO)
    }

    fun postQuickSale(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        saleOrderDTO: SaleOrderModel
    ): ApiResponse<SaleOrderModel> {
        return api.postQuickSale(entity, fingerprint, signature, token, saleOrderDTO)
    }


    /**DATABASE **/
    fun insert(saleOrderEntity: SaleOrderEntity): Long {
        return saleOrderDao.insert(saleOrderEntity)
    }

    fun update(saleOrderEntity: SaleOrderEntity) {
        saleOrderDao.update(saleOrderEntity)
    }

    fun get(internalId: Long): SaleOrderEntity? {
        return saleOrderDao.get(internalId)
    }

    fun list(internalStatus: Int): List<SaleOrderEntity> {
        return saleOrderDao.list(internalStatus)
    }

}