package ndp.posapp.repository

import ndp.posapp.data.db.dao.ObjectDao
import ndp.posapp.data.db.entity.ObjectEntity
import ndp.posapp.data.model.ObjectModel
import ndp.posapp.data.network.api.secEngine.SecEngineService
import ndp.posapp.data.network.response.ApiResponse

class ObjectRepository(private val objectDao: ObjectDao) {

    private val api = SecEngineService()

    /** SERVICE **/
    fun permissionListByRole(token: String, fingerprint: String, objectId: String): ApiResponse<List<ObjectModel>> {
        return api.permissionListByRole(token, fingerprint, objectId)
    }


    /**DATABASE **/

    fun insert(objectEntity: ObjectEntity) {
        objectDao.insert(objectEntity)
    }

    fun list() : List<ObjectEntity> {
        return objectDao.list()
    }

    fun deleteAll(){
        objectDao.deleteAll()
    }
}