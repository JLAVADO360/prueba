package ndp.posapp.repository

import ndp.posapp.data.db.dao.PriceListDetailDao
import ndp.posapp.data.db.entity.PriceListDetailEntity

class PriceListDetailRepository(private val priceListDetailDao: PriceListDetailDao) {

    fun insert(priceListDetailEntity: PriceListDetailEntity){
        priceListDetailDao.insert(priceListDetailEntity)
    }

    fun list(company: String, priceList: List<Int>, item: String) : List<PriceListDetailEntity>{
         return priceListDetailDao.list(company, priceList,item)
    }

    fun list(company: String, item: String) : List<PriceListDetailEntity>{
         return priceListDetailDao.list(company, item)
    }

}