package ndp.posapp.repository

import ndp.posapp.data.db.dao.CurrencyDao
import ndp.posapp.data.db.entity.CurrencyEntity
import ndp.posapp.data.model.CurrencyModel
import ndp.posapp.data.network.api.admOperation.AdmOperationService
import ndp.posapp.data.network.response.ApiPaginationResponse


class CurrencyRepository(private val currencyDao: CurrencyDao) {

    private val api = AdmOperationService()


    /** SERVICE **/

    fun listCurrency(signature: String, token: String, fingerprint: String, entity: String, repository: String, page: Int, syncType: Int): ApiPaginationResponse<List<CurrencyModel>> {
        return api.listCurrency(signature, token, fingerprint, entity, repository, page, syncType)
    }

    fun getCurrency(signature: String, token: String, fingerprint: String, entity: String): MutableList<CurrencyModel> {
        return api.getCurrency(signature, token, entity, fingerprint)
    }


    /**DATABASE **/
    fun insert(currencyEntity: CurrencyEntity) {
        currencyDao.insert(currencyEntity)
    }
}