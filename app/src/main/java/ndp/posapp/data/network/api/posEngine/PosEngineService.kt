package ndp.posapp.data.network.api.posEngine

import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import ndp.posapp.core.RetrofitHelper
import ndp.posapp.data.model.*
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.data.network.response.Pagination
import ndp.posapp.utils.Const
import org.json.JSONObject

class PosEngineService {
    private val route = "posengine/"
    private val posEngineApi = RetrofitHelper.getRetrofit(route).create(PosEngineApi::class.java)

    //GG
    fun OpeningListCashier(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterCode: String,
        startDate: String,
        endDate: String,
        page: Int,
        itemPerPage : Int
    ): ApiPaginationResponse<List<OpeningListCashierModel>>
    {
        val apiPaginationResponse = ApiPaginationResponse<List<OpeningListCashierModel>>()
        try {
            val call = posEngineApi.listOpeningListCashier(entity, fingerprint, signature, token,Const.APPLICATION_POS_APP, page,itemPerPage, cashRegisterCode, startDate, endDate)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<OpeningListCashierModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun ClosingListCashier(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterCode: String,
        startDate: String,
        endDate: String,
        page: Int,
        itemPerPage : Int
    ): ApiPaginationResponse<List<ClosingListCashierModel>>
    {
        val apiPaginationResponse = ApiPaginationResponse<List<ClosingListCashierModel>>()
        try {
            val call = posEngineApi.listClosingListCashier(entity, fingerprint, signature, token,Const.APPLICATION_POS_APP, page,itemPerPage, cashRegisterCode, startDate, endDate)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code       = response.body()?.get("code")!!.asString
            apiPaginationResponse.message    = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<ClosingListCashierModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun cashRegisterBalance(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterBalanceDTOModel: CashRegisterBalanceDTOModel
    ): ApiPaginationResponse<List<CashRegisterBalanceModel>>
    {
        val apiPaginationResponse = ApiPaginationResponse<List<CashRegisterBalanceModel>>()
        try {
            val call = posEngineApi.cashRegisterBalance(entity, fingerprint, signature, token,Const.APPLICATION_POS_APP, cashRegisterBalanceDTOModel)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<CashRegisterBalanceModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun BalanceListCashier(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterCode: String,
        startDate: String,
        endDate: String,
        page: Int
    ): ApiPaginationResponse<List<BalanceListCashierModel>>
    {
        val apiPaginationResponse = ApiPaginationResponse<List<BalanceListCashierModel>>()
        try {
            val call = posEngineApi.listBalanceListCashier(entity, fingerprint, signature, token,Const.APPLICATION_POS_APP, page, cashRegisterCode, startDate, endDate)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<BalanceListCashierModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun ListCashRegisterTurn(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        dateTurn: String,
        cashRegisterCode: String,
        store:String
    //): ApiPaginationResponse<List<TurnModel>>
    ): MutableList<TurnModel>
    {
        //val apiPaginationResponse = ApiPaginationResponse<List<TurnModel>>()
        val apiPaginationResponse = mutableListOf<TurnModel>()
        var objModel:TurnModel
        try {
            val call = posEngineApi.listCashRegisterTurn(entity, fingerprint, signature, token,Const.APPLICATION_POS_APP, dateTurn, cashRegisterCode, store)
            val response = call.execute()
            //apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            //apiPaginationResponse.code = response.body()?.get("code")!!.asString
            //apiPaginationResponse.message = response.body()?.get("message")!!.asString
            var g = Gson()
            var jsonArr= response.body()?.get("data")?.asJsonArray
            var jasize = jsonArr?.size()!! - 1
            var jsonObject: JsonObject
            for (i in 0..jasize)
            {
                //jsonObject.add("count",jsonArr.get(i).asJsonObject)
                //val type = object : TypeToken<Pagination<List<TurnModel>>>() {}.type
                //apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
                //apiPaginationResponse.add(jsonObject.g)
                jsonObject = jsonArr.get(i).asJsonObject
                val cashRegister:String = jsonObject.get("cashRegister").toString()
                val code:String = jsonObject.get("code").toString()
                val turn:Int = jsonObject.get("turn").asInt
                val status:Int = jsonObject.get("status").asInt
                val startTurn:String = jsonObject.get("startTurn").toString()
                val endTurn:String = jsonObject.get("endTurn").toString()
                val turnCashRegisterDate:String = jsonObject.get("turnCashRegisterDate").toString()
                val openingCreationUser:String = jsonObject.get("openingCreationUser").toString()
                val closureCreationUser:String = jsonObject.get("closureCreationUser").toString()
                objModel = TurnModel(cashRegister,code, turn, status, startTurn, endTurn, turnCashRegisterDate, openingCreationUser, closureCreationUser)
                apiPaginationResponse.add(objModel)
            }

        } catch (e: Exception) {
            //apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun MovementListCashier(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        page: Int,
        cashRegisterCode: String,
        store: String,
        turnDate: String,
    ): ApiPaginationResponse<List<MovementListCashierModel>>
    {
        val apiPaginationResponse = ApiPaginationResponse<List<MovementListCashierModel>>()
        try {
            val call = posEngineApi.listMovementListCashier(entity, fingerprint, signature, token,Const.APPLICATION_POS_APP, page, cashRegisterCode, store, turnDate)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<MovementListCashierModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun OpeningCashRegister(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        openingCashRegisterSaveModel: OpeningCashRegisterSaveModel
    ): ApiResponse<OpeningCashRegisterSaveModel> {
        val apiResponse = ApiResponse<OpeningCashRegisterSaveModel>()
        try {
            val call = posEngineApi.OpeningCashRegister(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, openingCashRegisterSaveModel)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<OpeningCashRegisterSaveModel>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }
        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }
    //GG

    fun listSaleOrder(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        codclient: String,
        startDate: String,
        endDate: String,
        page: Int
    ): ApiPaginationResponse<List<SaleOrderModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<SaleOrderModel>>()
        try {
            val call = posEngineApi.listSaleOrder(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, codclient, startDate, endDate, page)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<SaleOrderModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listPaymentReceipt(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        businessPartner: String,
        startDate: String,
        endDate: String,
        store: String,
        page: Int
    ): ApiPaginationResponse<List<PaymentReceiptModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<PaymentReceiptModel>>()
        try {
            val call = posEngineApi.listPaymentReceipt(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, businessPartner, startDate, endDate, store, page)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<PaymentReceiptModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun getPaymentReceipt(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        code : String
    ): ApiPaginationResponse<List<PaymentReceiptModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<PaymentReceiptModel>>()
        try {
            val call = posEngineApi.getPaymentReceipt(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, code)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<PaymentReceiptModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun postSaleOrder(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        saleOrderDTO: SaleOrderModel
    ): ApiResponse<SaleOrderModel> {
        val apiResponse = ApiResponse<SaleOrderModel>()
        try {
            val call = posEngineApi.postSaleOrder(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, saleOrderDTO)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<SaleOrderModel>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun postQuickSale(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        saleOrderDTO: SaleOrderModel
    ): ApiResponse<SaleOrderModel> {
        val apiResponse = ApiResponse<SaleOrderModel>()
        try {
            val call = posEngineApi.postQuickSale(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, saleOrderDTO)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<SaleOrderModel>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun listCashRegisterInfo(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        userIdentifier: String
    ): ApiResponse<List<CashRegisterInfoModel>> {
        val apiResponse = ApiResponse<List<CashRegisterInfoModel>>()
        try {
            val call = posEngineApi.listCashRegisterInfo(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, userIdentifier)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonArray = response.body()?.get("data")?.asJsonArray
            val type = object : TypeToken<List<CashRegisterInfoModel>>() {}.type
            if (jsonArray != null) {
                apiResponse.data = g.fromJson(jsonArray.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun postQuickPayment(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        paymentModelDTO: PaymentModel
    ): ApiResponse<PaymentModel> {
        val apiResponse = ApiResponse<PaymentModel>()
        try {
            val call = posEngineApi.postQuickPayment(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, paymentModelDTO)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<PaymentModel>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun getCreditNote(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        creditNoteNumber : String
    ): ApiPaginationResponse<List<CreditNoteModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<CreditNoteModel>>()
        try {
            val call = posEngineApi.getCreditNote(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, creditNoteNumber)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<CreditNoteModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listClosingListCashierGenerate(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterBalanceDTOModel: CashRegisterBalanceDTOModel
    ): ApiResponse<ClosingListCashierGenerateModel> {
        val apiResponse = ApiResponse<ClosingListCashierGenerateModel>()
        try {
            val call = posEngineApi.listClosingCashGenerate(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP,cashRegisterBalanceDTOModel)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code       = response.body()?.get("code")!!.asString
            apiResponse.message    = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<ClosingListCashierGenerateModel>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado...."
        }
        return apiResponse
    }

    fun closingCashierRegister(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        closingCashRegisterSaveModel: ClosingCashRegisterSaveModel
    ): ApiResponse<ClosingCashRegisterSaveModel> {
        val apiResponse = ApiResponse<ClosingCashRegisterSaveModel>()
        try {
            val call = posEngineApi.closingCashierRegister(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP,closingCashRegisterSaveModel)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code       = response.body()?.get("code")!!.asString
            apiResponse.message    = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<ClosingCashRegisterSaveModel>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }
        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado...."
        }
        return apiResponse
    }

    fun listReasonImbalance(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String
    ): ApiResponse<List<ReasonImbalanceModel>>
    {
        val apiResponse = ApiResponse<List<ReasonImbalanceModel>>()
        try {
            val call = posEngineApi.listReasonImbalance(entity, fingerprint, signature, token,Const.APPLICATION_POS_APP)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code       = response.body()?.get("code")!!.asString
            apiResponse.message    = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonArray = response.body()?.get("data")?.asJsonArray
            val type = object : TypeToken<List<ReasonImbalanceModel>>() {}.type
            if (jsonArray != null) {
                apiResponse.data = g.fromJson(jsonArray.toString(), type)
            }
        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun movementCashierRegister(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        movementTransferIOModel: MovementTransferIOModel
    ): ApiResponse<MovementTransferIOModel> {
        val apiResponse = ApiResponse<MovementTransferIOModel>()
        try {
            val call = posEngineApi.movementTransferIO(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP,movementTransferIOModel)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code       = response.body()?.get("code")!!.asString
            apiResponse.message    = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<MovementTransferIOModel>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }
        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado...."
        }
        return apiResponse
    }




}