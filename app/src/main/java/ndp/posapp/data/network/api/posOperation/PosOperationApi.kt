package ndp.posapp.data.network.api.posOperation

import com.google.gson.JsonObject
import ndp.posapp.data.model.PaymentModel
import ndp.posapp.data.model.SaleOrderModel
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import retrofit2.http.Path
import retrofit2.http.POST

interface PosOperationApi {

    @GET("correlative")
    fun listCorrelative(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Query("cashRegisterCode") cashRegisterCode: String
    ): Call<JsonObject>
}