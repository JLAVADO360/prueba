package ndp.posapp.data.network.response


class ApiResponse<T> {
    var successful: Boolean = false
    var code: String = ""
    var message: String = ""
    var data: T? = null

}