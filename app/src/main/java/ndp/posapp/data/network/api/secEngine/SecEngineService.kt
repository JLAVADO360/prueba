package ndp.posapp.data.network.api.secEngine

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ndp.posapp.core.RetrofitHelper
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.MemberDTOModel
import ndp.posapp.data.model.ObjectModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.utils.Const
import ndp.posapp.utils.shared.SharedPreferencesLogin


class SecEngineService {

    private val route = "secengine/"
    private val secEngineApi: SecEngineApi = RetrofitHelper.getRetrofit(route).create(SecEngineApi::class.java)

    fun login(email: String, signature: String, fingerprint: String, context: Context): ApiResponse<MemberDTOModel> {
        val apiResponse = ApiResponse<MemberDTOModel>()
        try {
            val call = secEngineApi.login(email, signature, fingerprint, Const.APPLICATION_POS_APP)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<MemberDTOModel?>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }
            if (apiResponse.successful) {
                val headers = response.headers()
                val loggerUserModel = LoggerUserModel()
                val sharedPreferencesLogin = SharedPreferencesLogin()
                val memberDTOModel = apiResponse.data!!
                loggerUserModel.documentNumber = memberDTOModel.documentNumber
                loggerUserModel.documentType = memberDTOModel.documentType
                loggerUserModel.email = memberDTOModel.email
                loggerUserModel.name = memberDTOModel.name
                loggerUserModel.surname = memberDTOModel.surname
                loggerUserModel.sex = memberDTOModel.sex
                loggerUserModel.roleCode = memberDTOModel.role.code
                loggerUserModel.roleDescription = if (memberDTOModel.role.description != null) memberDTOModel.role.description else ""
                loggerUserModel.roleName = memberDTOModel.role.name
                loggerUserModel.entities = memberDTOModel.entities
                loggerUserModel.entity = memberDTOModel.entities.first().id
                loggerUserModel.fingerPrint = fingerprint
                loggerUserModel.token = headers["token"].toString()
                loggerUserModel.signature = signature
                loggerUserModel.userIdentifier = memberDTOModel.id
                sharedPreferencesLogin.editSharedPreferencesLogin(context, loggerUserModel)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun permissionListByRole(token: String, fingerprint: String, objectId: String): ApiResponse<List<ObjectModel>> {
        val apiResponse = ApiResponse<List<ObjectModel>>()
        try {

            val call = secEngineApi.permissionListByRole("TOKEN-TEST-5b28-423d-8591-ef5ff1a50b9d","FINGERPRINT-FOR-TEST",objectId,Const.APPLICATION_TAKE_ORDER) //TODO CAMBIAR PERMISOS PARA POS APP
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonArray = response.body()?.get("data")?.asJsonArray
            val type = object : TypeToken<List<ObjectModel>?>() {}.type
            if (jsonArray != null) {
                apiResponse.data = g.fromJson(jsonArray.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

}