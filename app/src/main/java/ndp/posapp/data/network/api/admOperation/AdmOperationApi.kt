package ndp.posapp.data.network.api.admOperation


import com.google.gson.JsonObject
import ndp.posapp.data.model.BusinessPartnerModel
import ndp.posapp.data.model.RepositorySyncHistoryModel
import retrofit2.Call
import retrofit2.http.*
import retrofit2.http.GET


interface AdmOperationApi {

    //GG
    @GET("currency")
    fun GetCurrency(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String
    )
    :Call<JsonObject>
    //GG

    @GET("affectation")
    fun listAffectation(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String
    ): Call<JsonObject>


    @GET("affectation-type")
    fun listAffectationType(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String
    ): Call<JsonObject>


    @GET("parameter/sync-data")
    fun listParameter(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

    @GET("tax/sync-data")
    fun listTax(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

    @GET("currency/sync-data")
    fun listCurrency(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

    @GET("payment-conditions/sync-data")
    fun listPaymentConditions(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>


    @GET("warehouse/sync-data")
    fun listWarehouse(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

    @GET("item/sync-data")
    fun listItem(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

    @GET("pricelist/sync-data")
    fun listPriceList(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

    @GET("businesspartner/sync-data")
    fun listBusinessPartner(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int,
        @Query("partnerType") partnerType: String
    ): Call<JsonObject>

    @GET("businesspartner")
    fun getBusinessPartner(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Query("partnerType") partnerType: String,
        @Query("nif") nif: String
    ): Call<JsonObject>

    @POST("businesspartner")
    fun postBusinessPartner(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Body businessPartnerDTO: BusinessPartnerModel
    ): Call<JsonObject>

    @GET("whskardex/sync-data")
    fun whskardex(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("warehouse") warehouse: String,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>


    @PUT("repository-sync-history")
    fun syncHistory(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Body repositorySyncHistoryModel: RepositorySyncHistoryModel
    ): Call<JsonObject>

    @GET("user/info/{userIdentifier}")
    fun getUserInfo(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Path("userIdentifier") userIdentifier: String
    ): Call<JsonObject>

    @GET("receipttype")
    fun listReceiptType(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String
    ): Call<JsonObject>

    @GET("item")
    fun listItemFilter(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Query("code") itemCode: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int
    ): Call<JsonObject>

    @GET("ratestore")
    fun getRateStore(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Query("status") status: Int,
        @Query("rateDate") rateDate: String,
        @Query("store") store: String
    ): Call<JsonObject>

    @GET("payment-way")
    fun listPaymentWay(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Query("store") store: String
    ): Call<JsonObject>

    @GET("currency")
    fun getCurrency(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String
    ):Call<JsonObject>
}