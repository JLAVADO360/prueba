package ndp.posapp.data.network.api.admEngine

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface AdmEngineApi {

    @GET("consult/dni/{dni}")
    fun consultDNI(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Path("dni") dni: String
    ): Call<JsonObject>

    @GET("consult/ruc/{ruc}")
    fun consultRUC(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Path("ruc") ruc: String
    ): Call<JsonObject>

    @GET("region/sync-data")
    fun listRegion(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

    @GET("region-relation/sync-data")
    fun listRegionRelation(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>


    @GET("region-type/sync-data")
    fun listRegionType(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

}