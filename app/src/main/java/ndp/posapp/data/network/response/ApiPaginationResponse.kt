package ndp.posapp.data.network.response

class ApiPaginationResponse<T> {

    var successful: Boolean = false
    var code: String = ""
    var message: String ?=null
    var data: Pagination<T>? = null
}

