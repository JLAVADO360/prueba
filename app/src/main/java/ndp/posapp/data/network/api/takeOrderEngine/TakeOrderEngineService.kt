package ndp.posapp.data.network.api.takeOrderEngine

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ndp.posapp.core.RetrofitHelper
import ndp.posapp.data.model.SaleOrderModel
import ndp.posapp.data.model.ZoneModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.Pagination
import ndp.posapp.utils.Const

class TakeOrderEngineService {

    private val route = "takeorder-engine/"
    private val takeOrderEngineApi = RetrofitHelper.getRetrofit(route).create(TakeOrderEngineApi::class.java)


    fun listZone(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int,
        user: String
    ): ApiPaginationResponse<List<ZoneModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<ZoneModel>>()
        try {
            val call = takeOrderEngineApi.listZone(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType, user)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<ZoneModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }


    fun listSaleOrder(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        businessPartner: String,
        startDate: String,
        endDate: String,
        page: Int
    ): ApiPaginationResponse<List<SaleOrderModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<SaleOrderModel>>()
        try {
            val call = takeOrderEngineApi.listSaleOrder(signature,token, fingerprint, entity, Const.APPLICATION_POS_APP, businessPartner, Const.APPLICATION_CHANNEL, startDate, endDate, page)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<SaleOrderModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

}