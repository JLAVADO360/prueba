package ndp.posapp.data.network.api.takeOrderEngine

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface TakeOrderEngineApi {


    @GET("zone")
    fun listZone(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage: Int,
        @Query("syncType") syncType: Int,
        @Query("user") user: String
    ): Call<JsonObject>

    @GET("sale-order")
    fun listSaleOrder(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Query("businessPartner") businessPartner: String,
        @Query("channel") channel: String,
        @Query("startDate") startDate: String,
        @Query("endDate") endDate: String,
        @Query("page") page: Int
    ): Call<JsonObject>

    @GET("visit")
    fun listVisit(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Query("businessPartner") businessPartner: String,
        @Query("startDate") startDate: String,
        @Query("endDate") endDate: String,
        @Query("page") page: Int
    ): Call<JsonObject>


    @GET("payment-receipt")
    fun listPaymentReceipt(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("businessPartner") businessPartner: String,
        @Query("startDate") startDate: String,
        @Query("endDate") endDate: String,
        @Query("page") page: Int,
        @Query("channel") channel: String
    ): Call<JsonObject?>


}