package ndp.posapp.data.network.response

class Pagination<T> {

    var count : Int = 0
    var nextPage : String ? = null
    var previousPage : String ? = null
    var value : T? = null

}