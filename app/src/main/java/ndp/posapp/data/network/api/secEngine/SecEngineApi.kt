package ndp.posapp.data.network.api.secEngine

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path


interface SecEngineApi {

    @POST("auth/login")
    fun login(
        @Header("identifier") identifier: String,
        @Header("signature") signature: String,
        @Header("fingerprint") fingerprint: String,
        @Header("application") application: String
    ): Call<JsonObject>


    @GET("permission/listByRole/{objectId}")
    fun permissionListByRole(
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Path("objectId") objectId: String,
        @Header("application") application: String
    ): Call<JsonObject>


}