package ndp.posapp.data.network.api.posEngine

import com.google.gson.JsonObject
import ndp.posapp.data.model.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import retrofit2.http.Path
import retrofit2.http.POST

interface PosEngineApi {

    //GG
    @GET("secengine/permission/listByRole/")
    fun loadPermission(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String
        )

    @GET("cashregister/opening")
    fun listOpeningListCashier(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,

        @Query("page") page : Int,
        @Query("itemPerPage") itemPerPage:Int,
        @Query("cashRegisterCode") cashRegisterCode : String,
        @Query("startDate") startDate : String,
        @Query("endDate") endDate: String

    )
    :Call<JsonObject>

    @POST("cashregister/opening")
    fun OpeningCashRegister(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Body openingCashRegisterSaveModel: OpeningCashRegisterSaveModel
    ):Call<JsonObject>

    @GET("cashregister/balance")
    fun listBalanceListCashier(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,

        @Query("page") page : Int,
        @Query("cashRegisterCode") cashRegisterCode : String,
        @Query("startDate") startDate : String,
        @Query("endDate") endDate: String

    )
            :Call<JsonObject>

    @GET("cashregister/balance")
    fun cashRegisterBalance(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Body cashRegisterBalanceDTO: CashRegisterBalanceDTOModel

    )
            :Call<JsonObject>

    @GET("cashregister/closure")
    fun listClosingListCashier(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Query("page") page : Int,
        @Query("itemPerPage") itemPerPage:Int,
        @Query("cashRegisterCode") cashRegisterCode : String,
        @Query("startDate") startDate : String,
        @Query("endDate") endDate: String

    )
            :Call<JsonObject>

    @GET("cashregister/movement")
    fun listMovementListCashier(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,

        @Query("page") page : Int,
        @Query("cashRegisterCode") cashRegisterCode : String,
        @Query("store") store : String,
        @Query("turntDate") startDate : String,

    )
            :Call<JsonObject>

    @GET("cashregister/turn")
    fun listCashRegisterTurn(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,

        @Query("dateTurn") dateTurn : String,
        @Query("cashRegisterCode") cashRegisterCode : String,
        @Query("store") store : String,
        )
            :Call<JsonObject>
    //GG
    @GET("saleorder")
    fun listSaleOrder(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Query("codclient") codclient : String,
        @Query("startDate") startDate : String,
        @Query("endDate") endDate: String,
        @Query("page") page : Int
    ): Call<JsonObject>

    @GET("paymentreceipt")
    fun listPaymentReceipt(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Query("businessPartner") businessPartner : String,
        @Query("startDate") startDate : String,
        @Query("endDate") endDate: String,
        @Query("store") store: String,
        @Query("page") page : Int
    ): Call<JsonObject>

    @GET("paymentreceipt")
    fun getPaymentReceipt(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Query("code") code : String
    ): Call<JsonObject>

    @POST("saleorder")
    fun postSaleOrder(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Body SaleOrderModelDTO: SaleOrderModel
    ): Call<JsonObject>

    @POST("saleorder/quick-sale")
    fun postQuickSale(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Body SaleOrderModelDTO: SaleOrderModel
    ): Call<JsonObject>

    @GET("cashregister/info/{userIdentifier}")
    fun listCashRegisterInfo(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Path("userIdentifier") userIdentifier: String
    ): Call<JsonObject>

    @POST("payment/quick-payment")
    fun postQuickPayment(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Body PaymentModelDTO: PaymentModel
    ): Call<JsonObject>

    @GET("creditnote")
    fun getCreditNote(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Query("creditNoteNumber") creditNoteNumber : String,
    ): Call<JsonObject>

    @POST("cashregister/balance/generate")
    fun listClosingCashGenerate(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Body cashRegisterBalanceDTO: CashRegisterBalanceDTOModel
    ): Call<JsonObject>

    @POST("cashregister/closure")
    fun closingCashierRegister(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Body closingCashRegisterSaveModel: ClosingCashRegisterSaveModel
    ):Call<JsonObject>


    @GET("cashregister/imbalancereason")
    fun listReasonImbalance(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
    ):Call<JsonObject>

    @POST("vault-movement")
    fun movementTransferIO(
        @Header("entity") entity: String,
        @Header("fingerprint") fingerprint: String,
        @Header("signature") signature : String,
        @Header("token") token : String,
        @Header("application") application: String,
        @Body movementTransferIOModel: MovementTransferIOModel
    ):Call<JsonObject>



}