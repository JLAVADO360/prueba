package ndp.posapp.data.network.api.posOperation

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ndp.posapp.core.RetrofitHelper
import ndp.posapp.data.model.CorrelativeModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.Pagination
import ndp.posapp.utils.Const

class PosOperationService {
    private val route = "posoperation/"
    private val posOperationApi = RetrofitHelper.getRetrofit(route).create(PosOperationApi::class.java)

    fun listCorrelative(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String,
        cashRegisterCode: String
    ): ApiPaginationResponse<List<CorrelativeModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<CorrelativeModel>>()
        try {
            val call = posOperationApi.listCorrelative(entity, fingerprint, signature, token, Const.APPLICATION_POS_APP, cashRegisterCode)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<CorrelativeModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }
}