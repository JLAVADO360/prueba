package ndp.posapp.data.network.api.admEngine

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import ndp.posapp.core.RetrofitHelper
import ndp.posapp.data.model.RegionModel
import ndp.posapp.data.model.RegionRelationModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.data.network.response.Pagination
import ndp.posapp.utils.Const
import java.lang.Exception

class AdmEngineService {

    private val route = "admengine/"
    private val admEngineApi = RetrofitHelper.getRetrofit(route).create(AdmEngineApi::class.java)

    fun consultDNI(signature: String, token: String, fingerprint: String, entity: String, dni: String): ApiResponse<JsonObject> {
        val apiResponse = ApiResponse<JsonObject>()
        try {
            val call = admEngineApi.consultDNI(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, dni)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            apiResponse.data = response.body()?.get("data")!!.asJsonObject
        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun consultRUC(signature: String, token: String, fingerprint: String, entity: String, ruc: String): ApiResponse<JsonObject> {
        val apiResponse = ApiResponse<JsonObject>()
        try {
            val call = admEngineApi.consultRUC(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, ruc)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            apiResponse.data = response.body()?.get("data")!!.asJsonObject
        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun listRegion(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<RegionModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<RegionModel>>()
        try {
            val call = admEngineApi.listRegion(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<RegionModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message
        }
        return apiPaginationResponse
    }


    fun listRegionRelation(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<RegionRelationModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<RegionRelationModel>>()
        try {
            val call = admEngineApi.listRegionRelation(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<RegionRelationModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message
        }
        return apiPaginationResponse
    }
}