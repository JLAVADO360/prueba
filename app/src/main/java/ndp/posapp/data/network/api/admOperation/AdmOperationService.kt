package ndp.posapp.data.network.api.admOperation

import android.app.Application
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import ndp.posapp.core.RetrofitHelper
import ndp.posapp.data.db.entity.CurrencyEntity
import ndp.posapp.data.model.*
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.data.network.response.Pagination
import ndp.posapp.utils.Const

class AdmOperationService {

    private val route = "admoperation/"
    private val admOperationApi = RetrofitHelper.getRetrofit(route).create(AdmOperationApi::class.java)


    fun listAffectation(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String
    ): ApiPaginationResponse<List<AffectationModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<AffectationModel>>()
        try {
            val call = admOperationApi.listAffectation(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<AffectationModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listAffectationType(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String
    ): ApiPaginationResponse<List<AffectationTypeModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<AffectationTypeModel>>()
        try {
            val call = admOperationApi.listAffectationType(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<AffectationTypeModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }


    fun listParameter(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<ParameterModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<ParameterModel>>()
        try {
            val call = admOperationApi.listParameter(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<ParameterModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listTax(signature: String, token: String, fingerprint: String, entity: String, repository: String, page: Int, syncType: Int): ApiPaginationResponse<List<TaxModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<TaxModel>>()
        try {
            val call = admOperationApi.listTax(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<TaxModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    //ApiPaginationResponse
    fun getCurrency(signature: String, token: String, fingerprint: String, entity: String): MutableList<CurrencyModel> {
        var apiPaginationResponse:MutableList<CurrencyModel> = mutableListOf()
        try {
            val call = admOperationApi.GetCurrency(fingerprint, entity, signature, token, Const.APPLICATION_POS_APP)
            val response = call.execute()
            /*apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString*/
            //apiPaginationResponse.data = response.body()?.get("data")!!.
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val jsonArray = jsonObject?.get("value")!!.asJsonArray
            val jsonEntity = jsonArray.get(0)
            var mlist:MutableList<CurrencyModel> = mutableListOf()
            var dmlistSol:MutableList<CurrencyDenominationModel> = mutableListOf()
            var dmlistEuro:MutableList<CurrencyDenominationModel> = mutableListOf()
            var dmlistUSD:MutableList<CurrencyDenominationModel> = mutableListOf()
            for (i in 0..jsonArray.size()-1) {
                val jobject:JsonObject = jsonArray.get(i) as JsonObject
                var CM:CurrencyModel = CurrencyModel()
                //val type = object : TypeToken<Pagination<CurrencyModel>>() {}.type
                ///apiPaginationResponse.data = g.fromJson(jobject.toString(), type)
                CM.code = jobject.get("code").asString
                CM.description = jobject.get("description").asString
                CM.status = jobject.get("status").asInt
                CM.name = jobject.get("name").asString
                CM.symbol = jobject.get("symbol").asString
                CM.isDefault = jobject.get("isDefault").asInt
                /*CM.createUser = jobject.get("createUser").asString
                CM.updateUser = jobject.get("updateUser").asString*/
                CM.createDate = jobject.get("createDate").asString
                //CM.updateDate = jobject.get("updateDate").asString
                if(CM.code.equals("SOL"))
                {

                    var jobj = jobject.get("denomination").asJsonArray
                for(j in 0..jobj.size()-1)
                {
                    val jobj:JsonObject = jobj.get(j) as JsonObject
                    var CDM:CurrencyDenominationModel = CurrencyDenominationModel()
                    CDM.status = jobj.get("status").asInt
                    CDM.value = jobj.get("value").asDouble
                    dmlistSol.add(CDM)
                }
                CM.denomination = dmlistSol
                }
                if(CM.code.equals("EUR"))
                {

                    var jobj = jobject.get("denomination").asJsonArray
                    for(j in 0..jobj.size()-1)
                    {
                        val jobj:JsonObject = jobj.get(j) as JsonObject
                        var CDM:CurrencyDenominationModel = CurrencyDenominationModel()
                        CDM.status = jobj.get("status").asInt
                        CDM.value = jobj.get("value").asDouble
                        dmlistEuro.add(CDM)
                    }
                    CM.denomination = dmlistEuro
                }
                if(CM.code.equals("USD"))
                {

                    var jobj = jobject.get("denomination").asJsonArray
                    for(j in 0..jobj.size()-1)
                    {
                        val jobj:JsonObject = jobj.get(j) as JsonObject
                        var CDM:CurrencyDenominationModel = CurrencyDenominationModel()
                        CDM.status = jobj.get("status").asInt
                        CDM.value = jobj.get("value").asDouble
                        dmlistUSD.add(CDM)
                    }
                    CM.denomination = dmlistUSD
                }
                //CM.denomination = jobject.get("denomination")
                //CM.company = jobject.get("company").asString
                //val caca:String = jobject.asString
                //val rrr = caca
                //val asss = jsonArray
                //val fff = asss
                /*CM.code = jsonArray.get(i).asString
                mlist.add(CM)
                //mlist.add(jsonArray.get(i)
                apiPaginationResponse.data?.value = mlist*/
                mlist.add(CM)
                //var k:Int=dmlist.size-1
                //if(k>0) {*/
                    /*while (k >= 0) {
                        dmlist.removeAt(k)
                        k = k - 1
                    }*/
                //}
            }
            apiPaginationResponse=mlist
            //Toast.makeText(Application, mlist.get(2).denomination.get(2).value.toString(), Toast.LENGTH_SHORT).show
            val type = object : TypeToken<Pagination<CurrencyModel>>() {}.type
            if (jsonObject != null) {
                //apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            //apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listCurrency(signature: String, token: String, fingerprint: String, entity: String, repository: String, page: Int, syncType: Int): ApiPaginationResponse<List<CurrencyModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<CurrencyModel>>()
        try {
            val call = admOperationApi.listCurrency(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<CurrencyModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listPaymentConditions(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<PaymentConditionsModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<PaymentConditionsModel>>()
        try {
            val call = admOperationApi.listPaymentConditions(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<PaymentConditionsModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listWarehouse(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<WarehouseModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<WarehouseModel>>()
        try {
            val call = admOperationApi.listWarehouse(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<WarehouseModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listItem(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<ItemModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<ItemModel>>()
        try {
            val call = admOperationApi.listItem(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<ItemModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }


    fun listPriceList(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<PriceListModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<PriceListModel>>()
        try {
            val call = admOperationApi.listPriceList(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<PriceListModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listBusinessPartner(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<BusinessPartnerModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<BusinessPartnerModel>>()
        try {
            val call = admOperationApi.listBusinessPartner(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository, page, Const.DEFAULT_ITEM_PER_PAGE, syncType, "cCustomer")
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<BusinessPartnerModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun getBusinessPartner(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        nif: String
    ): ApiPaginationResponse<List<BusinessPartnerModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<BusinessPartnerModel>>()
        try {
            val call = admOperationApi.getBusinessPartner(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, "C", nif)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<BusinessPartnerModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun postBusinessPartner(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        businessPartnerDTO: BusinessPartnerModel
    ): ApiResponse<BusinessPartnerModel> {
        val apiResponse = ApiResponse<BusinessPartnerModel>()
        try {
            val call = admOperationApi.postBusinessPartner(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, businessPartnerDTO)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<BusinessPartnerModel?>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun syncWhskardex(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        warehouse: String,
        syncType: Int
    ): ApiResponse<List<WhsKardexModel>> {
        val apiResponse = ApiResponse<List<WhsKardexModel>>()
        try {
            val call = admOperationApi.whskardex(signature, token, fingerprint, entity, Const.APPLICATION_CHANNEL, fingerprint, warehouse, syncType)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonArray = response.body()?.get("data")?.asJsonArray
            val type = object : TypeToken<List<WhsKardexModel>>() {}.type
            if (jsonArray != null) {
                apiResponse.data = g.fromJson(jsonArray.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun syncHistory(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repositorySyncHistoryModel: RepositorySyncHistoryModel
    ): ApiResponse<RepositorySyncHistoryModel> {
        val apiResponse = ApiResponse<RepositorySyncHistoryModel>()
        try {
            val call = admOperationApi.syncHistory(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, fingerprint, repositorySyncHistoryModel)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<RepositorySyncHistoryModel?>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun getUserInfo(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        userIdentifier: String
    ): ApiResponse<UserInfoModel> {
        val apiResponse = ApiResponse<UserInfoModel>()
        try {
            val call = admOperationApi.getUserInfo(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, userIdentifier)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code = response.body()?.get("code")!!.asString
            apiResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<UserInfoModel?>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado"
        }
        return apiResponse
    }

    fun listReceiptType(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String
    ): ApiPaginationResponse<List<ReceiptTypeModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<ReceiptTypeModel>>()
        try {
            val call = admOperationApi.listReceiptType(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, repository)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<ReceiptTypeModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listItemFilter(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        itemCode: String
    ): ApiPaginationResponse<List<ItemModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<ItemModel>>()
        try {
            val call = admOperationApi.listItemFilter(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, itemCode, 0, Const.DEFAULT_ITEM_PER_PAGE)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<ItemModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun getRateStore(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        status: Int,
        rateDate: String,
        store: String
    ): ApiPaginationResponse<List<RateStoreModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<RateStoreModel>>()
        try {
            val call = admOperationApi.getRateStore(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, status, rateDate, store)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<RateStoreModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }

    fun listPaymentWay(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        store: String
    ): ApiPaginationResponse<List<PaymentWayModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<PaymentWayModel>>()
        try {
            val call = admOperationApi.listPaymentWay(signature, token, fingerprint, entity, Const.APPLICATION_POS_APP, store)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<PaymentWayModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e: Exception) {
            apiPaginationResponse.message = e.message ?: "Error Inesperado"
        }
        return apiPaginationResponse
    }


    fun getCurrencyData(
        entity: String,
        fingerprint: String,
        signature: String,
        token: String
    ): ApiResponse<CurrencyDataModel> {
        var apiResponse = ApiResponse<CurrencyDataModel>()
        try {
            val call = admOperationApi.getCurrency( entity, fingerprint,signature, token, Const.APPLICATION_POS_APP)
            val response = call.execute()
            apiResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiResponse.code       = response.body()?.get("code")!!.asString
            apiResponse.message    = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<CurrencyDataModel>() {}.type
            if (jsonObject != null) {
                apiResponse.data = g.fromJson(jsonObject.toString(), type)
            }
        } catch (e: Exception) {
            apiResponse.message = e.message ?: "Error Inesperado...."
        }
        return apiResponse
    }

}