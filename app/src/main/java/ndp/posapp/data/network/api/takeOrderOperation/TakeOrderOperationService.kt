package ndp.posapp.data.network.api.takeOrderOperation

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ndp.posapp.core.RetrofitHelper
import ndp.posapp.data.model.VisitReasonModel
import ndp.posapp.data.network.response.ApiPaginationResponse
import ndp.posapp.data.network.response.Pagination
import ndp.posapp.utils.Const

class TakeOrderOperationService {

    private val route = "takeorder-operation/"
    private val takeOrderOperationApi = RetrofitHelper.getRetrofit(route).create(TakeOrderOperationApi::class.java)

    fun listVisitReason(
        signature: String,
        token: String,
        fingerprint: String,
        entity: String,
        repository: String,
        page: Int,
        syncType: Int
    ): ApiPaginationResponse<List<VisitReasonModel>> {
        val apiPaginationResponse = ApiPaginationResponse<List<VisitReasonModel>>()
        try {
            val call = takeOrderOperationApi.listVisitReason(signature,token,fingerprint,entity,Const.APPLICATION_POS_APP,repository,page,Const.DEFAULT_ITEM_PER_PAGE,syncType)
            val response = call.execute()
            apiPaginationResponse.successful = response.body()?.get("successful")!!.asBoolean
            apiPaginationResponse.code = response.body()?.get("code")!!.asString
            apiPaginationResponse.message = response.body()?.get("message")!!.asString
            val g = Gson()
            val jsonObject = response.body()?.get("data")?.asJsonObject
            val type = object : TypeToken<Pagination<List<VisitReasonModel>>>() {}.type
            if (jsonObject != null) {
                apiPaginationResponse.data = g.fromJson(jsonObject.toString(), type)
            }

        } catch (e : Exception){
            apiPaginationResponse.message = e.message
        }
        return apiPaginationResponse
    }
}