package ndp.posapp.data.network.api.takeOrderOperation

import com.google.gson.JsonObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface TakeOrderOperationApi {

    @GET("visitreason/sync-data")
    fun listVisitReason(
        @Header("signature") signature: String,
        @Header("token") token: String,
        @Header("fingerprint") fingerprint: String,
        @Header("entity") entity: String,
        @Header("application") application: String,
        @Header("repository") repository: String,
        @Query("page") page: Int,
        @Query("itemPerPage") itemPerPage : Int,
        @Query("syncType") syncType: Int
    ): Call<JsonObject>

}