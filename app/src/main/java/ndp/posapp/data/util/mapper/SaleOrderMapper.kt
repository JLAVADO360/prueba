package ndp.posapp.data.util.mapper

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ndp.posapp.data.db.entity.SaleOrderEntity
import ndp.posapp.data.model.DocumentInstallmentsModel
import ndp.posapp.data.model.PickUpDocumentModel
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.data.model.SaleOrderModel
import ndp.posapp.data.model.SampleModel


fun SaleOrderModel.toEntity(company: String, internalStatus: Int, receiptNumber: String): SaleOrderEntity {
    return SaleOrderEntity().also {
        it.company = company
        it.internalStatus = internalStatus
        it.internalReceiptNumber = receiptNumber
        it.additionalData = Gson().toJson(additionalData, object : TypeToken<MutableMap<String, Any>?>() {}.type)
        it.bonus = bonus
        it.businessPartnerCode = businessPartnerCode
        it.businessPartnerName = businessPartnerName
        it.code = code
        it.codeERP = codeERP
        it.comment = comment
        it.createDate = createDate
        it.createUser = createUser
        it.deliveryDate = deliveryDate
        it.detail = Gson().toJson(detail, object : TypeToken<List<SaleOrderDetailModel>>() {}.type)
        it.detraction = detraction
        it.discount = discount
        it.discountPercentage = discountPercentage
        it.docCurrency = docCurrency
        it.docDate = docDate
        it.docNum = docNum
        it.documentInstallments = Gson().toJson(documentInstallments, object : TypeToken<List<DocumentInstallmentsModel>>() {}.type)
        it.docYear = docYear
        it.emailSalePerson = emailSalePerson
        it.erpDocNum = erpDocNum
        it.erpSerialNumber = erpSerialNumber
        it.icbper = icbper
        it._isDispatch = isDispatch
        it.migration = migration
        it.migrationDate = migrationDate
        it.migrationStatus = migrationStatus
        it.nameSalePerson = nameSalePerson
        it.nif = nif
        it.payAddress = payAddress
        it.payAddressName = payAddressName
        it.paymentConditionName = paymentConditionName
        it.paymentReceiptNumber = paymentReceiptNumber
        it.paymentReceiptType = paymentReceiptType
        it.pickUps = Gson().toJson(pickUps, object : TypeToken<List<PickUpDocumentModel>>() {}.type)
        it.pickingStatus = pickingStatus
        it.plateNumber = plateNumber
        it.reservationInvoice = reservationInvoice
        it.saleType = saleType
        it.sampleCode = sampleCode
        it.samples = Gson().toJson(samples, object : TypeToken<List<SampleModel>>() {}.type)
        it.shipAddress = shipAddress
        it.shipAddressName = shipAddressName
        it.status = status
        it.store = store
        it.storeAddress = storeAddress
        it.subtotal = subtotal
        it.subtotalDiscount = subtotalDiscount
        it.taxAmount = taxAmount
        it.total = total
        it.totalDetraction = totalDetraction
        it.transactionalCode = transactionalCode
        it.u_VS_AFEDET = u_VS_AFEDET
        it.u_VS_CODSER = u_VS_CODSER
        it.u_VS_MONDET = u_VS_MONDET
        it.u_VS_PORDET = u_VS_PORDET
        it.ubication = ubication
        it.ubicationEntry = ubicationEntry
        it.updateDate = updateDate
        it.updateUser = updateUser
        it.warehouse = warehouse
    }

}

fun SaleOrderEntity.toModel(): SaleOrderModel {
    val additional: MutableMap<String, Any?>? = Gson().fromJson(additionalData, object : TypeToken<MutableMap<String, Any>?>() {}.type)
    additional?.forEach { (key, value) -> if (value is Double && value % 1 == 0.0) additional[key] = value.toInt() }

    return SaleOrderModel().also {
        it.additionalData = additional
        it.bonus = bonus
        it.businessPartnerCode = businessPartnerCode
        it.businessPartnerName = businessPartnerName
        it.code = code
        it.codeERP = codeERP
        it.comment = comment
        it.createDate = createDate
        it.createUser = createUser
        it.deliveryDate = deliveryDate
        it.detail = Gson().fromJson(detail, object : TypeToken<List<SaleOrderDetailModel>>() {}.type)
        it.detraction = detraction
        it.discount = discount
        it.discountPercentage = discountPercentage
        it.docCurrency = docCurrency
        it.docDate = docDate
        it.docNum = docNum
        it.documentInstallments = Gson().fromJson(documentInstallments, object : TypeToken<List<DocumentInstallmentsModel>>() {}.type)
        it.docYear = docYear
        it.emailSalePerson = emailSalePerson
        it.erpDocNum = erpDocNum
        it.erpSerialNumber = erpSerialNumber
        it.icbper = icbper
        it.isDispatch = _isDispatch
        it.migration = migration
        it.migrationDate = migrationDate
        it.migrationStatus = migrationStatus
        it.nameSalePerson = nameSalePerson
        it.nif = nif
        it.payAddress = payAddress
        it.payAddressName = payAddressName
        it.paymentConditionName = paymentConditionName
        it.paymentReceiptNumber = paymentReceiptNumber
        it.paymentReceiptType = paymentReceiptType
        it.pickUps = Gson().fromJson(pickUps, object : TypeToken<List<PickUpDocumentModel>>() {}.type)
        it.pickingStatus = pickingStatus
        it.plateNumber = plateNumber
        it.reservationInvoice = reservationInvoice
        it.saleType = saleType
        it.sampleCode = sampleCode
        it.samples = Gson().fromJson(samples, object : TypeToken<List<SampleModel>>() {}.type)
        it.shipAddress = shipAddress
        it.shipAddressName = shipAddressName
        it.status = status
        it.store = store
        it.storeAddress = storeAddress
        it.subtotal = subtotal
        it.subtotalDiscount = subtotalDiscount
        it.taxAmount = taxAmount
        it.total = total
        it.totalDetraction = totalDetraction
        it.transactionalCode = transactionalCode
        it.u_VS_AFEDET = u_VS_AFEDET
        it.u_VS_CODSER = u_VS_CODSER
        it.u_VS_MONDET = u_VS_MONDET
        it.u_VS_PORDET = u_VS_PORDET
        it.ubication = ubication
        it.ubicationEntry = ubicationEntry
        it.updateDate = updateDate
        it.updateUser = updateUser
        it.warehouse = warehouse
    }
}