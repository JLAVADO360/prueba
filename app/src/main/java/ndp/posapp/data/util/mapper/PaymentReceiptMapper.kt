package ndp.posapp.data.util.mapper

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ndp.posapp.data.db.entity.PaymentReceiptEntity
import ndp.posapp.data.model.PaymentModel
import ndp.posapp.data.model.PaymentReceiptModel

fun PaymentReceiptModel.toEntity(company: String, internalStatus: Int, paymentModel: PaymentModel): PaymentReceiptEntity {
    return PaymentReceiptEntity().also {
        it.company = company
        it.internalStatus = internalStatus
        it.businessName = businessName
        it.code = code
        it.createDate = createDate
        it.credit = credit
        it.currency = currency
        it.nif = nif
        it.paymentReceiptNumber = paymentReceiptNumber
        it.payment = Gson().toJson(paymentModel, object : TypeToken<PaymentModel>() {}.type)
        it.status = status
        it.total = total
    }
}

fun PaymentReceiptEntity.toModel(): PaymentReceiptModel {
    val payments: MutableList<PaymentModel> = ArrayList()
    val paymentModel: PaymentModel? = Gson().fromJson(payment, object : TypeToken<PaymentModel>() {}.type)
    paymentModel?.let {
        payments.add(paymentModel)
    }

    return PaymentReceiptModel().also {
        it.businessName = businessName
        it.code = code
        it.createDate = createDate
        it.credit = credit
        it.currency = currency
        it.nif = nif
        it.paymentReceiptNumber = paymentReceiptNumber
        it.payments = payments
        it.status = status
        it.total = total
    }
}