package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "t_paymentWay", primaryKeys = ["company", "store"])
class PaymentWayEntity {
    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "store")
    var store: String = String()

    @ColumnInfo(name = "paymentWayBody")
    var paymentWayBody: String = String()
}