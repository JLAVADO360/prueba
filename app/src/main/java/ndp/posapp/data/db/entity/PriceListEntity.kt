package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "t_priceList", primaryKeys = ["code","company"])
class PriceListEntity {
    @ColumnInfo(name = "code")
    var code: Int = 0

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "currency")
    var currency: String? = null

    @ColumnInfo(name = "description")
    var description: String? = null

    @ColumnInfo(name = "item")
    var item: String? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int? = null

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "price")
    var price: Double? = null

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "type")
    var type: String? = null

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null
}