package ndp.posapp.data.db.entityView

import ndp.posapp.data.db.entity.KardexEntity
import java.io.Serializable


data class Kardex(
    var warehouse: String,
    var itemEntity: String,
    var logicStatus: Int?,
    var stock: Double,
    var stockCommitted: Double,
    var stockDisponible: Double,
    var stockNoForSale: Double,
    var stockRequested: Double,
    var store: String?,
    var kardexUbication: MutableList<KardexEntity> = ArrayList()
) : Serializable

