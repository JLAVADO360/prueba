package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import ndp.posapp.data.db.entity.BusinessPartnerEntity
import ndp.posapp.data.db.entityView.BusinessPartnerView

@Dao
interface BusinessPartnerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(businessPartnerEntity: BusinessPartnerEntity)

    @Transaction
    @Query("SELECT * FROM t_businessPartner WHERE company = :company")
    fun list(company: String): LiveData<List<BusinessPartnerView>>
}