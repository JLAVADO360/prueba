package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.TaxEntity

@Dao
interface TaxDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(taxEntity: TaxEntity)

    @Query("SELECT * FROM t_tax WHERE company = :company")
    fun list(company: String) : LiveData<List<TaxEntity>>
}