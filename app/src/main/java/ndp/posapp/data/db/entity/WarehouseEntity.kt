package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "t_warehouse", primaryKeys = ["code","company"])
class WarehouseEntity {

    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "address")
    var address: String? = null

    @ColumnInfo(name = "description")
    var description: String? = null

    @ColumnInfo(name = "distributionList")
    var distributionList: String? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int? = null

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "store")
    var store: String? = null

}