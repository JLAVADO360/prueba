package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable


@Entity(tableName = "t_item", primaryKeys = ["code","company"])
class ItemEntity : Serializable{
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "barCode")
    var barCode: String? = null

    @ColumnInfo(name = "color")
    var color: String? = null

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "inventoryItem")
    var inventoryItem: Int ? = null

    @ColumnInfo(name = "inventoryUnit")
    var inventoryUnit: String? = null

    @ColumnInfo(name = "isbn")
    var isbn: String? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int ? = null

    @ColumnInfo(name = "name")
    var name: String ? = null

    @ColumnInfo(name = "pricingUnit")
    var pricingUnit: Int? = null

    @ColumnInfo(name = "projectName")
    var projectName: String ? = null

    @ColumnInfo(name = "salesItem")
    var salesItem: Int? = null

    @ColumnInfo(name = "salesItemsPerUnit")
    var salesItemsPerUnit: Double? = null

    @ColumnInfo(name = "salesUnit")
    var salesUnit: String? = null

    @ColumnInfo(name = "size")
    var size: String? = null

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "taxCode")
    var taxCode: String ? = null

    @ColumnInfo(name = "type")
    var type: String ? = null

    @ColumnInfo(name = "u_VS_AFEDET")
    var u_VS_AFEDET: String? = null

    @ColumnInfo(name = "u_VS_CODDET")
    var u_VS_CODDET: String? = null

    @ColumnInfo(name = "u_VS_PORDET")
    var u_VS_PORDET: Double? = null

    @ColumnInfo(name = "unitGroupEntry")
    var unitGroupEntry: Int? = null

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null

    @ColumnInfo(name = "weight")
    var weight: Double? = null
}