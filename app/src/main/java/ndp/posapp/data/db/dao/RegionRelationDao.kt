package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.RegionRelationEntity

@Dao
interface RegionRelationDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(regionRelationEntity: RegionRelationEntity)

    @Query("SELECT childrenRegion FROM t_regionRelation WHERE parentRegion=:parentRegion")
    fun list(parentRegion : String?) : List<String>

    @Query("SELECT COUNT(childrenRegion) FROM t_regionRelation")
    fun countRegionRelation() : Int

}