package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_paymentReceipt")
class PaymentReceiptEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "internalId")
    var internalId: Long? = null

    @ColumnInfo(name = "saleOrderId")
    var saleOrderId: Long? = null

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "internalStatus")
    var internalStatus: Int = 1

    @ColumnInfo(name = "businessName")
    var businessName: String? = null

    @ColumnInfo(name = "code")
    var code: String? = null

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "credit")
    var credit: Double? = null

    @ColumnInfo(name = "currency")
    var currency: String? = null

    @ColumnInfo(name = "nif")
    var nif: String? = null

    @ColumnInfo(name = "paymentReceiptNumber")
    var paymentReceiptNumber: String? = null

    @ColumnInfo(name = "payment")
    var payment: String? = null

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "total")
    var total: Double? = null

}