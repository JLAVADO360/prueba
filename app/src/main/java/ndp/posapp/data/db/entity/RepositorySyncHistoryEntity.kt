package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "t_repositorySyncHistory", primaryKeys = ["user","company"])
class RepositorySyncHistoryEntity {

    @ColumnInfo(name = "user")
    var user: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "channel")
    var channel: String = ""

    @ColumnInfo(name = "lastSync")
    var lastSync: String = ""

    @ColumnInfo(name = "repository")
    var repository: String = ""

    @ColumnInfo(name = "warehouse")
    var warehouse: String? = null
}