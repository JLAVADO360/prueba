package ndp.posapp.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import ndp.posapp.data.db.dao.*
import ndp.posapp.data.db.entity.*

@Database(
    entities =
    [
        RepositorySyncHistoryEntity::class,
        ObjectEntity::class,
        SaleOrderEntity::class,
        AffectationEntity::class,
        AffectationTypeEntity::class,
        ReceiptTypeEntity::class,
        ParameterEntity::class,
        VisitReasonEntity::class,
        ZoneEntity::class,
        ZoneUserEntity::class,
        ZoneWarehouseEntity::class,
        TaxEntity::class,
        CurrencyEntity::class,
        PaymentConditionsEntity::class,
        WarehouseEntity::class,
        UbicationEntity::class,
        ItemEntity::class,
        WhsKardexEntity::class,
        KardexEntity::class,
        PriceListDetailEntity::class,
        PriceListEntity::class,
        RateStoreEntity::class,
        BusinessPartnerEntity::class,
        BusinessPartnerAddressEntity::class,
        ContactPersonEntity::class,
        RegionEntity::class,
        RegionRelationEntity::class,
        CashRegisterInfoEntity::class,
        CorrelativeEntity::class,
        PaymentWayEntity::class,
        PaymentReceiptEntity::class
    ],
    version = 1
)
abstract class PosAppDb : RoomDatabase() {


    abstract fun repositorySyncHistoryDao(): RepositorySyncHistoryDao
    abstract fun objectDao(): ObjectDao
    //GG
    abstract fun openingListCashierDao():OpeningListCashierDao
    abstract fun closingListCashierDao():ClosingListCashierDao
    abstract fun balanceListCashierDao():BalanceListCashierDao
    abstract fun cashRegisterBalanceDao():CashRegisterBalanceDao
    abstract fun movementListCashierDao():MovementListCashierDao
    abstract fun reasonListImbalanceDao():ListReasonImbalanceDao
    abstract fun openingCashRegisterDao():OpeningCashRegisterDao
    abstract fun turnDao():TurnDao
    abstract fun closingCashierGenerateDAO():ClosingCashierGenerateDAO
    abstract fun closingCashRegisterDAO():ClosingCashRegisterDAO
    abstract fun movementCashRegisterDAO():MovementCashRegisterDAO
    //GG
    abstract fun saleOrderDao(): SaleOrderDao
    abstract fun affectationDao(): AffectationDao
    abstract fun affectationTypeDao(): AffectationTypeDao
    abstract fun receiptTypeDao(): ReceiptTypeDao
    abstract fun parameterDao(): ParameterDao
    abstract fun visitReasonDao(): VisitReasonDao
    abstract fun zoneDao(): ZoneDao
    abstract fun zoneUserDao(): ZoneUserDao
    abstract fun zoneWarehouseDao(): ZoneWarehouseDao
    abstract fun taxDao(): TaxDao
    abstract fun currencyDao(): CurrencyDao
    abstract fun paymentConditionsDao(): PaymentConditionsDao
    abstract fun warehouseDao(): WarehouseDao
    abstract fun ubicationDao(): UbicationDao
    abstract fun itemDao(): ItemDao
    abstract fun whsKardexDao(): WhsKardexDao
    abstract fun kardexDao(): KardexDao
    abstract fun priceListDetailDao(): PriceListDetailDao
    abstract fun priceListDao(): PriceListDao
    abstract fun rateStoreDao(): RateStoreDao
    abstract fun businessPartnerDao(): BusinessPartnerDao
    abstract fun businessPartnerAddressDao(): BusinessPartnerAddressDao
    abstract fun contactPersonDao(): ContactPersonDao
    abstract fun regionDao(): RegionDao
    abstract fun regionRelationDao(): RegionRelationDao
    abstract fun cashRegisterInfoDao(): CashRegisterInfoDao
    abstract fun correlativeDao(): CorrelativeDao
    abstract fun paymentWayDao(): PaymentWayDao
    abstract fun paymentReceiptDao(): PaymentReceiptDao

    abstract fun currencyDataDao(): CurrencyDataDAO

    companion object {

        @Volatile
        private var INSTANCE: PosAppDb? = null


        fun getDatabase(context: Context, scope: CoroutineScope): PosAppDb {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    PosAppDb::class.java,
                    "posApp.db"
                )
                    .fallbackToDestructiveMigration()
                    .addCallback(PosAppDbCallBack(scope))
                    .build()
                INSTANCE = instance
                instance
            }
        }

        private class PosAppDbCallBack(private val scope: CoroutineScope) : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {

                    }
                }
            }
        }

    }


}