package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable

@Entity(tableName = "t_kardex",primaryKeys = ["ubication", "item", "company"])
class KardexEntity: Serializable {

    @ColumnInfo(name = "ubication")
    var ubication: String = ""

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "item")
    var item: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int = 0

    @ColumnInfo(name = "status")
    var status: Int = 0

    @ColumnInfo(name = "stock")
    var stock: Double = 0.0

    @ColumnInfo(name = "stockCommitted")
    var stockCommitted: Double = 0.0

    @ColumnInfo(name = "stockRequest")
    var stockRequest: Double = 0.0

    @ColumnInfo(name = "store")
    var store: String? = null

    @ColumnInfo(name = "ubicationEntry")
    var ubicationEntry: Int = 0

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null

    @ColumnInfo(name = "warehouse")
    var warehouse: String = ""
}