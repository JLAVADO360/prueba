package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import ndp.posapp.data.db.entity.ContactPersonEntity

@Dao
interface ContactPersonDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(contactPersonEntity: ContactPersonEntity)
}