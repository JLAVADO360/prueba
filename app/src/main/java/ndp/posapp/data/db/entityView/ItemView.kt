package ndp.posapp.data.db.entityView

import androidx.room.Embedded
import androidx.room.Ignore
import androidx.room.Relation
import ndp.posapp.data.db.entity.ItemEntity
import ndp.posapp.data.db.entity.PriceListDetailEntity
import ndp.posapp.data.db.entity.WhsKardexEntity
import java.io.Serializable

data class ItemView(
    @Embedded var itemEntity: ItemEntity,
    @Relation(
        parentColumn = "code",
        entityColumn = "item"
    ) var priceListDetailEntity: MutableList<PriceListDetailEntity>,
    @Relation(
        parentColumn = "code",
        entityColumn = "itemEntity"
    ) var whsKardexEntity: MutableList<WhsKardexEntity>,
) : Serializable
