package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.WarehouseEntity

@Dao
interface WarehouseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(warehouseEntity: WarehouseEntity)

    @Query("SELECT * FROM t_warehouse WHERE code in(:warehouses) AND company = :company")
    fun list(company: String, warehouses: List<String>) : List<WarehouseEntity>

}