package ndp.posapp.data.db.entityView

data class DateSystem(
    var anio: String,
    var mes : String,
    var dia : String
)