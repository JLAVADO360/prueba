package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_parameter")
class ParameterEntity {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "application")
    var application: String ? = null

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "description")
    var description: String? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int ?=null

    @ColumnInfo(name = "name")
    var name: String ?=null

    @ColumnInfo(name = "objectUI")
    var objectUI: String? = null

    @ColumnInfo(name = "parameterType")
    var parameterType: String? = null

    @ColumnInfo(name = "status")
    var status: Int ?=null

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "value")
    var value: String ?=null


}