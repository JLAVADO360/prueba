package ndp.posapp.data.db.dao


import androidx.lifecycle.LiveData
import androidx.room.*
import ndp.posapp.data.db.entity.ItemEntity
import ndp.posapp.data.db.entityView.ItemView

@Dao
interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(itemEntity: ItemEntity)

    @Query("SELECT * FROM t_item WHERE code = :itemCode AND company = :company")
    fun get(company: String, itemCode : String): ItemEntity?

    @Query("SELECT * FROM t_item WHERE code NOT IN(:itemCodes) AND company = :company")
    fun list(company: String, itemCodes : List<String>?): List<ItemEntity>

    @Transaction
    @Query("SELECT * FROM t_item WHERE company = :company")
    fun list(company: String): LiveData<List<ItemView>>
}