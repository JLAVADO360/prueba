package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_contactPerson")
class ContactPersonEntity {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "address")
    var address: String? = null

    @ColumnInfo(name = "cellPhoneNumber")
    var cellPhoneNumber: String? = null

    @ColumnInfo(name = "email")
    var email: String? = null

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "phone")
    var phone: String? = null

    @ColumnInfo(name = "relationship")
    var relationship: String? = null
}