package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.CashRegisterInfoEntity

@Dao
interface CashRegisterInfoDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(cashRegisterInfoEntity: CashRegisterInfoEntity)

    @Query("SELECT * FROM t_cashRegisterInfo WHERE company = :company AND user = :user AND type = :type")
    fun get(company: String, user: String, type: String) : CashRegisterInfoEntity?
}