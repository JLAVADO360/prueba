package ndp.posapp.data.db.entityView

import androidx.room.Embedded
import androidx.room.Relation
import ndp.posapp.data.db.entity.BusinessPartnerAddressEntity
import ndp.posapp.data.db.entity.BusinessPartnerEntity
import java.io.Serializable


data class BusinessPartnerView(
    @Embedded val businessPartnerEntity: BusinessPartnerEntity,
    @Relation(
        parentColumn = "code",
        entityColumn = "codeBusinessPartner"
    ) val businessPartnerAddressEntity: List<BusinessPartnerAddressEntity>
) : Serializable

