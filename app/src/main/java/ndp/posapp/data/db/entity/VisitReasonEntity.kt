package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_visitReason")
class VisitReasonEntity {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "reason")
    var reason: String = ""

    @ColumnInfo(name = "status")
    var status: String = ""


}