package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity(tableName = "t_region")
class RegionEntity : Serializable {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "identifier")
    var identifier: String = ""

    @ColumnInfo(name = "code")
    var code: String? = null

    @ColumnInfo(name = "concatCode")
    var concatCode: String? = null

    @ColumnInfo(name = "country")
    var country: String? = null

    @ColumnInfo(name = "region")
    var region: String? = null

    @ColumnInfo(name = "regionType")
    var regionType: String? = null
}