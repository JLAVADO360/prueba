package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_zone")
class ZoneEntity {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "description")
    var description: String? = null

    @ColumnInfo(name = "status")
    var status: Int = 0

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null

    @ColumnInfo(name = "zone")
    var zone: String = ""

}