package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity


@Entity(tableName = "t_currency", primaryKeys = ["code","company"])
class CurrencyEntity {
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "description")
    var description: String? = null

    @ColumnInfo(name = "isDefault")
    var _isDefault: Int = 0

    @ColumnInfo(name = "name")
    var name: String = ""

    @ColumnInfo(name = "status")
    var status: Int = 0

    @ColumnInfo(name = "symbol")
    var symbol: String = ""

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null
}