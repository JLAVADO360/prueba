package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.ZoneWarehouseEntity

@Dao
interface ZoneWarehouseDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(zoneWarehouseEntity: ZoneWarehouseEntity)

    @Query("SELECT * FROM t_zoneWareHouse")
    fun list() : List<ZoneWarehouseEntity>
}