package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_object")
class ObjectEntity {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "description")
    var description: String = ""

    @ColumnInfo(name = "id")
    var id: String = ""

    @ColumnInfo(name = "name")
    var name: String = ""
}