package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.ObjectEntity

@Dao
interface ObjectDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(objectEntity: ObjectEntity)


    @Query("SELECT * FROM t_object")
    fun list() : List<ObjectEntity>

    @Query("DELETE FROM t_object")
    fun deleteAll()

}