package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.CorrelativeEntity

@Dao
interface CorrelativeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(correlativeEntity: CorrelativeEntity)

    @Query("SELECT * FROM t_correlative WHERE company = :company AND cashRegisterCode = :cashRegisterCode")
    fun list(company: String, cashRegisterCode: String) : LiveData<List<CorrelativeEntity>>
}