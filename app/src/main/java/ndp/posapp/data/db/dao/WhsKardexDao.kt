package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.WhsKardexEntity

@Dao
interface WhsKardexDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(whsKardexEntity: WhsKardexEntity)

    @Query("SELECT * FROM t_whsKardex WHERE warehouse=:warehouse and itemEntity=:item and company=:company")
    fun list(company: String, warehouse : String, item : String) : List<WhsKardexEntity>

}