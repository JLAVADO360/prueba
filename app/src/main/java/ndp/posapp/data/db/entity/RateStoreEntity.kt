package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "t_rateStore", primaryKeys = ["code","company"])
class RateStoreEntity {
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "currencyFrom")
    var currencyFrom: String? = null

    @ColumnInfo(name = "currencyTo")
    var currencyTo: String? = null

    @ColumnInfo(name = "rate")
    var rate: Double = 0.0

    @ColumnInfo(name = "rateDate")
    var rateDate: String? = ""

    @ColumnInfo(name = "status")
    var status: Int = 0

    @ColumnInfo(name = "store")
    var store: String? = null
}