package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.PriceListEntity

@Dao
interface PriceListDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(priceListEntity: PriceListEntity)

    @Query("SELECT * FROM t_priceList WHERE code=:code AND company = :company")
    fun get(company: String, code: Int) : LiveData<PriceListEntity>
}