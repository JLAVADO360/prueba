package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_regionRelation")
class RegionRelationEntity {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "childrenRegion")
    var childrenRegion: String = ""

    @ColumnInfo(name = "country")
    var country: String = ""

    @ColumnInfo(name = "parentRegion")
    var parentRegion: String = ""

    @ColumnInfo(name = "active")
    var active: Int? = null
}