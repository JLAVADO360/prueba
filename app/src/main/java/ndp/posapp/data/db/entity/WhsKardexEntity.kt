package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable

@Entity(tableName = "t_whsKardex", primaryKeys = ["warehouse", "itemEntity", "company"])
class WhsKardexEntity :Serializable {

    @ColumnInfo(name = "warehouse")
    var warehouse: String = ""

    @ColumnInfo(name = "itemEntity")
    var itemEntity: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int? = null

    @ColumnInfo(name = "stock")
    var stock: Double = 0.0

    @ColumnInfo(name = "stockCommitted")
    var stockCommitted: Double = 0.0

    @ColumnInfo(name = "stockDisponible")
    var stockDisponible: Double = 0.0

    @ColumnInfo(name = "stockNoForSale")
    var stockNoForSale: Double = 0.0

    @ColumnInfo(name = "stockRequested")
    var stockRequested: Double = 0.0

    @ColumnInfo(name = "store")
    var store: String? = null

}