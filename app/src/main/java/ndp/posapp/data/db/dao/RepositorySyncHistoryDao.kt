package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.RepositorySyncHistoryEntity

@Dao
interface RepositorySyncHistoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(repositorySyncHistoryEntity: RepositorySyncHistoryEntity)

    @Query("SELECT * FROM t_repositorySyncHistory WHERE company = :company")
    fun list(company: String): List<RepositorySyncHistoryEntity>

}