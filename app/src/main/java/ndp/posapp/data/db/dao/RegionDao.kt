package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.RegionEntity

@Dao
interface RegionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(regionEntity: RegionEntity)

    @Query("SELECT * FROM t_region WHERE region LIKE  '%' || :region || '%' AND regionType=:regionType")
    fun listDepartment(region : String, regionType : String) : LiveData<List<RegionEntity>>

    @Query("SELECT * FROM t_region WHERE region LIKE  '%' || :region || '%' AND regionType=:regionType AND identifier IN (:relation)")
    fun listRelation(region : String, regionType : String, relation : List<String>) : List<RegionEntity>

    @Query("SELECT COUNT(identifier) FROM t_region")
    fun countRegion() : Int

}