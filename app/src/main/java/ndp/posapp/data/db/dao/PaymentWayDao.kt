package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.PaymentWayEntity

@Dao
interface PaymentWayDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(paymentWayEntity: PaymentWayEntity)

    @Query("SELECT * FROM t_paymentWay WHERE company = :company AND store = :store")
    fun get(company: String, store: String) : PaymentWayEntity?
}