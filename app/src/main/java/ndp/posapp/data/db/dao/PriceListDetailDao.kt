package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.PriceListDetailEntity

@Dao
interface PriceListDetailDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(priceListDetailEntity: PriceListDetailEntity)

    @Query("SELECT * FROM t_priceListDetail WHERE priceList IN(:priceList) and item=:item and company=:company")
    fun list(company: String, priceList: List<Int>, item: String) : List<PriceListDetailEntity>

    @Query("SELECT * FROM t_priceListDetail WHERE item=:item and company=:company")
    fun list(company: String, item: String) : List<PriceListDetailEntity>
}