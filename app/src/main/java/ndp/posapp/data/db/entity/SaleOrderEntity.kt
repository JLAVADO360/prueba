package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_saleOrder")
class SaleOrderEntity {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "internalId")
    var internalId: Long? = null

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "internalStatus")
    var internalStatus: Int = 1

    @ColumnInfo(name = "internalReceiptNumber")
    var internalReceiptNumber: String = String()

    @ColumnInfo(name = "additionalData")
    var additionalData: String? = null

    @ColumnInfo(name = "bonus")
    var bonus: Double? = null

    @ColumnInfo(name = "businessPartnerCode")
    var businessPartnerCode: String? = null

    @ColumnInfo(name = "businessPartnerName")
    var businessPartnerName: String? = null

    @ColumnInfo(name = "code")
    var code: String? = null

    @ColumnInfo(name = "codeERP")
    var codeERP: String? = null

    @ColumnInfo(name = "comment")
    var comment: String? = null

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "deliveryDate")
    var deliveryDate: String? = null

    @ColumnInfo(name = "detail")
    var detail: String? = null

    @ColumnInfo(name = "detraction")
    var detraction: Int? = null

    @ColumnInfo(name = "discount")
    var discount: Double? = null

    @ColumnInfo(name = "discountPercentage")
    var discountPercentage: Double? = null

    @ColumnInfo(name = "docCurrency")
    var docCurrency: String? = null

    @ColumnInfo(name = "docDate")
    var docDate: String? = null

    @ColumnInfo(name = "docNum")
    var docNum: Int? = null

    @ColumnInfo(name = "documentInstallments")
    var documentInstallments: String? = null

    @ColumnInfo(name = "docYear")
    var docYear: Int? = null

    @ColumnInfo(name = "emailSalePerson")
    var emailSalePerson: String? = null

    @ColumnInfo(name = "erpDocNum")
    var erpDocNum: Int? = null

    @ColumnInfo(name = "erpSerialNumber")
    var erpSerialNumber: Int? = null

    @ColumnInfo(name = "icbper")
    var icbper: Double? = null

    @ColumnInfo(name = "isDispatch")
    var _isDispatch: Int? = null

    @ColumnInfo(name = "migration")
    var migration: String? = null

    @ColumnInfo(name = "migrationDate")
    var migrationDate: String? = null

    @ColumnInfo(name = "migrationStatus")
    var migrationStatus: Int? = null

    @ColumnInfo(name = "nameSalePerson")
    var nameSalePerson: String? = null

    @ColumnInfo(name = "nif")
    var nif: String? = null

    @ColumnInfo(name = "payAddress")
    var payAddress: String? = null

    @ColumnInfo(name = "payAddressName")
    var payAddressName: String? = null

    @ColumnInfo(name = "paymentConditionName")
    var paymentConditionName: String? = null

    @ColumnInfo(name = "paymentReceiptNumber")
    var paymentReceiptNumber: String? = null

    @ColumnInfo(name = "paymentReceiptType")
    var paymentReceiptType: String? = null

    @ColumnInfo(name = "pickUps")
    var pickUps: String? = null

    @ColumnInfo(name = "pickingStatus")
    var pickingStatus: String? = null

    @ColumnInfo(name = "plateNumber")
    var plateNumber: String? = null

    @ColumnInfo(name = "reservationInvoice")
    var reservationInvoice: String? = null

    @ColumnInfo(name = "saleType")
    var saleType: Int? = null

    @ColumnInfo(name = "sampleCode")
    var sampleCode: String? = null

    @ColumnInfo(name = "samples")
    var samples: String? = null

    @ColumnInfo(name = "shipAddress")
    var shipAddress: String? = null

    @ColumnInfo(name = "shipAddressName")
    var shipAddressName: String? = null

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "store")
    var store: String? = null

    @ColumnInfo(name = "storeAddress")
    var storeAddress: String? = null

    @ColumnInfo(name = "subtotal")
    var subtotal: Double? = null

    @ColumnInfo(name = "subtotalDiscount")
    var subtotalDiscount: Double? = null

    @ColumnInfo(name = "taxAmount")
    var taxAmount: Double? = null

    @ColumnInfo(name = "total")
    var total: Double? = null

    @ColumnInfo(name = "totalDetraction")
    var totalDetraction: Double? = null

    @ColumnInfo(name = "transactionalCode")
    var transactionalCode: String? = null

    @ColumnInfo(name = "u_VS_AFEDET")
    var u_VS_AFEDET: String? = null

    @ColumnInfo(name = "u_VS_CODSER")
    var u_VS_CODSER: String? = null

    @ColumnInfo(name = "u_VS_MONDET")
    var u_VS_MONDET: Double? = null

    @ColumnInfo(name = "u_VS_PORDET")
    var u_VS_PORDET: Double? = null

    @ColumnInfo(name = "ubication")
    var ubication: String? = null

    @ColumnInfo(name = "ubicationEntry")
    var ubicationEntry: Int? = null

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null

    @ColumnInfo(name = "warehouse")
    var warehouse: String? = null

}