package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "t_tax", primaryKeys = ["code","company"])
class TaxEntity {
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int = 0

    @ColumnInfo(name = "rate")
    var rate: Double = 0.0

    @ColumnInfo(name = "status")
    var status: Int = 0

    @ColumnInfo(name = "tax")
    var tax: String = ""

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null
}