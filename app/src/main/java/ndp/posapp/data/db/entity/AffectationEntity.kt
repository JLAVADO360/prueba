package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_affectation")
class AffectationEntity {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "description")
    var description: String? = null

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null
}