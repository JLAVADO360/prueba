package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import ndp.posapp.data.db.entity.PaymentReceiptEntity
import ndp.posapp.utils.Const

@Dao
interface PaymentReceiptDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(paymentReceiptEntity: PaymentReceiptEntity) : Long

    @Update
    fun update(paymentReceiptEntity: PaymentReceiptEntity)

    @Query("UPDATE t_paymentReceipt SET code = :code WHERE saleOrderId = :saleOrderId")
    fun updateCode(saleOrderId: Long, code: String)

    @Query("SELECT * FROM t_paymentReceipt WHERE internalId = :internalId")
    fun get(internalId: Long) : LiveData<PaymentReceiptEntity?>

    @Query("SELECT * FROM t_paymentReceipt WHERE code IS NOT NULL AND payment IS NOT NULL AND internalStatus = ${Const.PaymentReceiptInternalStatus.SAVED_LOCAL}")
    fun listPendingSend() : List<PaymentReceiptEntity>

    @Query("SELECT * FROM t_paymentReceipt WHERE saleOrderId = :saleOrderId")
    fun list(saleOrderId: Long) : List<PaymentReceiptEntity>
}