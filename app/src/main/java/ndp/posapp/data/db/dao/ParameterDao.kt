package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import ndp.posapp.data.db.entity.ParameterEntity
import ndp.posapp.data.model.ParameterModel

@Dao
interface ParameterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(parameterEntity: ParameterEntity)

    @Transaction
    @Query("SELECT * FROM t_parameter WHERE name=:name AND company = :company")
    fun get(company: String, name: String): LiveData<ParameterModel>
}