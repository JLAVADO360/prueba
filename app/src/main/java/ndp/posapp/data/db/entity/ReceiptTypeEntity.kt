package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable

@Entity(tableName = "t_receiptType", primaryKeys = ["code","company"])
class ReceiptTypeEntity : Serializable {
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "codeApplication")
    var codeApplication: String? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int? = null

    @ColumnInfo(name = "receipt")
    var receipt: String? = null

    @ColumnInfo(name = "status")
    var status: Int? = null
}