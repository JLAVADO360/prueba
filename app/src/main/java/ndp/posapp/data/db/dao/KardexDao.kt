package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.KardexEntity

@Dao
interface KardexDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(kardexEntity: KardexEntity)

    @Query("SELECT * FROM t_kardex WHERE warehouse=:warehouse and item=:item and company=:company")
    fun list(company: String, warehouse : String, item : String) : List<KardexEntity>
}