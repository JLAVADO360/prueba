package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_ubication", primaryKeys = ["code","company"])
class UbicationEntity {

    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "entry")
    var entry: Int ? = null

    @ColumnInfo(name = "isSale")
    var _isSale: Int ? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int ? = null

    @ColumnInfo(name = "name")
    var name: String? = null

    @ColumnInfo(name = "status")
    var status: Int ? = null

    @ColumnInfo(name = "type")
    var type: String? = null

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null

    @ColumnInfo(name = "warehouse")
    var warehouse: String? = null
}