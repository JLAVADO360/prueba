package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable

@Entity(tableName = "t_businessPartnerAddress", primaryKeys = ["codeBusinessPartner", "company", "type"])
class BusinessPartnerAddressEntity : Serializable {

    @ColumnInfo(name = "codeBusinessPartner")
    var codeBusinessPartner: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "address")
    var address: String? = null

    @ColumnInfo(name = "addressName")
    var addressName: String? = null

    @ColumnInfo(name = "country")
    var country: String? = null

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "department")
    var department: String? = null

    @ColumnInfo(name = "district")
    var district: String? = null

    @ColumnInfo(name = "invoicesCountry")
    var invoicesCountry: String? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int? = null

    @ColumnInfo(name = "province")
    var province: String? = null

    @ColumnInfo(name = "rowNum")
    var rowNum: Int? = null

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "type")
    var type: String = ""

    @ColumnInfo(name = "ubiquitous")
    var ubiquitous: String? = null
}