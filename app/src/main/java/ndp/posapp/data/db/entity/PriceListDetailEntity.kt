package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable

@Entity(tableName = "t_priceListDetail", primaryKeys = ["priceList", "item", "company"])
class PriceListDetailEntity : Serializable {

    @ColumnInfo(name = "priceList")
    var priceList: Int  = 0

    @ColumnInfo(name = "applyTax")
    var applyTax: Int? = null

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "currency")
    var currency: String? = null

    @ColumnInfo(name = "grossPrice")
    var grossPrice: Double? = null

    @ColumnInfo(name = "item")
    var item: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int? = null

    @ColumnInfo(name = "priceListName")
    var priceListName: String? = null

    @ColumnInfo(name = "priceListType")
    var priceListType: String? = null

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "store")
    var store: String? = null

    @ColumnInfo(name = "tax")
    var tax: String? = null

    @ColumnInfo(name = "taxPercentage")
    var taxPercentage: Double? = null

    @ColumnInfo(name = "unitMSR")
    var unitMSR: String? = null

    @ColumnInfo(name = "unitMSREntry")
    var unitMSREntry: Int? = null

    @ColumnInfo(name = "unitMSRName")
    var unitMSRName: String? = null

    @ColumnInfo(name = "unitPrice")
    var unitPrice: Double? = null

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null
}