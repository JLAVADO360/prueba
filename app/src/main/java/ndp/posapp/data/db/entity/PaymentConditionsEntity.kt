package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_paymentConditions")
class PaymentConditionsEntity {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "creditLimit")
    var creditLimit: Double = 0.0

    @ColumnInfo(name = "generalDiscount")
    var generalDiscount: Double = 0.0

    @ColumnInfo(name = "groupNumber")
    var groupNumber: Int = 0

    @ColumnInfo(name = "interestOnArrears")
    var interestOnArrears: Double = 0.0

    @ColumnInfo(name = "loadLimit")
    var loadLimit: Double = 0.0

    @ColumnInfo(name = "numberOfAdditionalDays")
    var numberOfAdditionalDays: Int = 0

    @ColumnInfo(name = "numberOfAdditionalMonths")
    var numberOfAdditionalMonths: Int = 0

    @ColumnInfo(name = "numberOfToleranceDays")
    var numberOfToleranceDays: Int = 0

    @ColumnInfo(name = "paymentTermsGroupName")
    var paymentTermsGroupName: String? = null

    @ColumnInfo(name = "priceListNo")
    var priceListNo: Int = 0

    @ColumnInfo(name = "type")
    var type: Int = 0

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null
}