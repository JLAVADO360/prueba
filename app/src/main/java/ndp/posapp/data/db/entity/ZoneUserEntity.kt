package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_zoneUser")
class ZoneUserEntity {

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "relationCode")
    var relationCode: String = ""

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "relationCreateDate")
    var relationCreateDate: String? = null

    @ColumnInfo(name = "relationUpdateDate")
    var relationUpdateDate: String? = null

    @ColumnInfo(name = "status")
    var status: Int = 0

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null

    @ColumnInfo(name = "user")
    var user: String? = null
}