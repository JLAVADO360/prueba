package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import ndp.posapp.data.db.entity.SaleOrderEntity

@Dao
interface SaleOrderDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(saleOrderEntity: SaleOrderEntity): Long

    @Update
    fun update(saleOrderEntity: SaleOrderEntity)

    @Query("SELECT * FROM t_saleOrder WHERE internalId = :internalId")
    fun get(internalId: Long): SaleOrderEntity?

    @Query("SELECT * FROM t_saleOrder WHERE internalStatus = :internalStatus")
    fun list(internalStatus: Int): List<SaleOrderEntity>
}