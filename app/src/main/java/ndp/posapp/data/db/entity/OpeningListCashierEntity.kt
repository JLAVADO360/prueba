package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "t_openingListCashier")
class OpeningListCashierEntity {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "cashRegister")
    var cashRegister: String = String()

    @ColumnInfo(name = "store")
    var store: String? = null

    @ColumnInfo(name = "observation")
    var observation: String? = null

    @ColumnInfo(name = "openingCashRegisterDate")
    var openingCashRegisterDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "createUserName")
    var createUserName: String ? = null

    @ColumnInfo(name = "createUserEmail")
    var createUserEmail: String ? = null

    @ColumnInfo(name = "status")
    var status: Int ? = null

    @ColumnInfo(name = "docNum")
    var docNum: Int ? = null

    @ColumnInfo(name = "docYear")
    var docYear: Int ? = null

    @ColumnInfo(name = "turn")
    var turn: Int ? = null
}