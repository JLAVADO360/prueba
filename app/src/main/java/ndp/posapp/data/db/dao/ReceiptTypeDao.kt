package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.ReceiptTypeEntity

@Dao
interface ReceiptTypeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(affectationTypeEntity: ReceiptTypeEntity)

    @Query("SELECT * FROM t_receiptType WHERE company = :company")
    fun list(company: String) : LiveData<List<ReceiptTypeEntity>>

}