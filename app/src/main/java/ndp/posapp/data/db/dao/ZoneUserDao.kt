package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import ndp.posapp.data.db.entity.ZoneUserEntity

@Dao
interface ZoneUserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(zoneUserEntity: ZoneUserEntity)
}