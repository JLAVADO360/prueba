package ndp.posapp.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.RateStoreEntity

@Dao
interface RateStoreDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(rateStoreEntity: RateStoreEntity)

    @Query("SELECT * FROM t_rateStore WHERE status=:status AND rateDate=:rateDate AND store=:store AND company = :company")
    fun get(company: String, status: Int, rateDate: String, store: String) : List<RateStoreEntity>

}