package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity


@Entity(tableName = "t_cashRegisterInfo", primaryKeys = ["code", "company", "user"])
class CashRegisterInfoEntity {
    @ColumnInfo(name = "code")
    var code: String = String()

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "user")
    var user: String = String()

    @ColumnInfo(name = "assigned")
    var assigned: Int? = null

    @ColumnInfo(name = "name")
    var name: String = String()

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "type")
    var type: String? = null

    @ColumnInfo(name = "openingCreationUser")
    var openingCreationUser: String? = null
}