package ndp.posapp.data.db.entityView

import ndp.posapp.data.db.entity.ItemEntity
import ndp.posapp.data.db.entity.PriceListDetailEntity
import java.io.Serializable

data class SaleItem(
    var itemEntity: ItemEntity,
    var priceListDetailEntity: MutableList<PriceListDetailEntity>,
    var kardex: MutableList<Kardex>,
) : Serializable
