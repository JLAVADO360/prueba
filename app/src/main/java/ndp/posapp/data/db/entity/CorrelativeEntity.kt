package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity


@Entity(tableName = "t_correlative", primaryKeys = ["id", "company", "cashRegisterCode"])
class CorrelativeEntity {
    @ColumnInfo(name = "id")
    var id: String = String()

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "cashRegisterCode")
    var cashRegisterCode: String = String()

    @ColumnInfo(name = "actualNumeration")
    var actualNumeration: Int = 0

    @ColumnInfo(name = "endNumeration")
    var endNumeration: Int = 999999999

    @ColumnInfo(name = "initialNumeration")
    var initialNumeration: Int? = null

    @ColumnInfo(name = "modified")
    var modified: Int? = null

    @ColumnInfo(name = "numerationLength")
    var numerationLength: Int? = null

    @ColumnInfo(name = "parentReceiptType")
    var parentReceiptType: String? = null

    @ColumnInfo(name = "receiptTypeCode")
    var receiptTypeCode: String = String()

    @ColumnInfo(name = "series")
    var series: String? = null

    @ColumnInfo(name = "status")
    var status: Int? = null

    @ColumnInfo(name = "storeCode")
    var storeCode: String? = null

    @ColumnInfo(name = "version")
    var version: Long? = null
}