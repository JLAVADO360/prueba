package ndp.posapp.data.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable

@Entity(tableName = "t_businessPartner", primaryKeys = ["code","company"])
class BusinessPartnerEntity : Serializable {

    @ColumnInfo(name = "code")
    var code: String = ""

    @ColumnInfo(name = "company")
    var company: String = String()

    @ColumnInfo(name = "alternativeEmail")
    var alternativeEmail: String? = null

    @ColumnInfo(name = "balanceSys")
    var balanceSys: Double? = null

    @ColumnInfo(name = "businessName")
    var businessName: String? = null

    @ColumnInfo(name = "comment")
    var comment: String? = null

    @ColumnInfo(name = "commercialName")
    var commercialName: String? = null

    @ColumnInfo(name = "createDate")
    var createDate: String? = null

    @ColumnInfo(name = "createUser")
    var createUser: String? = null

    @ColumnInfo(name = "creditBalance")
    var creditBalance: Double? = null

    @ColumnInfo(name = "creditLine")
    var creditLine: Double? = null

    @ColumnInfo(name = "currency")
    var currency: String? = null

    @ColumnInfo(name = "email")
    var email: String? = null

    @ColumnInfo(name = "hasBusiness")
    var hasBusiness: String? = null

    @ColumnInfo(name = "lastNameF")
    var lastNameF: String? = null

    @ColumnInfo(name = "lastNameM")
    var lastNameM: String? = null

    @ColumnInfo(name = "logicStatus")
    var logicStatus: Int? = null

    @ColumnInfo(name = "migration")
    var migration: String? = null

    @ColumnInfo(name = "migrationDate")
    var migrationDate: String? = null

    @ColumnInfo(name = "migrationMessage")
    var migrationMessage: String? = null

    @ColumnInfo(name = "migrationStatus")
    var migrationStatus: Int? = null

    @ColumnInfo(name = "names")
    var names: String? = null

    @ColumnInfo(name = "nif")
    var nif: String? = null

    @ColumnInfo(name = "partnerType")
    var partnerType: String? = null

    @ColumnInfo(name = "paymentCondition")
    var paymentCondition: Int? = null

    @ColumnInfo(name = "personType")
    var personType: Int? = null

    @ColumnInfo(name = "phoneNumber")
    var phoneNumber: String? = null

    @ColumnInfo(name = "priceList")
    var priceList: String? = null

    @ColumnInfo(name = "recordType")
    var recordType: Int? = null

    @ColumnInfo(name = "sunatAffectationType")
    var sunatAffectationType: String? = null

    @ColumnInfo(name = "sunatOneroso")
    var sunatOneroso: String? = null

    @ColumnInfo(name = "sunatOperation")
    var sunatOperation: String? = null

    @ColumnInfo(name = "sunatOperationType")
    var sunatOperationType: String? = null

    @ColumnInfo(name = "updateDate")
    var updateDate: String? = null

    @ColumnInfo(name = "updateUser")
    var updateUser: String? = null
}