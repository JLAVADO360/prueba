package ndp.posapp.data.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ndp.posapp.data.db.entity.PaymentConditionsEntity

@Dao
interface PaymentConditionsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(paymentConditionsEntity: PaymentConditionsEntity)

    @Query("SELECT * FROM t_paymentConditions WHERE numberOfAdditionalDays <= (SELECT numberOfAdditionalDays from t_paymentConditions where groupNumber =:groupNumber AND company = :company) AND company = :company")
    fun list(company: String, groupNumber: Int): LiveData<List<PaymentConditionsEntity>>

    @Query("SELECT * FROM t_paymentConditions WHERE paymentTermsGroupName = :name AND company = :company")
    fun get(company: String, name: String): LiveData<PaymentConditionsEntity>

}