package ndp.posapp.data.model

class DenominationModel {
    var denomination:Double?=null
    var numberLine:Int?=null
    var quantity:String?=null
    var total:Double?=null
}