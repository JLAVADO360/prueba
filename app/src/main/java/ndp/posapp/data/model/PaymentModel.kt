package ndp.posapp.data.model

import java.io.Serializable

class PaymentModel : Serializable {
    var cashRegister: String? = null
    var changeRoundingDifference: Double? = null
    var code: String? = null
    var comment: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var createUserEmail: String? = null
    var createUserName: String? = null
    var detail  : MutableList<PaymentDetailModel> = ArrayList()
    var docNum: Int? = null
    var docYear: Int? = null
    var identificationDocument: String? = null
    var lostAmount: Double? = null
    var payerName: String? = null
    var paymentAmount: Double? = null
    var paymentDate: String? = null
    var paymentReceipt: String? = null
    var paymentReceiptNumberConcat: String? = null
    var process: String? = null
    var rate   : List<RateModel> = ArrayList()
    var roundingAmount: Double? = null
    var status: Int? = null
    var store: String? = null
    var totalChange: Double? = null
    var totalReceived: Double? = null
    var updateDate: String? = null

}