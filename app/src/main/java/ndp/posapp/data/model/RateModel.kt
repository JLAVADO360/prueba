package ndp.posapp.data.model

import java.io.Serializable

class RateModel : Serializable {
    var currencyFrom: String? = null
    var currencyTo: String? = null
    var value: Double? = null
}