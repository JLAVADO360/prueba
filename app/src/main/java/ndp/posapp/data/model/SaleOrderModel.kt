package ndp.posapp.data.model

import java.io.Serializable

class SaleOrderModel : Serializable {
    var additionalData: MutableMap<String, Any?>? = null
    var bonus: Double? = null
    var businessPartnerCode: String? = null
    var businessPartnerName: String? = null
    var code: String? = null
    var codeERP: String? = null
    var comment: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var deliveryDate: String? = null
    var detail: MutableList<SaleOrderDetailModel> = ArrayList()
    var detraction: Int? = null
    var discount: Double? = null
    var discountPercentage: Double? = null
    var docCurrency: String? = null
    var docDate: String? = null
    var docNum: Int? = null
    var documentInstallments: MutableList<DocumentInstallmentsModel> = ArrayList()
    var docYear: Int? = null
    var emailSalePerson: String? = null
    var erpDocNum: Int? = null
    var erpSerialNumber: Int? = null
    var icbper: Double? = null
    var isDispatch: Int? = null
    var migration: String? = null
    var migrationDate: String? = null
    var migrationStatus: Int? = null
    var nameSalePerson: String? = null
    var nif: String? = null
    var payAddress: String? = null
    var payAddressName: String? = null
    var paymentConditionName: String? = null
    var paymentReceiptNumber: String? = null
    var paymentReceiptType: String? = null
    var pickUps: List<PickUpDocumentModel> = ArrayList()
    var pickingStatus: String? = null
    var plateNumber: String? = null
    var reservationInvoice: String? = null
    var saleType: Int? = null
    var sampleCode: String? = null
    var samples: List<SampleModel> = ArrayList()
    var shipAddress: String? = null
    var shipAddressName: String? = null
    var status: Int? = null
    var stockMovement: String? = null
    var store: String? = null
    var storeAddress: String? = null
    var subtotal: Double? = null
    var subtotalDiscount: Double? = null
    var taxAmount: Double? = null
    var total: Double? = null
    var totalDetraction: Double? = null
    var transactionalCode: String? = null
    var u_VS_AFEDET: String? = null
    var u_VS_CODSER: String? = null
    var u_VS_MONDET: Double? = null
    var u_VS_PORDET: Double? = null
    var ubication: String? = null
    var ubicationEntry: Int? = null
    var updateDate: String? = null
    var updateUser: String? = null
    var warehouse: String? = null
}