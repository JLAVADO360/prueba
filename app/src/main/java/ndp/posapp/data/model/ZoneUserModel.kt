package ndp.posapp.data.model

class ZoneUserModel {

    var relationCode: String = ""
    var createUser: String? = null
    var relationCreateDate: String? = null
    var relationUpdateDate: String? = null
    var status: Int = 0
    var updateUser: String? = null
    var user: String? = null
}