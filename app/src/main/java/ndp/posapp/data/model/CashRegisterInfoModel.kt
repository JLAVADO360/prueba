package ndp.posapp.data.model


data class CashRegisterInfoModel(
    var cashRegisterCode: String = "",
    var cashRegisterAssigned: Int? = null,
    var cashRegisterName: String? = null,
    var cashRegisterStatus: Int? = null,
    var cashRegisterType: String? = null,
    var openingCreationUser: String? = null
)