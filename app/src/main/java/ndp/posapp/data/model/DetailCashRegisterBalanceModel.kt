package ndp.posapp.data.model

data class  DetailCashRegisterBalanceModel(
    var numberLine:Int?,
    var payWay:String?,
    var payMethod:String?,
    var amount:Double?,
    //var count,
    var imbalanceReason:String?,
    var imbalanceComment:String?,
    //var denominations":

)
