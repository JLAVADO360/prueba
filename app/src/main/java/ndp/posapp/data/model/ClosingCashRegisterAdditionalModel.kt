package ndp.posapp.data.model

data class ClosingCashRegisterAdditionalModel (
    var operationNumber:String?=null,
    var docNumERP:String?=null,
    var moneyOut:Boolean?=null,
    )