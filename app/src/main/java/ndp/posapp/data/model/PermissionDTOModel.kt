package ndp.posapp.data.model

class PermissionDTOModel {
    var enabled : String = ""
    var id : String = ""
    var objectCode : String  = ""
    var objectDescription : String = ""
    var objectId :  String = ""
    var objectName : String = ""
    var roleId : String = ""
    var roleName : String = ""
}