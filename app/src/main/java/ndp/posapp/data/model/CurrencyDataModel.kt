package ndp.posapp.data.model

data class CurrencyDataModel (
    var count: Int?,
    var value: MutableList<CurrencyModel>?
    )