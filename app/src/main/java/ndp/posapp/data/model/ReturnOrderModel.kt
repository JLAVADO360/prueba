package ndp.posapp.data.model

import java.io.Serializable

class ReturnOrderModel : Serializable {

    var businessPartner: String? = null
    var businessPartnerName: String? = null
    var code: String? = null
    var comment: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var createUserEmail: String? = null
    var createUserName: String? = null
    var data: ReturnOrdenDataModel? = null
    var detail   : List<ReturnOrderDetailModel> = ArrayList()
    var discount: Double? = null
    var discountPercentage: Double? = null
    var docNum: Int? = null
    var docYear: Int? = null
    var nif: String? = null
    var paymentReceipt: String? = null
    var paymentReceiptDate: String? = null
    var paymentReceiptNumber: String? = null
    var paymentReceiptType: String? = null
    var status: Int? = null
    var store: String? = null
    var subtotal: Double? = null
    var subtotalDiscount: Double? = null
    var taxAmount: Double? = null
    var total: Double? = null
    var transactionalCode: String? = null

}