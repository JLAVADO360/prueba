package ndp.posapp.data.model

import java.io.Serializable

class PaymentReceiptDetailModel : Serializable {
    var baseQuantity: Double? = null
    var createDate: String? = null
    var currency: String? = null
    var discount: Double? = null
    var discountPercentage: Double? = null
    var docReference: String? = null
    var grossPrice: Double? = null
    var inventoryItem: String? = null
    var itemCode: String? = null
    var itemName: String? = null
    var numberLine: Int? = null
    var onlyTax: String? = null
    var quantity: Double? = null
    var quantityReturned: Double? = null
    var quantitySaleOrder: Double? = null
    var saleOrderCode: String? = null
    var saleOrderEntry: String? = null
    var saleOrderLine: Int? = null
    var subtotal: Double? = null
    var subtotalDiscount: Double? = null
    var sunatAffectationType: String? = null
    var sunatOneroso: String? = null
    var sunatOperation: String? = null
    var sunatOperationType: String? = null
    var tax: Double? = null
    var taxAmount: Double? = null
    var taxCode: String? = null
    var total: Double? = null
    var ubication: String? = null
    var ubicationEntry: Int? = null
    var unitGroupEntry: Int? = null
    var unitMSRCode: String? = null
    var unitMSREntry: Int? = null
    var unitMSRName: String? = null
    var unitPrice: Double? = null
    var unitPriceReal: Double? = null
    var updateDate: String? = null
    var warehouse: String? = null
}