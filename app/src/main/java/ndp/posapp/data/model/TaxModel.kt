package ndp.posapp.data.model

class TaxModel {
    var code: String = ""
    var company: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var logicStatus: Int = 0
    var rate: Double = 0.0
    var status: Int = 0
    var tax: String = ""
    var updateDate: String? = null
    var updateUser: String? = null
}