package ndp.posapp.data.model

import java.io.Serializable

class PaymentDetailModel : Serializable {
    var accountDescription: String? = null
    var accountNumber: String? = null
    var amount: Double? = null
    var bank: String? = null
    var cardNumber: String? = null
    var cardType: String? = null
    var code: String? = null
    var codeERP: String? = null
    var creditNote: String? = null
    var currency: String? = null
    var currencyReceived: String? = null
    var erpDocNum: Int? = null
    var erpSerialNumber: Int? = null
    var exchangeAccountNumber: String? = null
    var numberLine: Int? = null
    var payWay: String? = null
    var payWayName: String? = null
    var paymentCondition: String? = null
    var paymentGroup: String? = null
    var paymentMethod: String? = null
    var paymentMethodName: String? = null
    var rateValue: Double? = null
    var receivedAmountForeignCurrency: Double? = null
    var receivedAmountNationalCurrency: Double? = null
    var remainingAmount: Double? = null
    var saleAmount: Double? = null
    var saleCurrency: String? = null
    var status: Int? = null
    var store: String? = null
    var totalChange: Double? = null
    var totalReceived: Double? = null
    var voucherNumber: String? = null

}