package ndp.posapp.data.model

class WarehouseModel {
    var code: String = ""
    var company: String? = null
    var address: String? = null
    var description: String? = null
    var distributionList: String? = null
    var logicStatus: Int? = null
    var name: String? = null
    var status: Int? = null
    var store: String? = null
    var ubications: List<UbicationModel>? = ArrayList()
}