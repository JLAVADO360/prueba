package ndp.posapp.data.model

data class PaymentMethodStoreModel(
    var code: String = "",
    var account: String? = null,
    var createDate: String? = null,
    var createUser: String? = null,
    var store: String? = null,
    var updateDate: String? = null,
    var updateUser: String? = null
)