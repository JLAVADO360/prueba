package ndp.posapp.data.model

class MovementTransferIODetailModel {
    var amount    : Double?=null
    var currency  : String?=null
    var name      : String?=null
    var numberLine: Int?=null
}