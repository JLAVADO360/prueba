package ndp.posapp.data.model

class DetailCashRegisterSaveModel {
    var numberLine: Int? = null
    var payWay: String? = null
    var payMethod: String? = null
    var amount: Double? = null
    var count: Double? = null
    var imbalanceReason: String? = null
    var imbalanceComment: String? = null
    var denominations: MutableList<DenominationsCashRegisterSaveModel>? = null
    var account: String? = null
    var diference: Double? = null
    var totalOut: Double? = null
    var imbalanceNumberAccount:String?=null

}

