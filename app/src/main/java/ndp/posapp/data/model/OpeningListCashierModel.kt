package ndp.posapp.data.model

data class OpeningListCashierModel(
    var code:String?,
    var cashRegister:String?,
    var store:String?,
    var observation:String?,
    var openingCashRegisterDate:String?,
    var createUser:String?,
    var createUserName:String?,
    var createUserEmail:String?,
    var status:Int?,
    var docNum:Int?,
    var docYear:Int?,
    var turn:Int?
)
