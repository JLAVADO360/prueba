package ndp.posapp.data.model

class OpeningCashRegisterSaveModel {
    var code:            String? = null
    var createUser:      String? = null
    var createUserEmail: String? = null
    var createUserName:  String? = null
    var store:           String? = null
    var status:          Int? = null
    var cashRegister:    String? = null
    var observation:     String? = null
    var detail: MutableList<DetailOpeningCashRegisterSaveModel>? = null
}