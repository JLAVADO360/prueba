package ndp.posapp.data.model

data class CurrencyModel (
    var code: String = "",
    var company: String? = null,
    var createDate: String? = null,
    var createUser: String? = null,
    var denomination: List<CurrencyDenominationModel> = ArrayList(),
    var description: String? = null,
    var isDefault: Int = 0,
    var name: String = "",
    var status: Int = 0,
    var symbol: String = "",
    var updateDate: String? = null,
    var updateUser: String? = null
    )