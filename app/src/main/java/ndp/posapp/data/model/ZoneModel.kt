package ndp.posapp.data.model

class ZoneModel {
    var code: String = ""
    var createDate: String? = null
    var createUser: String? = null
    var description: String? = null
    var status: Int = 0
    var updateDate: String? = null
    var updateUser: String? = null
    var zone: String = ""
    var users : List<ZoneUserModel> = ArrayList()
    var warehouses : List<ZoneWarehouseModel> = ArrayList()
}