package ndp.posapp.data.model

class PickUpDocumentDetailModel {

    var document: String? = null
    var itemCode: String? = null
    var itemName: String? = null
    var numberLine: Int? = null
    var quantity: Double? = null
    var status: String? = null
    var store: String? = null
    var transactionalCode: String? = null
    var ubicationEntryFrom: Int? = null
    var ubicationEntryTo: Int? = null
    var ubicationFrom: String? = null
    var ubicationTo: String? = null
    var unitMSRCode: String? = null
    var warehouseFrom: String? = null
    var warehouseTo: String? = null
}