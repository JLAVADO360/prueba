package ndp.posapp.data.model

import java.io.Serializable

class ReturnOrderDetailModel : Serializable {
    var baseQuantity: Double? = null
    var currency: String? = null
    var discount: Double? = null
    var discountPercentage: Double? = null
    var grossPrice: Double? = null
    var inventoryItem: String? = null
    var isMerma: Int? = null
    var itemCode: String? = null
    var itemName: String? = null
    var numberLine: Int? = null
    var onlyTax: String? = null
    var quantity: Double? = null
    var salePrice: Double? = null
    var subtotal: Double? = null
    var subtotalDiscount: Double? = null
    var sunatAffectationType: String? = null
    var sunatOneroso: String? = null
    var sunatOperation: String? = null
    var sunatOperationType: String? = null
    var tax: Double? = null
    var taxAmount: Double? = null
    var taxCode: String? = null
    var total: Double? = null
    var ubication: String? = null
    var ubicationEntry: Int? = null
    var unitGroupEntry: Int? = null
    var unitMSR: String? = null
    var unitMSRCode: String? = null
    var unitMSREntry: Int? = null
    var unitMSRName: String? = null
    var unitPrice: Double? = null
    var warehouse: String? = null
}