package ndp.posapp.data.model

data class CashRegisterBalanceModel(
    var code:String?,
    var cashRegister:String?,
    var store:String?,
    var observation:String?,
    var balanceCashRegisterDate:String?,
    var createUser:String?,
    var createUserName:String?,
    var createUserEmail:String?,
    var turn:Int?,
    var detail:MutableList<DetailCashRegisterBalanceModel>?
)
