package ndp.posapp.data.model


class AffectationModel {
    var code: String = ""
    var company: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var description: String? = null
    var updateDate: String? = null
    var updateUser: String? = null
}