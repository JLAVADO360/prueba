package ndp.posapp.data.model

class RegionModel {
    var identifier: String = ""
    var code: String? = null
    var concatCode: String? = null
    var country: String? = null
    var region: String? = null
    var regionType: String? = null
}