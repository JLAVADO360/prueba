package ndp.posapp.data.model

class UbicationModel {
    var code: String = ""
    var createDate: String? = null
    var createUser: String? = null
    var entry: Int? = null
    var isSale: Int? = null
    var logicStatus: Int? = null
    var name: String? = null
    var status: Int? = null
    var type: String? = null
    var updateDate: String? = null
    var updateUser: String? = null
    var warehouse: String? = null
}