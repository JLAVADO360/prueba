package ndp.posapp.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReasonImbalanceModel (
    var code:String?=null,
    var reason:String?=null,
    var accountNumber:String?=null,
    var status:Int?=null,
 ):Parcelable