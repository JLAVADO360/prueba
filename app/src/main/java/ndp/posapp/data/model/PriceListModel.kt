package ndp.posapp.data.model


class PriceListModel {

    var code: Int = 0
    var company: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var currency: String? = null
    var description: String? = null
    var item: String? = null
    var logicStatus: Int? = null
    var name: String? = null
    var price: Double? = null
    var status: Int? = null
    var type: String? = null
    var updateDate: String? = null
    var updateUser: String? = null
}