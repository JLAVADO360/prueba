package ndp.posapp.data.model

data class AccountBankModel(
    var accountCurrency: String? = null,
    var accountCurrencyCode: String? = null,
    var accountDescription: String? = null,
    var accountType: String? = null,
    var accounts: List<PaymentMethodStoreModel> = ArrayList(),
    var methodCode: String = ""
)