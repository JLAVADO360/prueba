package ndp.posapp.data.model

class MovementTransferIOModel {
    var company         : String?=null
    var createUser      : String?=null
    var createUserEmail : String?=null
    var createUserName  : String?=null
    var status          : String?=null
    var store           : String?=null
    var type            : String?=null
    var vault           : String?=null
    var operationNumber : String?=null
    var docNumERP       : String?=null
    var detail          : MutableList<MovementTransferIODetailModel>? = null
//    var createDate      : String?=null
//    var updateDate      : String?=null
//    var docNum          : Int?=null
//    var docYear         : Int?=null
//    var creationTurn    : Int?=null
//    var comment         : String?=null
}