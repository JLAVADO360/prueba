package ndp.posapp.data.model

class ApplicationDTOModel {
    var code : String = ""
    var logoPath : String = ""
    var name : String = ""
    var objectId : String = ""
    var url : String = ""
}