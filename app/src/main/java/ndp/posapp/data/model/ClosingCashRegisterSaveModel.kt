package ndp.posapp.data.model

class ClosingCashRegisterSaveModel {
    var additionalData:  ClosingCashRegisterAdditionalModel? = null
    var cashRegister:    String? = null
    var store:           String? = null
    var createUser:      String? = null
    var createUserEmail: String? = null
    var createUserName:  String? = null
    var closureDate:     String? = null
    var status:          Int? = null
    var detail:          MutableList<DetailCashRegisterSaveModel>? = null
    var comment:         String? = null
}