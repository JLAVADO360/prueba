package ndp.posapp.data.model

data class CorrelativeModel(
    var actualNumeration: Int? = null,
    var companyIdentifier: String = String(),
    var endNumeration: Int? = null,
    var id: String = String(),
    var initialNumeration: Int? = null,
    var modified: Int? = null,
    var numerationLength: Int? = null,
    var parentReceiptType: String? = null,
    var receiptTypeCode: String? = null,
    var series: String? = null,
    var status: Int? = null,
    var storeCode: String? = null,
    var version: Long? = null
)