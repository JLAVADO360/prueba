package ndp.posapp.data.model

class LoggerCashRegisterModel {
    var code: String = ""
    var name: String = ""
    var company: String = ""
    var assigned: Int? = null
    var type: String = ""
    var user: String = ""
    var openingCreationUser: String = ""
    var status: Int? = null
}