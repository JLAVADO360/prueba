package ndp.posapp.data.model


class RepositorySyncHistoryModel {

    var user: String = ""
    var company: String? = null
    var channel: String = ""
    var lastSync: String = ""
    var repository: String = ""
    var warehouse: String ? = null
}