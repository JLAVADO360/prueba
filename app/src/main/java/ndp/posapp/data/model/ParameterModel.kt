package ndp.posapp.data.model


class ParameterModel {
    var code: String = ""
    var company: String? = null
    var application: String? = null
    var createDate: String? = null
    var description: String? = null
    var logicStatus: Int ?=null
    var name: String ?=null
    var objectUI: String? = null
    var parameterType: String? = null
    var status: Int ?=null
    var updateDate: String? = null
    var value: String ?=null
}