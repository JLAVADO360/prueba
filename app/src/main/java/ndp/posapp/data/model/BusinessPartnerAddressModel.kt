package ndp.posapp.data.model

class BusinessPartnerAddressModel {

    var address: String? = null
    var addressName: String? = null
    var country: String? = null
    var createDate: String? = null
    var department: String? = null
    var district: String? = null
    var invoicesCountry: String? = null
    var logicStatus: Int? = null
    var province: String? = null
    var rowNum: Int? = null
    var status: Int? = null
    var type: String = ""
    var ubiquitous: String? = null
}