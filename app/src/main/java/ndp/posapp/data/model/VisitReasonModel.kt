package ndp.posapp.data.model

class VisitReasonModel {
    var code: String = ""
    var reason: String = ""
    var status: String = ""
}