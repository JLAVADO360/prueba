package ndp.posapp.data.model


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BoxClosedDetail (
    var amount:Double?=null,
    var numberLine:Int?=null,
    var payWay:String?=null,
    var payMethod:String?=null,

    ):Parcelable