package ndp.posapp.data.model

data class MovementListCashierModel(
    var code:String?,
    var cashRegister:String?,
    var store:String?,
    var amount:Double?,
    var movementType:String?,
    var description:String?,
    var documentType:String?,
    var comment:String?,
    var payWay:String?,
    var payMethod:String?,
    var createUser:String?,
    var createUserName:String?,
    var createUserEmail:String?,
    var movementDate:String?,
    var currency:String?,
    var docReference:String?,
    var docNum:String?,
    var docYear:Int?,
    var documentCode:String?,
    var status:Int?,
    var turn:Int?
)