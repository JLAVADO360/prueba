package ndp.posapp.data.model

class ContactPersonModel {

    var code: String? = null
    var address: String? = null
    var cellPhoneNumber: String? = null
    var email: String? = null
    var name: String? = null
    var phone: String? = null
    var relationship: String? = null
}