package ndp.posapp.data.model

class KardexModel {


    var ubication: String = ""
    var createDate: String? = null
    var createUser: String? = null
    var item: String = ""
    var logicStatus: Int = 0
    var status: Int = 0
    var stock: Double = 0.0
    var stockCommitted: Double = 0.0
    var stockRequest: Double = 0.0
    var store: String? = null
    var ubicationEntry: Int = 0
    var updateDate: String? = null
    var updateUser: String? = null
    var warehouse: String = ""
}