package ndp.posapp.data.model

data class TurnModel(
    var cashRegister:String?,
    var code:String?,
    var turn:Int?,
    var status:Int?,
    var startTurn:String?,
    var endTurn:String?,
    var turnCashRegisterDate:String?,
    var openingCreationUser:String?,
    var closureCreationUser:String?
)