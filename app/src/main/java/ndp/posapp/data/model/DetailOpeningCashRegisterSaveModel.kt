package ndp.posapp.data.model

data class DetailOpeningCashRegisterSaveModel (
    var closureAmount: Int? = null,
    var currency: String? = null,
    var name: String? = null,
    var currentBalance: Double? = null,
    var previousClosure: Double? = null,
    var numberLine: Int? = null,
    var rate: Int? = null,
    var total: Double? = null,
    var difference: Double? = null,//
    var diference: Double? = null,//
    var imbalanceReason: String? = null,//
    var imbalanceNumberAccount:String?=null,//
    var imbalanceComment: String? = null,//
    var denominations: MutableList<DenominationsCashRegisterSaveModel>? = null
 )