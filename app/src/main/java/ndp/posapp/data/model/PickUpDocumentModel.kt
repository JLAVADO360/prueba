package ndp.posapp.data.model

class PickUpDocumentModel {

    var code: String? = null
    var comment: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var createUserEmail: String? = null
    var createUserName: String? = null
    var dateArrival: String? = null
    var detail  : List<PickUpDocumentDetailModel> = ArrayList()
    var docNum: Int? = null
    var docReference: String? = null
    var docYear: Int? = null
    var document: String? = null
    var erpDocNum: Int? = null
    var erpSerialNumber: Int? = null
    var moreData  :  MoreDataModel ?=null
    var pickUpDate: String? = null
    var process: String? = null
    var receivingUser: String? = null
    var receivingUserEmail: String? = null
    var receivingUserName: String? = null
    var status: String? = null
    var store: String? = null
    var transactionalCode: String? = null
    var updateDate: String? = null
}