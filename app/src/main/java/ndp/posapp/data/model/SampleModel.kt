package ndp.posapp.data.model

class SampleModel {

    var attentionUser: String? = null
    var attentionUserEmail: String? = null
    var attentionUserName: String? = null
    var code: String? = null
    var comment: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var createUserEmail: String? = null
    var createUserName: String? = null
    var dateArrival: String? = null
    var detail    :List<SampleDetailModel> = ArrayList()
    var docNum: Int? = null
    var docReference: String? = null
    var docYear: Int? = null
    var document: String? = null
    var documentCode: String? = null
    var erpDocNum: Int? = null
    var erpSerialNumber: Int? = null
    var receivingUser: String? = null
    var receivingUserEmail: String? = null
    var receivingUserName: String? = null
    var sampleDate: String? = null
    var status: String? = null
    var store: String? = null
    var transactionalCode: String? = null
    var updateDate: String? = null
    var warehouse: String? = null
}