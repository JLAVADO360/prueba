package ndp.posapp.data.model

data class RateStoreModel(
    var code: String = "",
    var company: String? = null,
    var createDate: String? = null,
    var currencyFrom: String? = null,
    var currencyFromModel: CurrencyModel? = null,
    var currencyTo: String? = null,
    var currencyToModel: CurrencyModel? = null,
    var rate: Double? = null,
    var rateDate: String? = null,
    var status: Int? = null,
    var store: String? = null
)