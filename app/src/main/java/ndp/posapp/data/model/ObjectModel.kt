package ndp.posapp.data.model

class ObjectModel {
    var code : String = ""
    var description : String = ""
    var id : String = ""
    var name : String = ""
    var objectType : ObjectTypeModel = ObjectTypeModel()
}