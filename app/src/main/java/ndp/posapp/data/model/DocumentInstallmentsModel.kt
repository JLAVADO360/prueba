package ndp.posapp.data.model

import java.io.Serializable

class DocumentInstallmentsModel : Serializable {
    var code: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var dueDate: String? = null
    var paymentOrdered: String? = null
    var percentage: Double? = null
    var type: String? = null
    var typeService: String? = null
    var updateDate: String? = null
    var updateUser: String? = null
}