package ndp.posapp.data.model

class RoleDTOModel {
    var applications : List<ApplicationDTOModel> = ArrayList()
    var code : String = ""
    var description : String = ""
    var enabled : String = ""
    var entity : EntityDTOModel = EntityDTOModel()
    var id : String = ""
    var name : String = ""
    var permissions : List<PermissionDTOModel> = ArrayList()
}