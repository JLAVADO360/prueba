package ndp.posapp.data.model

data class DetailClosingCashierModel(
    var numberLine:Int?,
    var payWay:String?,
    var payMethod:String?,
    var amount:Double,
    var count:Double?,
    var account:String?
)