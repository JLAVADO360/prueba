package ndp.posapp.data.model

class BusinessPartnerModel {
    var additionalData: Any? = null
    var code: String = ""
    var company: String? = null
    var addresses: List<BusinessPartnerAddressModel> = ArrayList()
    var alternativeEmail: String? = null
    var approved: String? = null
    var balanceSys: Double? = null
    var businessName: String? = null
    var comment: String? = null
    var commercialName: String? = null
    var contactPersons: List<ContactPersonModel> = ArrayList()
    var createDate: String? = null
    var createUser: String? = null
    var creditBalance: Double? = null
    var creditLine: Double? = null
    var currency: String? = null
    var customerType: String? = null
    var email: String? = null
    var hasBusiness: String? = null
    var lastNameF: String? = null
    var lastNameM: String? = null
    var logicStatus: Int? = null
    var migration: String? = null
    var migrationDate: String? = null
    var migrationMessage: String? = null
    var migrationStatus: Int? = null
    var names: String? = null
    var nif: String? = null
    var partnerType: String? = null
    var paymentCondition: Int? = null
    var personType: Int? = null
    var phoneNumber: String? = null
    var priceList: String? = null
    var recordType: Int? = null
    var sunatAffectationType: String? = null
    var sunatOneroso: String? = null
    var sunatOperation: String? = null
    var sunatOperationType: String? = null
    var updateDate: String? = null
    var updateUser: String? = null
}