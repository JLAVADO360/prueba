package ndp.posapp.data.model

class LoggerUserModel {
    var documentNumber: String = ""
    var documentType: String = ""
    var email: String = ""
    var name: String = ""
    var surname: String = ""
    var sex: String = ""
    var roleCode: String = ""
    var roleDescription: String = ""
    var roleName: String = ""
    var entities : List<EntityDTOModel> = ArrayList()
    var entity: String = ""
    var fingerPrint: String = ""
    var token: String = ""
    var signature: String = ""
    var isLogged: Boolean = false
    var userIdentifier: String = ""
    var store: String? = null
    var storeAddress: String? = null
    var storePriceList: Int? = null
    var ubication: String? = null
    var ubicationEntry: Int? = null
    var warehouse: String? = null

}