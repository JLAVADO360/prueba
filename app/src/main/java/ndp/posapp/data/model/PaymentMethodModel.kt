package ndp.posapp.data.model

data class PaymentMethodModel(
    var code: String = "",
    var accountCurrency: String? = null,
    var accountCurrencyCode: String? = null,
    var accountDescription: String? = null,
    var accountName: String? = null,
    var accountType: String? = null,
    var accounts: List<PaymentMethodStoreModel> = ArrayList(),
    var cardType: String? = null,
    var cartTypeCode: String? = null,
    var default: Int? = null,
    var method: String? = null
) {
    override fun toString(): String {
        return accountName ?: String()
    }
}