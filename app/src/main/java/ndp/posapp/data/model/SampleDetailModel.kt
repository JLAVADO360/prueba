package ndp.posapp.data.model

class SampleDetailModel {

    var dateArrival: String? = null
    var id: Int? = null
    var itemCode: String? = null
    var itemName: String? = null
    var numberLine: Int? = null
    var quantity: Double? = null
    var status: String? = null
    var ubicationEntryFrom: Int? = null
    var ubicationEntryTo: Int? = null
    var ubicationFrom: String? = null
    var ubicationTo: String? = null
    var unitMsrCode: String? = null

}