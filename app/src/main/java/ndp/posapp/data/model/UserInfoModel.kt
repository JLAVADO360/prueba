package ndp.posapp.data.model

import java.io.Serializable

class UserInfoModel : Serializable {
    var store: String? = null
    var storeAccount: String? = null
    var storeAddress: String? = null
    var storeMaxDecrementPriceUnit: Double? = null
    var storeName: String? = null
    var storePriceList: Int? = null
    var storeUserAdmin: String? = null
    var ubication: String? = null
    var ubicationEntry: Int? = null
    var warehouse: String? = null
}