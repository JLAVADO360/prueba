package ndp.posapp.data.model

data class BankModel(
    var accounts: List<AccountBankModel> = ArrayList(),
    var bankCode: String = "",
    var bankName: String? = null
)