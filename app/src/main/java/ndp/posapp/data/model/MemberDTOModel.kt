package ndp.posapp.data.model

class MemberDTOModel {

    var activated : String = ""
    var address : String = ""
    var birthday : String = ""
    var blocked : String = ""
    var documentNumber : String = ""
    var documentType : String = ""
    var email : String = ""
    var enabled : String = ""
    var entities : List<EntityDTOModel> = ArrayList()
    var entity : EntityDTOModel = EntityDTOModel()
    var id : String = ""
    var memberType : MemberTypeDTOModel = MemberTypeDTOModel()
    var memberTypeCode : String = ""
    var name : String = ""
    var phoneNumbers : List<String> = ArrayList()
    var protocolCode : String = ""
    var role : RoleDTOModel = RoleDTOModel()
    var roles : List<RoleDTOModel> = ArrayList()
    var sex : String = ""
    var surname : String = ""
    var unlimitedSessionUser : String = ""

}