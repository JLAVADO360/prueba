package ndp.posapp.data.model

import java.io.Serializable

class CashRegisterBalanceDTOModel : Serializable {
    var store:String?=null
    var cashRegister:String?=null
    var createUser:String?=null
    var createUserEmail:String?=null
    var createUserName:String?=null
}