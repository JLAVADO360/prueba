package ndp.posapp.data.model

class EntityDTOModel {

    var address: String = ""
    var businessName: String = ""
    var enabled: String = ""
    var federation: FederationDTOModel = FederationDTOModel()
    var id: String = ""
    var licenses: List<EntityApplicationDTOModel> = ArrayList()
    var ruc: String = ""
}