package ndp.posapp.data.model

import java.io.Serializable

class OpeningCashRegisterModel: Serializable {
    var detail:MutableList<OpeningDetailCasghRegisterModel> = ArrayList()
    var code:String?=null
    var createUser:String?=null
    var createUserEmail:String?=null
    var createUserName:String?=null
    var store:String?=null
    var status:Int?=null
    var cashRegister:String?=null
    var observation:String?=null
}