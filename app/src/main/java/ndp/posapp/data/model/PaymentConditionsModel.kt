package ndp.posapp.data.model

class PaymentConditionsModel {
    var code: String = ""
    var company: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var creditLimit: Double = 0.0
    var generalDiscount: Double = 0.0
    var groupNumber: Int = 0
    var interestOnArrears: Double = 0.0
    var loadLimit: Double = 0.0
    var numberOfAdditionalDays: Int = 0
    var numberOfAdditionalMonths: Int = 0
    var numberOfToleranceDays: Int = 0
    var paymentTermsGroupName: String? = null
    var priceListNo: Int = 0
    var type: Int = 0
    var updateDate: String? = null
    var updateUser: String? = null
}