package ndp.posapp.data.model

class ItemModel {
    var code: String = ""
    var company: String? = null
    var barCode: String? = null
    var color: String? = null
    var createDate: String? = null
    var createUser: String? = null
    var inventoryItem: Int? = null
    var inventoryUnit: String? = null
    var isbn: String? = null
    var kardex: List<WhsKardexModel> = ArrayList()
    var logicStatus: Int? = null
    var name: String? = null
    var priceList: List<PriceListDetailModel> = ArrayList()
    var pricingUnit: Int? = null
    var projectName: String? = null
    var salesItem: Int? = null
    var salesItemsPerUnit: Double? = null
    var salesUnit: String? = null
    var size: String? = null
    var status: Int? = null
    var taxCode: String? = null
    var u_VS_AFEDET: String? = null
    var u_VS_CODDET: String? = null
    var u_VS_PORDET: Double? = null
    var unitGroupEntry: Int? = null
    var updateDate: String? = null
    var updateUser: String? = null
    var weight: Double? = null
}