package ndp.posapp.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReasonImbalanceObjectModel(
    var method:String?=null,
    var amountImbalance:Double?=null,
    var listReasonImbalance:ArrayList<ReasonImbalanceModel>?=null,
    var comment:String?=null
):Parcelable
