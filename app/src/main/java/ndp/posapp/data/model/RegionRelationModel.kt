package ndp.posapp.data.model

class RegionRelationModel {
    var childrenRegion: String = ""
    var country: String = ""
    var parentRegion: String = ""
    var active: Int? = null
}