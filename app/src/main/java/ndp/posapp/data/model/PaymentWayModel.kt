package ndp.posapp.data.model

data class PaymentWayModel(
    var banks: List<BankModel> = ArrayList(),
    var code: String = "",
    var methods: MutableList<PaymentMethodModel> = ArrayList(),
    var paymentWay: String? = null,
    var store: String? = null,
) {
    override fun toString(): String {
        return paymentWay ?: String()
    }
}