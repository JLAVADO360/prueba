package ndp.posapp.data.model

class PriceListDetailModel {

    var applyTax: Int? = null
    var createDate: String? = null
    var createUser: String? = null
    var currency: String? = null
    var grossPrice: Double? = null
    var item: String = ""
    var logicStatus: Int? = null
    var priceList: Int  = 0
    var priceListName: String? = null
    var priceListType: String? = null
    var status: Int? = null
    var store: String? = null
    var tax: String? = null
    var taxPercentage: Double? = null
    var unitMSR: String? = null
    var unitMSREntry: Int? = null
    var unitMSRName: String? = null
    var unitPrice: Double? = null
    var updateDate: String? = null
    var updateUser: String? = null


}