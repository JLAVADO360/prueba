package ndp.posapp.data.model

class DenominationsCashRegisterSaveModel (
    var denomination:Double?=null,
    var numberLine:Int?=null,
    var quantity:String?=null,
    var total:Double?=null
)