package ndp.posapp.data.model

class WhsKardexModel {

    var warehouse: String = ""
    var itemEntity: String = ""
    var kardexUbication: List<KardexModel> = ArrayList()
    var logicStatus: Int? = null
    var stock: Double = 0.0
    var stockCommitted: Double = 0.0
    var stockDisponible: Double = 0.0
    var stockNoForSale: Double = 0.0
    var stockRequested: Double = 0.0
    var store: String? = null
}