package ndp.posapp.data.model

data class ReconciliationModel(
    var amount: Double? = null,
    var cashRegister: String? = null,
    var code: String = String(),
    var createDate: String? = null,
    var createUser: String? = null,
    var createUserEmail: String? = null,
    var createUserName: String? = null,
    var creditNote: String? = null,
    var creditNoteDTO: Any? = null,
    var currency: String? = null,
    var paymentReceipt: String? = null,
    var paymentReceiptDTO: PaymentReceiptModel,
    var store: String? = null,
    var turnCashRegister: String? = null,
    var updateDate: String? = null
)