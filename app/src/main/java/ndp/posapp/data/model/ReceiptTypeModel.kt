package ndp.posapp.data.model

class ReceiptTypeModel {
    var code: String = ""
    var company: String? = null
    var codeApplication: String? = null
    var logicStatus: Int? = null
    var receipt: String? = null
    var status: Int? = null
}