package ndp.posapp.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReasonImbalanceSaveModel(
    var imbalanceReason:String?=null,
    var imbalanceComment:String?=null,
    var imbalanceNumberAccount:String?=null,
    var position:Int?=null
):Parcelable
