package ndp.posapp.data.model

data class ClosingListCashierGenerateModel (
    var balanceDate:String?,
    var cashRegister:String?,
    var code:String?,
    var comment:String?,
    var createUser:String?,
    var createUserEmail:String?,
    var createUserName:String?,
    var store:String?,
    var turn:Int?,
    var detail:MutableList<DetailCashRegisterBalanceModel>?
        )


