package ndp.posapp.data.model

import java.io.Serializable

class SalesOrdersModel : Serializable {
    var code: String? = null
    var docNum: Int? = null
    var docYear: Int? = null
    var nameSalePerson: String? = null
}