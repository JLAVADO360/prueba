package ndp.posapp.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BoxClosedLast (
    var boxName : String ?=null,
    var status: Int ?=null,
    var boxCode: String?=null,
    var store: String?=null,
    var lista :ArrayList<BoxClosedDetail>? = null

):Parcelable