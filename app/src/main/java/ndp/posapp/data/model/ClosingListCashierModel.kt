package ndp.posapp.data.model

data class ClosingListCashierModel(
    var code:String?,
    var cashRegister:String?,
    var store:String?,
    var observation:String?,
    var closureDate:String?,
    var createUser:String?,
    var createUserName:String?,
    var createUserEmail:String?,
    var status:Int?,
    var docNum:Int?,
    var docYear:Int?,
    var turn:Int?,
    //GG
    var detail:MutableList<DetailClosingCashierModel>
    //GG
)
