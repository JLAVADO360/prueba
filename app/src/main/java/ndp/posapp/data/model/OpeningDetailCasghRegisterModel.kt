package ndp.posapp.data.model

class OpeningDetailCasghRegisterModel {
    var closureAmount:Int?=null
    var currency:String?=null
    var name:String?=null
    var denominations:MutableList<DenominationModel> = ArrayList()
    var currentBalance:Double?=null
    var previousClosure:Int?=null
    var numberLine:Int?=null
    var rate:Int?=null
    var total:Double?=null
}