package ndp.posapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.CashRegisterBalanceDTOModel
import ndp.posapp.data.model.ClosingListCashierModel
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.ActivityClosingListCashierBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.DatePickerFragment
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.EventObserver
import ndp.posapp.utils.interaction.getDate
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.activity.QueryActivity
import ndp.posapp.view.adapter.ClosingListCashierAdapter
import ndp.posapp.view.adapter.ClosingListCashierAdapterx
import ndp.posapp.viewModel.*
import xdroid.toaster.Toaster
import kotlin.collections.ArrayList

class ClosingListCashierActivity : AppCompatActivity(),SwipeRefreshLayout.OnRefreshListener {
    private lateinit var searchView: SearchView
    private lateinit var binding: ActivityClosingListCashierBinding

    private lateinit var loggerUserModel: LoggerUserModel

    private lateinit var cashRegisterViewModel: CashRegisterViewModel

    private lateinit var Code:String
    private lateinit var Name:String
    private lateinit var Company:String
    private lateinit var OeningCreationUser:String
    private lateinit var User:String
    private lateinit var Type:String
    private var Assigned:Int=-1
    private var Status:Int=-1

    private var currentBusinessPartner: BusinessPartnerView? = null
    private var from = ""+ getDate().anio+"-"+ getDate().mes+"-"+ getDate().dia
    private var to = ""+ getDate().anio+"-"+ getDate().mes+"-"+ getDate().dia

    private lateinit var closingListCashierAdapter: ClosingListCashierAdapter
    private lateinit var closingListCashierAdapterx: ClosingListCashierAdapterx

    /** VIEW MODEL **/
    private lateinit var closingListCashierViewModel: ClosingListCashierViewModel
    private lateinit var generateViewModel: GenerateViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityClosingListCashierBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        cashRegisterViewModel = ViewModelProvider(this).get(CashRegisterViewModel::class.java)
        generateViewModel     = ViewModelProvider(this).get(GenerateViewModel::class.java)

        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        getCashRegisterData()

        binding.IVCleanCLC.setOnClickListener(){
            closingListCashierAdapterx.clear()
        }
        binding.TVOLCashierCL.text = Name

        binding.ETStartDateCL.setText("   "+getDate().anio+"-"+getDate().mes+"-"+getDate().dia)
        binding.ETEndDateCL.setText("   "+getDate().anio+"-"+getDate().mes+"-"+getDate().dia)
        binding.ETStartDateCL.setOnClickListener(){
            showStartDatePickerDialog()
        }
        binding.ETEndDateCL.setOnClickListener(){
            showEndDatePickerDialog()
        }
        binding.IVAddCLC.setOnClickListener(){
            val intent = Intent(this, ClosingCashRegisterActivity::class.java)
            startActivity(intent)
        }
        binding.IVSearchCLC.setOnClickListener(){
            from = binding.ETStartDateCL.text.toString().trim()
            to = binding.ETEndDateCL.text.toString().trim()
            initViewModel()
        }
        initElements()
        generateCall()

    }

    private fun dd(){
        var asig:Int = -1
        val cashRegisterLiveData = cashRegisterViewModel.get(loggerUserModel.entity, loggerUserModel.userIdentifier, Const.CASH_REGISTER_TYPE_DEFAULT)
        cashRegisterLiveData.observe(this) { cashRegister ->
            cashRegisterLiveData.removeObservers(this)
            if (cashRegister != null) {
                cashRegisterViewModel.listCashRegisterInfo(loggerUserModel)
                Name = cashRegister.name
                binding.TVOLCashierCL.setText(Name)
                if (cashRegister != null) {
                    //SaveCashRe/88gisterData(cashRegister)
                    //asig= cashRegister.assigned!!
                    if(cashRegister.assigned==0)
                    //if(asig==1 && asig==1)
                        binding.IVAddCLC.visibility = View.GONE
                }
            }
        }
    }

    private fun updateViewClosing(){
        cashRegisterViewModel.listCashRegisterInfo(loggerUserModel)
        cashRegisterViewModel.result.observe(this){
            if(it.successful){
                it.data?.let {objeto->
                    if (it.data!!.size==1){
                        if(it.data!![0].cashRegisterAssigned==0){
                            binding.IVAddCLC.visibility = View.GONE
                        }else{
                            binding.IVAddCLC.visibility = View.VISIBLE
                        }
                    }
                }
            }else{
                Toaster.toast("Error al actualizar!!!")
                finish()
            }
        }
    }

    private fun showStartDatePickerDialog() {
        val datePicker = DatePickerFragment {day, month, year -> onStartDateSelected(day, month+1, year)}
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun showEndDatePickerDialog() {
        val datePicker = DatePickerFragment {day, month, year -> onEndDateSelected(day, month+1, year)}
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun onStartDateSelected(day:Int, month:Int, year:Int){
        binding.ETStartDateCL.setText(ndp.posapp.utils.interaction.getCompleteDate(year,month,day))
    }

    private fun onEndDateSelected(day:Int, month:Int, year:Int){
        binding.ETEndDateCL.setText(ndp.posapp.utils.interaction.getCompleteDate(year,month,day))
    }

    private fun getCashRegisterData() {
        val sharedPref: SharedPreferences = getSharedPreferences("CashRegisterData", Context.MODE_PRIVATE)
        Code = sharedPref.getString("code", null).toString()
        Name = sharedPref.getString("name", null).toString()
        Company = sharedPref.getString("company", null).toString()
        OeningCreationUser = sharedPref.getString("openingCreationUser", null).toString()
        User = sharedPref.getString("user", null).toString()
        Type = sharedPref.getString("type", null).toString()
        Assigned = sharedPref.getInt("assigned",-1)
        Status = sharedPref.getInt("status",-1)
    }

    private fun initElements() {
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = "Lista de Cierres"
        dd()
        initViewModel()
        initActions()
    }

    private fun initViewModel() {
        closingListCashierViewModel = ViewModelProvider(this).get(ClosingListCashierViewModel::class.java)
        closingListCashierCall(0)
    }

    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener {
            this.finish()
        }
    }

    private fun initRecyclerView(PclosingListCashierAdapter:ClosingListCashierAdapter) {
        binding.rvClosingList.setHasFixedSize(true)
        binding.rvClosingList.adapter = PclosingListCashierAdapter
        binding.rvClosingList.layoutManager = LinearLayoutManager(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.INTENT_BUSINESS_PARTNER -> {
                if (data != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        currentBusinessPartner = data.getSerializableExtra(Const.INTENT_BUSINESS_PARTNER_TAG) as BusinessPartnerView
                        //binding.tvBusinessPartner.text = currentBusinessPartner?.businessPartnerEntity?.businessName

                    } else {
                        PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                    }
                } else {
                    PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                }
            }
            QueryActivity.PAYMENT_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    /*if (::receiptPaymentDetailDialog.isInitialized) receiptPaymentDetailDialog.close()
                    filter()*/
                }
            }
        }
    }

    private fun closingListCashierCall(page: Int) {
        var itemPerPage : Int = 100;
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_order))
        closingListCashierViewModel.listClosingListCashier(loggerUserModel, Code, from, to, page,itemPerPage)
        closingListCashierViewModel.resultClosingListCashier.observe(this, EventObserver {
            PopUp.closeProgressDialog()
            if (it.successful) {
                it.data?.value.let { list ->
                    closingListCashierAdapter = ClosingListCashierAdapter(list!!,Name)
                    initRecyclerView(closingListCashierAdapter)
                    showMessageInfo(0,"")
                }
            } else {
                PopUp.showAlert(this, it.message)
                showMessageInfo(1,""+it.message)
                val list:List<ClosingListCashierModel> =ArrayList()
                initRecyclerView(ClosingListCashierAdapter(list,Name))
            }
        })
    }

    private fun generateCall() {
        var cashRegisterBalanceDTOModel: CashRegisterBalanceDTOModel = CashRegisterBalanceDTOModel()
        cashRegisterBalanceDTOModel.store = loggerUserModel.store
        cashRegisterBalanceDTOModel.cashRegister = Code
        cashRegisterBalanceDTOModel.createUser = loggerUserModel.userIdentifier
        cashRegisterBalanceDTOModel.createUserEmail = loggerUserModel.email
        cashRegisterBalanceDTOModel.createUserName= loggerUserModel.name
        generateViewModel.listClosingCashGenerate(loggerUserModel,cashRegisterBalanceDTOModel)
        generateViewModel.result.observe(this, androidx.lifecycle.Observer {
            if(it.successful){
                it.data?.let {
                    objeto->
                    Log.e("prueba1:", ""+objeto.balanceDate)
                    Log.e("prueba2:", ""+objeto.cashRegister)
                    Log.e("prueba3:", ""+objeto.turn)
                    for (closingListCashierGenerateModel in objeto.detail!!){
                        Log.e("prueba4:", ""+closingListCashierGenerateModel.amount)
                    }
                }

            }else{
                Log.e("pruebax:", it.message)
            }
        })
    }

    /*private fun initRecyclerView() {
        binding.rvOpeningList.setHasFixedSize(true)
        itemAdapter = ItemAdapter(this, this)
        binding.rvOpeningList.adapter = itemAdapter
        binding.rvOpeningList.layoutManager = LinearLayoutManager(this)
        itemViewModel.list(loggerUserModel.entity).observe(this, {
            it.let { itemAdapter.setList(it) }
        })

    }*/

    /*override fun onSelectedItem(itemView: ItemView) {
        val intent = Intent(this, ItemDetailActivity::class.java)
        intent.putExtra(getString(R.string.item),itemView)
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivity(intent)
    }*/

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //return super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        //searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnSearchClickListener {
            binding.appBar.tvTitleToolbar.visibility = View.GONE
            binding.appBar.ivBackToolbar.visibility = View.GONE
        }
        searchView.setOnCloseListener {
            binding.appBar.tvTitleToolbar.visibility = View.VISIBLE
            binding.appBar.ivBackToolbar.visibility = View.VISIBLE
            false
        }
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                itemAdapter.filter.filter(newText)
                return true
            }

        })
        return true
    }*/

    private fun showMessageInfo(estate :Int,message:String){
        when(estate){
            0->{binding.llMessageClousing.llNoRegisters.visibility = View.GONE
                binding.llMessageClousing.tvNoRegisters.text = ""}
            1->{binding.llMessageClousing.llNoRegisters.visibility = View.VISIBLE
                binding.llMessageClousing.tvNoRegisters.text = message}
        }
    }

    override fun onBackPressed() {
        if (!searchView.isIconified) {
            searchView.isIconified = true
            return
        }
        super.onBackPressed()
    }

    override fun onRefresh() {
        closingListCashierAdapterx.clear()
    }

    override fun onResume() {
        super.onResume()
        updateViewClosing()
        //closingListCashierCall(0)
    }
}