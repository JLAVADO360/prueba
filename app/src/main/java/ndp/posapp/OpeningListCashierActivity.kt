package ndp.posapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.*
import ndp.posapp.databinding.ActivityOpeningListCashierBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.DatePickerFragment
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.EventObserver
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.activity.QueryActivity
import ndp.posapp.view.adapter.OpeningListCashierAdapter
import ndp.posapp.viewModel.*
import ndp.posapp.utils.interaction.*
import xdroid.toaster.Toaster

class OpeningListCashierActivity : AppCompatActivity() {
    private lateinit var searchView: SearchView
    private lateinit var binding: ActivityOpeningListCashierBinding
    private lateinit var Code: String
    private lateinit var Name: String
    private lateinit var Company: String
    private lateinit var OeningCreationUser: String
    private lateinit var User: String
    private lateinit var Type: String
    private var Assigned: Int = -1
    private var Status: Int = -1

    private lateinit var cashRegisterViewModel: CashRegisterViewModel
    private lateinit var currencyViewModel:CurrencyViewModel

    private lateinit var cashRegisterBalanceViewModel: CashRegisterBalanceViewModel

    private var currentBusinessPartner: BusinessPartnerView? = null
    private var from = ""+getDate().anio+"-"+getDate().mes+"-"+getDate().dia
    private var to = ""+getDate().anio+"-"+getDate().mes+"-"+getDate().dia

    private lateinit var listaMontos:MutableList<Double>
    private lateinit var listaCurrencyMontos:MutableList<String>
    private lateinit var loggerUserModel: LoggerUserModel
    private lateinit var loggerUserViewModel:LoggerUserViewModel

    private lateinit var openingListCashierAdapter: OpeningListCashierAdapter

    /** VIEW MODEL **/
    private lateinit var openinListCashierViewModel: OpeninListCashierViewModel
    private lateinit var closingListCashierViewModel: ClosingListCashierViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityOpeningListCashierBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        listaMontos = mutableListOf()
        listaCurrencyMontos = mutableListOf()
        listaCurrencyMontos.clear()
        listaMontos.clear()
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        cashRegisterViewModel = ViewModelProvider(this).get(CashRegisterViewModel::class.java)
        currencyViewModel = ViewModelProvider(this).get(CurrencyViewModel::class.java)
        closingListCashierViewModel = ViewModelProvider(this).get(ClosingListCashierViewModel::class.java)
        getCashRegisterData()

        binding.IVCleanOLC.setOnClickListener(){}
        binding.TVOLCashier.text = Name

        binding.ETStartDate.setText("   "+getDate().anio+"-"+getDate().mes+"-"+getDate().dia)
        binding.ETEndDate.setText("   "+getDate().anio+"-"+getDate().mes+"-"+getDate().dia)
        binding.ETStartDate.setOnClickListener(){
            showStartDatePickerDialog()
        }
        binding.ETEndDate.setOnClickListener(){
            showEndDatePickerDialog()
        }
        binding.IVAddOLC.setOnClickListener(){
            getClosingCashier()
            //balanceListCashierCall(0)
        }
        binding.IVSearchOLC.setOnClickListener(){
            from = binding.ETStartDate.text.toString().trim()
            to   = binding.ETEndDate.text.toString().trim()
            initViewModel()
        }
        initElements()

    }

    private fun showStartDatePickerDialog() {
        val datePicker = DatePickerFragment {day, month, year -> onStartDateSelected(day, month+1, year)}
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun showEndDatePickerDialog() {
        val datePicker = DatePickerFragment {day, month, year -> onEndDateSelected(day, month+1, year)}
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun onStartDateSelected(day:Int, month:Int, year:Int){
        binding.ETStartDate.setText(ndp.posapp.utils.interaction.getCompleteDate(year,month,day))
    }

    private fun onEndDateSelected(day:Int, month:Int, year:Int){
        binding.ETEndDate.setText(ndp.posapp.utils.interaction.getCompleteDate(year,month,day))
    }

    private fun getCashRegisterData() {
        val sharedPref: SharedPreferences = getSharedPreferences("CashRegisterData", Context.MODE_PRIVATE)
        Code = sharedPref.getString("code", null).toString()
        Name = sharedPref.getString("name", null).toString()
        Company = sharedPref.getString("company", null).toString()
        OeningCreationUser = sharedPref.getString("openingCreationUser", null).toString()
        User = sharedPref.getString("user", null).toString()
        Type = sharedPref.getString("type", null).toString()
        Assigned = sharedPref.getInt("assigned",-1)
        Status = sharedPref.getInt("status",-1)
    }

    private fun initElements() {
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = "Lista de Aperturas"//getString(R.string.opening_list_cahsier)
        //Inserta data en BD interna
        cashRegisterViewModel.listCashRegisterInfo(loggerUserModel)
        dd()
        cur()
        initViewModel()
        initActions()
    }

    private fun cur(){
        val currencyLiveData = currencyViewModel.getCurrency(loggerUserModel)
        currencyLiveData.observe(this) { currency ->
            currencyLiveData.removeObservers(this)
            if (currency != null) {
                //currencyViewModel.getCurrency(loggerUserModel)
                //binding.TVOLCashier.setText(cashRegister.name)
                if (currency != null) {
                    //SaveCashRe/88gisterData(cashRegister)
                    //asig= cashRegister.assigned!!
                    //Toast.makeText(this, "CACASADASDAS" + currency.data?.value, Toast.LENGTH_SHORT).show()
                    //if(asig==1 && asig==1)
                        //binding.IVAddOLC.visibility = View.INVISIBLE
                }
            }
        }
    }

    private fun dd(){
        var asig:Int = -1
        val cashRegisterLiveData = cashRegisterViewModel.get(loggerUserModel.entity, loggerUserModel.userIdentifier, Const.CASH_REGISTER_TYPE_DEFAULT)
        cashRegisterLiveData.observe(this) { cashRegister ->
        cashRegisterLiveData.removeObservers(this)
        if (cashRegister != null) {
            cashRegisterViewModel.listCashRegisterInfo(loggerUserModel)
            Name = cashRegister.name

                binding.TVOLCashier.setText(Name)
            if (cashRegister != null) {
                var code=cashRegister.code
                var user=cashRegister.user
                var asddd = cashRegister.openingCreationUser
                var pl = cashRegister.company
                var qw=code
                var rt=user
                var poo=asddd
                var ggggg = Name
                //SaveCashRe/88gisterData(cashRegister)
                    //asig= cashRegister.assigned!!
                /*if(cashRegister.assigned==1)
                //if(asig==1 && asig==1)
                    binding.IVAddOLC.visibility = View.INVISIBLE*/
                if(cashRegister.assigned!=0)
                //if(asig==1 && asig==1)
                    binding.IVAddOLC.visibility = View.GONE
                }
            }
        }
    }

    private fun updateViewClosing(){
        cashRegisterViewModel.listCashRegisterInfo(loggerUserModel)
        cashRegisterViewModel.result.observe(this){
            if(it.successful){
                it.data?.let {objeto->
                    if (it.data!!.size==1){
                        if(it.data!![0].cashRegisterAssigned!=0){
                            binding.IVAddOLC.visibility = View.GONE
                        }else{
                            binding.IVAddOLC.visibility = View.VISIBLE
                        }
                    }
                }
            }else{
                Toaster.toast("Error al actualizar!!!")
                finish()
            }
        }
    }

    private fun initViewModel() {
        openinListCashierViewModel = ViewModelProvider(this).get(OpeninListCashierViewModel::class.java)
        openingListCashierCall(0)
    }

    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener {
            this.finish()
        }
    }

    private fun initRecyclerView(PopeningListCashierAdapter:OpeningListCashierAdapter) {
        binding.rvOpeningList.setHasFixedSize(true)
        binding.rvOpeningList.adapter = PopeningListCashierAdapter
        binding.rvOpeningList.layoutManager = LinearLayoutManager(this)
    }

    private fun cashRegisterBalance(page: Int) {
        var cashRegisterBalanceDTOModel: CashRegisterBalanceDTOModel = CashRegisterBalanceDTOModel()
        //cashRegisterBalanceDTOModel.store =
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_order))
        cashRegisterBalanceViewModel.cashRegisterBalance(loggerUserModel, cashRegisterBalanceDTOModel)
        cashRegisterBalanceViewModel.resultCashRegisterBalanceModel.observe(this, EventObserver {
            PopUp.closeProgressDialog()
            if (it.successful) {
                if (it.data?.nextPage != null) {
                    //binding.fabNextPage.visibility = View.VISIBLE
                    //nextPage = it.data!!.nextPage!!.split("&")[0].replace("saleorder?page=", "").toInt()
                } else {
                    //binding.fabNextPage.visibility = View.GONE
                }
                it.data?.value.let { list ->
                    var fff = list!!.get(0).detail!!.get(0).payMethod
                    Toast.makeText(this, fff, Toast.LENGTH_LONG).show()//list!!.get(0).detail.get(0).
                    //binding.llNoMatches.llNoMatches.visibility = View.GONE
                    //queryOrderAdapter.setList(list!!)
                    //balanceListCashierAdapter = BalanceListCashierAdapter(list!!)
                    //initRecyclerView(balanceListCashierAdapter)
                }
            } else {
                PopUp.showAlert(this, it.message)
            }

        })
    }

    private fun getClosingCashier():MutableList<Double> {
        listaMontos = mutableListOf()
        listaCurrencyMontos = mutableListOf()
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_order))
        closingListCashierViewModel.listClosingListCashier(loggerUserModel, Code, "2000-01-01", "2999-12-31", 0,100)
        closingListCashierViewModel.resultClosingListCashier.observe(this, EventObserver {
            PopUp.closeProgressDialog()
            if (it.successful) {
                var asss:Double = it.data!!.value!!.get(0).detail.get(0).amount
                var sizeamounts=it.data!!.value!!.get(0).detail.size
                for (i in 0.. sizeamounts-1)
                {
                    listaMontos.add(it.data!!.value!!.get(0).detail.get(i).amount)
                    listaCurrencyMontos.add(it.data!!.value!!.get(0).detail.get(i).payMethod.toString())
                }
                var listaMontosSize = listaMontos.size
                var listaCurrencyMontosSize = listaCurrencyMontos.size
                val I = Intent(this, OpeningCashRegisterActivity::class.java)

                val lista : MutableList<DetailClosingCashierModel> = it.data!!.value!![0].detail
                val objeto : BoxClosedLast = BoxClosedLast()
                val listaObjeto = ArrayList<BoxClosedDetail>()
                for (detailClosingCashierModel in lista){
                    var boxClosedDetail:BoxClosedDetail= BoxClosedDetail()
                    boxClosedDetail.amount = detailClosingCashierModel.amount
                    boxClosedDetail.numberLine = detailClosingCashierModel.numberLine
                    boxClosedDetail.payMethod = detailClosingCashierModel.payMethod
                    boxClosedDetail.payWay = detailClosingCashierModel.payWay
                    listaObjeto?.add(boxClosedDetail)
                }

                objeto.boxName = Name
                objeto.status  = Status
                objeto.boxCode = Code
                objeto.store   = it.data!!.value!![0].store
                objeto.lista   = listaObjeto

                I.putExtra("lista",objeto)
                I.putExtra("_Status", Status)
                I.putExtra("_CashRegCode", Code)
                //I.putExtra("_Store", )
                I.putExtra("CajaName", Name)
                I.putExtra("size",listaMontosSize)
                for (i in 0..listaMontosSize-1)
                    I.putExtra("monto"+i.toString(),listaMontos.get(i))
                I.putExtra("sizeListaCurrencyMontos", listaCurrencyMontosSize)
                for (j in 0..listaCurrencyMontosSize-1)
                    I.putExtra("moneda"+j.toString(), listaCurrencyMontos.get(j))

                startActivity(I)
            } else {
                PopUp.showAlert(this, it.message)
            }
        })
        return listaMontos
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.INTENT_BUSINESS_PARTNER -> {
                if (data != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        currentBusinessPartner = data.getSerializableExtra(Const.INTENT_BUSINESS_PARTNER_TAG) as BusinessPartnerView
                        //binding.tvBusinessPartner.text = currentBusinessPartner?.businessPartnerEntity?.businessName

                    } else {
                        PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                    }
                } else {
                    PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                }
            }
            QueryActivity.PAYMENT_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    /*if (::receiptPaymentDetailDialog.isInitialized) receiptPaymentDetailDialog.close()
                    filter()*/
                }
            }
        }
    }

    private fun openingListCashierCall(page: Int) {
        var itemPerPage : Int = 100;
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_order))
        openinListCashierViewModel.listOpeningListCashier(loggerUserModel, Code, from, to, page,itemPerPage)
        openinListCashierViewModel.resultOpeningListCashier.observe(this, EventObserver {
            PopUp.closeProgressDialog()
            if (it.successful) {
                if (it.data?.nextPage != null) {
                    //binding.fabNextPage.visibility = View.VISIBLE
                    //nextPage = it.data!!.nextPage!!.split("&")[0].replace("saleorder?page=", "").toInt()
                } else {
                    //binding.fabNextPage.visibility = View.GONE
                }
                it.data?.value.let { list ->
                    //list!!.get(0).
                    //binding.llNoMatches.llNoMatches.visibility = View.GONE
                    //queryOrderAdapter.setList(list!!)
                    openingListCashierAdapter = OpeningListCashierAdapter(list!!)
                    initRecyclerView(openingListCashierAdapter)
                    showMessageInfo(0,"")
                }
            } else {
                PopUp.showAlert(this, it.message)
                showMessageInfo(1,""+it.message)
                val lista:List<OpeningListCashierModel> =ArrayList()
                initRecyclerView(OpeningListCashierAdapter(lista))
            }

        })
    }

    private fun showMessageInfo(estate :Int,message:String){
        when(estate){
            0->{binding.llMessageOpening.llNoRegisters.visibility = View.GONE
                binding.llMessageOpening.tvNoRegisters.text = ""}
            1->{binding.llMessageOpening.llNoRegisters.visibility = View.VISIBLE
                binding.llMessageOpening.tvNoRegisters.text = message
            }
        }
    }

    /*private fun initRecyclerView() {
        binding.rvOpeningList.setHasFixedSize(true)
        itemAdapter = ItemAdapter(this, this)
        binding.rvOpeningList.adapter = itemAdapter
        binding.rvOpeningList.layoutManager = LinearLayoutManager(this)
        itemViewModel.list(loggerUserModel.entity).observe(this, {
            it.let { itemAdapter.setList(it) }
        })

    }*/

    /*override fun onSelectedItem(itemView: ItemView) {
        val intent = Intent(this, ItemDetailActivity::class.java)
        intent.putExtra(getString(R.string.item),itemView)
        overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
        startActivity(intent)
    }*/

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        //return super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_search, menu)
        val searchManager = getSystemService(SEARCH_SERVICE) as SearchManager
        //searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        searchView.setOnSearchClickListener {
            binding.appBar.tvTitleToolbar.visibility = View.GONE
            binding.appBar.ivBackToolbar.visibility = View.GONE
        }
        searchView.setOnCloseListener {
            binding.appBar.tvTitleToolbar.visibility = View.VISIBLE
            binding.appBar.ivBackToolbar.visibility = View.VISIBLE
            false
        }
        searchView.maxWidth = Int.MAX_VALUE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                itemAdapter.filter.filter(newText)
                return true
            }

        })
        return true
    }*/

    override fun onBackPressed() {
        finish()
        super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        updateViewClosing()
        //openingListCashierCall(0)
    }

}