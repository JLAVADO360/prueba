package ndp.posapp

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.valdesekamdem.library.mdtoast.MDToast
import ndp.posapp.data.db.entityView.BusinessPartnerView
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.databinding.ActivityMovementListCashierBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.DatePickerFragment
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.EventObserver
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.activity.QueryActivity
import ndp.posapp.view.adapter.MovementListCashierAdapter
import ndp.posapp.viewModel.ListCashierTurnViewModel
import ndp.posapp.viewModel.MovementListCashierViewModel
import ndp.posapp.utils.interaction.*
import ndp.posapp.view.box.MovementExternalFragment

class MovementListCashierActivity : AppCompatActivity() {
    private lateinit var searchView: SearchView
    private lateinit var binding: ActivityMovementListCashierBinding

    private lateinit var loggerUserModel: LoggerUserModel

    //private lateinit var sharedPref: SharedPreferences = getSharedPreferences("CashRegisterData", Context.MODE_PRIVATE)
    private lateinit var Code:String
    private lateinit var Name:String
    private lateinit var Company:String
    private lateinit var OeningCreationUser:String
    private lateinit var User:String
    private lateinit var Type:String
    private var Assigned:Int=-1
    private var Status:Int=-1

    private var currentBusinessPartner: BusinessPartnerView? = null
    private var from = ""
    private var to = ""

    private lateinit var movementListCashierAdapter: MovementListCashierAdapter

    private lateinit var movementListCashierViewModel:MovementListCashierViewModel
    private lateinit var turnViewModel:ListCashierTurnViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        binding = ActivityMovementListCashierBinding.inflate(layoutInflater)
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        loggerUserModel = SharedPreferencesLogin().getSharedPreferences(this)
        getCashRegisterData()

        binding.IVCleanMLC.setOnClickListener(){}
        binding.TVOLCashierML.setText(Name)

        binding.ETStartDateML.setText("   "+getDate().anio+"-"+getDate().mes+"-"+getDate().dia)
        binding.ETStartDateML.setCompoundDrawablesWithIntrinsicBounds(R.drawable.baseline_calendar_month_black_24dp, 0, 0, 0);
        binding.ETStartDateML.setOnClickListener(){
            showStartDatePickerDialog()

        }
        binding.IVSearchMLC.setOnClickListener(){
            from = binding.ETStartDateML.text.toString().trim()
            //to = binding.ETEndDateSC.text.toString().trim()

            /*val f1 = binding.ETStartDate.text.toString()
            val f2 = binding.ETEndDate.text.toString()

            val da1 = LocalDate.parse(f1, DateTimeFormatter.ISO_DATE)
            val da2 = LocalDate.parse(f2, DateTimeFormatter.ISO_DATE)
            //val d1 = LocalDate.parse(f1, formatter)
            //val d2 = LocalDate.parse(f2, formatter)
            from = da1.toString()
            to = da2.toString()*/
            initViewModel()
        }

        binding.FBAddMLC.setOnClickListener(){
//            val intent = Intent(this, MovementCashRegisterActivity::class.java)
//            startActivity(intent)
            showTransferExternal()
        }


        from = binding.ETStartDateML.text.toString().trim()
        initElements()

    }
    private fun showStartDatePickerDialog() {
        val datePicker = DatePickerFragment {day, month, year -> onStartDateSelected(day, month+1, year)}
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun onStartDateSelected(day:Int, month:Int, year:Int)
    {
        binding.ETStartDateML.setText(getCompleteDate(year,month,day))
        from = binding.ETStartDateML.text.toString().trim()
        turnViewModel = ViewModelProvider(this).get(ListCashierTurnViewModel::class.java)
        movementListCashierTurnCall()
    }

    private fun getCashRegisterData() {
        val sharedPref: SharedPreferences = getSharedPreferences("CashRegisterData", Context.MODE_PRIVATE)
        Code = sharedPref.getString("code", null).toString()
        Name = sharedPref.getString("name", null).toString()
        Company = sharedPref.getString("company", null).toString()
        OeningCreationUser = sharedPref.getString("openingCreationUser", null).toString()
        User = sharedPref.getString("user", null).toString()
        Type = sharedPref.getString("type", null).toString()
        Assigned = sharedPref.getInt("assigned",-1)
        Status = sharedPref.getInt("status",-1)
    }

    private fun initElements() {
        setSupportActionBar(binding.appBar.tbToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        binding.appBar.tvTitleToolbar.text = "Operaciones de movimiento"

        initActions()
        initViewModel()
        //initRecyclerView()
    }

    private fun initViewModel() {
        movementListCashierViewModel = ViewModelProvider(this).get(MovementListCashierViewModel::class.java)
        movementListCashierCall(0)
    }

    private fun initActions() {
        binding.appBar.ivBackToolbar.setOnClickListener {
            this.finish()
        }
    }

    private fun initRecyclerView(PMovementListCashierAdapter: MovementListCashierAdapter) {
        binding.rvMovementList.setHasFixedSize(true)
        binding.rvMovementList.adapter = PMovementListCashierAdapter
        binding.rvMovementList.layoutManager = LinearLayoutManager(this)
        /*itemViewModel.list(loggerUserModel.entity).observe(this, {
            it.let { itemAdapter.setList(it) }
        })*/
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            Const.INTENT_BUSINESS_PARTNER -> {
                if (data != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        currentBusinessPartner = data.getSerializableExtra(Const.INTENT_BUSINESS_PARTNER_TAG) as BusinessPartnerView
                        //binding.tvBusinessPartner.text = currentBusinessPartner?.businessPartnerEntity?.businessName

                    } else {
                        PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                    }
                } else {
                    PopUp.mdToast(this, getString(R.string.no_get_client), MDToast.TYPE_WARNING)
                }
            }
            QueryActivity.PAYMENT_REQUEST_CODE -> {
                if (resultCode == Activity.RESULT_OK) {
                    /*if (::receiptPaymentDetailDialog.isInitialized) receiptPaymentDetailDialog.close()
                    filter()*/
                }
            }
        }
    }

    private fun movementListCashierCall(page: Int) {
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_order))
        movementListCashierViewModel.listMovementListCashier(loggerUserModel, page, Code, loggerUserModel.store!!, from)
        movementListCashierViewModel.resultMovementListCashier.observe(this, EventObserver {
            PopUp.closeProgressDialog()
            if (it.successful) {
                if (it.data?.nextPage != null) {
                    //binding.fabNextPage.visibility = View.VISIBLE
                    //nextPage = it.data!!.nextPage!!.split("&")[0].replace("saleorder?page=", "").toInt()
                } else {
                    //binding.fabNextPage.visibility = View.GONE
                }
                it.data?.value.let { list ->
                    //binding.llNoMatches.llNoMatches.visibility = View.GONE
                    //queryOrderAdapter.setList(list!!)
                    movementListCashierAdapter = MovementListCashierAdapter(list!!)
                    initRecyclerView(movementListCashierAdapter)
                    //showMessageInfo(0,"")
                }
            } else {
                PopUp.showAlert(this, it.message)
                //showMessageInfo(1,""+it.message)
            }

        })
    }

    private fun movementListCashierTurnCall() {
        var listaTurnos:MutableList<String>
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.consulting_order))
        turnViewModel.listTurnCashier(loggerUserModel, from, Code, loggerUserModel.store!!)
        turnViewModel.resultTurn.observe(this, {
            PopUp.closeProgressDialog()
            //if (it.successful) {
                /*if (it.data?.nextPage != null) {
                    //binding.fabNextPage.visibility = View.VISIBLE
                    //nextPage = it.data!!.nextPage!!.split("&")[0].replace("saleorder?page=", "").toInt()
                } else {
                    //binding.fabNextPage.visibility = View.GONE
                }*/
                 /*   it.get()
                it.value.let { list ->
                    //binding.llNoMatches.llNoMatches.visibility = View.GONE
                    //queryOrderAdapter.setList(list!!)
                    var turns = mutableListOf<String>()
                    for (i in 0..list!!.size-1)
                        turns.add("Turno " + list!!.get(i).turn.toString())*/
                    //movementListCashierAdapter = MovementListCashierAdapter(list!!.get(0).turn)
            listaTurnos = mutableListOf()
            var n = it.size
            for (i in 0..n-1)
            {
                listaTurnos.add("Turno " + it.get(i).turn.toString())
            }
                    val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listaTurnos)
                    binding.SPTurn.adapter = adapter
                /*}
            } else {
                PopUp.showAlert(this, it.message)
            }*/

            })
    }

    private fun showMessageInfo(estate :Int,message:String){
        when(estate){
            0->{binding.llMessageMovement.llNoRegisters.visibility = View.VISIBLE
                binding.llMessageMovement.tvNoRegisters.text = ""}
            1->{binding.llMessageMovement.llNoRegisters.visibility = View.GONE
                binding.llMessageMovement.tvNoRegisters.text = message}
        }
    }

    private fun showTransferExternal(){
            val movementExternalFragment = MovementExternalFragment()
            val args = Bundle()
            args.putString("codeBox",Code)
            args.putString("nameBox",Name)
            movementExternalFragment.arguments = args
            movementExternalFragment.show(supportFragmentManager,"dialogMovementExternal")

    }

}