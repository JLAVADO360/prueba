package ndp.posapp.utils.dialog

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.view.View
import android.view.Window
import androidx.recyclerview.widget.LinearLayoutManager
import ndp.posapp.R
import ndp.posapp.data.model.PaymentReceiptModel
import ndp.posapp.databinding.DetailDocumentDialogBinding
import ndp.posapp.utils.use.Mappers
import ndp.posapp.view.activity.PaymentReceivedActivity
import ndp.posapp.view.activity.QueryActivity
import ndp.posapp.view.adapter.PaymentReceiptDetailAdapter

class ReceiptPaymentDetailDialog internal constructor(
    private val activity: Activity,
    private val paymentReceiptModel: PaymentReceiptModel
) {
    val dialog = Dialog(activity, R.style.Dialog_AppTheme_Transparent)
    @SuppressLint("SetTextI18n")
    fun open() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        val binding = DetailDocumentDialogBinding.inflate(activity.layoutInflater)
        dialog.setContentView(binding.root)
        val paymentReceiptDetailAdapter = PaymentReceiptDetailAdapter(activity)
        binding.tvTitle.text = "${Mappers.mapReceiptTypeName(paymentReceiptModel.receiptType)} ${paymentReceiptModel.paymentReceiptNumber}"
        binding.tvBusinessPartner.text = paymentReceiptModel.businessName
        binding.tvBusinessPartnerCode.text = paymentReceiptModel.nif
        binding.rvDetail.setHasFixedSize(true)
        binding.rvDetail.adapter = paymentReceiptDetailAdapter
        binding.rvDetail.layoutManager = LinearLayoutManager(activity)
        paymentReceiptDetailAdapter.setList(paymentReceiptModel.detail)

        binding.ivPaymentReceived.visibility = View.VISIBLE
        binding.ivPaymentReceived.setOnClickListener {
            activity.overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
            activity.startActivityForResult(PaymentReceivedActivity.newIntent(context = activity, paymentReceipt = paymentReceiptModel), QueryActivity.PAYMENT_REQUEST_CODE)
        }

        dialog.show()
    }

    fun close () {
        if (dialog.isShowing) dialog.dismiss()
    }

}