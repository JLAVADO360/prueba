package ndp.posapp.utils.dialog

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.view.Window
import android.widget.RadioButton
import androidx.core.content.res.ResourcesCompat
import ndp.posapp.R
import ndp.posapp.data.db.entity.WarehouseEntity
import ndp.posapp.databinding.DialogRadiobuttonListBinding
import java.util.*

class WareHouseDialog internal constructor(
    private val activity: Activity,
    private val warehouses: List<WarehouseEntity>,
    private val listener: Listener,
    private val warehouseSelected: String
) {

    interface Listener {
        fun onSelectedWarehouse(warehouseEntity: WarehouseEntity)
    }

    @SuppressLint("SetTextI18n")
    fun open() {
        val dialog = Dialog(activity, R.style.Dialog_AppTheme_Transparent)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding = DialogRadiobuttonListBinding.inflate(activity.layoutInflater)
        dialog.setContentView(binding.root)
        binding.tvTitle.text = activity.getString(R.string.warehouse_title)
        val typeface = ResourcesCompat.getFont(activity, R.font.montserrat)
        warehouses.forEach { warehouse ->
            val rb = RadioButton(activity)
            rb.typeface = typeface
            rb.text = "${warehouse.code}-${warehouse.description}"
            rb.setOnClickListener {
                listener.onSelectedWarehouse(warehouse)
                dialog.dismiss()
            }
            binding.radioGroup.addView(rb)
            val warehouseCode = rb.text.toString().split("-")[0]
            if (warehouseSelected.lowercase(Locale.getDefault()) == warehouseCode.lowercase(Locale.getDefault())) {
                rb.isChecked = true
            }
        }
        dialog.show()
    }
}