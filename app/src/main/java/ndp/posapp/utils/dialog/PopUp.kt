package ndp.posapp.utils.dialog


import android.annotation.SuppressLint
import android.app.Activity
import ndp.posapp.R
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import java.lang.annotation.RetentionPolicy;
import android.content.DialogInterface
import android.view.View
import android.view.Window
import android.widget.*
import java.lang.annotation.Retention;
import androidx.annotation.IntDef
import com.valdesekamdem.library.mdtoast.MDToast

import android.app.ProgressDialog
import androidx.core.content.res.ResourcesCompat
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import ndp.posapp.data.db.entity.PaymentConditionsEntity
import ndp.posapp.databinding.DialogProgressTaskBinding
import xdroid.toaster.Toaster
import java.util.*


object PopUp {

    private const val MSG_SYMBOL_SUCCESS = "✓"
    private const val MSG_SYMBOL_WARNING = "!"
    private const val MSG_SYMBOL_ERROR = "X"

    const val MSG_TITLE_SUCCESS = "Confirmación"
    const val MSG_TITLE_WARNING = "Advertencia"
    const val MSG_TITLE_ERROR = "Error"

    const val MSG_TYPE_SUCCESS = 0
    const val MSG_TYPE_WARNING = 1
    const val MSG_TYPE_ERROR = 2

    private lateinit var dialog: ProgressDialog

    private lateinit var dialogSyncProgress: Dialog
    private lateinit var bind: DialogProgressTaskBinding

    @IntDef(MSG_TYPE_SUCCESS, MSG_TYPE_WARNING, MSG_TYPE_ERROR)
    @Retention(RetentionPolicy.SOURCE)
    annotation class TypeMsg
    //endregion

    //endregion
    //region Const for buttons
    const val BUTTON_NAME_ACCEPT = "Aceptar"
    const val BUTTON_NAME_CONTINUE = "Continuar"
    const val BUTTON_NAME_RETRY = "Reintentar"
    const val BUTTON_NAME_SAVE = "Guardar"
    const val BUTTON_NAME_PERMISSION = "Sí, conceder permisos"
    const val BUTTON_NAME_SETTINGS = "Ir Ajustes"
    const val BUTTON_NAME_notPERMISSION = "No, Salir"
    const val BUTTON_NAME_EMAIL = "Cliente"
    const val BUTTON_NAME_EMAIL_NEW = "Nuevo"

    const val BUTTON_TYPE_ACCEPT = 0
    const val BUTTON_TYPE_CONTINUE = 1
    const val BUTTON_TYPE_RETRY = 2
    const val BUTTON_TYPE_SAVE = 3
    const val BUTTON_TYPE_PERMISSION = 4
    const val BUTTON_TYPE_SETTINGS = 5
    const val BUTTON_TYPE_SYNC = 8
    const val BUTTON_TYPE_CHANGE_EMAIL = 9

    @IntDef(
        BUTTON_TYPE_ACCEPT,
        BUTTON_TYPE_CONTINUE,
        BUTTON_TYPE_RETRY,
        BUTTON_TYPE_SAVE,
        BUTTON_TYPE_PERMISSION,
        BUTTON_TYPE_SETTINGS,
        BUTTON_TYPE_SYNC,
        BUTTON_TYPE_CHANGE_EMAIL
    )
    @Retention(RetentionPolicy.SOURCE)
    annotation class TypeButton
    //endregion

    //region AlertDialogs
    //region public static void showAlert(Context context, String message)

    //endregion
    //region AlertDialogs
    //region public static void showAlert(Context context, String message)
    fun showAlert(context: Context, message: String?) {
        GlobalScope.launch(Dispatchers.Main) {
            showAlert(context, context.getString(R.string.titulo_error), message)
        }
    }

    fun showAlert(context: Context?, title: String?, message: String?) {
            AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.ok) { dialog, which -> dialog.dismiss() }
                .setCancelable(false).show() //.setIcon(R.drawable.alerta).show();
    }


    fun showAlertFinish(context: Activity, title: String?, message: String?) {
        AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.ok) { dialog, which ->
                dialog.dismiss()
                context.finish()
            }.setCancelable(false).show() //.setIcon(R.drawable.alerta).show();
    }

    //endregion

    //endregion
    //region  public static void ShowAlert()
    fun ShowAlertDialog(
        context: Context?,
        title: String?,
        message: String?,
        positive: DialogInterface.OnClickListener?
    ) {
        val alert: AlertDialog.Builder = AlertDialog.Builder(context)
        alert.setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.ok, positive)
            .setCancelable(false)
        alert.create()
        alert.show()
    }

    fun ShowAlertDialog(
        context: Context,
        title: String,
        message: String,
        positive: DialogInterface.OnClickListener,
        negative: DialogInterface.OnClickListener,
        isCancelable: Boolean
    ) {
        val alert: AlertDialog.Builder = AlertDialog.Builder(context)
        alert.setTitle(title)
            .setMessage(message)
            .setNegativeButton(R.string.cancel, positive)
            .setPositiveButton(
                R.string.ok,
                DialogInterface.OnClickListener { dialog, which ->
                    negative?.onClick(dialog, which)
                    dialog.dismiss()
                })
            .setCancelable(isCancelable)
        alert.create()
        alert.show()
    }

    fun ShowAlertDialog(
        context: Context,
        title: String,
        message: String,
        positive: DialogInterface.OnClickListener,
        cancelable: Boolean
    ) {
        val alert: AlertDialog.Builder = AlertDialog.Builder(context)
        alert.setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.ok, positive)
            .setCancelable(cancelable)
        alert.create()
        alert.show()
    }
    //endregion

    //endregion
    //region  public static void ShowAlert()
    fun ShowAlertDialog(
        context: Context?,
        title: String?,
        message: String?,
        positive: DialogInterface.OnClickListener?,
        negative: DialogInterface.OnClickListener?
    ) {
        val alert: AlertDialog.Builder = AlertDialog.Builder(context)
        alert.setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.ok, positive)
            .setNegativeButton(
                R.string.cancel,
                DialogInterface.OnClickListener { dialog, which ->
                    negative?.onClick(dialog, which)
                    dialog.dismiss()
                })
            .setCancelable(false)
        alert.create()
        alert.show()
    }
    //endregion
    //endregion

    //endregion
    //endregion
    //region ShowDialog
    fun ShowDialog(
        context: Context,
        message: String?,
        title: String,
        @TypeMsg typeMessage: Int,
        @TypeButton typeButton: Int,
        action1: View.OnClickListener,
        action2: View.OnClickListener?,
        cancel: DialogInterface.OnCancelListener?
    ) {
        val dialog = Dialog(context, R.style.Dialog_AppTheme_Transparent)
        val cancelable =
            typeButton == BUTTON_TYPE_SAVE || typeButton == BUTTON_TYPE_PERMISSION || typeButton == BUTTON_TYPE_SETTINGS
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setContentView(R.layout.custom_dialog_alert)
        (dialog.findViewById(R.id.dialog_alert_tvMensaje) as TextView).text = message
        (dialog.findViewById(R.id.dialog_alert_tvTitulo) as TextView).text = title
        (dialog.findViewById(R.id.dialog_alert_tvTipoMensaje) as TextView).text =
            getSymbol(typeMessage)
        var button: Button
        //region btnAceptar_Click
        button = dialog.findViewById(R.id.dialog_alert_btnAceptar)
        button.setText(
            if (typeButton == BUTTON_TYPE_PERMISSION) BUTTON_NAME_PERMISSION else if (typeButton == BUTTON_TYPE_SETTINGS) BUTTON_NAME_SETTINGS else getButtonName(
                typeButton
            )
        )
        button.setOnClickListener { v: View? ->
            if (action1 != null) action1.onClick(v)
            dialog.dismiss()
        }
        //endregion

        //region btnCancelar_Click
        button = dialog.findViewById(R.id.dialog_alert_btnCancelar) as Button
        if (typeButton == BUTTON_TYPE_SAVE) button.setText(BUTTON_NAME_RETRY)
        if (typeButton == BUTTON_TYPE_PERMISSION || typeButton == BUTTON_TYPE_SETTINGS) button.setText(
            BUTTON_NAME_notPERMISSION
        )
        if (typeButton == BUTTON_TYPE_CHANGE_EMAIL) button.setText(BUTTON_NAME_EMAIL_NEW)
        button.setOnClickListener { v: View? ->
            if (action2 != null) action2.onClick(v)
            dialog.dismiss()
        }
        //endregion
        dialog.setOnCancelListener(DialogInterface.OnCancelListener { dialog ->
            cancel?.onCancel(
                dialog
            )
        })
        dialog.show()
    }

    //region ShowDialog
    fun ShowDialogCustomButton(
        context: Context,
        message: String,
        title: String,
        button1: String,
        button2: String,
        @TypeMsg typeMessage: Int,
        @TypeButton typeButton: Int,
        action1: View.OnClickListener,
        action2: View.OnClickListener,
        cancel: DialogInterface.OnCancelListener?
    ) {
        val dialog = Dialog(context, R.style.Dialog_AppTheme_Transparent)
        val cancelable =
            typeButton == BUTTON_TYPE_SAVE || typeButton == BUTTON_TYPE_PERMISSION || typeButton == BUTTON_TYPE_SETTINGS || typeButton == BUTTON_TYPE_SYNC
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setContentView(R.layout.custom_dialog_alert)
        (dialog.findViewById(R.id.dialog_alert_tvMensaje) as TextView).text = message
        (dialog.findViewById(R.id.dialog_alert_tvTitulo) as TextView).text = title
        (dialog.findViewById(R.id.dialog_alert_tvTipoMensaje) as TextView).text =
            getSymbol(typeMessage)
        var button: Button
        //region btnAceptar_Click
        button = dialog.findViewById(R.id.dialog_alert_btnAceptar)
        button.setText(button1)
        button.setOnClickListener { v: View? ->
            if (action1 != null) action1.onClick(v)
            dialog.dismiss()
        }
        //endregion

        //region btnCancelar_Click
        button = dialog.findViewById(R.id.dialog_alert_btnCancelar) as Button
        button.setText(button2)
        button.setOnClickListener { v: View? ->
            if (action2 != null) action2.onClick(v)
            dialog.dismiss()
        }
        //endregion
        dialog.setOnCancelListener(DialogInterface.OnCancelListener { dialog ->
            cancel?.onCancel(
                dialog
            )
        })
        dialog.show()
    }


    fun ShowDialogHideButton(
        context: Context,
        message: String,
        title: String,
        button1: String,
        button2: String,
        @TypeMsg typeMessage: Int,
        @TypeButton typeButton: Int,
        action1: View.OnClickListener,
        action2: View.OnClickListener,
        cancel: DialogInterface.OnCancelListener
    ) {
        val dialog = Dialog(context, R.style.Dialog_AppTheme_Transparent)
        val cancelable =
            typeButton == BUTTON_TYPE_SAVE || typeButton == BUTTON_TYPE_PERMISSION || typeButton == BUTTON_TYPE_SETTINGS
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(cancelable)
        dialog.setContentView(R.layout.custom_dialog_alert)
        (dialog.findViewById(R.id.dialog_alert_tvMensaje) as TextView).text = message
        (dialog.findViewById(R.id.dialog_alert_tvTitulo) as TextView).text = title
        (dialog.findViewById(R.id.dialog_alert_tvTipoMensaje) as TextView).text =
            getSymbol(typeMessage)
        var button: Button
        //region btnAceptar_Click
        button = dialog.findViewById(R.id.dialog_alert_btnAceptar)
        button.setText(button1)
        button.setOnClickListener { v: View? ->
            if (action1 != null) action1.onClick(v)
            dialog.dismiss()
        }
        //endregion

        //region btnCancelar_Click
        //region btnCancelar_Click
        button = dialog.findViewById<View>(R.id.dialog_alert_btnCancelar) as Button
        if (action2 != null) {
            button.setText(button2)
            button.setOnClickListener { v: View? ->
                action2.onClick(v)
                dialog.dismiss()
            }
        } else {
            button.visibility = View.GONE
        }
        //endregion

        //endregion
        dialog.setOnCancelListener { dialog1: DialogInterface? ->
            if (cancel != null) cancel.onCancel(
                dialog1
            )
        }

        dialog.show()
    }

    fun ShowDialog(
        context: Context,
        message: String?,
        title: String,
        @TypeMsg typeMessage: Int,
        action1: View.OnClickListener
    ) {
        ShowDialog(context, message, title, typeMessage, BUTTON_TYPE_ACCEPT, action1, null)
    }

    fun ShowDialog(
        context: Context,
        message: String?,
        title: String,
        @TypeMsg typeMessage: Int,
        @TypeButton typeButton: Int,
        action1: View.OnClickListener,
        action2: View.OnClickListener?
    ) {
        ShowDialog(context, message, title, typeMessage, typeButton, action1, action2, null)
    }

    //endregion

    //endregion
    //region Get property
    private fun getSymbol(@TypeMsg tipoMensaje: Int): String? {
        return if (tipoMensaje == MSG_TYPE_SUCCESS) {
            MSG_SYMBOL_SUCCESS
        } else if (tipoMensaje == MSG_TYPE_WARNING) {
            MSG_SYMBOL_WARNING
        } else {
            MSG_SYMBOL_ERROR
        }
    }

    private fun getButtonName(@TypeButton tipoButton: Int): String? {
        return if (tipoButton == BUTTON_TYPE_ACCEPT) {
            BUTTON_NAME_ACCEPT
        } else if (tipoButton == BUTTON_TYPE_CONTINUE) {
            BUTTON_NAME_CONTINUE
        } else if (tipoButton == BUTTON_TYPE_RETRY) {
            BUTTON_NAME_RETRY
        } else if (tipoButton == BUTTON_TYPE_CHANGE_EMAIL) {
            BUTTON_NAME_EMAIL
        } else {
            BUTTON_NAME_SAVE
        }
    }
    //endregion

    //endregion
    fun showDialog(
        title: String?, msg: String?, positiveLabel: String?,
        positiveOnClick: DialogInterface.OnClickListener?,
        negativeLabel: String?, negativeOnClick: DialogInterface.OnClickListener?,
        isCancelAble: Boolean, context: Context?
    ): AlertDialog? {
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setTitle(title)
        builder.setCancelable(isCancelAble)
        builder.setMessage(msg)
        builder.setPositiveButton(positiveLabel, positiveOnClick)
        builder.setNegativeButton(negativeLabel, negativeOnClick)
        val alert: AlertDialog = builder.create()
        alert.show()
        return alert
    }

    /**   @SuppressLint("SetTextI18n")
    fun showRadioButtonDialog(
    _activity: Activity?,
    direccions: List<Addresses>,
    view: TextView?,
    title: String?
    ) {
    val dialog = Dialog(_activity, R.style.Dialog_AppTheme_Transparent)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.setContentView(R.layout.dialog_radiobutton_list)
    val rg: RadioGroup = dialog.findViewById(R.id.radio_group)
    val t1: TextView = dialog.findViewById(R.id.dialog_alert_tvTitulo)
    t1.text = title
    val typeface = ResourcesCompat.getFont(_activity!!, R.font.montserrat)
    for (i in direccions.indices) {
    val rb = RadioButton(_activity)
    rb.id = i
    rb.setTypeface(typeface)
    rb.setText(direccions[i].getAddress())
    rg.setOnCheckedChangeListener { group, checkedId ->
    val childCount = group.childCount
    for (x in 0 until childCount) {
    val btn = group.getChildAt(x) as RadioButton
    if (btn.id == checkedId) {
    if (view != null) {
    view.text = btn.text.toString()
    dialog.dismiss()
    }
    }
    }
    }
    rg.addView(rb)
    if (view != null && view.text.toString().toLowerCase() == rb.text.toString()
    .toLowerCase()
    ) {
    rg.check(i)
    }
    }
    dialog.show()
    }*/


    @SuppressLint("SetTextI18n")
    fun showRadioButtonDialogPaymentCondition(
        context: Context,
        paymentConditions: List<PaymentConditionsEntity>,
        view: TextView?,
        title: String?,
        viewCode: TextView
    ) {
        val dialog = Dialog(context, R.style.Dialog_AppTheme_Transparent)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_radiobutton_list)
        val rg: RadioGroup = dialog.findViewById(R.id.radio_group)
        val t1: TextView = dialog.findViewById(R.id.tvTitle)
        t1.text = title
        val typeface = ResourcesCompat.getFont(context, R.font.montserrat)
        try {

            for (i in paymentConditions.indices) {
                val rb = RadioButton(context)
                rb.id = i
                rb.typeface = typeface
                rb.text = paymentConditions[i].paymentTermsGroupName
                rg.setOnCheckedChangeListener { group: RadioGroup, checkedId: Int ->
                    val childCount = group.childCount
                    for (x in 0 until childCount) {
                        val btn = group.getChildAt(x) as RadioButton
                        if (btn.id == checkedId) {
                            if (view != null) {
                                view.text = btn.text.toString()
                                //viewCode.setText(paymentConditions[checkedId].groupNumber)
                                dialog.dismiss()
                            }
                        }
                    }
                }
                rg.addView(rb)
                if (view != null && view.text.toString().equals(rb.text.toString(), ignoreCase = true)
                ) {
                    rg.check(i)
                }
            }
        } catch (e: Exception) {
            Toaster.toastLong(e.message)
        }
        dialog.show()
    }

    /*
      @SuppressLint("SetTextI18n")
      fun getDialogTarifario(
          _activity: Activity?,
          tarifarios: List<PriceList>,
          view: TextView?,
          title: String?
      ) {
          val dialog = Dialog(_activity, R.style.Dialog_AppTheme_Transparent)
          dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
          dialog.setContentView(R.layout.dialog_radiobutton_list)
          val rg: RadioGroup = dialog.findViewById(R.id.radio_group)
          val t1: TextView = dialog.findViewById(R.id.dialog_alert_tvTitulo)
          t1.text = title
          val typeface = ResourcesCompat.getFont(_activity!!, R.font.montserrat)
          for (i in tarifarios.indices) {
              val rb = RadioButton(_activity)
              rb.id = i
              rb.setTypeface(typeface)
              rb.setText(tarifarios[i].getId().toString() + "-" + tarifarios[i].getName())
              rg.setOnCheckedChangeListener { group, checkedId ->
                  val childCount = group.childCount
                  for (x in 0 until childCount) {
                      val btn = group.getChildAt(x) as RadioButton
                      if (btn.id == checkedId) {
                          if (view != null) {
                              view.text = btn.text.toString()
                              dialog.dismiss()
                          }
                      }
                  }
              }
              rg.addView(rb)
              if (view != null && view.text.toString() == rb.text.toString()) {
                  rg.check(i)
              }
          }
          dialog.show()
      }


      @SuppressLint("SetTextI18n")
      fun getDialogAlmacen(
          _activity: Activity?,
          wareHouseList: List<WareHouse>,
          title: String?,
          tvAlmacenCode: TextView
      ) {
          val dialog = Dialog(_activity, R.style.Dialog_AppTheme_Transparent)
          dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
          dialog.setContentView(R.layout.dialog_radiobutton_list)
          val rg: RadioGroup = dialog.findViewById(R.id.radio_group)
          val t1: TextView = dialog.findViewById(R.id.dialog_alert_tvTitulo)
          t1.text = title
          val typeface = ResourcesCompat.getFont(_activity!!, R.font.montserrat)
          for (i in wareHouseList.indices) {
              val rb = RadioButton(_activity)
              rb.id = i
              rb.setTypeface(typeface)
              rb.setText(wareHouseList[i].getCode().toString() + "-" + wareHouseList[i].getName())
              rg.setOnCheckedChangeListener { group: RadioGroup, checkedId: Int ->
                  val childCount = group.childCount
                  for (x in 0 until childCount) {
                      val btn = group.getChildAt(x) as RadioButton
                      if (btn.id == checkedId) {
                          val cadena =
                              btn.text.toString().split("-").toTypedArray()
                          tvAlmacenCode.setText(
                              WareHouse.find(
                                  WareHouse::class.java,
                                  Useful.getCol("code").toString() + " = ? ",
                                  cadena[0]
                              ).get(0).getCode()
                          )
                          dialog.dismiss()
                      }
                  }
              }
              rg.addView(rb)
          }
          dialog.show()
      }


      @SuppressLint("SetTextI18n")
      fun getDialogTipoOperacion(
          _activity: Activity?,
          affectationList: List<Affectation>,
          view: TextView?,
          title: String?,
          item: ArticuloOrder
      ) {
          val dialog = Dialog(_activity, R.style.Dialog_AppTheme_Transparent)
          dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
          dialog.setContentView(R.layout.dialog_radiobutton_list)
          val rg: RadioGroup = dialog.findViewById(R.id.radio_group)
          val t1: TextView = dialog.findViewById(R.id.dialog_alert_tvTitulo)
          t1.text = title
          val typeface = ResourcesCompat.getFont(_activity!!, R.font.montserrat)
          for (i in affectationList.indices) {
              val rb = RadioButton(_activity)
              rb.id = i
              rb.setTypeface(typeface)
              rb.setText(
                  affectationList[i].getCode().toString() + "-" + affectationList[i].getDescription()
              )
              rg.setOnCheckedChangeListener { group: RadioGroup, checkedId: Int ->
                  val childCount = group.childCount
                  for (x in 0 until childCount) {
                      val btn = group.getChildAt(x) as RadioButton
                      if (btn.id == checkedId) {
                          if (view != null) {
                              val affectacionCadena =
                                  btn.text.toString().split("-").toTypedArray()
                              val codAffectation = affectacionCadena[0]
                              item.setAffectation(codAffectation)
                              view.text = btn.text.toString()
                              dialog.dismiss()
                          }
                      }
                  }
              }
              rg.addView(rb)
              if (view != null && view.text.toString() == rb.text.toString()) {
                  rg.check(i)
              }
          }
          dialog.show()
      }


      @SuppressLint("SetTextI18n")
      fun getDialogCodigoAfectacion(
          _activity: Activity?,
          affectationTypeList: List<AffectationType>,
          tvAfectacionType: TextView?,
          tvAfectacion: TextView,
          title: String?,
          item: ArticuloOrder
      ) {
          val dialog = Dialog(_activity, R.style.Dialog_AppTheme_Transparent)
          dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
          dialog.setContentView(R.layout.dialog_radiobutton_list)
          val rg: RadioGroup = dialog.findViewById(R.id.radio_group)
          val t1: TextView = dialog.findViewById(R.id.dialog_alert_tvTitulo)
          val etBuscar: EditText = dialog.findViewById(R.id.etBuscar)
          val llBuscador: LinearLayout = dialog.findViewById(R.id.llBuscador)
          llBuscador.visibility = View.VISIBLE
          t1.text = title
          val typeface = ResourcesCompat.getFont(_activity!!, R.font.montserrat)
          etBuscar.addTextChangedListener(object : TextWatcher {
              override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
              override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                  val affectationTypeListFiltered: MutableList<AffectationType> = ArrayList()
                  rg.removeAllViews()
                  for (rowFilter in affectationTypeList) {
                      if (rowFilter.getCode().contains(etBuscar.text.toString())) {
                          affectationTypeListFiltered.add(rowFilter)
                      }
                  }
                  for (i in affectationTypeListFiltered.indices) {
                      val rb = RadioButton(_activity)
                      rb.id = i
                      rb.setTypeface(typeface)
                      rb.setText(
                          affectationTypeListFiltered[i].getCode()
                              .toString() + "-" + affectationTypeListFiltered[i].getDescription()
                      )
                      rg.setOnCheckedChangeListener { group: RadioGroup, checkedId: Int ->
                          val childCount = group.childCount
                          for (x in 0 until childCount) {
                              val btn = group.getChildAt(x) as RadioButton
                              if (btn.id == checkedId) {
                                  if (tvAfectacionType != null) {
                                      val codAfectacionCadena =
                                          btn.text.toString().split("-").toTypedArray()
                                      val codAfectacion = codAfectacionCadena[0]
                                      val codOperacionCadena =
                                          tvAfectacion.text.toString().split("-").toTypedArray()
                                      val codOperacion = codOperacionCadena[0]
                                      if (codAfectacion == "40" && codOperacion != "EX") {
                                          Toaster.toastLong("El tipo de operación debe ser: EX - EXPORTACION")
                                          dialog.dismiss()
                                      } else {
                                          item.setAffectationType(codAfectacion)
                                          tvAfectacionType.text = btn.text.toString()
                                          dialog.dismiss()
                                      }
                                  }
                              }
                          }
                      }
                      rg.addView(rb)
                      if (tvAfectacionType != null && tvAfectacionType.text.toString() == rb.text.toString()) {
                          rg.check(i)
                      }
                  }
              }

              override fun afterTextChanged(s: Editable) {}
          })
          for (i in affectationTypeList.indices) {
              val rb = RadioButton(_activity)
              rb.id = i
              rb.setTypeface(typeface)
              rb.setText(
                  affectationTypeList[i].getCode()
                      .toString() + "-" + affectationTypeList[i].getDescription()
              )
              rg.setOnCheckedChangeListener { group: RadioGroup, checkedId: Int ->
                  val childCount = group.childCount
                  for (x in 0 until childCount) {
                      val btn = group.getChildAt(x) as RadioButton
                      if (btn.id == checkedId) {
                          if (tvAfectacionType != null) {
                              val codAfectacionCadena =
                                  btn.text.toString().split("-").toTypedArray()
                              val codAfectacion = codAfectacionCadena[0]
                              val codOperacionCadena =
                                  tvAfectacion.text.toString().split("-").toTypedArray()
                              val codOperacion = codOperacionCadena[0]
                              if (codAfectacion == "40" && codOperacion != "EX") {
                                  Toaster.toastLong("El tipo de operación debe ser: EX - EXPORTACION")
                                  dialog.dismiss()
                              } else {
                                  item.setAffectationType(codAfectacion)
                                  tvAfectacionType.text = btn.text.toString()
                                  dialog.dismiss()
                              }
                          }
                      }
                  }
              }
              rg.addView(rb)
              if (tvAfectacionType != null && tvAfectacionType.text.toString() == rb.text.toString()) {
                  rg.check(i)
              }
          }
          dialog.show()
      }*/

    fun mdToast(context: Context, message: String, type: Int) {
        MDToast.makeText(context, message, MDToast.LENGTH_LONG, type).show()
    }

    fun openProgressDialog(context: Context, title: String, message: String) {
        GlobalScope.launch(Dispatchers.Main) {
            dialog = ProgressDialog(context, R.style.ProgressDialog)
            dialog.setTitle(title)
            dialog.setMessage(message)
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    fun closeProgressDialog() {
        GlobalScope.launch(Dispatchers.Main) {
            dialog.dismiss()
        }
    }

    fun openProgressDialogCount(activity: Activity) {
        GlobalScope.launch(Dispatchers.Main) {
            dialogSyncProgress = Dialog(activity)
            dialogSyncProgress.requestWindowFeature(Window.FEATURE_NO_TITLE);
            bind = DialogProgressTaskBinding.inflate(activity.layoutInflater)
            dialogSyncProgress.setContentView(bind.root)
            dialogSyncProgress.setCancelable(false);
            dialogSyncProgress.show()
        }
    }


    fun onUpdateProgressDialogCountMessage(message: String) {
        GlobalScope.launch(Dispatchers.Main) {
            bind.tvMessage.text = message
        }
    }

    fun onUpdateProgressDialogCount(count: Int, index: Int) {
        GlobalScope.launch(Dispatchers.Main) {
            if (count == 0) {
                bind.cpSync.setProgress(0, false)
            } else {
                val progress = ((index * 100) / count)
                bind.cpSync.setProgress(progress, false)
            }
        }
    }

    fun closeProgressDialogCount() {
        GlobalScope.launch(Dispatchers.Main) {
            dialogSyncProgress.dismiss()
        }
    }

}