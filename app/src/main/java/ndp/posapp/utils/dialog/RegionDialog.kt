package ndp.posapp.utils.dialog

import android.app.Activity
import android.app.Dialog
import android.text.Editable
import android.text.TextWatcher
import android.view.Window
import androidx.annotation.StringRes
import androidx.lifecycle.LifecycleOwner
import ndp.posapp.R
import ndp.posapp.data.db.entity.RegionEntity
import ndp.posapp.databinding.DialogSimpleSearchObjectBinding
import ndp.posapp.utils.use.UsePhone.hideKeyboard
import ndp.posapp.utils.use.UsePhone.showKeyboard
import ndp.posapp.view.adapter.RegionAdapter
import ndp.posapp.viewModel.RegionRelationViewModel
import ndp.posapp.viewModel.RegionViewModel

class RegionDialog internal constructor(
    private val activity: Activity,
    private val regionViewModel: RegionViewModel,
    private val lifecycleOwner: LifecycleOwner,
    private val regionAdapter: RegionAdapter,
    private val regionType : String,
    private val regionEntity : RegionEntity?,
    private val regionRelationViewModel: RegionRelationViewModel,
    @StringRes private val fieldTextHint: Int
) {
    val dialog = Dialog(activity, R.style.Region_Dialog)

    fun open() {
        val binding = DialogSimpleSearchObjectBinding.inflate(activity.layoutInflater)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(binding.root)
        dialog.setCancelable(true)
        binding.searchTextInputLayout.setHint(fieldTextHint)
        binding.rvObject.adapter = regionAdapter
        filterRegions("")
        binding.etSearch.requestFocus()
        dialog.showKeyboard()

        binding.etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                filterRegions(s.toString())
            }

            override fun afterTextChanged(s: Editable?) {
            }

        })

        dialog.setOnDismissListener {
            dialog.hideKeyboard()
        }

        binding.root.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun filterRegions (search: String) {
        when(regionType) {
            "DTO" -> {
                regionViewModel.listDepartment(search,regionType).observe(lifecycleOwner, {
                    regionAdapter.setList(it)
                })
            }
            "PRO" -> {
                regionViewModel.listRelation(search,regionType,regionEntity,regionRelationViewModel).observe(lifecycleOwner, {
                    regionAdapter.setList(it)
                })
            }
            "DIS" -> {
                regionViewModel.listRelation(search,regionType,regionEntity,regionRelationViewModel).observe(lifecycleOwner, {
                    regionAdapter.setList(it)
                })
            }
        }
    }

    fun close() {
        dialog.dismiss()
    }
}