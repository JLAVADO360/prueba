package ndp.posapp.utils.dialog

import android.app.Activity
import android.app.Dialog
import android.view.Window
import android.widget.RadioButton
import androidx.core.content.res.ResourcesCompat
import ndp.posapp.R
import ndp.posapp.data.db.entity.PaymentConditionsEntity
import ndp.posapp.databinding.DialogRadiobuttonListBinding
import java.util.*

class PaymentConditionDialog internal constructor(
    private val activity: Activity,
    private val paymentConditions: List<PaymentConditionsEntity>,
    private val listener: Listener,
    private val paymentConditionSelected: String
) {


    interface Listener {
        fun onSelectedPaymentCondition(paymentConditionsEntity: PaymentConditionsEntity)
    }

    fun open() {
        val dialog = Dialog(activity, R.style.Dialog_AppTheme_Transparent)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val binding = DialogRadiobuttonListBinding.inflate(activity.layoutInflater)
        dialog.setContentView(binding.root)
        binding.tvTitle.text = activity.getString(R.string.paymentCondition)
        val typeface = ResourcesCompat.getFont(activity, R.font.montserrat)
        paymentConditions.forEach { paymentCondition ->
            val rb = RadioButton(activity)
            rb.typeface = typeface
            rb.text = paymentCondition.paymentTermsGroupName
            rb.setOnClickListener {
                listener.onSelectedPaymentCondition(paymentCondition)
                dialog.dismiss()
            }
            binding.radioGroup.addView(rb)
            if (paymentConditionSelected.lowercase(Locale.getDefault()) == rb.text.toString().lowercase(Locale.getDefault())) {
                rb.isChecked = true
            }
        }
        dialog.show()
    }

}