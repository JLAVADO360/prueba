package ndp.posapp.utils.dialog

import android.app.Activity
import android.app.Dialog
import android.view.Window
import androidx.recyclerview.widget.LinearLayoutManager
import ndp.posapp.R
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.databinding.LayoutItemSaleDialogBinding
import ndp.posapp.view.adapter.ItemSaleDialogAdapter

class ItemSaleDialog internal constructor(
    private val activity: Activity,
    private val saleOrderDetailModel: MutableList<SaleOrderDetailModel>,
    private val itemSaleDialogAdapter: ItemSaleDialogAdapter
) {
    val dialog = Dialog(activity, R.style.Dialog_AppTheme_Transparent)

    fun open() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        val binding = LayoutItemSaleDialogBinding.inflate(activity.layoutInflater)
        dialog.setContentView(binding.root)
        binding.rvItem.setHasFixedSize(true)
        binding.rvItem.adapter = itemSaleDialogAdapter
        binding.rvItem.layoutManager = LinearLayoutManager(activity)
        itemSaleDialogAdapter.setList(saleOrderDetailModel)
        dialog.show()
    }
    fun close() {
        dialog.dismiss()
    }
}