package ndp.posapp.utils.dialog

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.view.Window
import androidx.recyclerview.widget.LinearLayoutManager
import ndp.posapp.R
import ndp.posapp.data.model.SaleOrderDetailModel
import ndp.posapp.data.model.SaleOrderModel
import ndp.posapp.databinding.DetailDocumentDialogBinding
import ndp.posapp.utils.use.Mappers
import ndp.posapp.view.activity.PaymentReceivedActivity
import ndp.posapp.view.adapter.SaleOrderDetailAdapter

class SaleOrderDetailDialog internal constructor(
    private val activity: Activity,
    private val saleOrderModel: SaleOrderModel
) {
    val dialog = Dialog(activity, R.style.Dialog_AppTheme_Transparent)

    @SuppressLint("SetTextI18n")
    fun open() {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        val binding = DetailDocumentDialogBinding.inflate(activity.layoutInflater)
        dialog.setContentView(binding.root)
        val saleOrderDetailAdapter = SaleOrderDetailAdapter(activity)
        binding.tvTitle.text = "ORDEN DE VENTA ${saleOrderModel.docYear}-${saleOrderModel.docNum}"
        binding.tvBusinessPartner.text = saleOrderModel.businessPartnerName
        binding.tvBusinessPartnerCode.text = saleOrderModel.nif
        binding.rvDetail.setHasFixedSize(true)
        binding.rvDetail.adapter = saleOrderDetailAdapter
        binding.rvDetail.layoutManager = LinearLayoutManager(activity)
        saleOrderDetailAdapter.setList(saleOrderModel.detail)
        dialog.show()
    }

}