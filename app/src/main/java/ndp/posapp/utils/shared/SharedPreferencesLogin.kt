package ndp.posapp.utils.shared

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ndp.posapp.data.model.EntityDTOModel
import ndp.posapp.data.model.LoggerUserModel

class SharedPreferencesLogin {

    private val preferencesLogin = "preferencesLogin"

    private var documentNumber: String = "documentNumber"
    private var documentType: String = "documentType"
    private var email: String = "email"
    private var name: String = "name"
    private var surname: String = "surname"
    private var sex: String = "sex"
    private var roleCode: String = "roleCode"
    private var roleDescription: String = "roleDescription"
    private var roleName: String = "roleName"
    private var entities: String = "entities"
    private var entity: String = "entity"
    private var fingerPrint: String = "fingerPrint"
    private var token: String = "token"
    private var signature: String = "signature"
    private var isLogged: String = "isLogged"
    private var userIdentifier: String = "userIdentifier"
    private var store: String = "store"
    private var storeAddress: String = "storeAddress"
    private var storePriceList: String = "storePriceList"
    private var ubication: String = "ubication"
    private var ubicationEntry: String = "ubicationEntry"
    private var warehouse: String = "warehouse"

    private fun setSharedPreferencesLogin(context: Context): SharedPreferences {
        return context.getSharedPreferences(preferencesLogin, Context.MODE_PRIVATE)
    }

    fun editSharedPreferencesLogin(context: Context, loggerUserModel: LoggerUserModel) {
        val editor: SharedPreferences.Editor = setSharedPreferencesLogin(context).edit()
        editor.putString(documentNumber, loggerUserModel.documentNumber)
        editor.putString(documentType, loggerUserModel.documentType)
        editor.putString(email, loggerUserModel.email)
        editor.putString(name, loggerUserModel.name)
        editor.putString(surname, loggerUserModel.surname)
        editor.putString(sex, loggerUserModel.sex)
        editor.putString(roleCode, loggerUserModel.roleCode)
        editor.putString(roleDescription, loggerUserModel.roleDescription)
        editor.putString(roleName, loggerUserModel.roleName)
        editor.putString(entities, Gson().toJson(loggerUserModel.entities, object : TypeToken<List<EntityDTOModel>>() {}.type))
        editor.putString(entity, loggerUserModel.entity)
        editor.putString(fingerPrint, loggerUserModel.fingerPrint)
        editor.putString(token, loggerUserModel.token)
        editor.putString(signature, loggerUserModel.signature)
        editor.putBoolean(isLogged, loggerUserModel.isLogged)
        editor.putString(userIdentifier, loggerUserModel.userIdentifier)
        editor.putString(store,loggerUserModel.store)
        editor.putString(storeAddress,loggerUserModel.storeAddress)
        editor.putInt(storePriceList, loggerUserModel.storePriceList ?: -500)
        editor.putString(ubication,loggerUserModel.ubication)
        editor.putInt(ubicationEntry,loggerUserModel.ubicationEntry?:0)
        editor.putString(warehouse,loggerUserModel.warehouse)
        editor.apply()

    }

    fun getSharedPreferences(context: Context): LoggerUserModel {
        val loggerUserModel = LoggerUserModel()
        val preferences = setSharedPreferencesLogin(context)
        val priceListTem = preferences.getInt(storePriceList, -500)
        loggerUserModel.documentNumber = preferences.getString(documentNumber, "").toString()
        loggerUserModel.documentType = preferences.getString(documentType, "").toString()
        loggerUserModel.email = preferences.getString(email, "").toString()
        loggerUserModel.name = preferences.getString(name, "").toString()
        loggerUserModel.surname = preferences.getString(surname, "").toString()
        loggerUserModel.sex = preferences.getString(sex, "").toString()
        loggerUserModel.roleCode = preferences.getString(roleCode, "").toString()
        loggerUserModel.roleDescription = preferences.getString(roleDescription, "").toString()
        loggerUserModel.roleName = preferences.getString(roleName, "").toString()
        loggerUserModel.entities = Gson().fromJson(preferences.getString(entities, "[]").toString(), object : TypeToken<List<EntityDTOModel>>() {}.type)
        loggerUserModel.entity = preferences.getString(entity, "").toString()
        loggerUserModel.fingerPrint = preferences.getString(fingerPrint, "").toString()
        loggerUserModel.token = preferences.getString(token, "").toString()
        loggerUserModel.signature = preferences.getString(signature, "").toString()
        loggerUserModel.isLogged = preferences.getBoolean(isLogged, false)
        loggerUserModel.userIdentifier = preferences.getString(userIdentifier, "").toString()
        loggerUserModel.store = preferences.getString(store,"")
        loggerUserModel.storeAddress = preferences.getString(storeAddress,"")
        loggerUserModel.storePriceList = if (priceListTem != -500) priceListTem else null
        loggerUserModel.ubication = preferences.getString(ubication,"")
        loggerUserModel.ubicationEntry = preferences.getInt(ubicationEntry,0)
        loggerUserModel.warehouse = preferences.getString(warehouse,"")
        return loggerUserModel
    }

}