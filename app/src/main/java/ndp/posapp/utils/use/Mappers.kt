package ndp.posapp.utils.use

import ndp.posapp.data.model.BusinessPartnerModel
import ndp.posapp.utils.Const

object Mappers {

    private const val undefined = "Indefinido"

    fun mapOrderStatus(status: Int?): String {
        return when (status) {
            Const.SALE_ORDER_OPENED -> "Abierta"
            Const.SALE_ORDER_CLOSED -> "Cerrada"
            Const.SALE_ORDER_SAVED -> "Guardado"
            Const.SALE_ORDER_REMOVED -> "Eliminada"
            Const.SALE_ORDER_LOCKED -> "Bloqueda"
            Const.SALE_ORDER_PICKING -> "Picking"
            Const.SALE_ORDER_CANCELED -> "Cancelada"
            Const.SALE_ORDER_CHARGE -> "Cobro"
            Const.SALE_ORDER_EDITING -> "Edicion"
            Const.SALE_ORDER_PENDING_APPROVAL -> "Por Aprobar"
            Const.SALE_ORDER_APPROVED -> "Aprobada"
            Const.SALE_ORDER_REJECTED -> "Rechazada"
            else -> undefined
        }
    }

    fun mapCurrencyName(string: String?): String {
        return when (string) {
            "SOL" -> "S/."
            "USD" -> "$"
            else -> undefined
        }
    }


    fun mapSaleType(status: Int?): String {
        return when (status) {
            1 -> "VENTA RÁPIDA"
            else -> "VENTA SINÉRGICA"
        }
    }

    fun mapReceiptTypeName(code: String?): String {
        return when (code) {
            "01" -> "FACTURA"
            "03" -> "BOLETA"
            else -> undefined
        }
    }

    fun getQueryType(): List<String> {
        val list: MutableList<String> = ArrayList()
        list.add("TIPO CONSULTA")
        list.add("1-ORDEN DE VENTA")
        list.add("2-FACTURA")
        list.add("3-VISITA")
        return list
    }

    fun getPersonType(): List<String> {
        val list: MutableList<String> = ArrayList()
        list.add("PERSONA NATURAL")
        list.add("PERSONA JURIDICA")
        list.add("SIN DENOMINACIÓN")
        return list
    }

    fun getDocumentType(): List<String> {
        val list: MutableList<String> = ArrayList()
        list.add("RUC")
        list.add("DNI")
        list.add("CARNET DE EXTRANJERIA")
        list.add("PASAPORTE")
        return list
    }

    fun validateBusinessModel(businessPartnerModel: BusinessPartnerModel): Boolean {
        return false
    }

}