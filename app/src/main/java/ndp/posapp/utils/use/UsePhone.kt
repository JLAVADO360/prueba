package ndp.posapp.utils.use

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.provider.Settings
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.text.Editable
import android.text.TextWatcher
import android.view.View


object UsePhone {

    fun hideKeyBoard(activity: Activity) {
        val focus = activity.currentFocus
        if (focus != null) {
            val imm =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(focus.windowToken, 0)
        }
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    fun Dialog.showKeyboard() {
        val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun Dialog.hideKeyboard() {
        val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }

    fun showKeyboard(activity: Activity, target: EditText) {
        val inputMethodManager: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        target.requestFocus()
        target.setSelection(target.text.length)
    }
    fun getAndroidId(context : Context) : String {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    fun isOnline(): Boolean {
        return try {
            val policy = ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
            val timeoutMs = 3000
            val sock = Socket()
            sock.connect(InetSocketAddress("8.8.8.8", 53), timeoutMs)
            sock.close()
            true
        } catch (e: IOException) {
            false
        }
    }

    fun EditText.textChangedEvent(action: TextWatcher.(s: CharSequence, start: Int, before: Int, count: Int) -> Unit) {
        this.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                //Do nothing
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                action(this, s, start, before, count)
            }

            override fun afterTextChanged(p0: Editable?) {
                //Do nothing
            }
        })
    }
}