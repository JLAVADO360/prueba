package ndp.posapp.utils.use

import android.app.Activity
import android.content.Context
import java.util.regex.Matcher
import java.util.regex.Pattern
import ndp.posapp.R
import xdroid.toaster.Toaster
import java.text.DecimalFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.TimeZone
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.widget.TextView
import com.ibm.icu.text.RuleBasedNumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat

object UseApp {

    fun validateEmail(string: String): Boolean {
        val email = string.lowercase()
        val emailPattern = "[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\\.+[a-z0-9-]+"
        val pattern: Pattern = Pattern.compile(emailPattern)
        val matcher: Matcher = pattern.matcher(email)
        return matcher.matches()
    }

    fun isMobileNumber(mobileNumber: String) = Pattern.compile("[0-9]{9}").matcher(mobileNumber).matches()

    fun isRUC20(ruc20: String) = Pattern.compile("20[0-9]{9}").matcher(ruc20).matches()

    fun isRUC10(ruc10: String) = Pattern.compile("10[0-9]{9}").matcher(ruc10).matches()

    fun isDNI(dni: String) = Pattern.compile("[0-9]{8}").matcher(dni).matches()

    fun isImmigrationCard(number: String) = Pattern.compile("[0-9]{12}").matcher(number).matches()

    fun timeBack(context: Context, back_pressed: Long): Long {
        val delay = 2000
        return if (back_pressed + delay <= System.currentTimeMillis()) {
            Toaster.toast(context.getString(R.string.press_again_to_exit))
            System.currentTimeMillis()
        } else {
            -1
        }
    }


    /**DATE**/

    fun datePick(activity: Activity, textView: TextView, calendar: Calendar, limitToday: Boolean, callback: (String) -> Unit) {
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        val mMonth = calendar.get(Calendar.MONTH)
        val mYear = calendar.get(Calendar.YEAR)

        val datePickerDialog = DatePickerDialog(
            activity, R.style.DialogTheme, { _, year, monthOfYear, dayOfMonth ->
                calendar.set(year, monthOfYear, dayOfMonth)
                val date = "${getDigits(year)}-${getDigits(monthOfYear + 1)}-${getDigits(dayOfMonth)}"
                textView.text = date
                callback(date)

            }, mYear, mMonth, mDay
        )

        if (limitToday) datePickerDialog.datePicker.maxDate = Calendar.getInstance().timeInMillis
        datePickerDialog.setTitle("")
        datePickerDialog.show()
    }

    fun timePick(context: Context, calendar: Calendar, callback: (time12h: String, time24h: String) -> Unit) {
        val clockPickerDialog = TimePickerDialog(
            context,
            R.style.DialogTheme,
            { _, _hour, _minute ->
                calendar.set(Calendar.HOUR_OF_DAY, _hour)
                calendar.set(Calendar.MINUTE, _minute)
                val hour = if (calendar.get(Calendar.HOUR) == 0) 12 else calendar.get(Calendar.HOUR)
                val minute = if (_minute < 10) "0$_minute" else "$_minute"
                val amPm = if (calendar.get(Calendar.AM_PM) == Calendar.AM) "a. m." else "p. m."
                callback("$hour:$minute $amPm", "$_hour:$minute")
            },
            calendar.get(Calendar.HOUR_OF_DAY),
            calendar.get(Calendar.MINUTE),
            false
        )

        clockPickerDialog.setTitle("")
        clockPickerDialog.show()
    }

    private fun getDigits(value: Int): String {
        val s1 = value.toString()
        return if (s1.length < 2) "0$value" else s1
    }

    fun getDate(format: CharSequence): String {
        return android.text.format.DateFormat.format(format, Date()).toString()
    }

    fun formatDateString(dateString: String?, currentFormat: String, resultFormat: String): String? {
        if (dateString != null) {
            try {
                val date = SimpleDateFormat(currentFormat, Locale.getDefault()).parse(dateString)
                return android.text.format.DateFormat.format(resultFormat, date).toString()
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            return ""
        } else {
            return null
        }
    }

    fun localDateFromServer(date: String?, dateFormat: String = "dd/MM/yyyy h:mm a"): String {
        return try {
            if (date != null) {
                val parser = SimpleDateFormat("yyy-MM-dd HH:mm:ss", Locale.getDefault())
                val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
                parser.timeZone = TimeZone.getTimeZone("UTC")
                formatter.timeZone = TimeZone.getDefault()
                formatter.format(parser.parse(date)!!)
            } else {
                ""
            }
        } catch (e: Exception) {
            ""
        }
    }

    /**NUMBERS**/

    fun doubleToString(number: Double?, decimal: Int): String {
        var pattern = ""
        when (decimal) {
            0 -> pattern = "###,###,##0"
            1 -> pattern = "###,###,##0.0"
            2 -> pattern = "###,###,##0.00"
            3 -> pattern = "###,###,##0.000"
        }
        val form = DecimalFormat(pattern)
        return form.format(number)
    }

    fun stringToDouble(string: String, decimal: Int): Double {
        return if (string.isEmpty() || string == ".") 0.00 else roundDecimal(string.replace(",", "").toDouble(), decimal)
    }

    fun roundDecimal(number: Double?, decimal: Int): Double {
        var pattern = ""
        when (decimal) {
            0 -> pattern = "###"
            1 -> pattern = "###.#"
            2 -> pattern = "###.##"
            3 -> pattern = "###.###"
        }
        val form = DecimalFormat(pattern)

        return form.format(number).toDouble()
    }

    fun numberToWords(value: Double?): String? {
        if (value == null) return null
        val local = Locale("es", "PE")
        val ruleBasedNumberFormat = RuleBasedNumberFormat(local, RuleBasedNumberFormat.SPELLOUT)
        return ruleBasedNumberFormat.format(value.toInt())
    }
}