package ndp.posapp.utils.interaction

import android.R
import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Spinner
import ndp.posapp.data.db.entityView.DateSystem
import ndp.posapp.data.model.DenominationsCashRegisterSaveModel
import java.util.*
import kotlin.collections.ArrayList

fun getDate(): DateSystem {
        val cal = Calendar.getInstance()
        val d = cal.get(Calendar.DAY_OF_MONTH)
        val m = cal.get(Calendar.MONTH) + 1
        val y = cal.get(Calendar.YEAR)

        var dd: String = d.toString()
        var mm: String = m.toString()
        var yy: String = y.toString()

        if (d < 10) {
            dd = "0${d.toString()}"
        }
        if (m < 10) {
            mm = "0${m.toString()}"
        }

        return DateSystem(yy, mm, dd)

    }

    fun getCompleteDate(year:Int,month:Int,day:Int): String {

        var dd: String = day.toString()
        var mm: String = month.toString()
        var yy: String = year.toString()

        if (day< 10) {
            dd = "0${day.toString()}"
        }
        if (month < 10) {
            mm = "0${month.toString()}"
        }

        return "   $yy-$mm-$dd"

    }

    fun getDateNow(): String? {
        val cal   = Calendar.getInstance()
        val d     = cal.get(Calendar.DAY_OF_MONTH)
        val m     = cal.get(Calendar.MONTH) + 1
        val y     = cal.get(Calendar.YEAR)
        val h     = cal.get(Calendar.HOUR_OF_DAY)
        val mi    = cal.get(Calendar.MINUTE)
        val s     = cal.get(Calendar.SECOND)
        return "${checkDigito(d)}-${checkDigito(m)}-${checkDigito(d)} ${checkDigito(h)}:${checkDigito(mi)}:${checkDigito(s)}"
    }

    fun checkDigito(number: Int): String? {
        return if (number <= 9) "0$number" else number.toString()
    }

    fun getTwoDigitsDecimal(amount:Double):Double{
        val str = amount.toString()
        val intNumber    = str.substring(0, str.indexOf('.')).toInt()//350
        val decNumberInt = str.substring(str.indexOf('.') + 1).toInt()//0.20
        val numberTwoDigits = decNumberInt.toString().substring(0,4)

        return numberTwoDigits.toDouble()
    }

    fun getConvertEmpty(amount:String):Float{
        var number:Float = 0f
        if(amount!=""){
            number = amount.toFloat()
        }
        return number
    }

    fun loadSpinner(data: ArrayList<String>?, spinner: Spinner, context: Context?) {
        val adapter = ArrayAdapter(context!!, R.layout.simple_spinner_item, data!!)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter
    }

fun main() {
    System.out.println("x:"+ getConvertEmpty("0.30"))
}
