package ndp.posapp.utils.sync

import android.app.Activity
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ndp.posapp.R
import ndp.posapp.data.db.entity.PaymentReceiptEntity
import ndp.posapp.data.db.entity.SaleOrderEntity
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.data.model.PaymentModel
import ndp.posapp.data.model.SaleOrderModel
import ndp.posapp.data.network.response.ApiResponse
import ndp.posapp.data.util.mapper.toModel
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.viewModel.CashRegisterViewModel
import ndp.posapp.viewModel.PaymentReceiptViewModel
import ndp.posapp.viewModel.CorrelativeViewModel
import ndp.posapp.viewModel.PaymentViewModel
import ndp.posapp.viewModel.SaleOrderViewModel

class SyncSaleOrder(
    private val activity: Activity,
    private val storeOwner: ViewModelStoreOwner,
    private val owner: LifecycleOwner,
    private val loggerUserModel: LoggerUserModel,
    private val showProgress: Boolean
) {

    /** VIEW MODEL **/
    private var saleOrderViewModel: SaleOrderViewModel = ViewModelProvider(storeOwner).get(SaleOrderViewModel::class.java)
    private var paymentReceiptViewModel: PaymentReceiptViewModel = ViewModelProvider(storeOwner).get(PaymentReceiptViewModel::class.java)
    private var paymentViewModel: PaymentViewModel = ViewModelProvider(storeOwner).get(PaymentViewModel::class.java)
    private lateinit var cashRegisterViewModel: CashRegisterViewModel
    private lateinit var correlativeViewModel: CorrelativeViewModel

    private var saleOrderList: List<SaleOrderEntity> = ArrayList()
    private var paymentReceiptList: List<PaymentReceiptEntity> = ArrayList()
    private var indexSaleOrder = 0
    private var indexPayment = 0
    private lateinit var currentSaleOrderEntity: SaleOrderEntity
    private lateinit var currentPaymentReceiptEntity: PaymentReceiptEntity
    private val tempEntity = loggerUserModel.entity


    fun sync() {
        val liveData = saleOrderViewModel.list(Const.SaleOrderInternalStatus.SAVED_LOCAL)
        liveData.observe(owner) {
            liveData.removeObservers(owner)
            if (it?.isNotEmpty() == true) {
                if (showProgress) {
                    PopUp.openProgressDialog(activity, activity.getString(R.string.operation_running), activity.getString(R.string.sync_sale_order))
                }

                saleOrderList = it
                syncNextSaleOrder()
            } else {
                syncPayment()
            }
        }
    }

    private fun syncNextSaleOrder() {
        if (indexSaleOrder < saleOrderList.size) {
            currentSaleOrderEntity = saleOrderList[indexSaleOrder]
            loggerUserModel.entity = currentSaleOrderEntity.company
            saleOrderViewModel.postQuickSale(loggerUserModel, currentSaleOrderEntity.toModel(), ::onSaleOrderSaved)
            indexSaleOrder++
        } else {
            if (showProgress) PopUp.closeProgressDialog()
            if (saleOrderList.isNotEmpty()) syncCorrelative()
            syncPayment()
        }
    }

    private fun onSaleOrderSaved(apiResponse: ApiResponse<SaleOrderModel>) {
        if (apiResponse.successful) {
            currentSaleOrderEntity.code = apiResponse.data?.code
            currentSaleOrderEntity.paymentReceiptNumber = apiResponse.data?.paymentReceiptNumber
            if (currentSaleOrderEntity.internalReceiptNumber == currentSaleOrderEntity.paymentReceiptNumber) {
                currentSaleOrderEntity.internalStatus = Const.SaleOrderInternalStatus.SAVED_SERVER
            } else {
                currentSaleOrderEntity.internalStatus = Const.SaleOrderInternalStatus.SAVED_SERVER_CHANGE_CORRELATIVE
            }
            saleOrderViewModel.update(currentSaleOrderEntity)

            val liveData = paymentReceiptViewModel.list(currentSaleOrderEntity.internalId!!)
            liveData.observe(owner) {
                liveData.removeObservers(owner)
                if (it?.isNotEmpty() == true) {
                    val paymentReceiptEntity = it.first()
                    paymentReceiptEntity.code = currentSaleOrderEntity.code
                    paymentReceiptViewModel.update(paymentReceiptEntity)
                }
                syncNextSaleOrder()
            }
        } else {
            syncNextSaleOrder()
        }
    }


    private fun syncPayment() {
        val liveData = paymentReceiptViewModel.listPendingSend()
        liveData.observe(owner) {
            liveData.removeObservers(owner)
            if (it?.isNotEmpty() == true) {
                if (showProgress) {
                    PopUp.openProgressDialog(activity, activity.getString(R.string.operation_running), activity.getString(R.string.sync_payment_receipt))
                }

                paymentReceiptList = it
                syncNextPayment()
            }
        }
    }

    private fun syncNextPayment() {
        if (indexPayment < paymentReceiptList.size) {
            currentPaymentReceiptEntity = paymentReceiptList[indexPayment]
            loggerUserModel.entity = currentPaymentReceiptEntity.company
            val paymentModel: PaymentModel = Gson().fromJson(currentPaymentReceiptEntity.payment, object : TypeToken<PaymentModel>() {}.type)
            paymentModel.paymentReceipt = currentPaymentReceiptEntity.code
            currentPaymentReceiptEntity.payment = Gson().toJson(paymentModel, object : TypeToken<PaymentModel>() {}.type)
            paymentViewModel.postQuickPayment(loggerUserModel, paymentModel, ::onPaymentSaved)
            indexPayment++
        } else if (showProgress) {
            PopUp.closeProgressDialog()
        }
    }

    private fun onPaymentSaved(apiResponse: ApiResponse<PaymentModel>) {
        if (apiResponse.successful) {
            currentPaymentReceiptEntity.internalStatus = Const.PaymentReceiptInternalStatus.SAVED_SERVER
            currentPaymentReceiptEntity.status = Const.PAYMENT_RECEIPT_PAID
            paymentReceiptViewModel.update(currentPaymentReceiptEntity)
        }
        syncNextPayment()
    }


    private fun syncCorrelative () {
        cashRegisterViewModel = ViewModelProvider(storeOwner).get(CashRegisterViewModel::class.java)
        correlativeViewModel = ViewModelProvider(storeOwner).get(CorrelativeViewModel::class.java)

        val liveData = cashRegisterViewModel.get(tempEntity, loggerUserModel.userIdentifier, Const.CASH_REGISTER_TYPE_DEFAULT)
        liveData.observe(owner) {
            liveData.removeObservers(owner)
            if (it != null) {
                loggerUserModel.entity = tempEntity
                correlativeViewModel.listCorrelative(loggerUserModel, it.code, false)
            }
        }
    }
}