package ndp.posapp.utils.sync

import android.app.Activity
import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelStoreOwner
import ndp.posapp.R
import ndp.posapp.data.model.LoggerUserModel
import ndp.posapp.utils.Const
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.EventObserver
import ndp.posapp.viewModel.*

class SyncData(
    private val activity: Activity,
    storeOwner: ViewModelStoreOwner,
    private val owner: LifecycleOwner,
    private val loggerUserModel: LoggerUserModel,
) {

    /** VIEW MODEL **/
    private var repositorySyncHistoryViewModel: RepositorySyncHistoryViewModel = ViewModelProvider(storeOwner).get(RepositorySyncHistoryViewModel::class.java)
    private var parameterViewModel:             ParameterViewModel = ViewModelProvider(storeOwner).get(ParameterViewModel::class.java)
    private var visitReasonViewModel:           VisitReasonViewModel = ViewModelProvider(storeOwner).get(VisitReasonViewModel::class.java)
    private var taxViewModel:                   TaxViewModel = ViewModelProvider(storeOwner).get(TaxViewModel::class.java)
    private var currencyViewModel:              CurrencyViewModel = ViewModelProvider(storeOwner).get(CurrencyViewModel::class.java)
    private var paymentConditionsViewModel:     PaymentConditionsViewModel = ViewModelProvider(storeOwner).get(PaymentConditionsViewModel::class.java)
    private var warehouseViewModel:             WarehouseViewModel = ViewModelProvider(storeOwner).get(WarehouseViewModel::class.java)
    private var ubicationViewModel:             UbicationViewModel = ViewModelProvider(storeOwner).get(UbicationViewModel::class.java)
    private var itemViewModel:                  ItemViewModel = ViewModelProvider(storeOwner).get(ItemViewModel::class.java)
    private var whsKardexViewModel:             WhsKardexViewModel = ViewModelProvider(storeOwner).get(WhsKardexViewModel::class.java)
    private var kardexViewModel:                KardexViewModel = ViewModelProvider(storeOwner).get(KardexViewModel::class.java)
    private var priceListDetailViewModel:       PriceListDetailViewModel = ViewModelProvider(storeOwner).get(PriceListDetailViewModel::class.java)
    private var priceListViewModel:             PriceListViewModel = ViewModelProvider(storeOwner).get(PriceListViewModel::class.java)
    private var businessPartnerViewModel:       BusinessPartnerViewModel = ViewModelProvider(storeOwner).get(BusinessPartnerViewModel::class.java)
    private var businessPartnerAddressViewModel: BusinessPartnerAddressViewModel = ViewModelProvider(storeOwner).get(BusinessPartnerAddressViewModel::class.java)
    private var regionViewModel:                 RegionViewModel = ViewModelProvider(storeOwner).get(RegionViewModel::class.java)
    private var regionRelationViewModel:        RegionRelationViewModel = ViewModelProvider(storeOwner).get(RegionRelationViewModel::class.java)
    private var receiptTypeViewModel:           ReceiptTypeViewModel = ViewModelProvider(storeOwner).get(ReceiptTypeViewModel::class.java)


    fun sync(syncType: Int, onFinish: (ok: Boolean, message: String?) -> Unit) {

        val liveDataParameter = parameterViewModel.result
        val liveDataVisitReason = visitReasonViewModel.result
        val liveDataTax = taxViewModel.result
        val liveDataCurrency = currencyViewModel.result
        val liveDataPaymentConditions = paymentConditionsViewModel.result
        val liveDataWarehouse = warehouseViewModel.result
        val liveDataItem = itemViewModel.result
        val liveDataPriceList = priceListViewModel.result
        val liveDataBusinessPartner = businessPartnerViewModel.result
        val liveDataRegion =     regionViewModel.result
        val liveDataRegionRelation = regionRelationViewModel.result
        val liveDataRepositoryHistory = repositorySyncHistoryViewModel.result
        val liveDataReceiptType = receiptTypeViewModel.result


        PopUp.openProgressDialogCount(activity)
        PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_parameter_list))
        parameterViewModel.listParameter(loggerUserModel, 0, syncType)
        liveDataParameter.observe(owner, EventObserver { parameter ->
            liveDataParameter.removeObservers(owner)
            if (parameter.successful) {
                PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_visit_reason_list))
                visitReasonViewModel.listVisitReason(loggerUserModel, 0, syncType)
                liveDataVisitReason.observe(owner, EventObserver { visitReason ->
                    liveDataVisitReason.removeObservers(owner)
                    if (visitReason.successful) {
                        PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_tax))
                        taxViewModel.listTax(loggerUserModel, 0, syncType)
                        liveDataTax.observe(owner, EventObserver { tax ->
                            liveDataTax.removeObservers(owner)
                            if (tax.successful) {
                                PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_currency))
                                currencyViewModel.listCurrency(loggerUserModel, 0, syncType)
                                liveDataCurrency.observe(owner, EventObserver { currency ->
                                    liveDataCurrency.removeObservers(owner)
                                    if (currency.successful) {
                                        PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_payment_condition))
                                        paymentConditionsViewModel.listPaymentConditions(loggerUserModel, 0, syncType)
                                        liveDataPaymentConditions.observe(owner, EventObserver { paymentCondition ->
                                            liveDataPaymentConditions.removeObservers(owner)
                                            if (paymentCondition.successful) {
                                                PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_warehouse))
                                                warehouseViewModel.listWarehouse(loggerUserModel, 0, syncType, ubicationViewModel)
                                                liveDataWarehouse.observe(owner, EventObserver { warehouse ->
                                                    liveDataWarehouse.removeObservers(owner)
                                                    if (warehouse.successful) {
                                                        PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_item))
                                                        itemViewModel.listItem(loggerUserModel, 0, syncType, whsKardexViewModel, kardexViewModel, priceListDetailViewModel)
                                                        liveDataItem.observe(owner, EventObserver { item ->
                                                            liveDataItem.removeObservers(owner)
                                                            if (item.successful) {
                                                                PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_price_list))
                                                                priceListViewModel.listPriceList(loggerUserModel, 0, syncType)
                                                                liveDataPriceList.observe(owner, EventObserver { priceList ->
                                                                    liveDataPriceList.removeObservers(owner)
                                                                    if (priceList.successful) {
                                                                        PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_business_partner))
                                                                        businessPartnerViewModel.listBusinessPartner(
                                                                            loggerUserModel,
                                                                            0,
                                                                            syncType,
                                                                            businessPartnerAddressViewModel
                                                                        )
                                                                        liveDataBusinessPartner.observe(owner, EventObserver { businessPartner ->
                                                                            liveDataBusinessPartner.removeObservers(owner)
                                                                            if (businessPartner.successful) {
                                                                                PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_receipt_type))
                                                                                receiptTypeViewModel.listReceiptType(loggerUserModel)
                                                                                liveDataReceiptType.observe(owner, EventObserver { receiptType ->
                                                                                    liveDataReceiptType.removeObservers(owner)
                                                                                    if (receiptType.successful) {
                                                                                        PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_region))
                                                                                        regionViewModel.listRegion(loggerUserModel, 0, syncType)
                                                                                        liveDataRegion.observe(owner, EventObserver { region ->
                                                                                            liveDataRegion.removeObservers(owner)
                                                                                            if (region.successful) {
                                                                                                PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_region_relation))
                                                                                                regionRelationViewModel.listRegionRelation(loggerUserModel, 0, syncType)
                                                                                                liveDataRegionRelation.observe(owner, EventObserver { regionRelation ->
                                                                                                    liveDataRegionRelation.removeObservers(owner)
                                                                                                    if (regionRelation.successful) {
                                                                                                        PopUp.onUpdateProgressDialogCountMessage(activity.getString(R.string.sync_finishing))
                                                                                                        repositorySyncHistoryViewModel.syncHistory(loggerUserModel, null)
                                                                                                        liveDataRepositoryHistory.observe(owner, EventObserver { syncHistory ->
                                                                                                            liveDataRepositoryHistory.removeObservers(owner)
                                                                                                            if (syncHistory.successful) {
                                                                                                                PopUp.closeProgressDialogCount()
                                                                                                                syncBag()
                                                                                                                onFinish(true, "")
                                                                                                            } else {
                                                                                                                PopUp.closeProgressDialogCount()
                                                                                                                onFinish(false, syncHistory.message)
                                                                                                            }
                                                                                                        })
                                                                                                    } else {
                                                                                                        PopUp.closeProgressDialogCount()
                                                                                                        onFinish(false, regionRelation.message)
                                                                                                    }
                                                                                                })

                                                                                            } else {
                                                                                                PopUp.closeProgressDialogCount()
                                                                                                onFinish(false, region.message)
                                                                                            }
                                                                                        })
                                                                                    } else {
                                                                                        PopUp.closeProgressDialogCount()
                                                                                        onFinish(false, receiptType.message)
                                                                                    }
                                                                                })
                                                                            } else {
                                                                                PopUp.closeProgressDialogCount()
                                                                                onFinish(false, businessPartner.message)
                                                                            }
                                                                        })
                                                                    } else {
                                                                        PopUp.closeProgressDialogCount()
                                                                        onFinish(false, priceList.message)
                                                                    }
                                                                })
                                                            } else {
                                                                PopUp.closeProgressDialogCount()
                                                                onFinish(false, item.message)
                                                            }
                                                        })
                                                    } else {
                                                        PopUp.closeProgressDialogCount()
                                                        onFinish(false, warehouse.message)
                                                    }
                                                })
                                            } else {
                                                PopUp.closeProgressDialogCount()
                                                onFinish(false, paymentCondition.message)
                                            }
                                        })
                                    } else {
                                        PopUp.closeProgressDialogCount()
                                        onFinish(false, currency.message)
                                    }
                                })
                            } else {
                                PopUp.closeProgressDialogCount()
                                onFinish(false, tax.message)
                            }
                        })
                    } else {
                        PopUp.closeProgressDialogCount()
                        onFinish(false, visitReason.message)
                    }
                })
            } else {
                PopUp.closeProgressDialogCount()
                onFinish(false, parameter.message)
            }
        })
    }

    private fun syncBag() {
        parameterViewModel.get(loggerUserModel.entity, Const.Parameter.BAG_NAME).observe(owner, { param ->
            param.value?.let {
                itemViewModel.listItemFilter(loggerUserModel, whsKardexViewModel, kardexViewModel, priceListDetailViewModel, it)
            }
        })

        parameterViewModel.get(loggerUserModel.entity, Const.Parameter.TAX_BAG_NAME).observe(owner, { param ->
            param.value?.let {
                itemViewModel.listItemFilter(loggerUserModel, whsKardexViewModel, kardexViewModel, priceListDetailViewModel, it)
            }
        })
    }
    
}