package ndp.posapp.utils

object Constants {

    const val PAY_METHOD_1 = "SOL"
    const val PAY_METHOD_2 = "EUR"
    const val PAY_METHOD_3 = "USD"
    const val PAY_METHOD_4 = "VISA"
    const val PAY_METHOD_5 = "MAST"
    const val PAY_METHOD_6 = "BCP1"
    const val PAY_METHOD_7 = "BCP3"
    const val PAY_METHOD_8 = "BBVA1"
    const val PAY_METHOD_9 = "INTERBK1"
    const val PAY_METHOD_10 = "BCP2"
    const val PAY_METHOD_11 = "BBVA2"
    const val PAY_METHOD_12 = "INTERBK2"
    const val PAY_METHOD_13 = "BANBIF1"
    const val PAY_METHOD_14 = "BANBIF2"
    const val PAY_METHOD_15 = "NTC"

}