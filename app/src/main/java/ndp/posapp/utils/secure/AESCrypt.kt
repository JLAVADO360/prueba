package ndp.posapp.utils.secure

import android.annotation.SuppressLint
import android.util.Base64;
import java.lang.Exception
import java.lang.RuntimeException
import java.lang.StringBuilder
import java.nio.charset.StandardCharsets
import java.security.Key
import java.security.MessageDigest
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec


class AESCrypt {

    private val ALGORITHM = "AES"
    private val KEY: String = "H!eW>@_'h4$7F?*S"

    @Throws(Exception::class)
    fun encrypt(value: String): String {
        val key = generateKey()
        @SuppressLint("GetInstance") val cipher = Cipher.getInstance(ALGORITHM)
        cipher.init(Cipher.ENCRYPT_MODE, key)
        val encryptedByteValue = cipher.doFinal(value.toByteArray(StandardCharsets.UTF_8))
        return Base64.encodeToString(encryptedByteValue, Base64.DEFAULT)
    }

    @Throws(Exception::class)
    fun decrypt(value: String?): String {
        val key = generateKey()
        @SuppressLint("GetInstance") val cipher = Cipher.getInstance(ALGORITHM)
        cipher.init(Cipher.DECRYPT_MODE, key)
        val decryptedValue64 = Base64.decode(value, Base64.DEFAULT)
        val decryptedByteValue = cipher.doFinal(decryptedValue64)
        return String(decryptedByteValue, StandardCharsets.UTF_8)
    }

    @Throws(Exception::class)
    private fun generateKey(): Key {
        return SecretKeySpec(KEY.toByteArray(), ALGORITHM)
    }

    fun generateHash(message: String, payload: String): String {
        val strBuilder = StringBuilder()
        for (i in payload.length - 1 downTo 0) {
            strBuilder.append(payload[i])
        }
        return sha256(message + "::" + sha256(strBuilder.toString()))
    }

    private fun sha256(base: String): String {
        return try {
            val digest: MessageDigest = MessageDigest.getInstance("SHA-256")
            val hash: ByteArray = digest.digest(base.toByteArray(charset("UTF-8")))
            val hexString = StringBuffer()
            for (b in hash) {
                val hex = Integer.toHexString(0xff and b.toInt())
                if (hex.length == 1) hexString.append('0')
                hexString.append(hex)
            }
            hexString.toString()
        } catch (ex: Exception) {
            throw RuntimeException(ex)
        }
    }
}