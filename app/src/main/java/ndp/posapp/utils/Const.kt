package ndp.posapp.utils

object Const {

    /** APPLICATION */
    const val APPLICATION_POS_APP = "app-posSystem-allowed"
    const val APPLICATION_TAKE_ORDER = "app-takeorderSystem-allowed"
    const val APPLICATION_CHANNEL= "TKO";

    /** PERMISSION */

    const val HOME_NAV_OPTIONS_OBJECT_PERMISSION = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed"

    const val HOME_FRAGMENT_OPTIONS_OBJECT_PERMISSION = "app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed"


    const val NAV_VISIT = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-navVisitas-shown"
    const val NAV_PENDING = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-navPendientes-shown"
    const val NAV_SALEORDER = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-navPedidos-shown"
    const val NAV_HOME = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-navInicio-shown"
    const val NAV_QUERY = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-navConsultas-shown"
    const val NAV_BUSINESSPARTNER = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-navClientes-shown"
    const val NAV_ITEM = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-navArticulos-shown"
    const val NAV_PROFILE = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-ivNavSettings-shown"
    const val NAV_EXIT = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-ivNavExit-shown"
    const val NAV_APPROVAL = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-ivNavAprob-shown"
    //GG
    const val NAV_CASHIER = "app-takeorderSystem-allowed_mn-home-shown_frm-navView-allowed_btn-navCash-shown"
    //GG


    const val CV_ORDER =  "app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardPedidos-shown"
    const val CV_VISIT ="app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardVisitas-shown"
    const val CV_BUSINESSPARTNER ="app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardCustomers-shown"
    const val CV_ITEM ="app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardItems-shown"
    const val CV_QUERY ="app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardConsultas-shown"
    const val CV_PENDING ="app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardPendientes-shown"
    const val CV_PROFILE ="app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardPerfil-shown"
    const val CV_EXIT ="app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardClose-shown"
    const val CV_APPROVAL ="app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardAprob-shown"

    //GG
    const val CV_CASHIER = "app-takeorderSystem-allowed_mn-home-shown_frm-fragmentHome-allowed_btn-cardCash-shown"
    //GG


    const val DEFAULT_ITEM_PER_PAGE = 150


    const val PARAMETER_UPDATE = "ERROR_PARAMETER_NOT_FOUND"
    const val ERROR_CREDIT_NOTE_NOT_FOUND = "ERROR_CREDITNOTE_NOT_FOUND"
    const val ERROR_STORE_USER_NOT_FOUND = "ERROR_STORE_USER_NOT_FOUND"
    const val ERROR_SESSION_EXPIRED = "ERROR_SESSION_EXPIRED"


    /** INTENT **/
    const val INTENT_BUSINESS_PARTNER = 10
    const val INTENT_BUSINESS_PARTNER_TAG = "businessPartner"
    const val INTENT_SALE_ORDER = 11


    /**DATE **/
    const val DATE_QUERY = "yyyy-MM-dd"
    const val DATE_TIME_QUERY = "yyyy-MM-dd HH:mm:ss"
    const val DATE_12 = "dd/MM/yyyy h:mm a"
    const val DATE_24 = "dd/MM/yyyy HH:mm:ss"
    const val TIME_STAMP = "yyyyMMddHHmmss"
    const val DATE_APP = "dd/MM/yyyy"


    const val SALE_ORDER_OPENED = 0
    const val SALE_ORDER_CLOSED = 1
    const val SALE_ORDER_SAVED = 2
    const val SALE_ORDER_REMOVED = 3
    const val SALE_ORDER_LOCKED = 4
    const val SALE_ORDER_PICKING = 5
    const val SALE_ORDER_CANCELED = 6
    const val SALE_ORDER_CHARGE = 7
    const val SALE_ORDER_EDITING = 8
    const val SALE_ORDER_PENDING_APPROVAL = 9
    const val SALE_ORDER_APPROVED = 10
    const val SALE_ORDER_REJECTED = 11



    const val SALE_ORDER = "ORDEN DE VENTA"
    const val RECEIPT = "FACTURA"
    const val VISIT = "VISITA"

    const val BOLETA_CODE = "BOL"
    const val FACTURA_CODE = "FAC"
    const val NOTA_CREDITO_CODE = "NTC"
    const val PAYMENT_CONDITION_DEFAULT = "Contado"
    const val CASH_REGISTER_TYPE_DEFAULT = "C"
    const val INVENTORY_ITEM_YES = "1"
    const val INVENTORY_ITEM_NO = "0"
    const val SALE_TYPE_SALE_ORDER = 0
    const val SALE_TYPE_QUICK_SALE = 1
    const val DETRACTION_YES = 1
    const val DETRACTION_NO = 0
    const val SUNAT_OPERATION = "01"
    const val SUNAT_OPERATION_TYPE = "A"
    const val TAX_CODE_DEFAULT = "I18"
    const val ONLY_TAX_YES = "1"
    const val ONLY_TAX_NO = "0"
    const val UNIT_GROUP_ENTRY_DEFAULT = -1
    const val SUNAT_ONEROSO = "1"
    const val SUNAT_NO_ONEROSO = "2"
    const val SUNAT_AFFECTATION_TYPE_GRAVADO_ONEROSO = "10"
    const val SUNAT_AFFECTATION_TYPE_GRAVADO_IMPUESTO = "13"
    const val SUNAT_AFFECTATION_TYPE_GRAVADO_BONIFICACION = "15"
    const val ITEM_TYPE_PRODUCT = "P"
    const val ITEM_TYPE_SERVICE = "S"
    const val DETRACTION_AFEDET= "D"
    const val SALE_TOTAL_FOR_DETRACTION = 400.0
    const val PAYMENT_ORDERED = "tNO" // TODO xq?

    const val DNI = "DNI"
    const val RUC = "RUC"
    const val CARNET_EXTRANJERIA = "CAE"
    const val NATURAL_PERSON = 1
    const val JURIDICAL_PERSON = 6
    const val WITHOUT_DENOMINATION = 3
    const val DEFAULT_PRICE_LIST_NEW_PARTNER = "5"

    const val RESERVATION_INVOICE_YES = "1"
    const val RESERVATION_INVOICE_NO = "0"
    const val PARAMETER_EMAIL_DEFAULT_CUSTOMER = "EMAIL_DEFAULT_CUSTOMER"

    const val PAYMENT_RECEIPT_EMITTED = 0
    const val PAYMENT_RECEIPT_PAID = 1

    object PaymentWayCode {
        const val EFECTIVO = "EFE"
        const val TARJETA = "TAR"
        const val DEPOSITO = "DEPO"
        const val NOTA_DE_CREDITO = "NTC"
    }

    object CurrencyCode {
        const val SOL = "SOL"
        const val USD = "USD"
        const val EUR = "EUR"
    }

    object Parameter {
        const val BAG_NAME = "BAG001"
        const val TAX_BAG_NAME = "IBAG"
        const val USE_TAX_BAG = "USE_TAX_BAG"
    }

    object SaleOrderInternalStatus {
        const val SAVED_LOCAL = 1
        const val SAVED_SERVER = 2
        const val SAVED_SERVER_CHANGE_CORRELATIVE = 3
    }

    object PaymentReceiptInternalStatus {
        const val SAVED_LOCAL = 1
        const val SAVED_SERVER = 2
    }

    const val CASH_REGISTER_DEFAULT = "CAREG009"

    const val NUMBER_OF_REGIONS = 0
    const val NUMBER_OF_REGION_RELATIONS = 0
}