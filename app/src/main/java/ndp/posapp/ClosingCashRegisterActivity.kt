package ndp.posapp

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import ndp.posapp.data.model.*
import ndp.posapp.databinding.ActivityClosingCashRegisterBinding
import ndp.posapp.utils.Const
import ndp.posapp.utils.event.EventObserver
import ndp.posapp.utils.interaction.*
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.view.box.ReasonImbalanceFragment
import xdroid.toaster.Toaster
import kotlin.collections.ArrayList
import ndp.posapp.utils.Constants
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.use.UsePhone
import ndp.posapp.viewModel.*

class ClosingCashRegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityClosingCashRegisterBinding

    private lateinit var cashRegisterViewModel:        CashRegisterViewModel
    private lateinit var closingCashRegisterViewModel: ClosingCashRegisterViewModel
    private lateinit var reasonImbalanceViewModel:     ReasonImbalanceViewModel
    private lateinit var generateViewModel:            GenerateViewModel

    private lateinit var regionViewModel:            RegionViewModel

    private var countListMethods:     MutableList<DetailCashRegisterBalanceModel>?= null
    private var listReasonImbalance = ArrayList<ReasonImbalanceModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityClosingCashRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.appBar.ivBackToolbar.setOnClickListener(){
            this.finish()
        }

        binding.IVSaveCCR.setOnClickListener(){
            sendClosingCashRegister()
        }

        getSharePreferenceUser()
        getSharePreferenceCashRegister()
        initViewModels()
        initComponents()
        initPreferencesReasonImbalance()
        getCashRegisterInfo()
        getDataGenerate()
        getDataReasonImbalanceService()
    }

    override fun onBackPressed() {
        this.finish()
        super.onBackPressed()
    }

    //PREFERENCES
    private fun getSharePreferenceUser():LoggerUserModel {
        return SharedPreferencesLogin().getSharedPreferences(this)
    }

    private fun getSharePreferenceCashRegister():LoggerCashRegisterModel{
        var loggerCashRegisterModel:LoggerCashRegisterModel = LoggerCashRegisterModel()
        val sharedPref: SharedPreferences = getSharedPreferences("CashRegisterData", Context.MODE_PRIVATE)
        var code                = sharedPref.getString("code", null).toString()
        var name                = sharedPref.getString("name", null).toString()
        var company             = sharedPref.getString("company", null).toString()
        var openingCreationUser = sharedPref.getString("openingCreationUser", null).toString()
        var user                = sharedPref.getString("user", null).toString()
        var type                = sharedPref.getString("type", null).toString()
        var assigned            = sharedPref.getInt("assigned",-1)
        var status              = sharedPref.getInt("status",-1)

        loggerCashRegisterModel.code = code
        loggerCashRegisterModel.name = name
        loggerCashRegisterModel.company = company
        loggerCashRegisterModel.openingCreationUser = openingCreationUser
        loggerCashRegisterModel.user = user
        loggerCashRegisterModel.type = type
        loggerCashRegisterModel.assigned = assigned
        loggerCashRegisterModel.status = status
        return loggerCashRegisterModel
    }

    // INIT
    private fun initViewModels(){
        cashRegisterViewModel        = ViewModelProvider(this).get(CashRegisterViewModel::class.java)
        closingCashRegisterViewModel = ViewModelProvider(this).get(ClosingCashRegisterViewModel::class.java)
        reasonImbalanceViewModel     = ViewModelProvider(this).get(ReasonImbalanceViewModel::class.java)
        generateViewModel            = ViewModelProvider(this).get(GenerateViewModel::class.java)
        regionViewModel            = ViewModelProvider(this).get(RegionViewModel::class.java)
    }

    private fun initComponents(){
        binding.appBar.tvTitleToolbar.text = "CIERRE DE CAJA - ${getSharePreferenceCashRegister().name}"
    }

    // GET DATA REPOSITORY (INTERNAL AND EXTERNAL)
    private fun getCashRegisterInfo(){
        val cashRegisterLiveData = cashRegisterViewModel.get(getSharePreferenceUser().entity,getSharePreferenceUser().userIdentifier, Const.CASH_REGISTER_TYPE_DEFAULT)
        cashRegisterLiveData.observe(this) { cashRegister ->
            cashRegisterLiveData.removeObservers(this)
            if (cashRegister != null) {
                cashRegisterViewModel.listCashRegisterInfo(getSharePreferenceUser())
            }
        }
    }

    private fun getDataGenerate() {
        var cashRegisterBalanceDTOModel = CashRegisterBalanceDTOModel()

        cashRegisterBalanceDTOModel.store           = getSharePreferenceUser().store
        cashRegisterBalanceDTOModel.cashRegister    = getSharePreferenceCashRegister().code
        cashRegisterBalanceDTOModel.createUser      = getSharePreferenceUser().userIdentifier
        cashRegisterBalanceDTOModel.createUserEmail = getSharePreferenceUser().email
        cashRegisterBalanceDTOModel.createUserName  = getSharePreferenceUser().name

        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.title_sync_data))
        generateViewModel.listClosingCashGenerate(getSharePreferenceUser(),cashRegisterBalanceDTOModel)
        generateViewModel.result.observe(this, androidx.lifecycle.Observer {
            PopUp.closeProgressDialog()
            if(it.successful){
                it.data?.let { objectJSON ->
                    if(objectJSON.detail!!.size>0){
                        countListMethods                = objectJSON.detail!!
                        createMethod(objectJSON.detail!!)
                    }else {
                        countListMethods!!.add(DetailCashRegisterBalanceModel(0, "EFE", "SOL", 0.0, "", ""))
                        createMethod(countListMethods!!)
                    }
                }
            }else{
                //Toaster.toast(""+it.message)
                Toaster.toast(R.string.title_error_internet)
                finish()
            }
        })
    }



    private fun getDataReasonImbalanceService(){
        reasonImbalanceViewModel.listReasonImbalance(getSharePreferenceUser())
        reasonImbalanceViewModel.resultReasonImbalance.observe(this, EventObserver{
            if(it.successful){
                   listReasonImbalance = (it.data as ArrayList<ReasonImbalanceModel>?)!!
            }else{
                Toaster.toast(R.string.title_error_internet)
                finish()
            }
        })
    }

    // SET DATA REPOSITORY
    private fun sendClosingCashRegister(){
        if (UsePhone.isOnline()) {
            if(validateEdtAmount(countListMethods!!)){
                if(validateReasonImbalance(countListMethods!!)) {
                        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.title_send_closure_box))
                        var closingCashRegisterSaveModel = generateClosureCashRegister()
                        closingCashRegisterViewModel.closingCashRegister(getSharePreferenceUser(), closingCashRegisterSaveModel) {
                            PopUp.closeProgressDialog()
                            if (it.successful) {
                                Toaster.toast(R.string.title_send_successful)
                                clearPreferencesReasonImbalance()
                                finish()
                            }else{
                                Toaster.toast(R.string.title_error_internet)
                            }
                        }
                    }
                }
        }else {
            Toaster.toast(R.string.title_error_no_internet)
        }
    }

    private fun proof(){
            if(validateEdtAmount(countListMethods!!)){
                if(validateReasonImbalance(countListMethods!!)) {
                    var closingCashRegisterSaveModel = generateClosureCashRegister()
                    Log.e("dato1.moneyOut:", "" + closingCashRegisterSaveModel.additionalData?.moneyOut)
                    Log.e("dato1.operationNumber:", "" + closingCashRegisterSaveModel.additionalData?.operationNumber)
                    Log.e("dato1.docNumERP:", "" + closingCashRegisterSaveModel.additionalData?.docNumERP)
                    Log.e("dato2.cashRegister:", "" + closingCashRegisterSaveModel.cashRegister)
                    Log.e("dato3.store:", "" + closingCashRegisterSaveModel.store)
                    Log.e("dato4.createUser:", "" + closingCashRegisterSaveModel.createUser)
                    Log.e("dato5.createUserEmail:", "" + closingCashRegisterSaveModel.createUserEmail)
                    Log.e("dato6.createUserName:", "" + closingCashRegisterSaveModel.createUserName)
                    Log.e("dato7.closureDate:", "" + closingCashRegisterSaveModel.closureDate)
                    Log.e("dato8.status:", "" + closingCashRegisterSaveModel.status)
                    for (valor in closingCashRegisterSaveModel.detail!!) {
                        Log.e("dato9.numberLine:", "" + valor.numberLine)
                        Log.e("dato9.payWay:", "" + valor.payWay)
                        Log.e("dato9.payMethod:", "" + valor.payMethod)
                        Log.e("dato9.amount:", "" + valor.amount)
                        Log.e("dato9.count:", "" + valor.count)
                        Log.e("dato9.imbalanceReason:", "" + valor.imbalanceReason)
                        Log.e("dato9.imbalanceComment:", "" + valor.imbalanceComment)
                        for (valox in valor.denominations!!) {
                            Log.e("dato9-1.denomination:", "" + valox.denomination)
                            Log.e("dato9-2.numberLine:", "" + valox.numberLine)
                            Log.e("dato9-3.quantity:", "" + valox.quantity)
                            Log.e("dato9-4.total:", "" + valox.total)
                        }
                        Log.e("dato99.account:", "" + valor.account)
                        Log.e("dato99.diference:", "" + valor.diference)
                        Log.e("dato99.totalOut:", "" + valor.totalOut)
                        Log.e("dato99.imbalanceNumberAccount:", "" + valor.imbalanceNumberAccount)
                    }
                    Log.e("dato10:", "" + closingCashRegisterSaveModel.comment)
                }
            }

    }

    // SHOW COMPONENTS
    private fun createMethod(list:MutableList<DetailCashRegisterBalanceModel>){
        for ((i, objectList) in list.withIndex()) {
            val lyContainer = LinearLayout(this)
            lyContainer.id           = i+1
            lyContainer.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
            lyContainer.orientation  = LinearLayout.VERTICAL
            lyContainer.setPadding(20,20,20,20)
            binding.idContainerClosing.addView(lyContainer)
            //LY 1
            val lyContainerChild1 = LinearLayout(this)
            lyContainerChild1.id           = i+100
            lyContainerChild1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
            lyContainerChild1.setBackgroundColor(Color.WHITE)
            lyContainerChild1.orientation  = LinearLayout.HORIZONTAL
            lyContainer.addView(lyContainerChild1)

            val titleMethod = TextView(this)
            titleMethod.id           = i+200
            titleMethod.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,2f)
            titleMethod.text = "${objectList.payWay}-${objectList.payMethod}"
            titleMethod.textSize = 16f
            titleMethod.setTypeface(null, Typeface.BOLD);
            titleMethod.setPadding(0,0,10,0)
            titleMethod.setTextColor(Color.BLACK)
            lyContainerChild1.addView(titleMethod)

            val titleAmount = TextView(this)
            titleAmount.id           = i+300
            titleAmount.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,1.1f)
            titleAmount.text         = "Monto:"
            titleAmount.setPadding(0,0,10,0)
            lyContainerChild1.addView(titleAmount)

            val amount = TextView(this)
            amount.id           = i+400
            amount.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,4f)
            amount.text = ""+objectList.amount
            amount.setPadding(0,0,0,0)
            lyContainerChild1.addView(amount)
            //LY 2
            val lyContainerChild2 = LinearLayout(this)
            lyContainerChild2.id           = i+500
            lyContainerChild2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
            lyContainerChild2.setBackgroundColor(Color.WHITE)
            lyContainerChild2.orientation  = LinearLayout.HORIZONTAL
            lyContainer.addView(lyContainerChild2)

            val etAmount = EditText(this)
            etAmount.id           = i+600
            etAmount.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,4f)
            etAmount.hint         = "Ingrese Cantidad"
            etAmount.inputType    = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            etAmount.setText(""+objectList.amount)
            etAmount.setPadding(15,20,15,20)
            etAmount.setTextColor(Color.BLACK)
            lyContainerChild2.addView(etAmount)

            val ivImbalance = ImageView(this)
            ivImbalance.id           = i+700
            ivImbalance.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,1f)
            ivImbalance.visibility   = View.INVISIBLE
            ivImbalance.setImageResource(R.drawable.baseline_money_off_red_800_36dp)
            lyContainerChild2.addView(ivImbalance)

            // Visible/Hide Button Imbalance
            etAmount.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence,i: Int,i1: Int,i2: Int) {}
                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                override fun afterTextChanged(editable: Editable) {
                    if (editable.toString()!! == amount.text.toString()){
                        ivImbalance.visibility = View.GONE
                    }else{
                        ivImbalance.visibility = View.VISIBLE
                    }
                }
            })
            // Show DialogFragment Imbalance
            ivImbalance.setOnClickListener(){
                showDialogReasonImbalance(objectList.amount!!,i,objectList.payMethod!!)
            }
        }
    }

    private fun showDialogReasonImbalance(amount:Double,index:Int,codeMethod:String){
            val editText = findViewById<View>(index + 600) as EditText
            val reasonImbalanceObjectModel                 = ReasonImbalanceObjectModel()
            reasonImbalanceObjectModel.method              = codeMethod
            reasonImbalanceObjectModel.amountImbalance     = amount.minus(editText.text.toString().toDouble())
            reasonImbalanceObjectModel.listReasonImbalance = listReasonImbalance
            reasonImbalanceObjectModel.comment             =  ""

            val reasonImbalanceFragment = ReasonImbalanceFragment()
            val args = Bundle()
            args.putParcelable("objectImbalance",reasonImbalanceObjectModel)
            reasonImbalanceFragment.arguments = args
            reasonImbalanceFragment.show(supportFragmentManager,"dialogReasonImbalance")
    }

    // VALIDATION OF COMPONENTS
    private fun validateEdtAmount(list:MutableList<DetailCashRegisterBalanceModel>): Boolean {
        for((i, objectList) in list.withIndex()){
            val editText = findViewById<View>(i + 600) as EditText
            if (editText.text.toString().isBlank()) {
                Toaster.toast("Debe ingresar Cantidades (${objectList.payMethod})")
                return false
            }
        }
        return true
    }

    private fun validateReasonImbalance(list:MutableList<DetailCashRegisterBalanceModel>): Boolean {
        val dataPref = getSharedPreferences("reason_imbalance_close", MODE_PRIVATE);
        for((i, objectList) in list.withIndex()){
            val imageView = findViewById<View>(i + 700) as ImageView
            var method    = dataPref.getString(objectList.payMethod,"EMPTY")
            if (imageView.isVisible && method=="EMPTY") {
                Toaster.toast("Ingrese motivo de DESCUADRE (${objectList.payMethod})")
                return false
            }
        }
        return true
    }

    // GENERATE SEND JSON
    private fun generateClosureCashRegister():ClosingCashRegisterSaveModel{
            var objectClosureCashRegister = ClosingCashRegisterSaveModel()
            objectClosureCashRegister.additionalData = ClosingCashRegisterAdditionalModel("","",false)
            objectClosureCashRegister.cashRegister   = getSharePreferenceCashRegister().code
            objectClosureCashRegister.store          = getSharePreferenceUser().store
            objectClosureCashRegister.createUser     = getSharePreferenceUser().userIdentifier
            objectClosureCashRegister.createUserEmail = getSharePreferenceUser().email
            objectClosureCashRegister.createUserName  = getSharePreferenceUser().name
            objectClosureCashRegister.closureDate     = getDateNow()
            objectClosureCashRegister.status          = 1
            objectClosureCashRegister.detail          = generateDetail(countListMethods!!)
            objectClosureCashRegister.comment         = binding.ETCCRComment.text.toString()
            return objectClosureCashRegister
    }

    private fun generateDetail(list:MutableList<DetailCashRegisterBalanceModel>): MutableList<DetailCashRegisterSaveModel> {
        var listDetail : MutableList<DetailCashRegisterSaveModel> = mutableListOf()
        for((i, objectList) in list.withIndex()){
            val etAmount = findViewById<View>(i + 600) as EditText
            var detail = DetailCashRegisterSaveModel()
            detail.numberLine             = objectList.numberLine
            detail.payWay                 = objectList.payWay
            detail.payMethod              = objectList.payMethod
            detail.amount                 = objectList.amount
            detail.count                  = etAmount.text.toString().toDouble()
            detail.imbalanceReason        = getDataPreferencesReasonImbalance(objectList.payMethod!!).imbalanceReason
            detail.imbalanceComment       = getDataPreferencesReasonImbalance(objectList.payMethod!!).imbalanceComment
            detail.denominations          = generateDenominations(etAmount.text.toString())
            detail.diference              = objectList.amount?.minus(etAmount.text.toString().toDouble())
            detail.totalOut               = etAmount.text.toString().toDouble()
            detail.imbalanceNumberAccount = getDataPreferencesReasonImbalance(objectList.payMethod!!).imbalanceNumberAccount
            listDetail.add(detail)
        }
        return listDetail
    }

    private fun generateDenominations(amount:String):MutableList<DenominationsCashRegisterSaveModel>{
        var listDenomination: MutableList<DenominationsCashRegisterSaveModel> = mutableListOf()
        val number:Double = amount.toDouble()//350.20
        val str = number.toString()

        val intNumber    = str.substring(0, str.indexOf('.')).toInt()//350
        val decNumberInt = str.substring(str.indexOf('.') + 1).toInt()//0.20

        when(amount){
            Constants.PAY_METHOD_1->{
                if(decNumberInt>0){
                    listDenomination.add(DenominationsCashRegisterSaveModel(0.1,0,decNumberInt.toString(),decNumberInt.toDouble()))
                    listDenomination.add(DenominationsCashRegisterSaveModel(1.0,3,intNumber.toString(),intNumber.toDouble()))
                }else{
                    listDenomination.add(DenominationsCashRegisterSaveModel(1.0,3,intNumber.toString(),intNumber.toDouble()))
                }
            }
            else->{
                //listDenomination.add(DenominationsCashRegisterSaveModel(0.1,0,decNumberInt.toString(),decNumberInt.toDouble()))
                listDenomination.add(DenominationsCashRegisterSaveModel(1.0,0,intNumber.toString(),intNumber.toDouble()))
            }
        }
        return listDenomination
    }

    // SHARED PREFERENCES
    private fun initPreferencesReasonImbalance(){
        clearPreferencesReasonImbalance()
        val dataPref = getSharedPreferences("reason_imbalance_close", MODE_PRIVATE);
        with(dataPref.edit()){
            putString("proceso","cierre")
            apply()
        }
    }

    private fun getDataPreferencesReasonImbalance(key:String):ReasonImbalanceSaveModel {
        val dataPref = getSharedPreferences("reason_imbalance_close", MODE_PRIVATE);
        val g = Gson()
        val json = dataPref.getString(key,"no hay objeto")
        val type = object : TypeToken<ReasonImbalanceSaveModel>() {}.type
        var objectImbalance = ReasonImbalanceSaveModel()
        try {
            objectImbalance = g.fromJson<ReasonImbalanceSaveModel>(json,type)
        }catch (e : JsonParseException){
            Log.e("errorJson:",""+e)
        }
        return objectImbalance
    }

    private fun clearPreferencesReasonImbalance(){
        val dataPref = getSharedPreferences("reason_imbalance_close", MODE_PRIVATE);
        with(dataPref.edit()){
            clear()
            apply()
        }
    }

}