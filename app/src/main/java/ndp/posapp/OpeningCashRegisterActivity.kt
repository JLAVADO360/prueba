package ndp.posapp

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.google.gson.Gson
import com.google.gson.JsonParseException
import com.google.gson.reflect.TypeToken
import ndp.posapp.data.model.*
import ndp.posapp.databinding.ActivityOpeningCashRegisterBinding
import ndp.posapp.utils.Constants
import ndp.posapp.utils.dialog.PopUp
import ndp.posapp.utils.event.EventObserver
import ndp.posapp.utils.interaction.*
import ndp.posapp.utils.shared.SharedPreferencesLogin
import ndp.posapp.utils.use.UsePhone
import ndp.posapp.view.box.OpeningReasonImbalanceFragment
import ndp.posapp.viewModel.*
import xdroid.toaster.Toaster

class OpeningCashRegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityOpeningCashRegisterBinding

    private lateinit var openingCashRegisterViewModel: OpeningCashRegisterViewModel
    private lateinit var reasonImbalanceViewModel:     ReasonImbalanceViewModel
    private lateinit var currencyDataViewModel:        CurrencyDataViewModel

    private var boxClosedLast:BoxClosedLast= BoxClosedLast()
    private var listReasonImbalance = ArrayList<ReasonImbalanceModel>()

    private var countListCurrency : MutableList<CurrencyModel>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOpeningCashRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        boxClosedLast = intent.getParcelableExtra<BoxClosedLast>("lista")!!

        binding.appBar.ivBackToolbar.setOnClickListener(){
            this.finish()
        }

        binding.IVSaveOCR.setOnClickListener(){
            sendOpeningCashRegister()
        }
        initViewModels()
        initComponents()
        initPreferencesReasonImbalance()
        getDataReasonImbalanceService()
        getCurrency()
    }

    override fun onBackPressed() {
        this.finish()
        super.onBackPressed()
    }

    //PREFERENCES
    private fun getSharePreferenceUser():LoggerUserModel {
        return SharedPreferencesLogin().getSharedPreferences(this)
    }

    // INIT
    private fun initViewModels(){
        openingCashRegisterViewModel = ViewModelProvider(this).get(OpeningCashRegisterViewModel::class.java)
        reasonImbalanceViewModel     = ViewModelProvider(this).get(ReasonImbalanceViewModel::class.java)
        currencyDataViewModel        = ViewModelProvider(this).get(CurrencyDataViewModel::class.java)
    }

    private fun initComponents(){
        binding.appBar.tvTitleToolbar.text = "APERTURA DE CAJA"
        binding.ETOCRCash.setText(""+boxClosedLast.boxName)
        binding.ETOCRDate.setText("  "+getDate().anio+"-"+getDate().mes+"-"+getDate().dia)
    }

    // GET DATA REPOSITORY (INTERNAL AND EXTERNAL)
    private fun getCurrency(){
        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.title_sync_data))
        currencyDataViewModel.getCurrencyData(getSharePreferenceUser())
        currencyDataViewModel.result.observe(this,{
            PopUp.closeProgressDialog()
            if(it.successful){
                it.data?.let { objectJSON ->
                    if(objectJSON.value!!.size>0){
                        countListCurrency  = objectJSON.value!!
                        createMethod(objectJSON.value!!)
                        setValuesComponent(boxClosedLast.lista!!)
                    }else {
                        countListCurrency!!.add(CurrencyModel("SOL", null,"","", emptyList(),"Soles",0, "Soles", 0, "SOL","",""))
                        createMethod(countListCurrency!!)
                        var listBox = ArrayList<BoxClosedDetail>()
                        listBox.add(BoxClosedDetail(0.0,0,"EFE","SOL"))
                        setValuesComponent(listBox)
                    }
                }
            }else{
                Toaster.toast(R.string.title_error_internet)
                finish()
            }
        })
    }

    private fun getDataReasonImbalanceService(){
        reasonImbalanceViewModel.listReasonImbalance(getSharePreferenceUser())
        reasonImbalanceViewModel.resultReasonImbalance.observe(this, EventObserver{
            if(it.successful){
                listReasonImbalance = (it.data as ArrayList<ReasonImbalanceModel>?)!!
            }else{
                Toaster.toast(R.string.title_error_internet)
                finish()
            }
        })
    }

    // SET DATA REPOSITORY
    private fun sendOpeningCashRegister(){
        if (UsePhone.isOnline()) {
            if(validateEdtAmount(countListCurrency!!)){
                if(validateReasonImbalance(countListCurrency!!)){
                        PopUp.openProgressDialog(this, getString(R.string.operation_running), getString(R.string.title_send_opening_box))
                        var openingCashRegisterSaveModel = generateOpeningCashRegister()
                        openingCashRegisterViewModel.openingCashRegister(getSharePreferenceUser(), openingCashRegisterSaveModel){
                            PopUp.closeProgressDialog()
                            if (it.successful) {
                                Toaster.toast(R.string.title_send_successful)
                                clearPreferencesReasonImbalance()
                                finish()
                            }else{
                                //Toaster.toast(R.string.title_error_internet)
                                Toaster.toast(""+it.message)
                            }
                        }
                    }
               }
        }else {
            Toaster.toast(R.string.title_error_no_internet)
        }
    }

    private fun proof(){
        if(validateEdtAmount(countListCurrency!!)){
            if(validateReasonImbalance(countListCurrency!!)){
                    var openingCashRegisterSaveModel = generateOpeningCashRegister()
                    Log.e("dato1.code:", "" + openingCashRegisterSaveModel.code)
                    Log.e("dato2.createUser:", "" + openingCashRegisterSaveModel.createUser)
                    Log.e("dato3.createUserEmail:", "" + openingCashRegisterSaveModel.createUserEmail)
                    Log.e("dato4.createUserName:", "" + openingCashRegisterSaveModel.createUserName)
                    Log.e("dato5.store:", "" + openingCashRegisterSaveModel.store)
                    Log.e("dato6.status:", "" + openingCashRegisterSaveModel.status)
                    Log.e("dato7.cashRegister:", "" + openingCashRegisterSaveModel.cashRegister)
                    Log.e("dato8.observation:", "" + openingCashRegisterSaveModel.observation)
                    for (valor in openingCashRegisterSaveModel.detail!!) {
                        Log.e("dato9-1.closureAmount:", "" + valor.closureAmount)
                        Log.e("dato9-2.currency:", "" + valor.currency)
                        Log.e("dato9-3.name:", "" + valor.name)
                        Log.e("dato9-4.currentBalance:", "" + valor.currentBalance)
                        Log.e("dato9-5.previousClosure:", "" + valor.previousClosure)
                        Log.e("dato9-6.numberLine:", "" + valor.numberLine)
                        Log.e("dato9-7.rate:", "" + valor.rate)
                        Log.e("dato9-8.total:", "" + valor.total)
                        Log.e("dato9-9.difference:", "" + valor.difference)
                        Log.e("dato9-10.diference:", "" + valor.diference)
                        Log.e("dato9-11.imbalanceReason:", "" + valor.imbalanceReason)
                        Log.e("dato9-12.imbalanceNumberAccount:", "" + valor.imbalanceNumberAccount)
                        Log.e("dato9-13.imbalanceComment:", "" + valor.imbalanceComment)
                        for (valox in valor.denominations!!) {
                            Log.e("dato9-111.denomination:", "" + valox.denomination)
                            Log.e("dato9-222.numberLine:", "" + valox.numberLine)
                            Log.e("dato9-333.quantity:", "" + valox.quantity)
                            Log.e("dato9-444.total:", "" + valox.total)
                        }
                    }

            }
        }
    }

    //SHOW COMPONENTS
    private fun createMethod(list:MutableList<CurrencyModel>){
        for ((i, objectList) in list.withIndex()) {
            val lyContainer = LinearLayout(this)
            lyContainer.id           = i+1
            lyContainer.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
            lyContainer.orientation  = LinearLayout.VERTICAL
            lyContainer.setPadding(20,20,20,20)
            binding.idContainerOpening.addView(lyContainer)
            //LY 1
            val lyContainerChild1 = LinearLayout(this)
            lyContainerChild1.id           = i+100
            lyContainerChild1.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
            lyContainerChild1.setBackgroundColor(Color.WHITE)
            lyContainerChild1.orientation  = LinearLayout.HORIZONTAL
            lyContainer.addView(lyContainerChild1)

            val titleMethod = TextView(this)
            titleMethod.id           = i+200
            titleMethod.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,2f)
            titleMethod.text = "${objectList.code}"
            titleMethod.textSize = 16f
            titleMethod.setTypeface(null, Typeface.BOLD);
            titleMethod.setPadding(0,0,10,0)
            titleMethod.setTextColor(Color.BLACK)
            lyContainerChild1.addView(titleMethod)

            val titleAmount = TextView(this)
            titleAmount.id           = i+300
            titleAmount.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,1.1f)
            titleAmount.text         = "Monto:"
            titleAmount.setPadding(0,0,10,0)
            lyContainerChild1.addView(titleAmount)

            val amount = TextView(this)
            amount.id           = i+400
            amount.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,4f)
            amount.text = "0.0"
            amount.setPadding(0,0,0,0)
            lyContainerChild1.addView(amount)
            //LY 2
            val lyContainerChild2 = LinearLayout(this)
            lyContainerChild2.id           = i+500
            lyContainerChild2.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
            lyContainerChild2.setBackgroundColor(Color.WHITE)
            lyContainerChild2.orientation  = LinearLayout.HORIZONTAL
            lyContainer.addView(lyContainerChild2)

            val etAmount = EditText(this)
            etAmount.id           = i+600
            etAmount.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,4f)
            etAmount.hint         = "Ingrese Cantidad"
            etAmount.inputType    = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            //etAmount.setText(""+objectList.amount)
            etAmount.setPadding(15,20,15,20)
            etAmount.setTextColor(Color.BLACK)
            lyContainerChild2.addView(etAmount)

            val ivImbalance = ImageView(this)
            ivImbalance.id           = i+700
            ivImbalance.layoutParams = LinearLayout.LayoutParams(0,LinearLayout.LayoutParams.WRAP_CONTENT,1f)
            ivImbalance.visibility   = View.INVISIBLE
            ivImbalance.setImageResource(R.drawable.baseline_money_off_red_800_36dp)
            lyContainerChild2.addView(ivImbalance)

            // Visible/Hide Button Imbalance
            etAmount.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(charSequence: CharSequence,i: Int,i1: Int,i2: Int) {}
                override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
                override fun afterTextChanged(editable: Editable) {
                    if(amount.text.toString() != "0.0"){
                        if (editable.toString()!! == amount.text.toString()){
                            ivImbalance.visibility = View.GONE
                        }else{
                            ivImbalance.visibility = View.VISIBLE
                        }
                    }
                }
            })
            // Show DialogFragment Imbalance
            ivImbalance.setOnClickListener(){
                showDialogReasonImbalance(amount.text.toString().toDouble(),i,objectList.code!!)
            }
        }
    }

    private fun setValuesComponent(list:ArrayList<BoxClosedDetail>){
        //Obtener solo Efectivo
        var newList = ArrayList<BoxClosedDetail>()
        for ((i, objectListCurrency) in countListCurrency!!.withIndex()){
            if(objectListCurrency.code==list!![i].payMethod){
                newList.add(list!![i])
            }
        }
        // setear valores en componentes
        for((i, objectList) in newList!!.withIndex()){
            val textView1 = findViewById<View>(i + 200) as TextView
            val textView2 = findViewById<View>(i + 400) as TextView
            val editText  = findViewById<View>(i + 600) as TextView
            if (textView1.text.toString()==objectList.payMethod) {
                textView2.text = objectList.amount.toString()
                editText.text  = objectList.amount.toString()
            }
        }
    }

    private fun showDialogReasonImbalance(amount:Double,index:Int,codeMethod:String){
        val editText = findViewById<View>(index + 600) as EditText
        val reasonImbalanceObjectModel                 = ReasonImbalanceObjectModel()
        reasonImbalanceObjectModel.method              = codeMethod
        reasonImbalanceObjectModel.amountImbalance     = amount.minus(editText.text.toString().toDouble())
        reasonImbalanceObjectModel.listReasonImbalance = listReasonImbalance
        reasonImbalanceObjectModel.comment             =  ""

        val reasonImbalanceFragment = OpeningReasonImbalanceFragment()
        val args = Bundle()
        args.putParcelable("objectImbalance",reasonImbalanceObjectModel)
        reasonImbalanceFragment.arguments = args
        reasonImbalanceFragment.show(supportFragmentManager,"dialogReasonImbalanceOpening")

    }

    // VALIDATION OF COMPONENTS
    private fun validateEdtAmount(list:MutableList<CurrencyModel>): Boolean {
        for((i, objectList) in list.withIndex()){
            val textView = findViewById<View>(i + 400) as TextView
            val editText = findViewById<View>(i + 600) as EditText
            if (editText.text.toString().isBlank() && textView.text.toString()!="0.0") {
                Toaster.toast("Debe ingresar Cantidades (${objectList.code})")
                return false
            }
        }
        return true
    }

    private fun validateReasonImbalance(list:MutableList<CurrencyModel>): Boolean {
        val dataPref = getSharedPreferences("reason_imbalance_open", MODE_PRIVATE);
        for((i, objectList) in list.withIndex()){
            val imageView = findViewById<View>(i + 700) as ImageView
            var method    = dataPref.getString(objectList.code,"EMPTY")
            if (imageView.isVisible && method=="EMPTY") {
                Toaster.toast("Ingrese motivo de DESCUADRE (${objectList.code})")
                return false
            }
        }
        return true
    }

    // GENERATE SEND JSON
    private fun generateOpeningCashRegister():OpeningCashRegisterSaveModel{
        var objectOpeningCashRegister = OpeningCashRegisterSaveModel()
        objectOpeningCashRegister.code            = ""
        objectOpeningCashRegister.createUser      = getSharePreferenceUser().userIdentifier
        objectOpeningCashRegister.createUserEmail = getSharePreferenceUser().email
        objectOpeningCashRegister.createUserName  = getSharePreferenceUser().name
        objectOpeningCashRegister.store           = boxClosedLast.store
        objectOpeningCashRegister.status          = 1
        objectOpeningCashRegister.cashRegister    = boxClosedLast.boxCode
        objectOpeningCashRegister.observation     = binding.ETOCRComment.text.toString()
        objectOpeningCashRegister.detail          = generateDetail(countListCurrency!!)
        return objectOpeningCashRegister
    }

    private fun generateDetail(list:MutableList<CurrencyModel>): MutableList<DetailOpeningCashRegisterSaveModel> {
        var listDetail : MutableList<DetailOpeningCashRegisterSaveModel> = mutableListOf()
        for((i, objectList) in list.withIndex()){
            val tvAmount = findViewById<View>(i + 400) as TextView
            val etAmount = findViewById<View>(i + 600) as EditText
            var detail = DetailOpeningCashRegisterSaveModel()
            if(etAmount.text.toString()!="" ||  getConvertEmpty(etAmount.text.toString())>0){
                detail.closureAmount          = 0
                detail.currency               = objectList.code
                detail.name                   = objectList.name
                detail.denominations          = generateDenominations(etAmount.text.toString())
                detail.currentBalance         = etAmount.text.toString().toDouble()
                if(tvAmount.text.toString().toDouble() != etAmount.text.toString().toDouble()){
                    detail.previousClosure    = tvAmount.text.toString().toDouble()
                }else{
                    detail.previousClosure    = 0.0
                }
                detail.numberLine             = i
                detail.rate                   = 0
                detail.total                  = etAmount.text.toString().toDouble()

                if(tvAmount.text.toString()!="0.0"){
                    detail.difference             = tvAmount.text.toString().toDouble().minus(etAmount.text.toString().toDouble())
                    detail.diference              = tvAmount.text.toString().toDouble().minus(etAmount.text.toString().toDouble())
                    detail.imbalanceReason        = getDataPreferencesReasonImbalance(objectList.code!!).imbalanceReason
                    detail.imbalanceNumberAccount = getDataPreferencesReasonImbalance(objectList.code!!).imbalanceNumberAccount
                    detail.imbalanceComment       = getDataPreferencesReasonImbalance(objectList.code!!).imbalanceComment
                }
                listDetail.add(detail)
            }
        }
        return listDetail
    }

    private fun generateDenominations(amount:String):MutableList<DenominationsCashRegisterSaveModel>{
        var listDenomination: MutableList<DenominationsCashRegisterSaveModel> = mutableListOf()
        val number:Double = amount.toDouble()//350.20
        val str = number.toString()

        val intNumber    = str.substring(0, str.indexOf('.')).toInt()//350
        val decNumberInt = str.substring(str.indexOf('.') + 1).toInt()//0.20

        when(amount){
            Constants.PAY_METHOD_1->{
                if(decNumberInt>0){
                    listDenomination.add(DenominationsCashRegisterSaveModel(0.1,0,decNumberInt.toString(),decNumberInt.toDouble()))
                    listDenomination.add(DenominationsCashRegisterSaveModel(1.0,3,intNumber.toString(),intNumber.toDouble()))
                }else{
                    listDenomination.add(DenominationsCashRegisterSaveModel(1.0,3,intNumber.toString(),intNumber.toDouble()))
                }
            }
            else->{
                //listDenomination.add(DenominationsCashRegisterSaveModel(0.1,0,decNumberInt.toString(),decNumberInt.toDouble()))
                listDenomination.add(DenominationsCashRegisterSaveModel(1.0,0,intNumber.toString(),intNumber.toDouble()))
            }
        }
        return listDenomination
    }

    //SHARED PREFERENCES

    private fun initPreferencesReasonImbalance(){
        clearPreferencesReasonImbalance()
        val dataPref = getSharedPreferences("reason_imbalance_open", MODE_PRIVATE);
        with(dataPref.edit()){
            putString("proceso","apertura")
            apply()
        }
    }

    private fun getDataPreferencesReasonImbalance(key:String):ReasonImbalanceSaveModel {
        val dataPref = getSharedPreferences("reason_imbalance_open", MODE_PRIVATE);
        val g = Gson()
        val json = dataPref.getString(key,"no hay objeto")
        val type = object : TypeToken<ReasonImbalanceSaveModel>() {}.type
        var objectImbalance = ReasonImbalanceSaveModel()
        try {
            objectImbalance = g.fromJson<ReasonImbalanceSaveModel>(json,type)
        }catch (e : JsonParseException){
            Log.e("errorJson:",""+e)
        }
        return objectImbalance
    }

    private fun clearPreferencesReasonImbalance(){
        val dataPref = getSharedPreferences("reason_imbalance_open", MODE_PRIVATE);
        with(dataPref.edit()){
            clear()
            apply()
        }
    }

}