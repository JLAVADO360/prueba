package ndp.posapp.core

import android.util.Log
import ndp.posapp.BuildConfig
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

import okhttp3.OkHttpClient
import okhttp3.Request
import java.util.concurrent.TimeUnit
import retrofit2.converter.gson.GsonConverterFactory

import retrofit2.Retrofit


object RetrofitHelper {

    private const val baseUrl = "https://services.360salesolutions.com/"
    //private const val baseUrl = "http://172.20.11.160:8090/"

    fun getRetrofit(route: String): Retrofit {

        val logging = HttpLoggingInterceptor { message: String ->
            Log.d("ConfigRetrofit", message)
        }
        logging.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        val httpClient: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging) // add logging as last interceptor (this is the important line)
            .addInterceptor(Interceptor { chain: Interceptor.Chain ->
                val original: Request = chain.request()
                // Request customization: add request headers
                val requestBuilder: Request.Builder =
                    original.newBuilder() //.addHeader("Cache-Control", "no-cache")
                val request: Request = requestBuilder.build()
                chain.proceed(request)
            })
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(180, TimeUnit.SECONDS)
            .writeTimeout(180, TimeUnit.SECONDS).build()


        return Retrofit.Builder()
            .baseUrl(baseUrl + route)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .build()

    }


}